Elmo
====
Elmo is a decision support tool for improving services architectures. It is developed and used by software company First8 BV ([www.first8.nl](https://www.first8.nl)) to be used in refactoring and designing services architectures. The tool, underlying models and companying papers are further developed as part of a PhD project in cooperation with the Open Universiteit ([www.ou.nl](https://www.ou.nl/)).

The application can be found in src. It is a standard Maven Java project using Java 11, JavaFX 11 and JUNG for graphs.

Building Elmo
-------------
You need a  JDK 11 and Maven to build Elmo. If you have both installed, you should be able to build a 'fat jar' for Elmo using:

mvn install

This will build a complete jar file in shade/elmo.jar. This jar should contain all native libraries and as such should be runnable on most platforms with JDK 11.


Running Elmo
------------

After building Elmo, you can simply run Elmo.jar using maven:

mvn exec:java

Or you can run the compiled fat jar:
java -jar shade/elmo.jar

Optionally, you can automatically have it open some files on startup:

java -jar shade/elmo.jar src/test/resources/example.elm

License
-------

Elmo is open source and is distributed under the Common Development and Distribution License 1.0 (CDDL-1.0). See the LICENSE file for the 
content of that license.

A very short summary of that license is that Elmo itself, and changes to it, stay open source. If the CDDL license does not 
fit your need, please contact me to see if we can work something out in a dual license model.
