{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "bt",
        "description" : "bt",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "at",
        "description" : "at",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "website",
        "description" : "website",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "upload",
        "description" : "upload",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fs",
        "description" : "fs",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "download",
        "description" : "download",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "s1",
        "description" : "s1",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "s2",
        "description" : "s2",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "s3",
        "description" : "s3",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 33,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "at",
        "sourceName" : "db",
        "targetName" : "at"
      }, {
        "id" : 34,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "s2",
        "targetName" : "download"
      }, {
        "id" : 1,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "file",
        "sourceName" : "fs",
        "targetName" : "bt"
      }, {
        "id" : 2,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "btmeta",
        "sourceName" : "bt",
        "targetName" : "db"
      }, {
        "id" : 3,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "btmeta",
        "sourceName" : "db",
        "targetName" : "bt"
      }, {
        "id" : 4,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "file",
        "sourceName" : "upload",
        "targetName" : "s1"
      }, {
        "id" : 5,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "file",
        "sourceName" : "upload",
        "targetName" : "db"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "db",
        "targetName" : "upload"
      }, {
        "id" : 7,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "finalfile",
        "sourceName" : "db",
        "targetName" : "download"
      }, {
        "id" : 8,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "download",
        "targetName" : "db"
      }, {
        "id" : 9,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "download",
        "targetName" : "s2"
      }, {
        "id" : 10,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "db",
        "targetName" : "website"
      }, {
        "id" : 11,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "website",
        "targetName" : "db"
      }, {
        "id" : 12,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "s1",
        "targetName" : "website"
      }, {
        "id" : 13,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "s2",
        "targetName" : "website"
      }, {
        "id" : 14,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "file",
        "sourceName" : "s1",
        "targetName" : "fs"
      }, {
        "id" : 15,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "file",
        "sourceName" : "fs",
        "targetName" : "s1"
      }, {
        "id" : 16,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "command",
        "sourceName" : "bt",
        "targetName" : "s1"
      }, {
        "id" : 17,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "bt",
        "targetName" : "s1"
      }, {
        "id" : 19,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "db",
        "targetName" : "s1"
      }, {
        "id" : 20,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "file",
        "sourceName" : "s2",
        "targetName" : "fs"
      }, {
        "id" : 21,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "file",
        "sourceName" : "fs",
        "targetName" : "s2"
      }, {
        "id" : 22,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "command",
        "sourceName" : "bt",
        "targetName" : "s2"
      }, {
        "id" : 23,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "bt",
        "targetName" : "s2"
      }, {
        "id" : 24,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "s2",
        "targetName" : "db"
      }, {
        "id" : 25,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "db",
        "targetName" : "s2"
      }, {
        "id" : 26,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "file",
        "sourceName" : "s3",
        "targetName" : "fs"
      }, {
        "id" : 27,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "file",
        "sourceName" : "fs",
        "targetName" : "s3"
      }, {
        "id" : 28,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "command",
        "sourceName" : "bt",
        "targetName" : "s3"
      }, {
        "id" : 29,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "bt",
        "targetName" : "s3"
      }, {
        "id" : 30,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "s3",
        "targetName" : "db"
      }, {
        "id" : 31,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "meta",
        "sourceName" : "db",
        "targetName" : "s3"
      }, {
        "id" : 32,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "at",
        "sourceName" : "at",
        "targetName" : "db"
      } ]
    },
    "name" : "current-data.elm",
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 6,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "upload_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 1,
      "callType" : "PULLED_FROM",
      "sourceName" : "fs_1",
      "targetName" : "bt_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 2,
      "callType" : "PUSHES_TO",
      "sourceName" : "bt_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 33,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "at_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 14,
      "callType" : "PUSHES_TO",
      "sourceName" : "s1_1",
      "targetName" : "fs_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 13,
      "callType" : "PULLED_FROM",
      "sourceName" : "s2_1",
      "targetName" : "website_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 10,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "website_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 4,
      "callType" : "PUSHES_TO",
      "sourceName" : "upload_1",
      "targetName" : "s1_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 7,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "download_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 26,
      "callType" : "PUSHES_TO",
      "sourceName" : "s3_1",
      "targetName" : "fs_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 11,
      "callType" : "PUSHES_TO",
      "sourceName" : "website_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 15,
      "callType" : "PULLED_FROM",
      "sourceName" : "fs_1",
      "targetName" : "s1_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 21,
      "callType" : "PULLED_FROM",
      "sourceName" : "fs_1",
      "targetName" : "s2_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 27,
      "callType" : "PULLED_FROM",
      "sourceName" : "fs_1",
      "targetName" : "s3_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 34,
      "callType" : "PULLED_FROM",
      "sourceName" : "s2_1",
      "targetName" : "download_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 30,
      "callType" : "PUSHES_TO",
      "sourceName" : "s3_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 19,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "s1_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 25,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "s2_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 31,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "s3_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 3,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "bt_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 8,
      "callType" : "PUSHES_TO",
      "sourceName" : "download_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 12,
      "callType" : "PULLED_FROM",
      "sourceName" : "s1_1",
      "targetName" : "website_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 32,
      "callType" : "PUSHES_TO",
      "sourceName" : "at_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 20,
      "callType" : "PUSHES_TO",
      "sourceName" : "s2_1",
      "targetName" : "fs_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 5,
      "callType" : "PUSHES_TO",
      "sourceName" : "upload_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 24,
      "callType" : "PUSHES_TO",
      "sourceName" : "s2_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callId" : 9,
      "callType" : "PUSHES_TO",
      "sourceName" : "download_1",
      "targetName" : "s2_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 17,
      "callType" : "PULLED_FROM",
      "sourceName" : "bt_1",
      "targetName" : "s1_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 22,
      "callType" : "PUSHES_TO",
      "sourceName" : "bt_1",
      "targetName" : "s2_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callId" : 28,
      "callType" : "PUSHES_TO",
      "sourceName" : "bt_1",
      "targetName" : "s3_1"
    } ],
    "serviceMachines" : [ {
      "name" : "db_1",
      "serviceName" : "db",
      "machineName" : "1",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "download_1",
      "serviceName" : "download",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "s3_1",
      "serviceName" : "s3",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "s2_1",
      "serviceName" : "s2",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "website_1",
      "serviceName" : "website",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "at_1",
      "serviceName" : "at",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "s1_1",
      "serviceName" : "s1",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "upload_1",
      "serviceName" : "upload",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "bt_1",
      "serviceName" : "bt",
      "machineName" : "3",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "fs_1",
      "serviceName" : "fs",
      "machineName" : "3",
      "statefulness" : "STATEFUL"
    } ],
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "upload",
      "location" : {
        "x" : 500.9420550882469,
        "y" : 98.64399564717917
      }
    }, {
      "name" : "fs",
      "location" : {
        "x" : 175.53859684276415,
        "y" : 293.8306148814467
      }
    }, {
      "name" : "s1",
      "location" : {
        "x" : 327.052717639565,
        "y" : 101.46366834392654
      }
    }, {
      "name" : "s3",
      "location" : {
        "x" : 326.03222920572017,
        "y" : 173.0938224008059
      }
    }, {
      "name" : "website",
      "location" : {
        "x" : 496.56516793719027,
        "y" : 168.79858533575867
      }
    }, {
      "name" : "at",
      "location" : {
        "x" : 635.8972445292688,
        "y" : 211.6702505515696
      }
    }, {
      "name" : "s2",
      "location" : {
        "x" : 334.95715663677197,
        "y" : 31.977014414696214
      }
    }, {
      "name" : "download",
      "location" : {
        "x" : 496.90950033289016,
        "y" : 29.573842054983686
      }
    }, {
      "name" : "bt",
      "location" : {
        "x" : 108.70644668140605,
        "y" : 71.17532733709697
      }
    }, {
      "name" : "db",
      "location" : {
        "x" : 436.64922053195943,
        "y" : 302.5197618972343
      }
    } ],
    "serviceMachines" : [ {
      "name" : "download_1",
      "location" : {
        "x" : 502.0195858782265,
        "y" : 85.64012158675513
      }
    }, {
      "name" : "fs_1",
      "location" : {
        "x" : 187.98484033869033,
        "y" : 221.31194003756707
      }
    }, {
      "name" : "website_1",
      "location" : {
        "x" : 510.5628815613277,
        "y" : 219.79755924137493
      }
    }, {
      "name" : "upload_1",
      "location" : {
        "x" : 507.69953301859357,
        "y" : 157.14469107468256
      }
    }, {
      "name" : "s3_1",
      "location" : {
        "x" : 317.41979304364133,
        "y" : 189.37474922274433
      }
    }, {
      "name" : "s2_1",
      "location" : {
        "x" : 316.8391056177354,
        "y" : 138.66639167613042
      }
    }, {
      "name" : "bt_1",
      "location" : {
        "x" : 140.6068521246213,
        "y" : 90.36245089074254
      }
    }, {
      "name" : "db_1",
      "location" : {
        "x" : 401.5786676922073,
        "y" : 362.8766219593282
      }
    }, {
      "name" : "at_1",
      "location" : {
        "x" : 636.9406785609593,
        "y" : 222.49000498850785
      }
    }, {
      "name" : "s1_1",
      "location" : {
        "x" : 316.2399422628335,
        "y" : 84.84561133298072
      }
    } ]
  }
}