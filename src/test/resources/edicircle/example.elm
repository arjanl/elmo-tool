bt: {STATEFUL}
fs: {STATEFUL}
btdb: {STATEFUL}
db: {STATEFUL}
upload: {STATELESS}
download: {STATELESS}
website: {STATELESS}
s1: {STATELESS}
s2: {STATELESS}
s3: {STATELESS}


fs >- bt
bt -> btdb
btdb >- bt

upload -> s1
upload -> db
db >- upload

db >- download
download -> db
download -> s2

db >- website
website -> db
s1 >- website
s2 >- website

s1 -> fs
fs >- s1
bt -> s1
bt >- s1
s1 -> db
db >- s1  

s2 -> fs
fs >- s2
bt -> s2
bt >- s2
s2 -> db
db >- s2 

s3 -> fs
fs >- s3
bt -> s3
bt >- s3
s3 -> db
db >- s3  

      
1 = {btdb, db}
2 = {upload, download, website}
3 = {bt,fs,s1,s2,s3}
