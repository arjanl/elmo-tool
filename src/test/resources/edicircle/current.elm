{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "bt",
        "description" : "bt",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "btdb",
        "description" : "btdb",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "website",
        "description" : "website",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "upload",
        "description" : "upload",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fs",
        "description" : "fs",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "download",
        "description" : "download",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "s1",
        "description" : "s1",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "s2",
        "description" : "s2",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "s3",
        "description" : "s3",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 1,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fs",
        "targetName" : "bt"
      }, {
        "id" : 2,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "bt",
        "targetName" : "btdb"
      }, {
        "id" : 3,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "btdb",
        "targetName" : "bt"
      }, {
        "id" : 4,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "upload",
        "targetName" : "s1"
      }, {
        "id" : 5,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "upload",
        "targetName" : "db"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "upload"
      }, {
        "id" : 7,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "download"
      }, {
        "id" : 8,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "download",
        "targetName" : "db"
      }, {
        "id" : 9,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "download",
        "targetName" : "s2"
      }, {
        "id" : 10,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "website"
      }, {
        "id" : 11,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "website",
        "targetName" : "db"
      }, {
        "id" : 12,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "s1",
        "targetName" : "website"
      }, {
        "id" : 13,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "s2",
        "targetName" : "website"
      }, {
        "id" : 14,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "s1",
        "targetName" : "fs"
      }, {
        "id" : 15,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fs",
        "targetName" : "s1"
      }, {
        "id" : 16,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "bt",
        "targetName" : "s1"
      }, {
        "id" : 17,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "bt",
        "targetName" : "s1"
      }, {
        "id" : 18,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "s1",
        "targetName" : "db"
      }, {
        "id" : 19,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "s1"
      }, {
        "id" : 20,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "s2",
        "targetName" : "fs"
      }, {
        "id" : 21,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fs",
        "targetName" : "s2"
      }, {
        "id" : 22,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "bt",
        "targetName" : "s2"
      }, {
        "id" : 23,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "bt",
        "targetName" : "s2"
      }, {
        "id" : 24,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "s2",
        "targetName" : "db"
      }, {
        "id" : 25,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "s2"
      }, {
        "id" : 26,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "s3",
        "targetName" : "fs"
      }, {
        "id" : 27,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fs",
        "targetName" : "s3"
      }, {
        "id" : 28,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "bt",
        "targetName" : "s3"
      }, {
        "id" : 29,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "bt",
        "targetName" : "s3"
      }, {
        "id" : 30,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "s3",
        "targetName" : "db"
      }, {
        "id" : 31,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "s3"
      } ]
    },
    "name" : "example.elm",
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    } ],
    "serviceMachines" : [ {
      "name" : "db_1",
      "serviceName" : "db",
      "machineName" : "1",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "download_1",
      "serviceName" : "download",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "s3_1",
      "serviceName" : "s3",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "s2_1",
      "serviceName" : "s2",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "website_1",
      "serviceName" : "website",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "s1_1",
      "serviceName" : "s1",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "upload_1",
      "serviceName" : "upload",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "btdb_1",
      "serviceName" : "btdb",
      "machineName" : "1",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "bt_1",
      "serviceName" : "bt",
      "machineName" : "3",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "fs_1",
      "serviceName" : "fs",
      "machineName" : "3",
      "statefulness" : "STATEFUL"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 6,
      "sourceName" : "db_1",
      "targetName" : "upload_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : true,
      "callId" : 1,
      "sourceName" : "fs_1",
      "targetName" : "bt_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : true,
      "callId" : 14,
      "sourceName" : "s1_1",
      "targetName" : "fs_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 10,
      "sourceName" : "db_1",
      "targetName" : "website_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 13,
      "sourceName" : "s2_1",
      "targetName" : "website_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 4,
      "sourceName" : "upload_1",
      "targetName" : "s1_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 3,
      "sourceName" : "btdb_1",
      "targetName" : "bt_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 7,
      "sourceName" : "db_1",
      "targetName" : "download_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : true,
      "callId" : 26,
      "sourceName" : "s3_1",
      "targetName" : "fs_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 11,
      "sourceName" : "website_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 18,
      "sourceName" : "s1_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : true,
      "callId" : 15,
      "sourceName" : "fs_1",
      "targetName" : "s1_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : true,
      "callId" : 21,
      "sourceName" : "fs_1",
      "targetName" : "s2_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : true,
      "callId" : 27,
      "sourceName" : "fs_1",
      "targetName" : "s3_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 30,
      "sourceName" : "s3_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 19,
      "sourceName" : "db_1",
      "targetName" : "s1_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 25,
      "sourceName" : "db_1",
      "targetName" : "s2_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 31,
      "sourceName" : "db_1",
      "targetName" : "s3_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 8,
      "sourceName" : "download_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : false,
      "callId" : 12,
      "sourceName" : "s1_1",
      "targetName" : "website_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : true,
      "callId" : 20,
      "sourceName" : "s2_1",
      "targetName" : "fs_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 5,
      "sourceName" : "upload_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 2,
      "sourceName" : "bt_1",
      "targetName" : "btdb_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 24,
      "sourceName" : "s2_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : false,
      "callId" : 9,
      "sourceName" : "download_1",
      "targetName" : "s2_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "local" : true,
      "callId" : 17,
      "sourceName" : "bt_1",
      "targetName" : "s1_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : true,
      "callId" : 22,
      "sourceName" : "bt_1",
      "targetName" : "s2_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "local" : true,
      "callId" : 28,
      "sourceName" : "bt_1",
      "targetName" : "s3_1"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "upload",
      "location" : {
        "x" : 458.9420550882469,
        "y" : 132.64399564717917
      }
    }, {
      "name" : "s1",
      "location" : {
        "x" : 287.052717639565,
        "y" : 70.46366834392657
      }
    }, {
      "name" : "website",
      "location" : {
        "x" : 457.56516793719027,
        "y" : 192.79858533575867
      }
    }, {
      "name" : "bt",
      "location" : {
        "x" : 108.70644668140605,
        "y" : 71.17532733709697
      }
    }, {
      "name" : "s3",
      "location" : {
        "x" : 287.03222920572017,
        "y" : 192.0938224008059
      }
    }, {
      "name" : "download",
      "location" : {
        "x" : 459.90950033289016,
        "y" : 69.57384205498367
      }
    }, {
      "name" : "fs",
      "location" : {
        "x" : 160.53859684276415,
        "y" : 222.8306148814467
      }
    }, {
      "name" : "db",
      "location" : {
        "x" : 457.64922053195943,
        "y" : 296.9397448073905
      }
    }, {
      "name" : "btdb",
      "location" : {
        "x" : 112.58698652512118,
        "y" : 294.87420030562464
      }
    }, {
      "name" : "s2",
      "location" : {
        "x" : 286.95715663677197,
        "y" : 133.9770144146962
      }
    } ],
    "serviceMachines" : [ {
      "name" : "s3_1",
      "location" : {
        "x" : 317.41979304364133,
        "y" : 189.37474922274433
      }
    }, {
      "name" : "bt_1",
      "location" : {
        "x" : 140.6068521246213,
        "y" : 90.36245089074254
      }
    }, {
      "name" : "upload_1",
      "location" : {
        "x" : 507.69953301859357,
        "y" : 157.14469107468256
      }
    }, {
      "name" : "fs_1",
      "location" : {
        "x" : 187.98484033869033,
        "y" : 221.31194003756707
      }
    }, {
      "name" : "s2_1",
      "location" : {
        "x" : 316.8391056177354,
        "y" : 138.66639167613042
      }
    }, {
      "name" : "download_1",
      "location" : {
        "x" : 505.0195858782265,
        "y" : 86.64012158675513
      }
    }, {
      "name" : "website_1",
      "location" : {
        "x" : 510.5628815613277,
        "y" : 219.79755924137493
      }
    }, {
      "name" : "btdb_1",
      "location" : {
        "x" : 141.18624240494793,
        "y" : 317.1722088208261
      }
    }, {
      "name" : "db_1",
      "location" : {
        "x" : 509.5786676922073,
        "y" : 319.8766219593282
      }
    }, {
      "name" : "s1_1",
      "location" : {
        "x" : 316.2399422628335,
        "y" : 84.84561133298072
      }
    } ]
  }
}