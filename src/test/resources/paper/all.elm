{
  "deployment" : {
    "architecture" : {
      "calls" : [ {
        "id" : 33,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "a",
        "targetName" : "r1"
      }, {
        "id" : 34,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "r1",
        "targetName" : "r2"
      }, {
        "id" : 35,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "a",
        "targetName" : "s1"
      }, {
        "id" : 36,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "s1",
        "targetName" : "s2"
      }, {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "b1",
        "targetName" : "a"
      }, {
        "id" : 2,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "b2",
        "targetName" : "b1"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "c1",
        "targetName" : "a"
      }, {
        "id" : 4,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "c1",
        "targetName" : "c2"
      }, {
        "id" : 5,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "d1",
        "targetName" : "a"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "d2",
        "targetName" : "d1"
      }, {
        "id" : 7,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "e1",
        "targetName" : "a"
      }, {
        "id" : 8,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "e1",
        "targetName" : "e2"
      }, {
        "id" : 9,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "f1",
        "targetName" : "a"
      }, {
        "id" : 10,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "f2",
        "targetName" : "f1"
      }, {
        "id" : 11,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "g1",
        "targetName" : "a"
      }, {
        "id" : 12,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "g1",
        "targetName" : "g2"
      }, {
        "id" : 13,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "h1",
        "targetName" : "a"
      }, {
        "id" : 14,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "h2",
        "targetName" : "h1"
      }, {
        "id" : 15,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "i1",
        "targetName" : "a"
      }, {
        "id" : 16,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "i1",
        "targetName" : "i2"
      }, {
        "id" : 17,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "j1",
        "targetName" : "a"
      }, {
        "id" : 18,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "j1",
        "targetName" : "j2"
      }, {
        "id" : 19,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "a",
        "targetName" : "k1"
      }, {
        "id" : 20,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "k2",
        "targetName" : "k1"
      }, {
        "id" : 21,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "a",
        "targetName" : "l1"
      }, {
        "id" : 22,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "l1",
        "targetName" : "l2"
      }, {
        "id" : 23,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "a",
        "targetName" : "m1"
      }, {
        "id" : 24,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "m2",
        "targetName" : "m1"
      }, {
        "id" : 25,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "a",
        "targetName" : "n1"
      }, {
        "id" : 26,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "n1",
        "targetName" : "n2"
      }, {
        "id" : 27,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "o1",
        "targetName" : "a"
      }, {
        "id" : 28,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "o2",
        "targetName" : "o1"
      }, {
        "id" : 29,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "p1",
        "targetName" : "a"
      }, {
        "id" : 30,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "p1",
        "targetName" : "p2"
      }, {
        "id" : 31,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "a",
        "targetName" : "q1"
      }, {
        "id" : 32,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "q2",
        "targetName" : "q1"
      } ],
      "services" : [ {
        "name" : "a",
        "originalName" : "a",
        "description" : "a",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "q2",
        "originalName" : "q2",
        "description" : "q2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "o1",
        "originalName" : "o1",
        "description" : "o1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "o2",
        "originalName" : "o2",
        "description" : "o2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "m1",
        "originalName" : "m1",
        "description" : "m1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "m2",
        "originalName" : "m2",
        "description" : "m2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "k1",
        "originalName" : "k1",
        "description" : "k1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "k2",
        "originalName" : "k2",
        "description" : "k2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "i1",
        "originalName" : "i1",
        "description" : "i1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "i2",
        "originalName" : "i2",
        "description" : "i2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "g1",
        "originalName" : "g1",
        "description" : "g1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "g2",
        "originalName" : "g2",
        "description" : "g2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "e1",
        "originalName" : "e1",
        "description" : "e1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "e2",
        "originalName" : "e2",
        "description" : "e2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c1",
        "originalName" : "c1",
        "description" : "c1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c2",
        "originalName" : "c2",
        "description" : "c2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "r1",
        "originalName" : "r1",
        "description" : "r1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "r2",
        "originalName" : "r2",
        "description" : "r2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p1",
        "originalName" : "p1",
        "description" : "p1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p2",
        "originalName" : "p2",
        "description" : "p2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "n1",
        "originalName" : "n1",
        "description" : "n1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "n2",
        "originalName" : "n2",
        "description" : "n2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "l1",
        "originalName" : "l1",
        "description" : "l1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "l2",
        "originalName" : "l2",
        "description" : "l2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "j1",
        "originalName" : "j1",
        "description" : "j1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "j2",
        "originalName" : "j2",
        "description" : "j2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "h1",
        "originalName" : "h1",
        "description" : "h1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "h2",
        "originalName" : "h2",
        "description" : "h2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "f1",
        "originalName" : "f1",
        "description" : "f1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "f2",
        "originalName" : "f2",
        "description" : "f2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "d1",
        "originalName" : "d1",
        "description" : "d1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "d2",
        "originalName" : "d2",
        "description" : "d2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "b1",
        "originalName" : "b1",
        "description" : "b1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "b2",
        "originalName" : "b2",
        "description" : "b2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "s1",
        "originalName" : "s1",
        "description" : "s1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "s2",
        "originalName" : "s2",
        "description" : "s2",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "q1",
        "originalName" : "q1",
        "description" : "q1",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ]
    },
    "name" : "all.elm",
    "serviceMachines" : [ {
      "name" : "o1_1",
      "index" : 1,
      "serviceName" : "o1",
      "machineName" : "4",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "m2_1",
      "index" : 1,
      "serviceName" : "m2",
      "machineName" : "7",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "s2_1",
      "index" : 1,
      "serviceName" : "s2",
      "machineName" : "37",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "q1_1",
      "index" : 1,
      "serviceName" : "q1",
      "machineName" : "38",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "q2_1",
      "index" : 1,
      "serviceName" : "q2",
      "machineName" : "3",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "k1_1",
      "index" : 1,
      "serviceName" : "k1",
      "machineName" : "8",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "o2_1",
      "index" : 1,
      "serviceName" : "o2",
      "machineName" : "5",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "m1_1",
      "index" : 1,
      "serviceName" : "m1",
      "machineName" : "6",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "k2_1",
      "index" : 1,
      "serviceName" : "k2",
      "machineName" : "9",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "i1_1",
      "index" : 1,
      "serviceName" : "i1",
      "machineName" : "10",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "i2_1",
      "index" : 1,
      "serviceName" : "i2",
      "machineName" : "11",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "b2_1",
      "index" : 1,
      "serviceName" : "b2",
      "machineName" : "35",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "b1_1",
      "index" : 1,
      "serviceName" : "b1",
      "machineName" : "34",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "e2_1",
      "index" : 1,
      "serviceName" : "e2",
      "machineName" : "15",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "c1_1",
      "index" : 1,
      "serviceName" : "c1",
      "machineName" : "16",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "g2_1",
      "index" : 1,
      "serviceName" : "g2",
      "machineName" : "13",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "e1_1",
      "index" : 1,
      "serviceName" : "e1",
      "machineName" : "14",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "g1_1",
      "index" : 1,
      "serviceName" : "g1",
      "machineName" : "12",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "s1_1",
      "index" : 1,
      "serviceName" : "s1",
      "machineName" : "36",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "p2_1",
      "index" : 1,
      "serviceName" : "p2",
      "machineName" : "21",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "p1_1",
      "index" : 1,
      "serviceName" : "p1",
      "machineName" : "20",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "r1_1",
      "index" : 1,
      "serviceName" : "r1",
      "machineName" : "18",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "r2_1",
      "index" : 1,
      "serviceName" : "r2",
      "machineName" : "19",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "n1_1",
      "index" : 1,
      "serviceName" : "n1",
      "machineName" : "22",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "l2_1",
      "index" : 1,
      "serviceName" : "l2",
      "machineName" : "25",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "n2_1",
      "index" : 1,
      "serviceName" : "n2",
      "machineName" : "23",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "l1_1",
      "index" : 1,
      "serviceName" : "l1",
      "machineName" : "24",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "j1_1",
      "index" : 1,
      "serviceName" : "j1",
      "machineName" : "26",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "j2_1",
      "index" : 1,
      "serviceName" : "j2",
      "machineName" : "27",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "h1_1",
      "index" : 1,
      "serviceName" : "h1",
      "machineName" : "28",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "h2_1",
      "index" : 1,
      "serviceName" : "h2",
      "machineName" : "29",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "c2_1",
      "index" : 1,
      "serviceName" : "c2",
      "machineName" : "17",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "d1_1",
      "index" : 1,
      "serviceName" : "d1",
      "machineName" : "32",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "d2_1",
      "index" : 1,
      "serviceName" : "d2",
      "machineName" : "33",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "f2_1",
      "index" : 1,
      "serviceName" : "f2",
      "machineName" : "31",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "a_1",
      "index" : 1,
      "serviceName" : "a",
      "machineName" : "2",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "f1_1",
      "index" : 1,
      "serviceName" : "f1",
      "machineName" : "30",
      "statefulness" : "STATEFUL"
    } ],
    "machines" : [ {
      "name" : "34"
    }, {
      "name" : "12"
    }, {
      "name" : "35"
    }, {
      "name" : "13"
    }, {
      "name" : "14"
    }, {
      "name" : "36"
    }, {
      "name" : "37"
    }, {
      "name" : "15"
    }, {
      "name" : "38"
    }, {
      "name" : "16"
    }, {
      "name" : "17"
    }, {
      "name" : "18"
    }, {
      "name" : "19"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    }, {
      "name" : "5"
    }, {
      "name" : "6"
    }, {
      "name" : "7"
    }, {
      "name" : "8"
    }, {
      "name" : "9"
    }, {
      "name" : "20"
    }, {
      "name" : "21"
    }, {
      "name" : "22"
    }, {
      "name" : "23"
    }, {
      "name" : "24"
    }, {
      "name" : "25"
    }, {
      "name" : "26"
    }, {
      "name" : "27"
    }, {
      "name" : "28"
    }, {
      "name" : "29"
    }, {
      "name" : "30"
    }, {
      "name" : "31"
    }, {
      "name" : "10"
    }, {
      "name" : "32"
    }, {
      "name" : "11"
    }, {
      "name" : "33"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "s1_1",
      "targetName" : "s2_1",
      "callId" : 36
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "n1_1",
      "callId" : 25
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "q2_1",
      "targetName" : "q1_1",
      "callId" : 32
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "f2_1",
      "targetName" : "f1_1",
      "callId" : 10
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "b2_1",
      "targetName" : "b1_1",
      "callId" : 2
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "m2_1",
      "targetName" : "m1_1",
      "callId" : 24
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "h1_1",
      "targetName" : "a_1",
      "callId" : 13
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "d1_1",
      "targetName" : "a_1",
      "callId" : 5
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "g1_1",
      "targetName" : "g2_1",
      "callId" : 12
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "r1_1",
      "targetName" : "r2_1",
      "callId" : 34
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "o1_1",
      "targetName" : "a_1",
      "callId" : 27
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "c1_1",
      "targetName" : "a_1",
      "callId" : 3
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "k1_1",
      "callId" : 19
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "c1_1",
      "targetName" : "c2_1",
      "callId" : 4
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "n1_1",
      "targetName" : "n2_1",
      "callId" : 26
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "m1_1",
      "callId" : 23
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "g1_1",
      "targetName" : "a_1",
      "callId" : 11
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "s1_1",
      "callId" : 35
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "j1_1",
      "targetName" : "j2_1",
      "callId" : 18
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "q1_1",
      "callId" : 31
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "h2_1",
      "targetName" : "h1_1",
      "callId" : 14
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "f1_1",
      "targetName" : "a_1",
      "callId" : 9
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p1_1",
      "targetName" : "a_1",
      "callId" : 29
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "o2_1",
      "targetName" : "o1_1",
      "callId" : 28
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "d2_1",
      "targetName" : "d1_1",
      "callId" : 6
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "b1_1",
      "targetName" : "a_1",
      "callId" : 1
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "k2_1",
      "targetName" : "k1_1",
      "callId" : 20
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "i1_1",
      "targetName" : "i2_1",
      "callId" : 16
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "j1_1",
      "targetName" : "a_1",
      "callId" : 17
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "e1_1",
      "targetName" : "e2_1",
      "callId" : 8
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p1_1",
      "targetName" : "p2_1",
      "callId" : 30
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "l1_1",
      "targetName" : "l2_1",
      "callId" : 22
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "l1_1",
      "callId" : 21
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "i1_1",
      "targetName" : "a_1",
      "callId" : 15
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "r1_1",
      "callId" : 33
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "e1_1",
      "targetName" : "a_1",
      "callId" : 7
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "s2",
      "location" : {
        "x" : 179.99679374694824,
        "y" : 713.2845147848129
      }
    }, {
      "name" : "r1",
      "location" : {
        "x" : 342.51193046569824,
        "y" : 677.2676080465317
      }
    }, {
      "name" : "g2",
      "location" : {
        "x" : 167.55192375183105,
        "y" : 267.17292726039886
      }
    }, {
      "name" : "e1",
      "location" : {
        "x" : 344.26896476745605,
        "y" : 198.0673211812973
      }
    }, {
      "name" : "m1",
      "location" : {
        "x" : 347.40964782238007,
        "y" : 493.7227973937988
      }
    }, {
      "name" : "i2",
      "location" : {
        "x" : 168.80620670318604,
        "y" : 335.5828467011452
      }
    }, {
      "name" : "c2",
      "location" : {
        "x" : 162.42757987976074,
        "y" : 125.30148255825043
      }
    }, {
      "name" : "f1",
      "location" : {
        "x" : 344.56173515319824,
        "y" : 233.64494264125824
      }
    }, {
      "name" : "r2",
      "location" : {
        "x" : 180.8752269744873,
        "y" : 674.7785943746567
      }
    }, {
      "name" : "k2",
      "location" : {
        "x" : 173.5146722793579,
        "y" : 414.6003419160843
      }
    }, {
      "name" : "b2",
      "location" : {
        "x" : 161.40270805358887,
        "y" : 86.35641419887543
      }
    }, {
      "name" : "j1",
      "location" : {
        "x" : 347.4899272918701,
        "y" : 379.4694513082504
      }
    }, {
      "name" : "a",
      "location" : {
        "x" : 889.5821332931519,
        "y" : 415.7778539657593
      }
    }, {
      "name" : "l2",
      "location" : {
        "x" : 174.28681755065918,
        "y" : 456.1883233785629
      }
    }, {
      "name" : "e2",
      "location" : {
        "x" : 163.7452220916748,
        "y" : 197.04243409633636
      }
    }, {
      "name" : "f2",
      "location" : {
        "x" : 166.9662914276123,
        "y" : 232.62008607387543
      }
    }, {
      "name" : "n1",
      "location" : {
        "x" : 344.12252616882324,
        "y" : 529.832641005516
      }
    }, {
      "name" : "d1",
      "location" : {
        "x" : 344.56173515319824,
        "y" : 163.51455628871918
      }
    }, {
      "name" : "s1",
      "location" : {
        "x" : 339.29098320007324,
        "y" : 715.1877740621567
      }
    }, {
      "name" : "o1",
      "location" : {
        "x" : 348.6196403503418,
        "y" : 564.1764023303986
      }
    }, {
      "name" : "m2",
      "location" : {
        "x" : 175.68000650405884,
        "y" : 490.2705798149109
      }
    }, {
      "name" : "l1",
      "location" : {
        "x" : 346.02584648132324,
        "y" : 456.77392518520355
      }
    }, {
      "name" : "n2",
      "location" : {
        "x" : 175.75089836120605,
        "y" : 526.3187860250473
      }
    }, {
      "name" : "b1",
      "location" : {
        "x" : 344.8545513153076,
        "y" : 86.20999848842621
      }
    }, {
      "name" : "g1",
      "location" : {
        "x" : 347.197509765625,
        "y" : 273.1826936006546
      }
    }, {
      "name" : "h1",
      "location" : {
        "x" : 346.46508598327637,
        "y" : 308.0213006734848
      }
    }, {
      "name" : "h2",
      "location" : {
        "x" : 166.81989860534668,
        "y" : 300.8471978902817
      }
    }, {
      "name" : "k1",
      "location" : {
        "x" : 349.58312344551086,
        "y" : 416.94291043281555
      }
    }, {
      "name" : "d2",
      "location" : {
        "x" : 162.72038078308105,
        "y" : 162.05041444301605
      }
    }, {
      "name" : "p2",
      "location" : {
        "x" : 177.80062675476074,
        "y" : 597.9132989645004
      }
    }, {
      "name" : "o2",
      "location" : {
        "x" : 177.0591607093811,
        "y" : 561.5871539115906
      }
    }, {
      "name" : "i1",
      "location" : {
        "x" : 345.63616275787354,
        "y" : 341.80948758125305
      }
    }, {
      "name" : "j2",
      "location" : {
        "x" : 170.33370780944824,
        "y" : 372.8809503316879
      }
    }, {
      "name" : "q1",
      "location" : {
        "x" : 349.539701461792,
        "y" : 637.7368158102036
      }
    }, {
      "name" : "q2",
      "location" : {
        "x" : 174.44565057754517,
        "y" : 633.8738284111023
      }
    }, {
      "name" : "c1",
      "location" : {
        "x" : 342.80477714538574,
        "y" : 126.61917817592621
      }
    }, {
      "name" : "p1",
      "location" : {
        "x" : 345.14742851257324,
        "y" : 600.8414911031723
      }
    } ],
    "serviceMachines" : [ {
      "name" : "p1_1",
      "location" : {
        "x" : 511.932373046875,
        "y" : 549.2592239379883
      }
    }, {
      "name" : "n1_1",
      "location" : {
        "x" : 514.1630859375,
        "y" : 465.39693450927734
      }
    }, {
      "name" : "i1_1",
      "location" : {
        "x" : 511.9323425292969,
        "y" : 266.52550506591797
      }
    }, {
      "name" : "d2_1",
      "location" : {
        "x" : 309.17974853515625,
        "y" : 85.8169174194336
      }
    }, {
      "name" : "a_1",
      "location" : {
        "x" : 1048.3162536621094,
        "y" : 247.7025146484375
      }
    }, {
      "name" : "e1_1",
      "location" : {
        "x" : 516.3846130371094,
        "y" : 122.11996459960938
      }
    }, {
      "name" : "j2_1",
      "location" : {
        "x" : 306.9676208496094,
        "y" : 307.77718353271484
      }
    }, {
      "name" : "q2_1",
      "location" : {
        "x" : 310.9420471191406,
        "y" : 582.1029739379883
      }
    }, {
      "name" : "i2_1",
      "location" : {
        "x" : 309.18902587890625,
        "y" : 273.6676940917969
      }
    }, {
      "name" : "h1_1",
      "location" : {
        "x" : 510.1701354980469,
        "y" : 229.03125762939453
      }
    }, {
      "name" : "m1_1",
      "location" : {
        "x" : 513.7039184570312,
        "y" : 427.2571029663086
      }
    }, {
      "name" : "l2_1",
      "location" : {
        "x" : 309.64830017089844,
        "y" : 379.66983795166016
      }
    }, {
      "name" : "s2_1",
      "location" : {
        "x" : 303.71853733062744,
        "y" : 655.6964421272278
      }
    }, {
      "name" : "d1_1",
      "location" : {
        "x" : 513.2355041503906,
        "y" : 83.60477447509766
      }
    }, {
      "name" : "m2_1",
      "location" : {
        "x" : 309.4730644226074,
        "y" : 419.8233733177185
      }
    }, {
      "name" : "g2_1",
      "location" : {
        "x" : 311.4757385253906,
        "y" : 195.73758697509766
      }
    }, {
      "name" : "s1_1",
      "location" : {
        "x" : 511.9043884277344,
        "y" : 669.4432754516602
      }
    }, {
      "name" : "h2_1",
      "location" : {
        "x" : 310.96063232421875,
        "y" : 235.18975067138672
      }
    }, {
      "name" : "o2_1",
      "location" : {
        "x" : 311.8231506347656,
        "y" : 501.3898239135742
      }
    }, {
      "name" : "n2_1",
      "location" : {
        "x" : 307.83013916015625,
        "y" : 459.21044158935547
      }
    }, {
      "name" : "e2_1",
      "location" : {
        "x" : 310.5014953613281,
        "y" : 123.43232727050781
      }
    }, {
      "name" : "k2_1",
      "location" : {
        "x" : 305.6273193359375,
        "y" : 343.3854446411133
      }
    }, {
      "name" : "r1_1",
      "location" : {
        "x" : 515.025634765625,
        "y" : 630.4036331176758
      }
    }, {
      "name" : "f1_1",
      "location" : {
        "x" : 511.9044189453125,
        "y" : 159.4904556274414
      }
    }, {
      "name" : "b2_1",
      "location" : {
        "x" : 314.93505859375,
        "y" : 17.541921615600586
      }
    }, {
      "name" : "g1_1",
      "location" : {
        "x" : 509.69232177734375,
        "y" : 197.50916290283203
      }
    }, {
      "name" : "p2_1",
      "location" : {
        "x" : 311.8697509765625,
        "y" : 544.3944931030273
      }
    }, {
      "name" : "b1_1",
      "location" : {
        "x" : 510.15155029296875,
        "y" : 17.513978004455566
      }
    }, {
      "name" : "c1_1",
      "location" : {
        "x" : 511.03265380859375,
        "y" : 52.55121994018555
      }
    }, {
      "name" : "c2_1",
      "location" : {
        "x" : 317.0951843261719,
        "y" : 50.193931579589844
      }
    }, {
      "name" : "r2_1",
      "location" : {
        "x" : 307.4082336425781,
        "y" : 620.2334671020508
      }
    }, {
      "name" : "f2_1",
      "location" : {
        "x" : 310.07025146484375,
        "y" : 158.91014099121094
      }
    }, {
      "name" : "k1_1",
      "location" : {
        "x" : 513.7132568359375,
        "y" : 349.63719940185547
      }
    }, {
      "name" : "q1_1",
      "location" : {
        "x" : 509.3020210266113,
        "y" : 589.7191198468208
      }
    }, {
      "name" : "o1_1",
      "location" : {
        "x" : 510.66080474853516,
        "y" : 507.92869931459427
      }
    }, {
      "name" : "j1_1",
      "location" : {
        "x" : 511.9324188232422,
        "y" : 307.3180160522461
      }
    }, {
      "name" : "l1_1",
      "location" : {
        "x" : 511.4638671875,
        "y" : 390.4110336303711
      }
    } ]
  }
}