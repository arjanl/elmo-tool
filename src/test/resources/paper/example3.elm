{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "acc",
        "originalName" : "acc",
        "description" : "acc",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ "order" ]
      }, {
        "name" : "price",
        "originalName" : "price",
        "description" : "price",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "price" ],
        "consumes" : [ "prod" ]
      }, {
        "name" : "db'",
        "originalName" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "chkout",
        "originalName" : "chkout",
        "description" : "chkout",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "order" ],
        "consumes" : [ "prod", "price" ]
      }, {
        "name" : "office",
        "originalName" : "office",
        "description" : "office",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "prod" ],
        "consumes" : [ ]
      }, {
        "name" : "db",
        "originalName" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 1,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "prod",
        "sourceName" : "db'",
        "targetName" : "price"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "prod",
        "sourceName" : "db'",
        "targetName" : "chkout"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "order",
        "sourceName" : "chkout",
        "targetName" : "db"
      }, {
        "id" : 4,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "price",
        "sourceName" : "price",
        "targetName" : "chkout"
      }, {
        "id" : 5,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "prod",
        "sourceName" : "office",
        "targetName" : "db'"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "order",
        "sourceName" : "db",
        "targetName" : "acc"
      } ]
    },
    "name" : "example3.elm",
    "serviceMachines" : [ {
      "name" : "db_1",
      "index" : 1,
      "statefulness" : "STATEFUL",
      "serviceName" : "db",
      "machineName" : "1"
    }, {
      "name" : "price_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "price",
      "machineName" : "2"
    }, {
      "name" : "acc_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "acc",
      "machineName" : "6"
    }, {
      "name" : "chkout_2",
      "index" : 2,
      "statefulness" : "STATELESS",
      "serviceName" : "chkout",
      "machineName" : "3"
    }, {
      "name" : "db_2",
      "index" : 2,
      "statefulness" : "STATEFUL",
      "serviceName" : "db'",
      "machineName" : "7"
    }, {
      "name" : "office_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "office",
      "machineName" : "5"
    }, {
      "name" : "chkout_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "chkout",
      "machineName" : "4"
    } ],
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    }, {
      "name" : "5"
    }, {
      "name" : "6"
    }, {
      "name" : "7"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 6,
      "sourceName" : "db_1",
      "targetName" : "acc_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 5,
      "sourceName" : "office_1",
      "targetName" : "db_2"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "sourceName" : "db_2",
      "targetName" : "price_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "sourceName" : "chkout_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "sourceName" : "chkout_2",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 4,
      "sourceName" : "price_1",
      "targetName" : "chkout_2"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "sourceName" : "db_2",
      "targetName" : "chkout_2"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 4,
      "sourceName" : "price_1",
      "targetName" : "chkout_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "sourceName" : "db_2",
      "targetName" : "chkout_1"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "office",
      "location" : {
        "x" : 183.7845969469165,
        "y" : 249.7391869622287
      }
    }, {
      "name" : "acc",
      "location" : {
        "x" : 371.01142808269304,
        "y" : 249.38769399765755
      }
    }, {
      "name" : "db",
      "location" : {
        "x" : 367.83047308053744,
        "y" : 149.97809716337312
      }
    }, {
      "name" : "price",
      "location" : {
        "x" : 163.60835681935794,
        "y" : 69.48051592677479
      }
    }, {
      "name" : "chkout",
      "location" : {
        "x" : 377.70311061269007,
        "y" : 61.54225840786921
      }
    }, {
      "name" : "db'",
      "location" : {
        "x" : 166.92046146343375,
        "y" : 155.06556806494297
      }
    } ],
    "serviceMachines" : [ {
      "name" : "db_1",
      "location" : {
        "x" : 351.4907574003298,
        "y" : 171.1787560007957
      }
    }, {
      "name" : "price_1",
      "location" : {
        "x" : 93.0342059644201,
        "y" : 57.805043394076165
      }
    }, {
      "name" : "db_2",
      "location" : {
        "x" : 214.55453574783348,
        "y" : 174.48667937255905
      }
    }, {
      "name" : "office_1",
      "location" : {
        "x" : 136.89575778282693,
        "y" : 301.7599618940651
      }
    }, {
      "name" : "acc_1",
      "location" : {
        "x" : 369.54268128036244,
        "y" : 302.8339762377884
      }
    }, {
      "name" : "chkout_2",
      "location" : {
        "x" : 502.88663659123165,
        "y" : 58.34452458938401
      }
    }, {
      "name" : "chkout_1",
      "location" : {
        "x" : 336.18151044399303,
        "y" : 57.44220186375378
      }
    } ]
  }
}