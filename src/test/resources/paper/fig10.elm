{
  "deployment" : {
    "architecture" : {
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "alpha",
        "sourceName" : "a",
        "targetName" : "b"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "beta",
        "sourceName" : "c",
        "targetName" : "a"
      }, {
        "id" : 3,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "beta",
        "sourceName" : "b",
        "targetName" : "c"
      } ],
      "services" : [ {
        "name" : "a",
        "description" : "a",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "alpha"],
        "consumes" : ["beta" ]
      }, {
        "name" : "b",
        "description" : "b",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "beta" ],
        "consumes" : [ "alpha"]
      }, {
        "name" : "c",
        "description" : "c",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ "beta" ]
      } ]
    },
    "name" : "beta 2.elm",
    "serviceMachines" : [ {
      "name" : "b_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "b",
      "machineName" : "3"
    }, {
      "name" : "a_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "a",
      "machineName" : "2"
    }, {
      "name" : "c_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "c",
      "machineName" : "4"
    } ],
    "machines" : [ {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "sourceName" : "c_1",
      "targetName" : "a_1",
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "sourceName" : "a_1",
      "targetName" : "b_1",
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "sourceName" : "b_1",
      "targetName" : "c_1",
      "callType" : "PULLED_FROM",
      "local" : false
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "c",
      "location" : {
        "x" : 225.05662035942078,
        "y" : 356.31420612335205
      }
    }, {
      "name" : "b",
      "location" : {
        "x" : 391.0822763442993,
        "y" : 119.69884538650513
      }
    }, {
      "name" : "a",
      "location" : {
        "x" : 84.3130854293704,
        "y" : 130.69885331392288
      }
    } ],
    "serviceMachines" : [ {
      "name" : "b_1",
      "location" : {
        "x" : 411.20501613616943,
        "y" : 78.15386027097702
      }
    }, {
      "name" : "a_1",
      "location" : {
        "x" : 131.69225120544434,
        "y" : 82.6922527551651
      }
    }, {
      "name" : "c_1",
      "location" : {
        "x" : 275.9999837875366,
        "y" : 299.10256469249725
      }
    } ]
  }
}