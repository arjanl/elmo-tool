ms: {STATELESS}

np: {STATELESS}
tscn: {STATELESS}
copu: {STATELESS}

co: {STATELESS}

bap: {STATELESS}

ew: {STATELESS}
bapi: {STATELESS}

fxr: {STATELESS}
cs: {STATELESS}
csdb: {STATELESS}

pgwm: {STATELESS}
pgww: {STATELESS}
d: {STATELESS}
tc: {STATELESS}
epm: {STATELESS}
adm: {STATELESS}

fw: {STATELESS}
fo: {STATELESS}
fc: {STATELESS}

cow: {STATELESS}
cifw: {STATELESS}

cpmw: {STATELESS}
cpmd: {STATELESS}
cka: {STATELESS}
hw: {STATELESS}
hd: {STATELESS}

mlr: {STATELESS}

pgwdb: {STATELESS}

cjms: {STATELESS}

fjms: {STATELESS}

fdb: {STATELESS}

codb: {STATELESS}

cckdb: {STATELESS}

cpmjms: {STATELESS}

hsm: {STATELESS}
hjms: {STATELESS}
hdb: {STATELESS}

pcjms: {STATELESS}

pcdb: {STATELESS}
pca: {STATELESS}
pcad: {STATELESS}

finjms: {STATELESS}

tryt: {STATELESS}
fnk: {STATELESS}

findb: {STATELESS}

npaesb: {STATELESS}
cas: {STATELESS}

npa: {STATELESS}

afmjms: {STATELESS}
afma: {STATELESS}
sma: {STATELESS}
smd: {STATELESS}
afmad: {STATELESS}

afmdb: {STATELESS}
afmsmdb: {STATELESS}

ms->pgwm
ms-<pgwm
ms->cifw
ms-<cifw
np->pgwm
np-<pgwm
np->fw
np-<fw
fo->tscn
copu->cow
copu-<cow
co->cow
co-<cow
co->cifw
co-<cifw
co->cpmw
co-<cpmw

cpmw->bap
bap->cpmw

ew->hw
ew-<hw
hd->ew
hd->bapi

fxr-<cs
cs-<csdb
cs->pgwdb
cs-<pgwdb
fw-<fxr

pgwm->cjms
pgwm->pgwdb
pgwm-<pgwdb
pgww->pgwdb
pgww-<pgwdb
pgww->cjms
cjms->d
npaesb->epm
tc->cjms
tc-<cjms
tc->pgwdb
tc-<pgwdb
epm-<pgwdb
fw->fjms
fw->fc
fo->fc
fjms->fw
fjms->fo
fo->fdb
fo-<fdb
fw->fdb
fw-<fdb
fc->fdb
fc-<fdb

cifw->cow
cow->cifw
cifw->codb
cifw-<codb
cow->codb
cow-<codb
cow->pgww
d->cow
adm->cow
npaesb->adm
cifw->cpmw
cpmw->cifw

cpmw->cpmjms
cpmjms->cpmw
cpmw->cka
cpmd->cka
cka->hsm
cka-<hsm
cpmw->cckdb
cpmw-<cckdb
cpmd->cckdb
cpmd-<cckdb
cka->cckdb
cka-<cckdb
hw -> cpmw


tc->hw
hw->hjms
hw-<hjms
hd->hjms
hd-<hjms
mlr->hd
hw->hdb
hw-<hdb
hd->hdb
hd-<hdb
 
cjms->finjms
fjms->cjms
cjms->fjms
cjms->hjms
hjms->cjms
afmjms->cjms
cjms->afmjms
cpmjms->cckdb
hjms->hdb
pcjms->pcdb
pca->pcdb
pca-<pcdb
pcad->pcdb
pcad-<pcdb
pcjms->pca
pca->pcjms
finjms->tryt
finjms->fnk
finjms->findb

tryt->fnk
afmjms->afma
afma->afmjms
afmjms->sma
afmjms->afmdb
npaesb->cas
npaesb->afma
sma->afmdb
sma-<afmdb
afma->afmdb
afma-<afmdb
afma->afmsmdb
afma-<afmsmdb
sma->afmsmdb
sma-<afmsmdb
smd->afmsmdb
smd-<afmsmdb
afma->afmsmdb
afma-<afmsmdb

1={ms}
2={np,tscn,copu}
3={co}
4={bap}
5={ew,bapi}
6={fxr,cs}
7={csdb}
8={pgwm,pgww,d,tc,epm,adm}
9={fw,fo,fc}
10={cow,cifw}
11={cpmw,cpmd,cka}
12={hw,hd}
13={mlr}
14={pgwdb}
15={cjms}
16={fjms}
17={fdb}
18={codb}
19={cckdb}
20={cpmjms}
21={hsm}
22={hjms}
23={hdb}
24={pcjms}
25={pcdb}
26={pca,pcad}
27={finjms}
28={tryt,fnk}
29={findb}
30={npaesb,cas}
31={npa}
32={afmjms}
33={afma,sma,smd,afmad}
34={afmdb}
35={afmsmdb}
