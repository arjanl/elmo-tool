{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "b",
        "description" : "b",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "d",
        "description" : "d",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "g",
        "description" : "g",
        "statefulness" : "PARTITIONED",
        "partitioning" : "session",
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "b",
        "sourceName" : "g"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "g",
        "sourceName" : "b"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "d",
        "sourceName" : "b"
      }, {
        "id" : 4,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "b",
        "sourceName" : "d"
      } ]
    },
    "name" : "gbd-codeploy.elm",
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    } ],
    "serviceMachines" : [ {
      "name" : "b_1",
      "serviceName" : "b",
      "machineName" : "1",
      "statefulness" : "STATELESS"
    }, {
      "name" : "d_1",
      "serviceName" : "d",
      "machineName" : "4",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "b_2",
      "serviceName" : "b",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "d_2",
      "serviceName" : "d",
      "machineName" : "3",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "g_2",
      "serviceName" : "g",
      "machineName" : "2",
      "statefulness" : "PARTITIONED"
    }, {
      "name" : "g_1",
      "serviceName" : "g",
      "machineName" : "1",
      "statefulness" : "PARTITIONED"
    } ],
    "connections" : [ {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 4,
      "targetName" : "b_1",
      "sourceName" : "d_2",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 4,
      "targetName" : "b_2",
      "sourceName" : "d_2",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 4,
      "targetName" : "b_1",
      "sourceName" : "d_1",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 4,
      "targetName" : "b_2",
      "sourceName" : "d_1",
      "local" : false
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "targetName" : "b_2",
      "sourceName" : "g_1",
      "local" : true
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "targetName" : "b_1",
      "sourceName" : "g_1",
      "local" : false
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "targetName" : "b_2",
      "sourceName" : "g_2",
      "local" : false
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "targetName" : "b_1",
      "sourceName" : "g_2",
      "local" : true
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "targetName" : "g_2",
      "sourceName" : "b_1",
      "local" : true
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "targetName" : "g_2",
      "sourceName" : "b_2",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "targetName" : "g_1",
      "sourceName" : "b_1",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "targetName" : "g_1",
      "sourceName" : "b_2",
      "local" : true
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "targetName" : "d_2",
      "sourceName" : "b_1",
      "local" : false
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "targetName" : "d_2",
      "sourceName" : "b_2",
      "local" : false
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "targetName" : "d_1",
      "sourceName" : "b_1",
      "local" : false
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "targetName" : "d_1",
      "sourceName" : "b_2",
      "local" : false
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "d",
      "location" : {
        "x" : 325.13386330402716,
        "y" : 217.59730854605073
      }
    }, {
      "name" : "g",
      "location" : {
        "x" : 328.7584450753691,
        "y" : 68.85465626338458
      }
    }, {
      "name" : "b",
      "location" : {
        "x" : 325.444675042419,
        "y" : 146.7263755118087
      }
    } ],
    "serviceMachines" : [ {
      "name" : "g_1",
      "location" : {
        "x" : 199.02329147293176,
        "y" : 78.95523667188763
      }
    }, {
      "name" : "d_1",
      "location" : {
        "x" : 198.78321486640647,
        "y" : 238.81967439612924
      }
    }, {
      "name" : "g_2",
      "location" : {
        "x" : 434.63081228422766,
        "y" : 81.10784813807624
      }
    }, {
      "name" : "b_2",
      "location" : {
        "x" : 433.54497843017657,
        "y" : 136.50316384271966
      }
    }, {
      "name" : "d_2",
      "location" : {
        "x" : 435.3448550784016,
        "y" : 234.57177831236913
      }
    }, {
      "name" : "b_1",
      "location" : {
        "x" : 203.44738013283063,
        "y" : 140.43768234302703
      }
    } ]
  }
}
