E: {STATELESS}
S: {STATELESS}
G: {STATELESS}
D: {PARTITIONED:id}

E -> D
D >- S
S >- G
        
1 = {E}
2 = {E}
3 = {D}
4 = {D}
5 = {S,G}
6 = {S,G}
