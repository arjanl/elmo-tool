p: {STATEFUL}
qdb: {STATEFUL}
c: {STATELESS}

p  -> qdb
qdb >- c

1 = {p}
2 = {qdb}
3 = {c}
        