G: {STATELESS}
U: {PARTITIONED:user}
D: {PARTITIONED:user}

G >= U
G => U
U -> D
U >- D
        
1 = {G}
2 = {G}
3 = {U,D}
4 = {U,D}
