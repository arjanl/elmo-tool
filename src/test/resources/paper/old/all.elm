{
  "deployment" : {
    "architecture" : {
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "p1",
        "targetName" : "S"
      }, {
        "id" : 2,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "p2",
        "targetName" : "S"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "p3",
        "targetName" : "S"
      }, {
        "id" : 4,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "p4",
        "targetName" : "S"
      }, {
        "id" : 5,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "p5",
        "targetName" : "S"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "p6",
        "targetName" : "S"
      }, {
        "id" : 7,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "p7",
        "targetName" : "S"
      }, {
        "id" : 8,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "p8",
        "targetName" : "S"
      }, {
        "id" : 9,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "S",
        "targetName" : "c1"
      }, {
        "id" : 10,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "S",
        "targetName" : "c2"
      }, {
        "id" : 11,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "S",
        "targetName" : "c3"
      }, {
        "id" : 12,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "S",
        "targetName" : "c4"
      }, {
        "id" : 13,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "S",
        "targetName" : "c5"
      }, {
        "id" : 14,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "S",
        "targetName" : "c6"
      }, {
        "id" : 15,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "S",
        "targetName" : "c7"
      }, {
        "id" : 16,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "S",
        "targetName" : "c8"
      }, {
        "id" : 17,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pp1",
        "targetName" : "p1"
      }, {
        "id" : 18,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pp2",
        "targetName" : "p2"
      }, {
        "id" : 19,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "p3",
        "targetName" : "pp3"
      }, {
        "id" : 20,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "p4",
        "targetName" : "pp4"
      }, {
        "id" : 21,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pp5",
        "targetName" : "p5"
      }, {
        "id" : 22,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pp6",
        "targetName" : "p6"
      }, {
        "id" : 23,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "p7",
        "targetName" : "pp7"
      }, {
        "id" : 24,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "p8",
        "targetName" : "pp8"
      }, {
        "id" : 25,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cc1",
        "targetName" : "c1"
      }, {
        "id" : 26,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cc2",
        "targetName" : "c2"
      }, {
        "id" : 27,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "c3",
        "targetName" : "cc3"
      }, {
        "id" : 28,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "c4",
        "targetName" : "cc4"
      }, {
        "id" : 29,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cc5",
        "targetName" : "c5"
      }, {
        "id" : 30,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cc6",
        "targetName" : "c6"
      }, {
        "id" : 31,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "c7",
        "targetName" : "cc7"
      }, {
        "id" : 32,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "c8",
        "targetName" : "cc8"
      } ],
      "services" : [ {
        "name" : "c1",
        "description" : "c1",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c2",
        "description" : "c2",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c3",
        "description" : "c3",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c4",
        "description" : "c4",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pp2",
        "description" : "pp2",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c5",
        "description" : "c5",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pp1",
        "description" : "pp1",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c6",
        "description" : "c6",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pp4",
        "description" : "pp4",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c7",
        "description" : "c7",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pp3",
        "description" : "pp3",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c8",
        "description" : "c8",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pp6",
        "description" : "pp6",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pp5",
        "description" : "pp5",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pp8",
        "description" : "pp8",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pp7",
        "description" : "pp7",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p1",
        "description" : "p1",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p2",
        "description" : "p2",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p3",
        "description" : "p3",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p4",
        "description" : "p4",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p5",
        "description" : "p5",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p6",
        "description" : "p6",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p7",
        "description" : "p7",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p8",
        "description" : "p8",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cc2",
        "description" : "cc2",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cc1",
        "description" : "cc1",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "S",
        "description" : "S",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cc4",
        "description" : "cc4",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cc3",
        "description" : "cc3",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cc6",
        "description" : "cc6",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cc5",
        "description" : "cc5",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cc8",
        "description" : "cc8",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cc7",
        "description" : "cc7",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ]
    },
    "name" : "all.elm",
    "machines" : [ {
      "name" : "34"
    }, {
      "name" : "12"
    }, {
      "name" : "13"
    }, {
      "name" : "14"
    }, {
      "name" : "15"
    }, {
      "name" : "16"
    }, {
      "name" : "17"
    }, {
      "name" : "18"
    }, {
      "name" : "19"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    }, {
      "name" : "5"
    }, {
      "name" : "6"
    }, {
      "name" : "7"
    }, {
      "name" : "8"
    }, {
      "name" : "9"
    }, {
      "name" : "20"
    }, {
      "name" : "21"
    }, {
      "name" : "22"
    }, {
      "name" : "23"
    }, {
      "name" : "24"
    }, {
      "name" : "25"
    }, {
      "name" : "26"
    }, {
      "name" : "27"
    }, {
      "name" : "28"
    }, {
      "name" : "29"
    }, {
      "name" : "30"
    }, {
      "name" : "31"
    }, {
      "name" : "32"
    }, {
      "name" : "10"
    }, {
      "name" : "33"
    }, {
      "name" : "11"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "pp1_1",
      "targetName" : "p1_1",
      "callId" : 17,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "pp5_1",
      "targetName" : "p5_1",
      "callId" : 21,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p7_1",
      "targetName" : "S_1",
      "callId" : 7,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "S_1",
      "targetName" : "c1_1",
      "callId" : 9,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "c4_1",
      "targetName" : "cc4_1",
      "callId" : 28,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "S_1",
      "targetName" : "c2_1",
      "callId" : 10,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "c8_1",
      "targetName" : "cc8_1",
      "callId" : 32,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p5_1",
      "targetName" : "S_1",
      "callId" : 5,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "S_1",
      "targetName" : "c3_1",
      "callId" : 11,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "S_1",
      "targetName" : "c4_1",
      "callId" : 12,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p1_1",
      "targetName" : "S_1",
      "callId" : 1,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p3_1",
      "targetName" : "pp3_1",
      "callId" : 19,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p7_1",
      "targetName" : "pp7_1",
      "callId" : 23,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "cc6_1",
      "targetName" : "c6_1",
      "callId" : 30,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p3_1",
      "targetName" : "S_1",
      "callId" : 3,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "cc2_1",
      "targetName" : "c2_1",
      "callId" : 26,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "pp2_1",
      "targetName" : "p2_1",
      "callId" : 18,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "pp6_1",
      "targetName" : "p6_1",
      "callId" : 22,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "c3_1",
      "targetName" : "cc3_1",
      "callId" : 27,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p8_1",
      "targetName" : "S_1",
      "callId" : 8,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "c7_1",
      "targetName" : "cc7_1",
      "callId" : 31,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p6_1",
      "targetName" : "S_1",
      "callId" : 6,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p4_1",
      "targetName" : "pp4_1",
      "callId" : 20,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p2_1",
      "targetName" : "S_1",
      "callId" : 2,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p8_1",
      "targetName" : "pp8_1",
      "callId" : 24,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "p4_1",
      "targetName" : "S_1",
      "callId" : 4,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "S_1",
      "targetName" : "c6_1",
      "callId" : 14,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "cc5_1",
      "targetName" : "c5_1",
      "callId" : 29,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "S_1",
      "targetName" : "c5_1",
      "callId" : 13,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "cc1_1",
      "targetName" : "c1_1",
      "callId" : 25,
      "callType" : "PUSHES_TO",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "S_1",
      "targetName" : "c8_1",
      "callId" : 16,
      "callType" : "PULLED_FROM",
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "S_1",
      "targetName" : "c7_1",
      "callId" : 15,
      "callType" : "PULLED_FROM",
      "local" : false
    } ],
    "serviceMachines" : [ {
      "name" : "cc1_1",
      "index" : 1,
      "serviceName" : "cc1",
      "machineName" : "27",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cc2_1",
      "index" : 1,
      "serviceName" : "cc2",
      "machineName" : "26",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cc3_1",
      "index" : 1,
      "serviceName" : "cc3",
      "machineName" : "30",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cc4_1",
      "index" : 1,
      "serviceName" : "cc4",
      "machineName" : "29",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cc5_1",
      "index" : 1,
      "serviceName" : "cc5",
      "machineName" : "32",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cc6_1",
      "index" : 1,
      "serviceName" : "cc6",
      "machineName" : "31",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cc7_1",
      "index" : 1,
      "serviceName" : "cc7",
      "machineName" : "34",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cc8_1",
      "index" : 1,
      "serviceName" : "cc8",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c1_1",
      "index" : 1,
      "serviceName" : "c1",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pp7_1",
      "index" : 1,
      "serviceName" : "pp7",
      "machineName" : "17",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pp8_1",
      "index" : 1,
      "serviceName" : "pp8",
      "machineName" : "16",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pp5_1",
      "index" : 1,
      "serviceName" : "pp5",
      "machineName" : "15",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pp3_1",
      "index" : 1,
      "serviceName" : "pp3",
      "machineName" : "12",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pp6_1",
      "index" : 1,
      "serviceName" : "pp6",
      "machineName" : "14",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pp4_1",
      "index" : 1,
      "serviceName" : "pp4",
      "machineName" : "10",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pp2_1",
      "index" : 1,
      "serviceName" : "pp2",
      "machineName" : "6",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pp1_1",
      "index" : 1,
      "serviceName" : "pp1",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "p2_1",
      "index" : 1,
      "serviceName" : "p2",
      "machineName" : "19",
      "statefulness" : "STATELESS"
    }, {
      "name" : "p1_1",
      "index" : 1,
      "serviceName" : "p1",
      "machineName" : "18",
      "statefulness" : "STATELESS"
    }, {
      "name" : "p6_1",
      "index" : 1,
      "serviceName" : "p6",
      "machineName" : "23",
      "statefulness" : "STATELESS"
    }, {
      "name" : "p5_1",
      "index" : 1,
      "serviceName" : "p5",
      "machineName" : "22",
      "statefulness" : "STATELESS"
    }, {
      "name" : "S_1",
      "index" : 1,
      "serviceName" : "S",
      "machineName" : "28",
      "statefulness" : "STATELESS"
    }, {
      "name" : "p4_1",
      "index" : 1,
      "serviceName" : "p4",
      "machineName" : "21",
      "statefulness" : "STATELESS"
    }, {
      "name" : "p3_1",
      "index" : 1,
      "serviceName" : "p3",
      "machineName" : "20",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c7_1",
      "index" : 1,
      "serviceName" : "c7",
      "machineName" : "11",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c6_1",
      "index" : 1,
      "serviceName" : "c6",
      "machineName" : "9",
      "statefulness" : "STATELESS"
    }, {
      "name" : "p8_1",
      "index" : 1,
      "serviceName" : "p8",
      "machineName" : "25",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c8_1",
      "index" : 1,
      "serviceName" : "c8",
      "machineName" : "13",
      "statefulness" : "STATELESS"
    }, {
      "name" : "p7_1",
      "index" : 1,
      "serviceName" : "p7",
      "machineName" : "24",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c3_1",
      "index" : 1,
      "serviceName" : "c3",
      "machineName" : "4",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c2_1",
      "index" : 1,
      "serviceName" : "c2",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c5_1",
      "index" : 1,
      "serviceName" : "c5",
      "machineName" : "7",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c4_1",
      "index" : 1,
      "serviceName" : "c4",
      "machineName" : "5",
      "statefulness" : "STATELESS"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "cc3",
      "location" : {
        "x" : 97.43589544296265,
        "y" : 1.9487228393554688
      }
    }, {
      "name" : "cc5",
      "location" : {
        "x" : 83.79487371444702,
        "y" : 87.69229888916016
      }
    }, {
      "name" : "p5",
      "location" : {
        "x" : 235.7948603630066,
        "y" : 448.20509338378906
      }
    }, {
      "name" : "c8",
      "location" : {
        "x" : 238.96750831604004,
        "y" : 211.95925545692444
      }
    }, {
      "name" : "cc6",
      "location" : {
        "x" : 79.89744329452515,
        "y" : 136.41024780273438
      }
    }, {
      "name" : "pp1",
      "location" : {
        "x" : 59.60499572753906,
        "y" : 273.3358271121979
      }
    }, {
      "name" : "c4",
      "location" : {
        "x" : 242.1885280609131,
        "y" : 47.687206506729126
      }
    }, {
      "name" : "pp5",
      "location" : {
        "x" : 67.99576568603516,
        "y" : 442.1817011833191
      }
    }, {
      "name" : "c3",
      "location" : {
        "x" : 243.79903745651245,
        "y" : 0.9824028015136719
      }
    }, {
      "name" : "c7",
      "location" : {
        "x" : 242.18852424621582,
        "y" : 168.4754602909088
      }
    }, {
      "name" : "p2",
      "location" : {
        "x" : 240.57803535461426,
        "y" : 311.8108911514282
      }
    }, {
      "name" : "p6",
      "location" : {
        "x" : 235.7948603630066,
        "y" : 487.17942810058594
      }
    }, {
      "name" : "c1",
      "location" : {
        "x" : 242.18853950500488,
        "y" : -85.9851496219635
      }
    }, {
      "name" : "p8",
      "location" : {
        "x" : 233.84613037109375,
        "y" : 563.1793975830078
      }
    }, {
      "name" : "pp6",
      "location" : {
        "x" : 69.26806545257568,
        "y" : 479.4811426997185
      }
    }, {
      "name" : "p4",
      "location" : {
        "x" : 235.79484176635742,
        "y" : 405.3332824707031
      }
    }, {
      "name" : "pp4",
      "location" : {
        "x" : 67.65753746032715,
        "y" : 398.955605506897
      }
    }, {
      "name" : "pp7",
      "location" : {
        "x" : 70.87856015563011,
        "y" : 522.030816078186
      }
    }, {
      "name" : "cc4",
      "location" : {
        "x" : 93.53846502304077,
        "y" : 46.769229888916016
      }
    }, {
      "name" : "pp3",
      "location" : {
        "x" : 70.87856006622314,
        "y" : 357.0823450088501
      }
    }, {
      "name" : "c5",
      "location" : {
        "x" : 240.57800778746605,
        "y" : 86.33944511413574
      }
    }, {
      "name" : "S",
      "location" : {
        "x" : 531.9999151229858,
        "y" : 241.64099884033203
      }
    }, {
      "name" : "p7",
      "location" : {
        "x" : 233.84614181518555,
        "y" : 524.2050399780273
      }
    }, {
      "name" : "p3",
      "location" : {
        "x" : 239.69228219985962,
        "y" : 358.5640411376953
      }
    }, {
      "name" : "cc8",
      "location" : {
        "x" : 70.1538553237915,
        "y" : 214.35894775390625
      }
    }, {
      "name" : "pp2",
      "location" : {
        "x" : 67.65755844116211,
        "y" : 315.20909690856934
      }
    }, {
      "name" : "cc1",
      "location" : {
        "x" : 89.64102077484131,
        "y" : -89.6410083770752
      }
    }, {
      "name" : "c2",
      "location" : {
        "x" : 243.7990436553955,
        "y" : -40.89086198806763
      }
    }, {
      "name" : "p1",
      "location" : {
        "x" : 238.96751260757446,
        "y" : 273.1586298942566
      }
    }, {
      "name" : "cc7",
      "location" : {
        "x" : 83.79488182067871,
        "y" : 173.43588256835938
      }
    }, {
      "name" : "cc2",
      "location" : {
        "x" : 89.6410294175148,
        "y" : -42.87178421020508
      }
    }, {
      "name" : "c6",
      "location" : {
        "x" : 240.57803630828857,
        "y" : 124.99169564247131
      }
    }, {
      "name" : "pp8",
      "location" : {
        "x" : 69.86394309997559,
        "y" : 561.6977014541626
      }
    } ],
    "serviceMachines" : [ {
      "name" : "cc3_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "S_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "cc7_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "c8_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "cc4_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "cc8_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "c1_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "p2_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "p3_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "p8_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "pp2_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "pp3_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "c6_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "c7_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "cc5_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "pp1_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "p5_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "c5_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "cc1_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "cc6_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "c4_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "pp8_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "pp5_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "p7_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "pp7_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "p1_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "p6_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "c3_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "c2_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "pp6_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "p4_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "cc2_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    }, {
      "name" : "pp4_1",
      "location" : {
        "x" : 0.0,
        "y" : 0.0
      }
    } ]
  }
}