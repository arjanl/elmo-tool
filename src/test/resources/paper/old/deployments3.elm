{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "gui",
        "description" : "gui",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "gui",
        "targetName" : "db"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "gui"
      } ]
    },
    "name" : "deployments3.elm",
    "serviceMachines" : [ {
      "name" : "db_1",
      "statefulness" : "STATEFUL",
      "serviceName" : "db",
      "machineName" : "2"
    }, {
      "name" : "gui_3",
      "statefulness" : "STATEFUL",
      "serviceName" : "gui",
      "machineName" : "4"
    }, {
      "name" : "gui_1",
      "statefulness" : "STATEFUL",
      "serviceName" : "gui",
      "machineName" : "1"
    }, {
      "name" : "gui_2",
      "statefulness" : "STATEFUL",
      "serviceName" : "gui",
      "machineName" : "3"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callType" : "PUSHES_TO",
      "sourceName" : "gui_1",
      "targetName" : "db_1",
      "callId" : 1
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "gui_3",
      "callId" : 2
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callType" : "PUSHES_TO",
      "sourceName" : "gui_3",
      "targetName" : "db_1",
      "callId" : 1
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callType" : "PUSHES_TO",
      "sourceName" : "gui_2",
      "targetName" : "db_1",
      "callId" : 1
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "gui_2",
      "callId" : 2
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "gui_1",
      "callId" : 2
    } ],
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "db",
      "location" : {
        "x" : 219.57806696517596,
        "y" : 246.24136058871227
      }
    }, {
      "name" : "gui",
      "location" : {
        "x" : 225.52640956957373,
        "y" : 88.62448022374483
      }
    } ],
    "serviceMachines" : [ {
      "name" : "db_1",
      "location" : {
        "x" : 242.35814845722498,
        "y" : 235.5709339595508
      }
    }, {
      "name" : "gui_1",
      "location" : {
        "x" : 78.77220878982024,
        "y" : 74.46730441483322
      }
    }, {
      "name" : "gui_2",
      "location" : {
        "x" : 241.51941825351236,
        "y" : 71.58839705568488
      }
    }, {
      "name" : "gui_3",
      "location" : {
        "x" : 402.7824933314421,
        "y" : 70.64941300720952
      }
    } ]
  }
}