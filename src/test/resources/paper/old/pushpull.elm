{
  "deployment" : {
    "architecture" : {
      "calls" : [ {
        "id" : 1,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "p",
        "targetName" : "c"
      }, {
        "id" : 2,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "q",
        "targetName" : "d"
      } ],
      "services" : [ {
        "name" : "q",
        "description" : "q",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "c",
        "description" : "c",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "d",
        "description" : "d",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "p",
        "description" : "p",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ]
    },
    "name" : "pushpull.elm",
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    } ],
    "serviceMachines" : [ {
      "name" : "p_1",
      "serviceName" : "p",
      "machineName" : "1",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "q_1",
      "serviceName" : "q",
      "machineName" : "3",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "d_1",
      "serviceName" : "d",
      "machineName" : "4",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c_1",
      "serviceName" : "c",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PULLED_FROM",
      "sourceName" : "p_1",
      "targetName" : "c_1",
      "callId" : 1,
      "local" : false
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callType" : "PUSHES_TO",
      "sourceName" : "q_1",
      "targetName" : "d_1",
      "callId" : 2,
      "local" : false
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "c",
      "location" : {
        "x" : 166.43141353311034,
        "y" : 76.4537550358921
      }
    }, {
      "name" : "d",
      "location" : {
        "x" : 375.75979416493414,
        "y" : 77.4945343059984
      }
    }, {
      "name" : "q",
      "location" : {
        "x" : 381.9016416929601,
        "y" : 220.72056637515647
      }
    }, {
      "name" : "p",
      "location" : {
        "x" : 159.24219797922927,
        "y" : 220.21336743097146
      }
    } ],
    "serviceMachines" : [ {
      "name" : "p_1",
      "location" : {
        "x" : 130.28065000535278,
        "y" : 217.1469294119343
      }
    }, {
      "name" : "q_1",
      "location" : {
        "x" : 383.7982348203834,
        "y" : 222.83644698681616
      }
    }, {
      "name" : "c_1",
      "location" : {
        "x" : 126.63314541049775,
        "y" : 71.89350834422567
      }
    }, {
      "name" : "d_1",
      "location" : {
        "x" : 388.4227573921442,
        "y" : 68.93353914795088
      }
    } ]
  }
}