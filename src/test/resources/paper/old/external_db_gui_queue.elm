E: {STATELESS}
QDB: {STATEFUL}
QR: {STATELESS}
G: {STATELESS}
D: {STATEFUL}

E  -> QDB
QDB >- QR
QR -> D
G 	-> D
D	>- G
        
1 = {E}
2 = {QDB}
3 = {QR}
4 = {D}
5 = {G}

