{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "gui",
        "description" : "gui",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "gui",
        "targetName" : "db"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "gui"
      } ]
    },
    "name" : "deployments1.elm",
    "serviceMachines" : [ {
      "name" : "db_1",
      "statefulness" : "STATEFUL",
      "serviceName" : "db",
      "machineName" : "1"
    }, {
      "name" : "gui_1",
      "statefulness" : "STATEFUL",
      "serviceName" : "gui",
      "machineName" : "1"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callType" : "PUSHES_TO",
      "sourceName" : "gui_1",
      "targetName" : "db_1",
      "callId" : 1
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : true,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "gui_1",
      "callId" : 2
    } ],
    "machines" : [ {
      "name" : "1"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "db",
      "location" : {
        "x" : 110.96174802246924,
        "y" : 205.20938042174816
      }
    }, {
      "name" : "gui",
      "location" : {
        "x" : 114.01891883265682,
        "y" : 74.65840038401656
      }
    } ],
    "serviceMachines" : [ {
      "name" : "db_1",
      "location" : {
        "x" : 112.15662204513069,
        "y" : 192.32815585850574
      }
    }, {
      "name" : "gui_1",
      "location" : {
        "x" : 112.57463484441405,
        "y" : 89.42348093142903
      }
    } ]
  }
}