{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "pgww",
        "description" : "pgww",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fdb",
        "description" : "fdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afmdb",
        "description" : "afmdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "d",
        "description" : "d",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pcjms",
        "description" : "pcjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hdb",
        "description" : "hdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "co",
        "description" : "co",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "tc",
        "description" : "tc",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "tryt",
        "description" : "tryt",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cs",
        "description" : "cs",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afmjms",
        "description" : "afmjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pgwm",
        "description" : "pgwm",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cka",
        "description" : "cka",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hd",
        "description" : "hd",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "csdb",
        "description" : "csdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fjms",
        "description" : "fjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "findb",
        "description" : "findb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "codb",
        "description" : "codb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pgwdb",
        "description" : "pgwdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cow",
        "description" : "cow",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afmad",
        "description" : "afmad",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hw",
        "description" : "hw",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "bap",
        "description" : "bap",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afma",
        "description" : "afma",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "bapi",
        "description" : "bapi",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "copu",
        "description" : "copu",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "epm",
        "description" : "epm",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pcad",
        "description" : "pcad",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cpmjms",
        "description" : "cpmjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cjms",
        "description" : "cjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fnk",
        "description" : "fnk",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "ms",
        "description" : "ms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "adm",
        "description" : "adm",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cckdb",
        "description" : "cckdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "finjms",
        "description" : "finjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cpmd",
        "description" : "cpmd",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "ew",
        "description" : "ew",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "mlr",
        "description" : "mlr",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "npaesb",
        "description" : "npaesb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cpmw",
        "description" : "cpmw",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fc",
        "description" : "fc",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afmsmdb",
        "description" : "afmsmdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hsm",
        "description" : "hsm",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "np",
        "description" : "np",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pcdb",
        "description" : "pcdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "sma",
        "description" : "sma",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "smd",
        "description" : "smd",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fo",
        "description" : "fo",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pca",
        "description" : "pca",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fw",
        "description" : "fw",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cas",
        "description" : "cas",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hjms",
        "description" : "hjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cifw",
        "description" : "cifw",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fxr",
        "description" : "fxr",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "npa",
        "description" : "npa",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "tscn",
        "description" : "tscn",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pgwm",
        "sourceName" : "ms"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "ms",
        "sourceName" : "pgwm"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cifw",
        "sourceName" : "ms"
      }, {
        "id" : 4,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "ms",
        "sourceName" : "cifw"
      }, {
        "id" : 5,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pgwm",
        "sourceName" : "np"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "np",
        "sourceName" : "pgwm"
      }, {
        "id" : 7,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fw",
        "sourceName" : "np"
      }, {
        "id" : 8,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "np",
        "sourceName" : "fw"
      }, {
        "id" : 9,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "tscn",
        "sourceName" : "fo"
      }, {
        "id" : 10,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cow",
        "sourceName" : "copu"
      }, {
        "id" : 11,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "copu",
        "sourceName" : "cow"
      }, {
        "id" : 12,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cow",
        "sourceName" : "co"
      }, {
        "id" : 13,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "co",
        "sourceName" : "cow"
      }, {
        "id" : 14,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cifw",
        "sourceName" : "co"
      }, {
        "id" : 15,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "co",
        "sourceName" : "cifw"
      }, {
        "id" : 16,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cpmw",
        "sourceName" : "co"
      }, {
        "id" : 17,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "co",
        "sourceName" : "cpmw"
      }, {
        "id" : 18,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "bap",
        "sourceName" : "cpmw"
      }, {
        "id" : 19,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cpmw",
        "sourceName" : "bap"
      }, {
        "id" : 20,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hw",
        "sourceName" : "ew"
      }, {
        "id" : 21,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "ew",
        "sourceName" : "hw"
      }, {
        "id" : 22,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "ew",
        "sourceName" : "hd"
      }, {
        "id" : 23,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "bapi",
        "sourceName" : "hd"
      }, {
        "id" : 24,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "fxr",
        "sourceName" : "cs"
      }, {
        "id" : 25,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "cs",
        "sourceName" : "csdb"
      }, {
        "id" : 26,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pgwdb",
        "sourceName" : "cs"
      }, {
        "id" : 27,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "cs",
        "sourceName" : "pgwdb"
      }, {
        "id" : 28,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "fw",
        "sourceName" : "fxr"
      }, {
        "id" : 29,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cjms",
        "sourceName" : "pgwm"
      }, {
        "id" : 30,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pgwdb",
        "sourceName" : "pgwm"
      }, {
        "id" : 31,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "pgwm",
        "sourceName" : "pgwdb"
      }, {
        "id" : 32,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pgwdb",
        "sourceName" : "pgww"
      }, {
        "id" : 33,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "pgww",
        "sourceName" : "pgwdb"
      }, {
        "id" : 34,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cjms",
        "sourceName" : "pgww"
      }, {
        "id" : 35,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "d",
        "sourceName" : "cjms"
      }, {
        "id" : 36,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "epm",
        "sourceName" : "npaesb"
      }, {
        "id" : 37,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cjms",
        "sourceName" : "tc"
      }, {
        "id" : 38,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "tc",
        "sourceName" : "cjms"
      }, {
        "id" : 39,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pgwdb",
        "sourceName" : "tc"
      }, {
        "id" : 40,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "tc",
        "sourceName" : "pgwdb"
      }, {
        "id" : 41,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "epm",
        "sourceName" : "pgwdb"
      }, {
        "id" : 42,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fjms",
        "sourceName" : "fw"
      }, {
        "id" : 43,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fc",
        "sourceName" : "fw"
      }, {
        "id" : 44,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fc",
        "sourceName" : "fo"
      }, {
        "id" : 45,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fw",
        "sourceName" : "fjms"
      }, {
        "id" : 46,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fo",
        "sourceName" : "fjms"
      }, {
        "id" : 47,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fdb",
        "sourceName" : "fo"
      }, {
        "id" : 48,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "fo",
        "sourceName" : "fdb"
      }, {
        "id" : 49,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fdb",
        "sourceName" : "fw"
      }, {
        "id" : 50,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "fw",
        "sourceName" : "fdb"
      }, {
        "id" : 51,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fdb",
        "sourceName" : "fc"
      }, {
        "id" : 52,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "fc",
        "sourceName" : "fdb"
      }, {
        "id" : 53,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cow",
        "sourceName" : "cifw"
      }, {
        "id" : 54,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cifw",
        "sourceName" : "cow"
      }, {
        "id" : 55,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "codb",
        "sourceName" : "cifw"
      }, {
        "id" : 56,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "cifw",
        "sourceName" : "codb"
      }, {
        "id" : 57,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "codb",
        "sourceName" : "cow"
      }, {
        "id" : 58,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "cow",
        "sourceName" : "codb"
      }, {
        "id" : 59,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pgww",
        "sourceName" : "cow"
      }, {
        "id" : 60,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cow",
        "sourceName" : "d"
      }, {
        "id" : 61,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cow",
        "sourceName" : "adm"
      }, {
        "id" : 62,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "adm",
        "sourceName" : "npaesb"
      }, {
        "id" : 63,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cpmw",
        "sourceName" : "cifw"
      }, {
        "id" : 64,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cifw",
        "sourceName" : "cpmw"
      }, {
        "id" : 65,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cpmjms",
        "sourceName" : "cpmw"
      }, {
        "id" : 66,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cpmw",
        "sourceName" : "cpmjms"
      }, {
        "id" : 67,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cka",
        "sourceName" : "cpmw"
      }, {
        "id" : 68,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cka",
        "sourceName" : "cpmd"
      }, {
        "id" : 69,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hsm",
        "sourceName" : "cka"
      }, {
        "id" : 70,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "cka",
        "sourceName" : "hsm"
      }, {
        "id" : 71,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cckdb",
        "sourceName" : "cpmw"
      }, {
        "id" : 72,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "cpmw",
        "sourceName" : "cckdb"
      }, {
        "id" : 73,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cckdb",
        "sourceName" : "cpmd"
      }, {
        "id" : 74,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "cpmd",
        "sourceName" : "cckdb"
      }, {
        "id" : 75,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cckdb",
        "sourceName" : "cka"
      }, {
        "id" : 76,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "cka",
        "sourceName" : "cckdb"
      }, {
        "id" : 77,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cpmw",
        "sourceName" : "hw"
      }, {
        "id" : 78,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hw",
        "sourceName" : "tc"
      }, {
        "id" : 79,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hjms",
        "sourceName" : "hw"
      }, {
        "id" : 80,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "hw",
        "sourceName" : "hjms"
      }, {
        "id" : 81,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hjms",
        "sourceName" : "hd"
      }, {
        "id" : 82,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "hd",
        "sourceName" : "hjms"
      }, {
        "id" : 83,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hd",
        "sourceName" : "mlr"
      }, {
        "id" : 84,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hdb",
        "sourceName" : "hw"
      }, {
        "id" : 85,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "hw",
        "sourceName" : "hdb"
      }, {
        "id" : 86,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hdb",
        "sourceName" : "hd"
      }, {
        "id" : 87,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "hd",
        "sourceName" : "hdb"
      }, {
        "id" : 88,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "finjms",
        "sourceName" : "cjms"
      }, {
        "id" : 89,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cjms",
        "sourceName" : "fjms"
      }, {
        "id" : 90,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fjms",
        "sourceName" : "cjms"
      }, {
        "id" : 91,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hjms",
        "sourceName" : "cjms"
      }, {
        "id" : 92,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cjms",
        "sourceName" : "hjms"
      }, {
        "id" : 93,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cjms",
        "sourceName" : "afmjms"
      }, {
        "id" : 94,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmjms",
        "sourceName" : "cjms"
      }, {
        "id" : 95,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cckdb",
        "sourceName" : "cpmjms"
      }, {
        "id" : 96,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "hdb",
        "sourceName" : "hjms"
      }, {
        "id" : 97,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pcdb",
        "sourceName" : "pcjms"
      }, {
        "id" : 98,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pcdb",
        "sourceName" : "pca"
      }, {
        "id" : 99,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "pca",
        "sourceName" : "pcdb"
      }, {
        "id" : 100,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pcdb",
        "sourceName" : "pcad"
      }, {
        "id" : 101,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "pcad",
        "sourceName" : "pcdb"
      }, {
        "id" : 102,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pca",
        "sourceName" : "pcjms"
      }, {
        "id" : 103,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "pcjms",
        "sourceName" : "pca"
      }, {
        "id" : 104,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "tryt",
        "sourceName" : "finjms"
      }, {
        "id" : 105,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fnk",
        "sourceName" : "finjms"
      }, {
        "id" : 106,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "findb",
        "sourceName" : "finjms"
      }, {
        "id" : 107,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "fnk",
        "sourceName" : "tryt"
      }, {
        "id" : 108,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afma",
        "sourceName" : "afmjms"
      }, {
        "id" : 109,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmjms",
        "sourceName" : "afma"
      }, {
        "id" : 110,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "sma",
        "sourceName" : "afmjms"
      }, {
        "id" : 111,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmdb",
        "sourceName" : "afmjms"
      }, {
        "id" : 112,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "cas",
        "sourceName" : "npaesb"
      }, {
        "id" : 113,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afma",
        "sourceName" : "npaesb"
      }, {
        "id" : 114,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmdb",
        "sourceName" : "sma"
      }, {
        "id" : 115,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "sma",
        "sourceName" : "afmdb"
      }, {
        "id" : 116,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmdb",
        "sourceName" : "afma"
      }, {
        "id" : 117,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "afma",
        "sourceName" : "afmdb"
      }, {
        "id" : 118,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmsmdb",
        "sourceName" : "afma"
      }, {
        "id" : 119,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "afma",
        "sourceName" : "afmsmdb"
      }, {
        "id" : 120,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmsmdb",
        "sourceName" : "sma"
      }, {
        "id" : 121,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "sma",
        "sourceName" : "afmsmdb"
      }, {
        "id" : 122,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmsmdb",
        "sourceName" : "smd"
      }, {
        "id" : 123,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "smd",
        "sourceName" : "afmsmdb"
      }, {
        "id" : 124,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "targetName" : "afmsmdb",
        "sourceName" : "afma"
      }, {
        "id" : 125,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "targetName" : "afma",
        "sourceName" : "afmsmdb"
      } ]
    },
    "name" : "psp-layout.elm",
    "machines" : [ {
      "name" : "12"
    }, {
      "name" : "34"
    }, {
      "name" : "35"
    }, {
      "name" : "13"
    }, {
      "name" : "14"
    }, {
      "name" : "15"
    }, {
      "name" : "16"
    }, {
      "name" : "17"
    }, {
      "name" : "18"
    }, {
      "name" : "19"
    }, {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    }, {
      "name" : "5"
    }, {
      "name" : "6"
    }, {
      "name" : "7"
    }, {
      "name" : "8"
    }, {
      "name" : "9"
    }, {
      "name" : "20"
    }, {
      "name" : "21"
    }, {
      "name" : "22"
    }, {
      "name" : "23"
    }, {
      "name" : "24"
    }, {
      "name" : "25"
    }, {
      "name" : "26"
    }, {
      "name" : "27"
    }, {
      "name" : "28"
    }, {
      "name" : "29"
    }, {
      "name" : "30"
    }, {
      "name" : "31"
    }, {
      "name" : "10"
    }, {
      "name" : "32"
    }, {
      "name" : "33"
    }, {
      "name" : "11"
    } ],
    "serviceMachines" : [ {
      "name" : "npaesb_1",
      "serviceName" : "npaesb",
      "machineName" : "30",
      "statefulness" : "STATELESS"
    }, {
      "name" : "codb_1",
      "serviceName" : "codb",
      "machineName" : "18",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pcdb_1",
      "serviceName" : "pcdb",
      "machineName" : "25",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afmsmdb_1",
      "serviceName" : "afmsmdb",
      "machineName" : "35",
      "statefulness" : "STATELESS"
    }, {
      "name" : "tscn_1",
      "serviceName" : "tscn",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "bapi_1",
      "serviceName" : "bapi",
      "machineName" : "5",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hw_1",
      "serviceName" : "hw",
      "machineName" : "12",
      "statefulness" : "STATELESS"
    }, {
      "name" : "co_1",
      "serviceName" : "co",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cpmjms_1",
      "serviceName" : "cpmjms",
      "machineName" : "20",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pgwdb_1",
      "serviceName" : "pgwdb",
      "machineName" : "14",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pcad_1",
      "serviceName" : "pcad",
      "machineName" : "26",
      "statefulness" : "STATELESS"
    }, {
      "name" : "smd_1",
      "serviceName" : "smd",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pgwm_1",
      "serviceName" : "pgwm",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "np_1",
      "serviceName" : "np",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cpmw_1",
      "serviceName" : "cpmw",
      "machineName" : "11",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cjms_1",
      "serviceName" : "cjms",
      "machineName" : "15",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pgww_1",
      "serviceName" : "pgww",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fjms_1",
      "serviceName" : "fjms",
      "machineName" : "16",
      "statefulness" : "STATELESS"
    }, {
      "name" : "ew_1",
      "serviceName" : "ew",
      "machineName" : "5",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cifw_1",
      "serviceName" : "cifw",
      "machineName" : "10",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cka_1",
      "serviceName" : "cka",
      "machineName" : "11",
      "statefulness" : "STATELESS"
    }, {
      "name" : "bap_1",
      "serviceName" : "bap",
      "machineName" : "4",
      "statefulness" : "STATELESS"
    }, {
      "name" : "epm_1",
      "serviceName" : "epm",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "npa_1",
      "serviceName" : "npa",
      "machineName" : "31",
      "statefulness" : "STATELESS"
    }, {
      "name" : "d_1",
      "serviceName" : "d",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "tc_1",
      "serviceName" : "tc",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cow_1",
      "serviceName" : "cow",
      "machineName" : "10",
      "statefulness" : "STATELESS"
    }, {
      "name" : "copu_1",
      "serviceName" : "copu",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "ms_1",
      "serviceName" : "ms",
      "machineName" : "1",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cas_1",
      "serviceName" : "cas",
      "machineName" : "30",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pca_1",
      "serviceName" : "pca",
      "machineName" : "26",
      "statefulness" : "STATELESS"
    }, {
      "name" : "csdb_1",
      "serviceName" : "csdb",
      "machineName" : "7",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afmdb_1",
      "serviceName" : "afmdb",
      "machineName" : "34",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cckdb_1",
      "serviceName" : "cckdb",
      "machineName" : "19",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fw_1",
      "serviceName" : "fw",
      "machineName" : "9",
      "statefulness" : "STATELESS"
    }, {
      "name" : "finjms_1",
      "serviceName" : "finjms",
      "machineName" : "27",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cpmd_1",
      "serviceName" : "cpmd",
      "machineName" : "11",
      "statefulness" : "STATELESS"
    }, {
      "name" : "tryt_1",
      "serviceName" : "tryt",
      "machineName" : "28",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afma_1",
      "serviceName" : "afma",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "sma_1",
      "serviceName" : "sma",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hdb_1",
      "serviceName" : "hdb",
      "machineName" : "23",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fc_1",
      "serviceName" : "fc",
      "machineName" : "9",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afmad_1",
      "serviceName" : "afmad",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fnk_1",
      "serviceName" : "fnk",
      "machineName" : "28",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fxr_1",
      "serviceName" : "fxr",
      "machineName" : "6",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fo_1",
      "serviceName" : "fo",
      "machineName" : "9",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cs_1",
      "serviceName" : "cs",
      "machineName" : "6",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afmjms_1",
      "serviceName" : "afmjms",
      "machineName" : "32",
      "statefulness" : "STATELESS"
    }, {
      "name" : "mlr_1",
      "serviceName" : "mlr",
      "machineName" : "13",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hjms_1",
      "serviceName" : "hjms",
      "machineName" : "22",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fdb_1",
      "serviceName" : "fdb",
      "machineName" : "17",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hsm_1",
      "serviceName" : "hsm",
      "machineName" : "21",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pcjms_1",
      "serviceName" : "pcjms",
      "machineName" : "24",
      "statefulness" : "STATELESS"
    }, {
      "name" : "adm_1",
      "serviceName" : "adm",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hd_1",
      "serviceName" : "hd",
      "machineName" : "12",
      "statefulness" : "STATELESS"
    }, {
      "name" : "findb_1",
      "serviceName" : "findb",
      "machineName" : "29",
      "statefulness" : "STATELESS"
    } ],
    "connections" : [ {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 84,
      "targetName" : "hdb_1",
      "sourceName" : "hw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 7,
      "targetName" : "fw_1",
      "sourceName" : "np_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 115,
      "targetName" : "sma_1",
      "sourceName" : "afmdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 15,
      "targetName" : "co_1",
      "sourceName" : "cifw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 97,
      "targetName" : "pcdb_1",
      "sourceName" : "pcjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 83,
      "targetName" : "hd_1",
      "sourceName" : "mlr_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 122,
      "targetName" : "afmsmdb_1",
      "sourceName" : "smd_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 86,
      "targetName" : "hdb_1",
      "sourceName" : "hd_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 60,
      "targetName" : "cow_1",
      "sourceName" : "d_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 111,
      "targetName" : "afmdb_1",
      "sourceName" : "afmjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 11,
      "targetName" : "copu_1",
      "sourceName" : "cow_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 61,
      "targetName" : "cow_1",
      "sourceName" : "adm_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 53,
      "targetName" : "cow_1",
      "sourceName" : "cifw_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 94,
      "targetName" : "afmjms_1",
      "sourceName" : "cjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 26,
      "targetName" : "pgwdb_1",
      "sourceName" : "cs_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 24,
      "targetName" : "fxr_1",
      "sourceName" : "cs_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 76,
      "targetName" : "cka_1",
      "sourceName" : "cckdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 70,
      "targetName" : "cka_1",
      "sourceName" : "hsm_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 91,
      "targetName" : "hjms_1",
      "sourceName" : "cjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 62,
      "targetName" : "adm_1",
      "sourceName" : "npaesb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 72,
      "targetName" : "cpmw_1",
      "sourceName" : "cckdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 47,
      "targetName" : "fdb_1",
      "sourceName" : "fo_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 73,
      "targetName" : "cckdb_1",
      "sourceName" : "cpmd_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 98,
      "targetName" : "pcdb_1",
      "sourceName" : "pca_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 54,
      "targetName" : "cifw_1",
      "sourceName" : "cow_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 58,
      "targetName" : "cow_1",
      "sourceName" : "codb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 41,
      "targetName" : "epm_1",
      "sourceName" : "pgwdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 34,
      "targetName" : "cjms_1",
      "sourceName" : "pgww_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 19,
      "targetName" : "cpmw_1",
      "sourceName" : "bap_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 71,
      "targetName" : "cckdb_1",
      "sourceName" : "cpmw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 116,
      "targetName" : "afmdb_1",
      "sourceName" : "afma_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 95,
      "targetName" : "cckdb_1",
      "sourceName" : "cpmjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 92,
      "targetName" : "cjms_1",
      "sourceName" : "hjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 87,
      "targetName" : "hd_1",
      "sourceName" : "hdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 14,
      "targetName" : "cifw_1",
      "sourceName" : "co_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 50,
      "targetName" : "fw_1",
      "sourceName" : "fdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 9,
      "targetName" : "tscn_1",
      "sourceName" : "fo_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 82,
      "targetName" : "hd_1",
      "sourceName" : "hjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 30,
      "targetName" : "pgwdb_1",
      "sourceName" : "pgwm_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 67,
      "targetName" : "cka_1",
      "sourceName" : "cpmw_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 108,
      "targetName" : "afma_1",
      "sourceName" : "afmjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 90,
      "targetName" : "fjms_1",
      "sourceName" : "cjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 48,
      "targetName" : "fo_1",
      "sourceName" : "fdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 66,
      "targetName" : "cpmw_1",
      "sourceName" : "cpmjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 106,
      "targetName" : "findb_1",
      "sourceName" : "finjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 3,
      "targetName" : "cifw_1",
      "sourceName" : "ms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 99,
      "targetName" : "pca_1",
      "sourceName" : "pcdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 55,
      "targetName" : "codb_1",
      "sourceName" : "cifw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 23,
      "targetName" : "bapi_1",
      "sourceName" : "hd_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 38,
      "targetName" : "tc_1",
      "sourceName" : "cjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 49,
      "targetName" : "fdb_1",
      "sourceName" : "fw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 69,
      "targetName" : "hsm_1",
      "sourceName" : "cka_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 33,
      "targetName" : "pgww_1",
      "sourceName" : "pgwdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 101,
      "targetName" : "pcad_1",
      "sourceName" : "pcdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 1,
      "targetName" : "pgwm_1",
      "sourceName" : "ms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 107,
      "targetName" : "fnk_1",
      "sourceName" : "tryt_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 21,
      "targetName" : "ew_1",
      "sourceName" : "hw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 112,
      "targetName" : "cas_1",
      "sourceName" : "npaesb_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 6,
      "targetName" : "np_1",
      "sourceName" : "pgwm_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 25,
      "targetName" : "cs_1",
      "sourceName" : "csdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 59,
      "targetName" : "pgww_1",
      "sourceName" : "cow_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 18,
      "targetName" : "bap_1",
      "sourceName" : "cpmw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 105,
      "targetName" : "fnk_1",
      "sourceName" : "finjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 110,
      "targetName" : "sma_1",
      "sourceName" : "afmjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 74,
      "targetName" : "cpmd_1",
      "sourceName" : "cckdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 12,
      "targetName" : "cow_1",
      "sourceName" : "co_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 89,
      "targetName" : "cjms_1",
      "sourceName" : "fjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 22,
      "targetName" : "ew_1",
      "sourceName" : "hd_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 13,
      "targetName" : "co_1",
      "sourceName" : "cow_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 37,
      "targetName" : "cjms_1",
      "sourceName" : "tc_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 52,
      "targetName" : "fc_1",
      "sourceName" : "fdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 96,
      "targetName" : "hdb_1",
      "sourceName" : "hjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 118,
      "targetName" : "afmsmdb_1",
      "sourceName" : "afma_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 17,
      "targetName" : "co_1",
      "sourceName" : "cpmw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 28,
      "targetName" : "fw_1",
      "sourceName" : "fxr_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 57,
      "targetName" : "codb_1",
      "sourceName" : "cow_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 113,
      "targetName" : "afma_1",
      "sourceName" : "npaesb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 103,
      "targetName" : "pcjms_1",
      "sourceName" : "pca_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 78,
      "targetName" : "hw_1",
      "sourceName" : "tc_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 27,
      "targetName" : "cs_1",
      "sourceName" : "pgwdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 56,
      "targetName" : "cifw_1",
      "sourceName" : "codb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 2,
      "targetName" : "ms_1",
      "sourceName" : "pgwm_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 65,
      "targetName" : "cpmjms_1",
      "sourceName" : "cpmw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 68,
      "targetName" : "cka_1",
      "sourceName" : "cpmd_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 102,
      "targetName" : "pca_1",
      "sourceName" : "pcjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 119,
      "targetName" : "afma_1",
      "sourceName" : "afmsmdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 32,
      "targetName" : "pgwdb_1",
      "sourceName" : "pgww_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 121,
      "targetName" : "sma_1",
      "sourceName" : "afmsmdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 42,
      "targetName" : "fjms_1",
      "sourceName" : "fw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 77,
      "targetName" : "cpmw_1",
      "sourceName" : "hw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 63,
      "targetName" : "cpmw_1",
      "sourceName" : "cifw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 8,
      "targetName" : "np_1",
      "sourceName" : "fw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 39,
      "targetName" : "pgwdb_1",
      "sourceName" : "tc_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 46,
      "targetName" : "fo_1",
      "sourceName" : "fjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 114,
      "targetName" : "afmdb_1",
      "sourceName" : "sma_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 20,
      "targetName" : "hw_1",
      "sourceName" : "ew_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 45,
      "targetName" : "fw_1",
      "sourceName" : "fjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 100,
      "targetName" : "pcdb_1",
      "sourceName" : "pcad_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 4,
      "targetName" : "ms_1",
      "sourceName" : "cifw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 85,
      "targetName" : "hw_1",
      "sourceName" : "hdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 80,
      "targetName" : "hw_1",
      "sourceName" : "hjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 88,
      "targetName" : "finjms_1",
      "sourceName" : "cjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 75,
      "targetName" : "cckdb_1",
      "sourceName" : "cka_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 104,
      "targetName" : "tryt_1",
      "sourceName" : "finjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 10,
      "targetName" : "cow_1",
      "sourceName" : "copu_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 81,
      "targetName" : "hjms_1",
      "sourceName" : "hd_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 64,
      "targetName" : "cifw_1",
      "sourceName" : "cpmw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 117,
      "targetName" : "afma_1",
      "sourceName" : "afmdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 79,
      "targetName" : "hjms_1",
      "sourceName" : "hw_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 5,
      "targetName" : "pgwm_1",
      "sourceName" : "np_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 40,
      "targetName" : "tc_1",
      "sourceName" : "pgwdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 36,
      "targetName" : "epm_1",
      "sourceName" : "npaesb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 43,
      "targetName" : "fc_1",
      "sourceName" : "fw_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 51,
      "targetName" : "fdb_1",
      "sourceName" : "fc_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 123,
      "targetName" : "smd_1",
      "sourceName" : "afmsmdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 44,
      "targetName" : "fc_1",
      "sourceName" : "fo_1",
      "local" : true
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 29,
      "targetName" : "cjms_1",
      "sourceName" : "pgwm_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PULLED_FROM",
      "callId" : 31,
      "targetName" : "pgwm_1",
      "sourceName" : "pgwdb_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 16,
      "targetName" : "cpmw_1",
      "sourceName" : "co_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 35,
      "targetName" : "d_1",
      "sourceName" : "cjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 93,
      "targetName" : "cjms_1",
      "sourceName" : "afmjms_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 120,
      "targetName" : "afmsmdb_1",
      "sourceName" : "sma_1",
      "local" : false
    }, {
      "optionality" : "COMPULSORY",
      "callType" : "PUSHES_TO",
      "callId" : 109,
      "targetName" : "afmjms_1",
      "sourceName" : "afma_1",
      "local" : false
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "hw",
      "location" : {
        "x" : 235.01958211231567,
        "y" : 157.0100166758352
      }
    }, {
      "name" : "afmjms",
      "location" : {
        "x" : 336.6565333957195,
        "y" : 249.05616136616104
      }
    }, {
      "name" : "npaesb",
      "location" : {
        "x" : 200.796637629094,
        "y" : 406.46895767715637
      }
    }, {
      "name" : "tc",
      "location" : {
        "x" : 204.55154521221067,
        "y" : 131.73032772252859
      }
    }, {
      "name" : "fo",
      "location" : {
        "x" : 62.00248364628308,
        "y" : 52.75692372957795
      }
    }, {
      "name" : "tscn",
      "location" : {
        "x" : 14.827742233927175,
        "y" : 135.3107424709646
      }
    }, {
      "name" : "hsm",
      "location" : {
        "x" : 678.7925781422881,
        "y" : 129.84237414188019
      }
    }, {
      "name" : "ms",
      "location" : {
        "x" : 316.01472353155987,
        "y" : 28.192618485980837
      }
    }, {
      "name" : "d",
      "location" : {
        "x" : 118.82148187259116,
        "y" : 180.86704662664306
      }
    }, {
      "name" : "cifw",
      "location" : {
        "x" : 391.9162997580106,
        "y" : 20.471689083310725
      }
    }, {
      "name" : "adm",
      "location" : {
        "x" : 427.4735569476159,
        "y" : 169.36073028148184
      }
    }, {
      "name" : "finjms",
      "location" : {
        "x" : 33.639166831818784,
        "y" : 333.14996428214863
      }
    }, {
      "name" : "cow",
      "location" : {
        "x" : 406.7758430647994,
        "y" : 79.54942183213157
      }
    }, {
      "name" : "pgwdb",
      "location" : {
        "x" : 213.23768301618625,
        "y" : 88.32162808842486
      }
    }, {
      "name" : "fdb",
      "location" : {
        "x" : 144.86585448076278,
        "y" : 120.04109848215242
      }
    }, {
      "name" : "pgww",
      "location" : {
        "x" : 320.72011093234687,
        "y" : 118.69568965268608
      }
    }, {
      "name" : "fw",
      "location" : {
        "x" : 215.80494653348043,
        "y" : 14.67687397211921
      }
    }, {
      "name" : "pcad",
      "location" : {
        "x" : 835.9107759356759,
        "y" : 291.4440100193495
      }
    }, {
      "name" : "cka",
      "location" : {
        "x" : 632.3002601267252,
        "y" : 30.318598978127056
      }
    }, {
      "name" : "smd",
      "location" : {
        "x" : 536.8707405901265,
        "y" : 342.415172447662
      }
    }, {
      "name" : "afmdb",
      "location" : {
        "x" : 594.2329457329602,
        "y" : 372.4539549308846
      }
    }, {
      "name" : "pcdb",
      "location" : {
        "x" : 587.6054639288674,
        "y" : 253.71041711673803
      }
    }, {
      "name" : "codb",
      "location" : {
        "x" : 444.9100856861583,
        "y" : 17.315534583734536
      }
    }, {
      "name" : "afmad",
      "location" : {
        "x" : 309.6186105855314,
        "y" : 368.83453861789513
      }
    }, {
      "name" : "epm",
      "location" : {
        "x" : 400.9275901711417,
        "y" : 172.7538453764423
      }
    }, {
      "name" : "ew",
      "location" : {
        "x" : 257.2644563272064,
        "y" : 220.02961150976603
      }
    }, {
      "name" : "pca",
      "location" : {
        "x" : 716.1338570758209,
        "y" : 372.9132509019893
      }
    }, {
      "name" : "mlr",
      "location" : {
        "x" : 787.0612568810802,
        "y" : 82.19610282164024
      }
    }, {
      "name" : "copu",
      "location" : {
        "x" : 542.4377660961109,
        "y" : -9.021837456320199
      }
    }, {
      "name" : "cpmw",
      "location" : {
        "x" : 398.2941418787524,
        "y" : 49.765859047109494
      }
    }, {
      "name" : "np",
      "location" : {
        "x" : 144.4346260423305,
        "y" : -54.3488003654096
      }
    }, {
      "name" : "co",
      "location" : {
        "x" : 430.6761075880869,
        "y" : 42.20104415397476
      }
    }, {
      "name" : "afma",
      "location" : {
        "x" : 512.6887498330864,
        "y" : 305.9641178948709
      }
    }, {
      "name" : "cjms",
      "location" : {
        "x" : 121.04777826205589,
        "y" : 269.98018044529
      }
    }, {
      "name" : "cpmd",
      "location" : {
        "x" : 621.5980920843981,
        "y" : 184.06377592807723
      }
    }, {
      "name" : "sma",
      "location" : {
        "x" : 540.682921739284,
        "y" : 396.24793348263086
      }
    }, {
      "name" : "fc",
      "location" : {
        "x" : 172.44397017794364,
        "y" : 124.53152041772506
      }
    }, {
      "name" : "cckdb",
      "location" : {
        "x" : 611.130459816326,
        "y" : 214.57814875852722
      }
    }, {
      "name" : "fjms",
      "location" : {
        "x" : 148.16978875714634,
        "y" : 95.17136677596056
      }
    }, {
      "name" : "findb",
      "location" : {
        "x" : 74.60581291884935,
        "y" : 459.1351609766266
      }
    }, {
      "name" : "bap",
      "location" : {
        "x" : 358.3086293728053,
        "y" : 19.748268800601366
      }
    }, {
      "name" : "pgwm",
      "location" : {
        "x" : 241.5973900569565,
        "y" : 52.210381574570626
      }
    }, {
      "name" : "bapi",
      "location" : {
        "x" : 448.993893105958,
        "y" : -48.74965015771289
      }
    }, {
      "name" : "hd",
      "location" : {
        "x" : 552.0074378477523,
        "y" : 98.86513546606767
      }
    }, {
      "name" : "csdb",
      "location" : {
        "x" : 414.29553639422727,
        "y" : 416.34177342576834
      }
    }, {
      "name" : "fnk",
      "location" : {
        "x" : 25.159114018545665,
        "y" : 423.1690143410808
      }
    }, {
      "name" : "afmsmdb",
      "location" : {
        "x" : 550.6172745811086,
        "y" : 450.589496351173
      }
    }, {
      "name" : "cpmjms",
      "location" : {
        "x" : 476.4963803607447,
        "y" : 225.80014761106142
      }
    }, {
      "name" : "cs",
      "location" : {
        "x" : 117.8773311682148,
        "y" : 51.1712273458612
      }
    }, {
      "name" : "hdb",
      "location" : {
        "x" : 555.3083321551055,
        "y" : 58.96883859205471
      }
    }, {
      "name" : "pcjms",
      "location" : {
        "x" : 709.2365494067298,
        "y" : 236.10077316444801
      }
    }, {
      "name" : "hjms",
      "location" : {
        "x" : 550.7635871191292,
        "y" : 153.94581281698805
      }
    }, {
      "name" : "cas",
      "location" : {
        "x" : 324.19135499470167,
        "y" : 286.951128719346
      }
    }, {
      "name" : "tryt",
      "location" : {
        "x" : 82.32636245823117,
        "y" : 369.72795721658895
      }
    }, {
      "name" : "fxr",
      "location" : {
        "x" : 211.12182758848635,
        "y" : 356.09312378339354
      }
    }, {
      "name" : "npa",
      "location" : {
        "x" : 204.24074233732148,
        "y" : 453.6722013762072
      }
    } ],
    "serviceMachines" : [ {
      "name" : "fjms_1",
      "location" : {
        "x" : 568.5110889941703,
        "y" : 372.24172612489485
      }
    }, {
      "name" : "pgwdb_1",
      "location" : {
        "x" : 301.90223493484825,
        "y" : 435.74021719759793
      }
    }, {
      "name" : "cifw_1",
      "location" : {
        "x" : 404.9625367594483,
        "y" : 163.20725426069245
      }
    }, {
      "name" : "fxr_1",
      "location" : {
        "x" : 46.67979018712646,
        "y" : 333.28311265914414
      }
    }, {
      "name" : "cka_1",
      "location" : {
        "x" : 568.4471500718801,
        "y" : 258.3694030026913
      }
    }, {
      "name" : "d_1",
      "location" : {
        "x" : 365.7909918858877,
        "y" : 255.13455215277855
      }
    }, {
      "name" : "smd_1",
      "location" : {
        "x" : 668.6002350628592,
        "y" : 521.1700643705578
      }
    }, {
      "name" : "csdb_1",
      "location" : {
        "x" : 98.69493865651708,
        "y" : 428.13096371549193
      }
    }, {
      "name" : "cas_1",
      "location" : {
        "x" : 388.9577662214437,
        "y" : 574.8554842213757
      }
    }, {
      "name" : "bapi_1",
      "location" : {
        "x" : 621.4920146550608,
        "y" : 36.95366689618456
      }
    }, {
      "name" : "mlr_1",
      "location" : {
        "x" : 975.0633569704082,
        "y" : 281.79526814729644
      }
    }, {
      "name" : "pcjms_1",
      "location" : {
        "x" : 1005.6958550523846,
        "y" : 152.5629436627495
      }
    }, {
      "name" : "fo_1",
      "location" : {
        "x" : 72.20751017773802,
        "y" : 233.57001395728417
      }
    }, {
      "name" : "tscn_1",
      "location" : {
        "x" : 227.87567818854995,
        "y" : 129.7810453271103
      }
    }, {
      "name" : "sma_1",
      "location" : {
        "x" : 668.3295348115405,
        "y" : 588.1615934892503
      }
    }, {
      "name" : "tc_1",
      "location" : {
        "x" : 329.37463971886933,
        "y" : 332.9516409631078
      }
    }, {
      "name" : "hw_1",
      "location" : {
        "x" : 716.3950944426308,
        "y" : 148.0874250405563
      }
    }, {
      "name" : "fc_1",
      "location" : {
        "x" : 73.20103620464153,
        "y" : 195.699647860754
      }
    }, {
      "name" : "cjms_1",
      "location" : {
        "x" : 850.8899669924033,
        "y" : 479.7893562077168
      }
    }, {
      "name" : "ew_1",
      "location" : {
        "x" : 686.5600184015882,
        "y" : 35.44067945986687
      }
    }, {
      "name" : "pcad_1",
      "location" : {
        "x" : 854.1099472946875,
        "y" : 49.51978576433051
      }
    }, {
      "name" : "fdb_1",
      "location" : {
        "x" : 238.73998256303776,
        "y" : 504.87613841315374
      }
    }, {
      "name" : "hjms_1",
      "location" : {
        "x" : 699.0141441935218,
        "y" : 289.71329915382006
      }
    }, {
      "name" : "tryt_1",
      "location" : {
        "x" : 165.74343257369043,
        "y" : 579.2919866235613
      }
    }, {
      "name" : "pgww_1",
      "location" : {
        "x" : 360.30915998658253,
        "y" : 292.55714045208236
      }
    }, {
      "name" : "pgwm_1",
      "location" : {
        "x" : 255.2014748268939,
        "y" : 299.3798285243379
      }
    }, {
      "name" : "findb_1",
      "location" : {
        "x" : 197.54641036326137,
        "y" : 729.1648999696904
      }
    }, {
      "name" : "adm_1",
      "location" : {
        "x" : 376.1636057971126,
        "y" : 369.14804228452846
      }
    }, {
      "name" : "cpmd_1",
      "location" : {
        "x" : 578.6857576337286,
        "y" : 185.14875343252604
      }
    }, {
      "name" : "cpmw_1",
      "location" : {
        "x" : 582.3105279931797,
        "y" : 225.05930567556902
      }
    }, {
      "name" : "cow_1",
      "location" : {
        "x" : 406.7170197349268,
        "y" : 119.8759584945949
      }
    }, {
      "name" : "np_1",
      "location" : {
        "x" : 223.87556244876805,
        "y" : 90.13341907062129
      }
    }, {
      "name" : "bap_1",
      "location" : {
        "x" : 510.23313693088426,
        "y" : 49.58650532252719
      }
    }, {
      "name" : "afmdb_1",
      "location" : {
        "x" : 856.2769083034249,
        "y" : 576.9050303896827
      }
    }, {
      "name" : "afmjms_1",
      "location" : {
        "x" : 851.0198585230327,
        "y" : 673.548023595115
      }
    }, {
      "name" : "npaesb_1",
      "location" : {
        "x" : 495.3914918028814,
        "y" : 571.8700636208039
      }
    }, {
      "name" : "cpmjms_1",
      "location" : {
        "x" : 348.64416205814155,
        "y" : 508.11326405896307
      }
    }, {
      "name" : "copu_1",
      "location" : {
        "x" : 223.20954054156664,
        "y" : 49.94882331019494
      }
    }, {
      "name" : "hdb_1",
      "location" : {
        "x" : 838.1880962286718,
        "y" : 378.18820124669116
      }
    }, {
      "name" : "afmad_1",
      "location" : {
        "x" : 667.9183291818807,
        "y" : 552.5499547590184
      }
    }, {
      "name" : "afma_1",
      "location" : {
        "x" : 664.5570310883743,
        "y" : 487.9275536532096
      }
    }, {
      "name" : "pcdb_1",
      "location" : {
        "x" : 1009.047372771412,
        "y" : 60.144904295033996
      }
    }, {
      "name" : "finjms_1",
      "location" : {
        "x" : 88.31443402580157,
        "y" : 674.8995455002675
      }
    }, {
      "name" : "hd_1",
      "location" : {
        "x" : 707.7123411854338,
        "y" : 192.94904659395846
      }
    }, {
      "name" : "fnk_1",
      "location" : {
        "x" : 160.72925318464422,
        "y" : 626.0509477384007
      }
    }, {
      "name" : "fw_1",
      "location" : {
        "x" : 71.6605418358964,
        "y" : 152.19165066043055
      }
    }, {
      "name" : "co_1",
      "location" : {
        "x" : 265.0374223158651,
        "y" : 258.1104203523122
      }
    }, {
      "name" : "epm_1",
      "location" : {
        "x" : 265.8863404763896,
        "y" : 349.5016133381277
      }
    }, {
      "name" : "npa_1",
      "location" : {
        "x" : 45.76186524388927,
        "y" : 47.0306777970214
      }
    }, {
      "name" : "cs_1",
      "location" : {
        "x" : 49.14017568084455,
        "y" : 373.538156344422
      }
    }, {
      "name" : "hsm_1",
      "location" : {
        "x" : 1033.9729714316072,
        "y" : 416.1983198825729
      }
    }, {
      "name" : "afmsmdb_1",
      "location" : {
        "x" : 564.8592191829566,
        "y" : 684.8036721806593
      }
    }, {
      "name" : "pca_1",
      "location" : {
        "x" : 857.6695980467852,
        "y" : 141.4990784104876
      }
    }, {
      "name" : "cckdb_1",
      "location" : {
        "x" : 499.8565211614812,
        "y" : 476.5961695522525
      }
    }, {
      "name" : "ms_1",
      "location" : {
        "x" : 368.0619293646722,
        "y" : 669.9729883253465
      }
    }, {
      "name" : "codb_1",
      "location" : {
        "x" : 495.5743593452075,
        "y" : 305.11081797703997
      }
    } ]
  }
}