p: {STATEFUL}
cw: {STATELESS}
cdb: {STATEFUL}
c: {STATELESS}

p  >- cw
cw -> cdb
cdb >- c

1 = {p}
2 = {cw,cdb,c}
        