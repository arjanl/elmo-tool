p: {STATEFUL}
qdb: {STATEFUL}
c: {STATELESS}

p  -> qdb
qdb >- c

1 = {p,qdb}
2 = {c}
        