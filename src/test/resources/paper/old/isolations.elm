
p: {STATEFUL}
c: {STATELESS}


p1: {STATEFUL}
cw1: {STATELESS}
cdb1: {STATEFUL}
c1: {STATELESS}

p1  >- cw1
cw1 -> cdb1
cdb1 >- c1

p >- c

1 = {p}
2 = {c}

3 = {p1}
4 = {cw1,cdb1,c1}
        