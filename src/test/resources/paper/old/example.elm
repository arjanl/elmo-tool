{
  "deployment" : {
    "architecture" : {
      "calls" : [ {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "targetName" : "chkout",
        "sourceName" : "db"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "targetName" : "db",
        "sourceName" : "chkout"
      }, {
        "id" : 4,
        "type" : "PULLED_FROM",
        "routable" : true,
        "targetName" : "chkout",
        "sourceName" : "price"
      }, {
        "id" : 5,
        "type" : "PUSHES_TO",
        "routable" : true,
        "targetName" : "db",
        "sourceName" : "office"
      }, {
        "id" : 1,
        "type" : "PULLED_FROM",
        "routable" : true,
        "targetName" : "price",
        "sourceName" : "db"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "targetName" : "acc",
        "sourceName" : "db"
      } ],
      "services" : [ {
        "name" : "acc",
        "description" : "acc",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "price",
        "description" : "price",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "chkout",
        "description" : "chkout",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "office",
        "description" : "office",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ]
    },
    "name" : "example.elm",
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    }, {
      "name" : "5"
    }, {
      "name" : "6"
    } ],
    "serviceMachines" : [ {
      "name" : "db_1",
      "serviceName" : "db",
      "machineName" : "1",
      "statefulness" : "STATEFUL"
    }, {
      "name" : "price_1",
      "serviceName" : "price",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "acc_1",
      "serviceName" : "acc",
      "machineName" : "6",
      "statefulness" : "STATELESS"
    }, {
      "name" : "chkout_2",
      "serviceName" : "chkout",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "chkout_1",
      "serviceName" : "chkout",
      "machineName" : "4",
      "statefulness" : "STATELESS"
    }, {
      "name" : "office_1",
      "serviceName" : "office",
      "machineName" : "5",
      "statefulness" : "STATELESS"
    } ],
    "connections" : [ {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "targetName" : "db_1",
      "sourceName" : "chkout_2",
      "local" : false
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "targetName" : "db_1",
      "sourceName" : "chkout_1",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "targetName" : "chkout_2",
      "sourceName" : "db_1",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "targetName" : "price_1",
      "sourceName" : "db_1",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 4,
      "targetName" : "chkout_1",
      "sourceName" : "price_1",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "targetName" : "chkout_1",
      "sourceName" : "db_1",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 6,
      "targetName" : "acc_1",
      "sourceName" : "db_1",
      "local" : false
    }, {
      "callType" : "PULLED_FROM",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 4,
      "targetName" : "chkout_2",
      "sourceName" : "price_1",
      "local" : false
    }, {
      "callType" : "PUSHES_TO",
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 5,
      "targetName" : "db_1",
      "sourceName" : "office_1",
      "local" : false
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "chkout",
      "location" : {
        "x" : 361.70311061269007,
        "y" : 39.54225840786922
      }
    }, {
      "name" : "office",
      "location" : {
        "x" : 473.7845969469165,
        "y" : 287.7391869622287
      }
    }, {
      "name" : "acc",
      "location" : {
        "x" : 161.01142808269304,
        "y" : 278.38769399765755
      }
    }, {
      "name" : "price",
      "location" : {
        "x" : 231.60835681935794,
        "y" : 14.480515926774785
      }
    }, {
      "name" : "db",
      "location" : {
        "x" : 315.83047308053744,
        "y" : 168.97809716337312
      }
    } ],
    "serviceMachines" : [ {
      "name" : "office_1",
      "location" : {
        "x" : 267.89575778282693,
        "y" : 155.7599618940651
      }
    }, {
      "name" : "acc_1",
      "location" : {
        "x" : 105.54268128036244,
        "y" : 140.83397623778842
      }
    }, {
      "name" : "chkout_2",
      "location" : {
        "x" : 406.88663659123165,
        "y" : 189.344524589384
      }
    }, {
      "name" : "price_1",
      "location" : {
        "x" : 442.0342059644201,
        "y" : 153.80504339407616
      }
    }, {
      "name" : "chkout_1",
      "location" : {
        "x" : 434.18151044399303,
        "y" : 93.44220186375378
      }
    }, {
      "name" : "db_1",
      "location" : {
        "x" : 257.4907574003298,
        "y" : 141.1787560007957
      }
    } ]
  }
}