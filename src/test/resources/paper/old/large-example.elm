{
  "deployment" : {
    "architecture" : {
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "ms",
        "targetName" : "pgwm"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwm",
        "targetName" : "ms"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "ms",
        "targetName" : "cifw"
      }, {
        "id" : 4,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cifw",
        "targetName" : "ms"
      }, {
        "id" : 5,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "np",
        "targetName" : "pgwm"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwm",
        "targetName" : "np"
      }, {
        "id" : 7,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "np",
        "targetName" : "fw"
      }, {
        "id" : 8,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fw",
        "targetName" : "np"
      }, {
        "id" : 9,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fo",
        "targetName" : "tscn"
      }, {
        "id" : 10,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "copu",
        "targetName" : "cow"
      }, {
        "id" : 11,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cow",
        "targetName" : "copu"
      }, {
        "id" : 12,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "co",
        "targetName" : "cow"
      }, {
        "id" : 13,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cow",
        "targetName" : "co"
      }, {
        "id" : 14,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "co",
        "targetName" : "cifw"
      }, {
        "id" : 15,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cifw",
        "targetName" : "co"
      }, {
        "id" : 16,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "co",
        "targetName" : "cpmw"
      }, {
        "id" : 17,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmw",
        "targetName" : "co"
      }, {
        "id" : 18,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmw",
        "targetName" : "bap"
      }, {
        "id" : 19,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "bap",
        "targetName" : "cpmw"
      }, {
        "id" : 20,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "ew",
        "targetName" : "hw"
      }, {
        "id" : 21,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "hw",
        "targetName" : "ew"
      }, {
        "id" : 22,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hd",
        "targetName" : "ew"
      }, {
        "id" : 23,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hd",
        "targetName" : "bapi"
      }, {
        "id" : 24,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cs",
        "targetName" : "fxr"
      }, {
        "id" : 25,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "csdb",
        "targetName" : "cs"
      }, {
        "id" : 26,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cs",
        "targetName" : "pgwdb"
      }, {
        "id" : 27,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwdb",
        "targetName" : "cs"
      }, {
        "id" : 28,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fxr",
        "targetName" : "fw"
      }, {
        "id" : 29,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwm",
        "targetName" : "cjms"
      }, {
        "id" : 30,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwm",
        "targetName" : "pgwdb"
      }, {
        "id" : 31,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwdb",
        "targetName" : "pgwm"
      }, {
        "id" : 32,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgww",
        "targetName" : "pgwdb"
      }, {
        "id" : 33,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwdb",
        "targetName" : "pgww"
      }, {
        "id" : 34,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgww",
        "targetName" : "cjms"
      }, {
        "id" : 35,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cjms",
        "targetName" : "d"
      }, {
        "id" : 36,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "npaesb",
        "targetName" : "epm"
      }, {
        "id" : 37,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "tc",
        "targetName" : "cjms"
      }, {
        "id" : 38,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cjms",
        "targetName" : "tc"
      }, {
        "id" : 39,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "tc",
        "targetName" : "pgwdb"
      }, {
        "id" : 40,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwdb",
        "targetName" : "tc"
      }, {
        "id" : 41,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pgwdb",
        "targetName" : "epm"
      }, {
        "id" : 42,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fw",
        "targetName" : "fjms"
      }, {
        "id" : 43,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fw",
        "targetName" : "fc"
      }, {
        "id" : 44,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fo",
        "targetName" : "fc"
      }, {
        "id" : 45,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fjms",
        "targetName" : "fw"
      }, {
        "id" : 46,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fjms",
        "targetName" : "fo"
      }, {
        "id" : 47,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fo",
        "targetName" : "fdb"
      }, {
        "id" : 48,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fdb",
        "targetName" : "fo"
      }, {
        "id" : 49,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fw",
        "targetName" : "fdb"
      }, {
        "id" : 50,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fdb",
        "targetName" : "fw"
      }, {
        "id" : 51,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fc",
        "targetName" : "fdb"
      }, {
        "id" : 52,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "fdb",
        "targetName" : "fc"
      }, {
        "id" : 53,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cifw",
        "targetName" : "cow"
      }, {
        "id" : 54,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cow",
        "targetName" : "cifw"
      }, {
        "id" : 55,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cifw",
        "targetName" : "codb"
      }, {
        "id" : 56,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "codb",
        "targetName" : "cifw"
      }, {
        "id" : 57,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cow",
        "targetName" : "codb"
      }, {
        "id" : 58,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "codb",
        "targetName" : "cow"
      }, {
        "id" : 59,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cow",
        "targetName" : "pgww"
      }, {
        "id" : 60,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "d",
        "targetName" : "cow"
      }, {
        "id" : 61,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "adm",
        "targetName" : "cow"
      }, {
        "id" : 62,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "npaesb",
        "targetName" : "adm"
      }, {
        "id" : 63,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cifw",
        "targetName" : "cpmw"
      }, {
        "id" : 64,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmw",
        "targetName" : "cifw"
      }, {
        "id" : 65,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmw",
        "targetName" : "cpmjms"
      }, {
        "id" : 66,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmjms",
        "targetName" : "cpmw"
      }, {
        "id" : 67,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmw",
        "targetName" : "cka"
      }, {
        "id" : 68,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmd",
        "targetName" : "cka"
      }, {
        "id" : 69,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cka",
        "targetName" : "hsm"
      }, {
        "id" : 70,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "hsm",
        "targetName" : "cka"
      }, {
        "id" : 71,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmw",
        "targetName" : "cckdb"
      }, {
        "id" : 72,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cckdb",
        "targetName" : "cpmw"
      }, {
        "id" : 73,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmd",
        "targetName" : "cckdb"
      }, {
        "id" : 74,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cckdb",
        "targetName" : "cpmd"
      }, {
        "id" : 75,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cka",
        "targetName" : "cckdb"
      }, {
        "id" : 76,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "cckdb",
        "targetName" : "cka"
      }, {
        "id" : 77,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hw",
        "targetName" : "cpmw"
      }, {
        "id" : 78,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "tc",
        "targetName" : "hw"
      }, {
        "id" : 79,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hw",
        "targetName" : "hjms"
      }, {
        "id" : 80,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "hjms",
        "targetName" : "hw"
      }, {
        "id" : 81,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hd",
        "targetName" : "hjms"
      }, {
        "id" : 82,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "hjms",
        "targetName" : "hd"
      }, {
        "id" : 83,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "mlr",
        "targetName" : "hd"
      }, {
        "id" : 84,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hw",
        "targetName" : "hdb"
      }, {
        "id" : 85,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "hdb",
        "targetName" : "hw"
      }, {
        "id" : 86,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hd",
        "targetName" : "hdb"
      }, {
        "id" : 87,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "hdb",
        "targetName" : "hd"
      }, {
        "id" : 88,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cjms",
        "targetName" : "finjms"
      }, {
        "id" : 89,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "fjms",
        "targetName" : "cjms"
      }, {
        "id" : 90,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cjms",
        "targetName" : "fjms"
      }, {
        "id" : 91,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cjms",
        "targetName" : "hjms"
      }, {
        "id" : 92,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hjms",
        "targetName" : "cjms"
      }, {
        "id" : 93,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmjms",
        "targetName" : "cjms"
      }, {
        "id" : 94,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cjms",
        "targetName" : "afmjms"
      }, {
        "id" : 95,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "cpmjms",
        "targetName" : "cckdb"
      }, {
        "id" : 96,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "hjms",
        "targetName" : "hdb"
      }, {
        "id" : 97,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pcjms",
        "targetName" : "pcdb"
      }, {
        "id" : 98,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pca",
        "targetName" : "pcdb"
      }, {
        "id" : 99,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pcdb",
        "targetName" : "pca"
      }, {
        "id" : 100,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pcad",
        "targetName" : "pcdb"
      }, {
        "id" : 101,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "pcdb",
        "targetName" : "pcad"
      }, {
        "id" : 102,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pcjms",
        "targetName" : "pca"
      }, {
        "id" : 103,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "pca",
        "targetName" : "pcjms"
      }, {
        "id" : 104,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "finjms",
        "targetName" : "tryt"
      }, {
        "id" : 105,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "finjms",
        "targetName" : "fnk"
      }, {
        "id" : 106,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "finjms",
        "targetName" : "findb"
      }, {
        "id" : 107,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "tryt",
        "targetName" : "fnk"
      }, {
        "id" : 108,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmjms",
        "targetName" : "afma"
      }, {
        "id" : 109,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "afma",
        "targetName" : "afmjms"
      }, {
        "id" : 110,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmjms",
        "targetName" : "sma"
      }, {
        "id" : 111,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmjms",
        "targetName" : "afmdb"
      }, {
        "id" : 112,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "npaesb",
        "targetName" : "cas"
      }, {
        "id" : 113,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "npaesb",
        "targetName" : "afma"
      }, {
        "id" : 114,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "sma",
        "targetName" : "afmdb"
      }, {
        "id" : 115,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmdb",
        "targetName" : "sma"
      }, {
        "id" : 116,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "afma",
        "targetName" : "afmdb"
      }, {
        "id" : 117,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmdb",
        "targetName" : "afma"
      }, {
        "id" : 118,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "afma",
        "targetName" : "afmsmdb"
      }, {
        "id" : 119,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmsmdb",
        "targetName" : "afma"
      }, {
        "id" : 120,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "sma",
        "targetName" : "afmsmdb"
      }, {
        "id" : 121,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmsmdb",
        "targetName" : "sma"
      }, {
        "id" : 122,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "smd",
        "targetName" : "afmsmdb"
      }, {
        "id" : 123,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmsmdb",
        "targetName" : "smd"
      }, {
        "id" : 124,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "afma",
        "targetName" : "afmsmdb"
      }, {
        "id" : 125,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "afmsmdb",
        "targetName" : "afma"
      } ],
      "services" : [ {
        "name" : "fdb",
        "description" : "fdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pgww",
        "description" : "pgww",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afmdb",
        "description" : "afmdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "d",
        "description" : "d",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hdb",
        "description" : "hdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pcjms",
        "description" : "pcjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "co",
        "description" : "co",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "tryt",
        "description" : "tryt",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "tc",
        "description" : "tc",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cs",
        "description" : "cs",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afmjms",
        "description" : "afmjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pgwm",
        "description" : "pgwm",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cka",
        "description" : "cka",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hd",
        "description" : "hd",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "csdb",
        "description" : "csdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "findb",
        "description" : "findb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fjms",
        "description" : "fjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "codb",
        "description" : "codb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pgwdb",
        "description" : "pgwdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cow",
        "description" : "cow",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afmad",
        "description" : "afmad",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hw",
        "description" : "hw",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "bap",
        "description" : "bap",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afma",
        "description" : "afma",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "bapi",
        "description" : "bapi",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "copu",
        "description" : "copu",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "epm",
        "description" : "epm",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pcad",
        "description" : "pcad",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cjms",
        "description" : "cjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cpmjms",
        "description" : "cpmjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fnk",
        "description" : "fnk",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "ms",
        "description" : "ms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "adm",
        "description" : "adm",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cckdb",
        "description" : "cckdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "finjms",
        "description" : "finjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cpmd",
        "description" : "cpmd",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "ew",
        "description" : "ew",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "mlr",
        "description" : "mlr",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cpmw",
        "description" : "cpmw",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "npaesb",
        "description" : "npaesb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fc",
        "description" : "fc",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "afmsmdb",
        "description" : "afmsmdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hsm",
        "description" : "hsm",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "np",
        "description" : "np",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pcdb",
        "description" : "pcdb",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "sma",
        "description" : "sma",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fo",
        "description" : "fo",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "smd",
        "description" : "smd",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "pca",
        "description" : "pca",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fw",
        "description" : "fw",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cas",
        "description" : "cas",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "hjms",
        "description" : "hjms",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "cifw",
        "description" : "cifw",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "fxr",
        "description" : "fxr",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "tscn",
        "description" : "tscn",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "npa",
        "description" : "npa",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ]
    },
    "name" : "large-example.elm",
    "machines" : [ {
      "name" : "12"
    }, {
      "name" : "34"
    }, {
      "name" : "35"
    }, {
      "name" : "13"
    }, {
      "name" : "14"
    }, {
      "name" : "15"
    }, {
      "name" : "16"
    }, {
      "name" : "17"
    }, {
      "name" : "18"
    }, {
      "name" : "19"
    }, {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    }, {
      "name" : "5"
    }, {
      "name" : "6"
    }, {
      "name" : "7"
    }, {
      "name" : "8"
    }, {
      "name" : "9"
    }, {
      "name" : "20"
    }, {
      "name" : "21"
    }, {
      "name" : "22"
    }, {
      "name" : "23"
    }, {
      "name" : "24"
    }, {
      "name" : "25"
    }, {
      "name" : "26"
    }, {
      "name" : "27"
    }, {
      "name" : "28"
    }, {
      "name" : "29"
    }, {
      "name" : "30"
    }, {
      "name" : "31"
    }, {
      "name" : "10"
    }, {
      "name" : "32"
    }, {
      "name" : "33"
    }, {
      "name" : "11"
    } ],
    "serviceMachines" : [ {
      "name" : "npaesb_1",
      "serviceName" : "npaesb",
      "machineName" : "30",
      "statefulness" : "STATELESS"
    }, {
      "name" : "codb_1",
      "serviceName" : "codb",
      "machineName" : "18",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pcdb_1",
      "serviceName" : "pcdb",
      "machineName" : "25",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afmsmdb_1",
      "serviceName" : "afmsmdb",
      "machineName" : "35",
      "statefulness" : "STATELESS"
    }, {
      "name" : "tscn_1",
      "serviceName" : "tscn",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "bapi_1",
      "serviceName" : "bapi",
      "machineName" : "5",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hw_1",
      "serviceName" : "hw",
      "machineName" : "12",
      "statefulness" : "STATELESS"
    }, {
      "name" : "co_1",
      "serviceName" : "co",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cpmjms_1",
      "serviceName" : "cpmjms",
      "machineName" : "20",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pgwdb_1",
      "serviceName" : "pgwdb",
      "machineName" : "14",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pcad_1",
      "serviceName" : "pcad",
      "machineName" : "26",
      "statefulness" : "STATELESS"
    }, {
      "name" : "smd_1",
      "serviceName" : "smd",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pgwm_1",
      "serviceName" : "pgwm",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "np_1",
      "serviceName" : "np",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cpmw_1",
      "serviceName" : "cpmw",
      "machineName" : "11",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cjms_1",
      "serviceName" : "cjms",
      "machineName" : "15",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pgww_1",
      "serviceName" : "pgww",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fjms_1",
      "serviceName" : "fjms",
      "machineName" : "16",
      "statefulness" : "STATELESS"
    }, {
      "name" : "ew_1",
      "serviceName" : "ew",
      "machineName" : "5",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cifw_1",
      "serviceName" : "cifw",
      "machineName" : "10",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cka_1",
      "serviceName" : "cka",
      "machineName" : "11",
      "statefulness" : "STATELESS"
    }, {
      "name" : "bap_1",
      "serviceName" : "bap",
      "machineName" : "4",
      "statefulness" : "STATELESS"
    }, {
      "name" : "epm_1",
      "serviceName" : "epm",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "npa_1",
      "serviceName" : "npa",
      "machineName" : "31",
      "statefulness" : "STATELESS"
    }, {
      "name" : "d_1",
      "serviceName" : "d",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "tc_1",
      "serviceName" : "tc",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cow_1",
      "serviceName" : "cow",
      "machineName" : "10",
      "statefulness" : "STATELESS"
    }, {
      "name" : "copu_1",
      "serviceName" : "copu",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "ms_1",
      "serviceName" : "ms",
      "machineName" : "1",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cas_1",
      "serviceName" : "cas",
      "machineName" : "30",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pca_1",
      "serviceName" : "pca",
      "machineName" : "26",
      "statefulness" : "STATELESS"
    }, {
      "name" : "csdb_1",
      "serviceName" : "csdb",
      "machineName" : "7",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afmdb_1",
      "serviceName" : "afmdb",
      "machineName" : "34",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cckdb_1",
      "serviceName" : "cckdb",
      "machineName" : "19",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fw_1",
      "serviceName" : "fw",
      "machineName" : "9",
      "statefulness" : "STATELESS"
    }, {
      "name" : "finjms_1",
      "serviceName" : "finjms",
      "machineName" : "27",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cpmd_1",
      "serviceName" : "cpmd",
      "machineName" : "11",
      "statefulness" : "STATELESS"
    }, {
      "name" : "tryt_1",
      "serviceName" : "tryt",
      "machineName" : "28",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afma_1",
      "serviceName" : "afma",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "sma_1",
      "serviceName" : "sma",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hdb_1",
      "serviceName" : "hdb",
      "machineName" : "23",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fc_1",
      "serviceName" : "fc",
      "machineName" : "9",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fnk_1",
      "serviceName" : "fnk",
      "machineName" : "28",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afmad_1",
      "serviceName" : "afmad",
      "machineName" : "33",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fxr_1",
      "serviceName" : "fxr",
      "machineName" : "6",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fo_1",
      "serviceName" : "fo",
      "machineName" : "9",
      "statefulness" : "STATELESS"
    }, {
      "name" : "cs_1",
      "serviceName" : "cs",
      "machineName" : "6",
      "statefulness" : "STATELESS"
    }, {
      "name" : "afmjms_1",
      "serviceName" : "afmjms",
      "machineName" : "32",
      "statefulness" : "STATELESS"
    }, {
      "name" : "mlr_1",
      "serviceName" : "mlr",
      "machineName" : "13",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hjms_1",
      "serviceName" : "hjms",
      "machineName" : "22",
      "statefulness" : "STATELESS"
    }, {
      "name" : "fdb_1",
      "serviceName" : "fdb",
      "machineName" : "17",
      "statefulness" : "STATELESS"
    }, {
      "name" : "pcjms_1",
      "serviceName" : "pcjms",
      "machineName" : "24",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hsm_1",
      "serviceName" : "hsm",
      "machineName" : "21",
      "statefulness" : "STATELESS"
    }, {
      "name" : "adm_1",
      "serviceName" : "adm",
      "machineName" : "8",
      "statefulness" : "STATELESS"
    }, {
      "name" : "hd_1",
      "serviceName" : "hd",
      "machineName" : "12",
      "statefulness" : "STATELESS"
    }, {
      "name" : "findb_1",
      "serviceName" : "findb",
      "machineName" : "29",
      "statefulness" : "STATELESS"
    } ],
    "connections" : [ {
      "optionality" : "COMPULSORY",
      "callId" : 84,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hw_1",
      "targetName" : "hdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 7,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "np_1",
      "targetName" : "fw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 115,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "afmdb_1",
      "targetName" : "sma_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 15,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cifw_1",
      "targetName" : "co_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 97,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pcjms_1",
      "targetName" : "pcdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 122,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "smd_1",
      "targetName" : "afmsmdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 83,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "mlr_1",
      "targetName" : "hd_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 86,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hd_1",
      "targetName" : "hdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 60,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "d_1",
      "targetName" : "cow_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 111,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "afmjms_1",
      "targetName" : "afmdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 11,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cow_1",
      "targetName" : "copu_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 61,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "adm_1",
      "targetName" : "cow_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 53,
      "callType" : "PUSHES_TO",
      "local" : true,
      "sourceName" : "cifw_1",
      "targetName" : "cow_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 94,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cjms_1",
      "targetName" : "afmjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 26,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cs_1",
      "targetName" : "pgwdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 24,
      "callType" : "PULLED_FROM",
      "local" : true,
      "sourceName" : "cs_1",
      "targetName" : "fxr_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 76,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cckdb_1",
      "targetName" : "cka_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 70,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "hsm_1",
      "targetName" : "cka_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 91,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cjms_1",
      "targetName" : "hjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 72,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cckdb_1",
      "targetName" : "cpmw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 62,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "npaesb_1",
      "targetName" : "adm_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 47,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "fo_1",
      "targetName" : "fdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 73,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cpmd_1",
      "targetName" : "cckdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 98,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pca_1",
      "targetName" : "pcdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 54,
      "callType" : "PUSHES_TO",
      "local" : true,
      "sourceName" : "cow_1",
      "targetName" : "cifw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 58,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "codb_1",
      "targetName" : "cow_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 41,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pgwdb_1",
      "targetName" : "epm_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 34,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pgww_1",
      "targetName" : "cjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 19,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "bap_1",
      "targetName" : "cpmw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 71,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cpmw_1",
      "targetName" : "cckdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 116,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "afma_1",
      "targetName" : "afmdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 95,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cpmjms_1",
      "targetName" : "cckdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 92,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hjms_1",
      "targetName" : "cjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 87,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "hdb_1",
      "targetName" : "hd_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 14,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "co_1",
      "targetName" : "cifw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 50,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "fdb_1",
      "targetName" : "fw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 9,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "fo_1",
      "targetName" : "tscn_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 82,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "hjms_1",
      "targetName" : "hd_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 67,
      "callType" : "PUSHES_TO",
      "local" : true,
      "sourceName" : "cpmw_1",
      "targetName" : "cka_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 30,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pgwm_1",
      "targetName" : "pgwdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 90,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cjms_1",
      "targetName" : "fjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 108,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "afmjms_1",
      "targetName" : "afma_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 48,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "fdb_1",
      "targetName" : "fo_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 66,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cpmjms_1",
      "targetName" : "cpmw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 3,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "ms_1",
      "targetName" : "cifw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 106,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "finjms_1",
      "targetName" : "findb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 99,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pcdb_1",
      "targetName" : "pca_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 55,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cifw_1",
      "targetName" : "codb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 23,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hd_1",
      "targetName" : "bapi_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 38,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cjms_1",
      "targetName" : "tc_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 49,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "fw_1",
      "targetName" : "fdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 69,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cka_1",
      "targetName" : "hsm_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 33,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pgwdb_1",
      "targetName" : "pgww_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 101,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pcdb_1",
      "targetName" : "pcad_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 1,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "ms_1",
      "targetName" : "pgwm_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 107,
      "callType" : "PUSHES_TO",
      "local" : true,
      "sourceName" : "tryt_1",
      "targetName" : "fnk_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 21,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "hw_1",
      "targetName" : "ew_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 112,
      "callType" : "PUSHES_TO",
      "local" : true,
      "sourceName" : "npaesb_1",
      "targetName" : "cas_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 6,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pgwm_1",
      "targetName" : "np_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 25,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "csdb_1",
      "targetName" : "cs_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 59,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cow_1",
      "targetName" : "pgww_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 18,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cpmw_1",
      "targetName" : "bap_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 105,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "finjms_1",
      "targetName" : "fnk_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 74,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cckdb_1",
      "targetName" : "cpmd_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 110,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "afmjms_1",
      "targetName" : "sma_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 12,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "co_1",
      "targetName" : "cow_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 22,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hd_1",
      "targetName" : "ew_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 89,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "fjms_1",
      "targetName" : "cjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 13,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cow_1",
      "targetName" : "co_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 37,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "tc_1",
      "targetName" : "cjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 52,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "fdb_1",
      "targetName" : "fc_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 96,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hjms_1",
      "targetName" : "hdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 118,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "afma_1",
      "targetName" : "afmsmdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 17,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cpmw_1",
      "targetName" : "co_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 28,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "fxr_1",
      "targetName" : "fw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 57,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cow_1",
      "targetName" : "codb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 113,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "npaesb_1",
      "targetName" : "afma_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 103,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pca_1",
      "targetName" : "pcjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 78,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "tc_1",
      "targetName" : "hw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 56,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "codb_1",
      "targetName" : "cifw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 27,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pgwdb_1",
      "targetName" : "cs_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 2,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pgwm_1",
      "targetName" : "ms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 65,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cpmw_1",
      "targetName" : "cpmjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 68,
      "callType" : "PUSHES_TO",
      "local" : true,
      "sourceName" : "cpmd_1",
      "targetName" : "cka_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 102,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pcjms_1",
      "targetName" : "pca_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 119,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "afmsmdb_1",
      "targetName" : "afma_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 32,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pgww_1",
      "targetName" : "pgwdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 121,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "afmsmdb_1",
      "targetName" : "sma_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 42,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "fw_1",
      "targetName" : "fjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 77,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hw_1",
      "targetName" : "cpmw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 63,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cifw_1",
      "targetName" : "cpmw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 39,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "tc_1",
      "targetName" : "pgwdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 8,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "fw_1",
      "targetName" : "np_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 46,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "fjms_1",
      "targetName" : "fo_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 114,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "sma_1",
      "targetName" : "afmdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 20,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "ew_1",
      "targetName" : "hw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 45,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "fjms_1",
      "targetName" : "fw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 100,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pcad_1",
      "targetName" : "pcdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 4,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "cifw_1",
      "targetName" : "ms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 85,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "hdb_1",
      "targetName" : "hw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 80,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "hjms_1",
      "targetName" : "hw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 88,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cjms_1",
      "targetName" : "finjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 75,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cka_1",
      "targetName" : "cckdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 10,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "copu_1",
      "targetName" : "cow_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 104,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "finjms_1",
      "targetName" : "tryt_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 81,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hd_1",
      "targetName" : "hjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 117,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "afmdb_1",
      "targetName" : "afma_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 64,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cpmw_1",
      "targetName" : "cifw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 79,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "hw_1",
      "targetName" : "hjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 5,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "np_1",
      "targetName" : "pgwm_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 43,
      "callType" : "PUSHES_TO",
      "local" : true,
      "sourceName" : "fw_1",
      "targetName" : "fc_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 36,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "npaesb_1",
      "targetName" : "epm_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 40,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pgwdb_1",
      "targetName" : "tc_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 123,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "afmsmdb_1",
      "targetName" : "smd_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 51,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "fc_1",
      "targetName" : "fdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 44,
      "callType" : "PUSHES_TO",
      "local" : true,
      "sourceName" : "fo_1",
      "targetName" : "fc_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 29,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "pgwm_1",
      "targetName" : "cjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 31,
      "callType" : "PULLED_FROM",
      "local" : false,
      "sourceName" : "pgwdb_1",
      "targetName" : "pgwm_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 16,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "co_1",
      "targetName" : "cpmw_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 35,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "cjms_1",
      "targetName" : "d_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 93,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "afmjms_1",
      "targetName" : "cjms_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 120,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "sma_1",
      "targetName" : "afmsmdb_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 109,
      "callType" : "PUSHES_TO",
      "local" : false,
      "sourceName" : "afma_1",
      "targetName" : "afmjms_1"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "tc",
      "location" : {
        "x" : 204.55154521221067,
        "y" : 131.73032772252859
      }
    }, {
      "name" : "finjms",
      "location" : {
        "x" : 33.639166831818784,
        "y" : 333.14996428214863
      }
    }, {
      "name" : "co",
      "location" : {
        "x" : 430.6761075880869,
        "y" : 42.20104415397476
      }
    }, {
      "name" : "codb",
      "location" : {
        "x" : 444.9100856861583,
        "y" : 17.315534583734536
      }
    }, {
      "name" : "cpmjms",
      "location" : {
        "x" : 476.4963803607447,
        "y" : 225.80014761106142
      }
    }, {
      "name" : "pca",
      "location" : {
        "x" : 716.1338570758209,
        "y" : 372.9132509019893
      }
    }, {
      "name" : "hsm",
      "location" : {
        "x" : 678.7925781422881,
        "y" : 129.84237414188019
      }
    }, {
      "name" : "cjms",
      "location" : {
        "x" : 121.04777826205589,
        "y" : 269.98018044529
      }
    }, {
      "name" : "adm",
      "location" : {
        "x" : 427.4735569476159,
        "y" : 169.36073028148184
      }
    }, {
      "name" : "fc",
      "location" : {
        "x" : 172.44397017794364,
        "y" : 124.53152041772506
      }
    }, {
      "name" : "npa",
      "location" : {
        "x" : 204.24074233732148,
        "y" : 453.6722013762072
      }
    }, {
      "name" : "findb",
      "location" : {
        "x" : 74.60581291884935,
        "y" : 459.1351609766266
      }
    }, {
      "name" : "ms",
      "location" : {
        "x" : 316.01472353155987,
        "y" : 28.192618485980837
      }
    }, {
      "name" : "pcad",
      "location" : {
        "x" : 835.9107759356759,
        "y" : 291.4440100193495
      }
    }, {
      "name" : "fjms",
      "location" : {
        "x" : 148.16978875714634,
        "y" : 95.17136677596056
      }
    }, {
      "name" : "fxr",
      "location" : {
        "x" : 211.12182758848635,
        "y" : 356.09312378339354
      }
    }, {
      "name" : "csdb",
      "location" : {
        "x" : 414.29553639422727,
        "y" : 416.34177342576834
      }
    }, {
      "name" : "epm",
      "location" : {
        "x" : 400.9275901711417,
        "y" : 172.7538453764423
      }
    }, {
      "name" : "mlr",
      "location" : {
        "x" : 787.0612568810802,
        "y" : 82.19610282164024
      }
    }, {
      "name" : "ew",
      "location" : {
        "x" : 257.2644563272064,
        "y" : 220.02961150976603
      }
    }, {
      "name" : "hw",
      "location" : {
        "x" : 235.01958211231567,
        "y" : 157.0100166758352
      }
    }, {
      "name" : "afmsmdb",
      "location" : {
        "x" : 550.6172745811086,
        "y" : 450.589496351173
      }
    }, {
      "name" : "fw",
      "location" : {
        "x" : 215.80494653348043,
        "y" : 14.67687397211921
      }
    }, {
      "name" : "hjms",
      "location" : {
        "x" : 550.7635871191292,
        "y" : 153.94581281698805
      }
    }, {
      "name" : "fnk",
      "location" : {
        "x" : 25.159114018545665,
        "y" : 423.1690143410808
      }
    }, {
      "name" : "cs",
      "location" : {
        "x" : 117.8773311682148,
        "y" : 51.1712273458612
      }
    }, {
      "name" : "sma",
      "location" : {
        "x" : 540.682921739284,
        "y" : 396.24793348263086
      }
    }, {
      "name" : "tryt",
      "location" : {
        "x" : 82.32636245823117,
        "y" : 369.72795721658895
      }
    }, {
      "name" : "d",
      "location" : {
        "x" : 118.82148187259116,
        "y" : 180.86704662664306
      }
    }, {
      "name" : "copu",
      "location" : {
        "x" : 542.4377660961109,
        "y" : -9.021837456320199
      }
    }, {
      "name" : "afma",
      "location" : {
        "x" : 512.6887498330864,
        "y" : 305.9641178948709
      }
    }, {
      "name" : "bap",
      "location" : {
        "x" : 358.3086293728053,
        "y" : 19.748268800601366
      }
    }, {
      "name" : "pgwdb",
      "location" : {
        "x" : 213.23768301618625,
        "y" : 88.32162808842486
      }
    }, {
      "name" : "fdb",
      "location" : {
        "x" : 144.86585448076278,
        "y" : 120.04109848215242
      }
    }, {
      "name" : "afmad",
      "location" : {
        "x" : 309.6186105855314,
        "y" : 368.83453861789513
      }
    }, {
      "name" : "smd",
      "location" : {
        "x" : 536.8707405901265,
        "y" : 342.415172447662
      }
    }, {
      "name" : "afmdb",
      "location" : {
        "x" : 594.2329457329602,
        "y" : 372.4539549308846
      }
    }, {
      "name" : "npaesb",
      "location" : {
        "x" : 200.796637629094,
        "y" : 406.46895767715637
      }
    }, {
      "name" : "afmjms",
      "location" : {
        "x" : 336.6565333957195,
        "y" : 249.05616136616104
      }
    }, {
      "name" : "cpmw",
      "location" : {
        "x" : 398.2941418787524,
        "y" : 49.765859047109494
      }
    }, {
      "name" : "hd",
      "location" : {
        "x" : 552.0074378477523,
        "y" : 98.86513546606767
      }
    }, {
      "name" : "cpmd",
      "location" : {
        "x" : 621.5980920843981,
        "y" : 184.06377592807723
      }
    }, {
      "name" : "pgwm",
      "location" : {
        "x" : 241.5973900569565,
        "y" : 52.210381574570626
      }
    }, {
      "name" : "np",
      "location" : {
        "x" : 144.4346260423305,
        "y" : -54.3488003654096
      }
    }, {
      "name" : "cas",
      "location" : {
        "x" : 324.19135499470167,
        "y" : 286.951128719346
      }
    }, {
      "name" : "pcdb",
      "location" : {
        "x" : 587.6054639288674,
        "y" : 253.71041711673803
      }
    }, {
      "name" : "tscn",
      "location" : {
        "x" : 14.827742233927175,
        "y" : 135.3107424709646
      }
    }, {
      "name" : "hdb",
      "location" : {
        "x" : 555.3083321551055,
        "y" : 58.96883859205471
      }
    }, {
      "name" : "cka",
      "location" : {
        "x" : 632.3002601267252,
        "y" : 30.318598978127056
      }
    }, {
      "name" : "pgww",
      "location" : {
        "x" : 320.72011093234687,
        "y" : 118.69568965268608
      }
    }, {
      "name" : "cifw",
      "location" : {
        "x" : 391.9162997580106,
        "y" : 20.471689083310725
      }
    }, {
      "name" : "cow",
      "location" : {
        "x" : 406.7758430647994,
        "y" : 79.54942183213157
      }
    }, {
      "name" : "cckdb",
      "location" : {
        "x" : 611.130459816326,
        "y" : 214.57814875852722
      }
    }, {
      "name" : "fo",
      "location" : {
        "x" : 62.00248364628308,
        "y" : 52.75692372957795
      }
    }, {
      "name" : "bapi",
      "location" : {
        "x" : 448.993893105958,
        "y" : -48.74965015771289
      }
    }, {
      "name" : "pcjms",
      "location" : {
        "x" : 709.2365494067298,
        "y" : 236.10077316444801
      }
    } ],
    "serviceMachines" : [ {
      "name" : "fnk_1",
      "location" : {
        "x" : 160.72925318464422,
        "y" : 626.0509477384007
      }
    }, {
      "name" : "cckdb_1",
      "location" : {
        "x" : 499.8565211614812,
        "y" : 476.5961695522525
      }
    }, {
      "name" : "pgwm_1",
      "location" : {
        "x" : 255.2014748268939,
        "y" : 299.3798285243379
      }
    }, {
      "name" : "co_1",
      "location" : {
        "x" : 265.0374223158651,
        "y" : 258.1104203523122
      }
    }, {
      "name" : "tscn_1",
      "location" : {
        "x" : 227.87567818854995,
        "y" : 129.7810453271103
      }
    }, {
      "name" : "cjms_1",
      "location" : {
        "x" : 850.8899669924033,
        "y" : 479.7893562077168
      }
    }, {
      "name" : "hjms_1",
      "location" : {
        "x" : 699.0141441935218,
        "y" : 289.71329915382006
      }
    }, {
      "name" : "hsm_1",
      "location" : {
        "x" : 1033.9729714316072,
        "y" : 416.1983198825729
      }
    }, {
      "name" : "afma_1",
      "location" : {
        "x" : 664.5570310883743,
        "y" : 487.9275536532096
      }
    }, {
      "name" : "cow_1",
      "location" : {
        "x" : 406.7170197349268,
        "y" : 119.8759584945949
      }
    }, {
      "name" : "cs_1",
      "location" : {
        "x" : 49.14017568084455,
        "y" : 373.538156344422
      }
    }, {
      "name" : "hd_1",
      "location" : {
        "x" : 707.7123411854338,
        "y" : 192.94904659395846
      }
    }, {
      "name" : "fo_1",
      "location" : {
        "x" : 72.20751017773802,
        "y" : 233.57001395728417
      }
    }, {
      "name" : "pcjms_1",
      "location" : {
        "x" : 1005.6958550523846,
        "y" : 152.5629436627495
      }
    }, {
      "name" : "afmjms_1",
      "location" : {
        "x" : 851.0198585230327,
        "y" : 673.548023595115
      }
    }, {
      "name" : "fxr_1",
      "location" : {
        "x" : 46.67979018712646,
        "y" : 333.28311265914414
      }
    }, {
      "name" : "tryt_1",
      "location" : {
        "x" : 165.74343257369043,
        "y" : 579.2919866235613
      }
    }, {
      "name" : "mlr_1",
      "location" : {
        "x" : 975.0633569704082,
        "y" : 281.79526814729644
      }
    }, {
      "name" : "pgww_1",
      "location" : {
        "x" : 360.30915998658253,
        "y" : 292.55714045208236
      }
    }, {
      "name" : "afmsmdb_1",
      "location" : {
        "x" : 564.8592191829566,
        "y" : 684.8036721806593
      }
    }, {
      "name" : "codb_1",
      "location" : {
        "x" : 495.5743593452075,
        "y" : 305.11081797703997
      }
    }, {
      "name" : "np_1",
      "location" : {
        "x" : 223.87556244876805,
        "y" : 90.13341907062129
      }
    }, {
      "name" : "ew_1",
      "location" : {
        "x" : 686.5600184015882,
        "y" : 35.44067945986687
      }
    }, {
      "name" : "epm_1",
      "location" : {
        "x" : 265.8863404763896,
        "y" : 349.5016133381277
      }
    }, {
      "name" : "cas_1",
      "location" : {
        "x" : 388.9577662214437,
        "y" : 574.8554842213757
      }
    }, {
      "name" : "npa_1",
      "location" : {
        "x" : 45.76186524388927,
        "y" : 47.0306777970214
      }
    }, {
      "name" : "pca_1",
      "location" : {
        "x" : 857.6695980467852,
        "y" : 141.4990784104876
      }
    }, {
      "name" : "findb_1",
      "location" : {
        "x" : 197.54641036326137,
        "y" : 729.1648999696904
      }
    }, {
      "name" : "finjms_1",
      "location" : {
        "x" : 88.31443402580157,
        "y" : 674.8995455002675
      }
    }, {
      "name" : "copu_1",
      "location" : {
        "x" : 223.20954054156664,
        "y" : 49.94882331019494
      }
    }, {
      "name" : "pgwdb_1",
      "location" : {
        "x" : 301.90223493484825,
        "y" : 435.74021719759793
      }
    }, {
      "name" : "afmdb_1",
      "location" : {
        "x" : 856.2769083034249,
        "y" : 576.9050303896827
      }
    }, {
      "name" : "hdb_1",
      "location" : {
        "x" : 838.1880962286718,
        "y" : 378.18820124669116
      }
    }, {
      "name" : "pcad_1",
      "location" : {
        "x" : 854.1099472946875,
        "y" : 49.51978576433051
      }
    }, {
      "name" : "fdb_1",
      "location" : {
        "x" : 238.73998256303776,
        "y" : 504.87613841315374
      }
    }, {
      "name" : "cka_1",
      "location" : {
        "x" : 568.4471500718801,
        "y" : 258.3694030026913
      }
    }, {
      "name" : "cpmw_1",
      "location" : {
        "x" : 582.3105279931797,
        "y" : 225.05930567556902
      }
    }, {
      "name" : "smd_1",
      "location" : {
        "x" : 668.6002350628592,
        "y" : 521.1700643705578
      }
    }, {
      "name" : "bap_1",
      "location" : {
        "x" : 510.23313693088426,
        "y" : 49.58650532252719
      }
    }, {
      "name" : "afmad_1",
      "location" : {
        "x" : 667.9183291818807,
        "y" : 552.5499547590184
      }
    }, {
      "name" : "bapi_1",
      "location" : {
        "x" : 621.4920146550608,
        "y" : 36.95366689618456
      }
    }, {
      "name" : "d_1",
      "location" : {
        "x" : 365.7909918858877,
        "y" : 255.13455215277855
      }
    }, {
      "name" : "adm_1",
      "location" : {
        "x" : 376.1636057971126,
        "y" : 369.14804228452846
      }
    }, {
      "name" : "sma_1",
      "location" : {
        "x" : 668.3295348115405,
        "y" : 588.1615934892503
      }
    }, {
      "name" : "csdb_1",
      "location" : {
        "x" : 98.69493865651708,
        "y" : 428.13096371549193
      }
    }, {
      "name" : "cpmd_1",
      "location" : {
        "x" : 578.6857576337286,
        "y" : 185.14875343252604
      }
    }, {
      "name" : "tc_1",
      "location" : {
        "x" : 329.37463971886933,
        "y" : 332.9516409631078
      }
    }, {
      "name" : "pcdb_1",
      "location" : {
        "x" : 1009.047372771412,
        "y" : 60.144904295033996
      }
    }, {
      "name" : "cpmjms_1",
      "location" : {
        "x" : 348.64416205814155,
        "y" : 508.11326405896307
      }
    }, {
      "name" : "fjms_1",
      "location" : {
        "x" : 568.5110889941703,
        "y" : 372.24172612489485
      }
    }, {
      "name" : "fc_1",
      "location" : {
        "x" : 73.20103620464153,
        "y" : 195.699647860754
      }
    }, {
      "name" : "fw_1",
      "location" : {
        "x" : 71.6605418358964,
        "y" : 152.19165066043055
      }
    }, {
      "name" : "npaesb_1",
      "location" : {
        "x" : 495.3914918028814,
        "y" : 571.8700636208039
      }
    }, {
      "name" : "ms_1",
      "location" : {
        "x" : 368.0619293646722,
        "y" : 669.9729883253465
      }
    }, {
      "name" : "hw_1",
      "location" : {
        "x" : 716.3950944426308,
        "y" : 148.0874250405563
      }
    }, {
      "name" : "cifw_1",
      "location" : {
        "x" : 404.9625367594483,
        "y" : 163.20725426069245
      }
    } ]
  }
}