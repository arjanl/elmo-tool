{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "gui",
        "description" : "gui",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      }, {
        "name" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "",
        "sourceName" : "gui",
        "targetName" : "db"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "",
        "sourceName" : "db",
        "targetName" : "gui"
      } ]
    },
    "name" : "deployments2.elm",
    "serviceMachines" : [ {
      "name" : "db_1",
      "statefulness" : "STATEFUL",
      "serviceName" : "db",
      "machineName" : "2"
    }, {
      "name" : "gui_1",
      "statefulness" : "STATEFUL",
      "serviceName" : "gui",
      "machineName" : "1"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callType" : "PUSHES_TO",
      "sourceName" : "gui_1",
      "targetName" : "db_1",
      "callId" : 1
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "local" : false,
      "callType" : "PULLED_FROM",
      "sourceName" : "db_1",
      "targetName" : "gui_1",
      "callId" : 2
    } ],
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "db",
      "location" : {
        "x" : 94.56068447807445,
        "y" : 157.79586839394022
      }
    }, {
      "name" : "gui",
      "location" : {
        "x" : 97.203736606289,
        "y" : 67.98422141586866
      }
    } ],
    "serviceMachines" : [ {
      "name" : "gui_1",
      "location" : {
        "x" : 90.459231532417,
        "y" : 78.26064860989678
      }
    }, {
      "name" : "db_1",
      "location" : {
        "x" : 91.42989900508553,
        "y" : 208.050898810229
      }
    } ]
  }
}