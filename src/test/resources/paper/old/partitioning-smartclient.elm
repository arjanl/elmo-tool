G: {STATELESS}
U: {PARTITIONED:user}
D: {PARTITIONED:user}

U >- G
G -> U
U -> D
D >- U
        
1 = {G}
2 = {G}
3 = {U,D}
4 = {U,D}
