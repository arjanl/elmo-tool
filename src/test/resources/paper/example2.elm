{
  "deployment" : {
    "architecture" : {
      "services" : [ {
        "name" : "acc",
        "originalName" : "acc",
        "description" : "acc",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ "order" ]
      }, {
        "name" : "price",
        "originalName" : "price",
        "description" : "price",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "price" ],
        "consumes" : [ "prod" ]
      }, {
        "name" : "office",
        "originalName" : "office",
        "description" : "office",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "prod" ],
        "consumes" : [ ]
      }, {
        "name" : "chkout",
        "originalName" : "chkout",
        "description" : "chkout",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "order" ],
        "consumes" : [ "prod", "price" ]
      }, {
        "name" : "db",
        "originalName" : "db",
        "description" : "db",
        "statefulness" : "STATEFUL",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ ]
      } ],
      "calls" : [ {
        "id" : 1,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "prod",
        "sourceName" : "db",
        "targetName" : "price"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "prod",
        "sourceName" : "db",
        "targetName" : "chkout"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "order",
        "sourceName" : "chkout",
        "targetName" : "db"
      }, {
        "id" : 4,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "price",
        "sourceName" : "price",
        "targetName" : "chkout"
      }, {
        "id" : 5,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "prod",
        "sourceName" : "office",
        "targetName" : "db"
      }, {
        "id" : 6,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "order",
        "sourceName" : "db",
        "targetName" : "acc"
      } ]
    },
    "name" : "example2.elm",
    "serviceMachines" : [ {
      "name" : "db_1",
      "index" : 1,
      "statefulness" : "STATEFUL",
      "serviceName" : "db",
      "machineName" : "1"
    }, {
      "name" : "price_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "price",
      "machineName" : "3"
    }, {
      "name" : "price_2",
      "index" : 2,
      "statefulness" : "STATELESS",
      "serviceName" : "price",
      "machineName" : "2"
    }, {
      "name" : "acc_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "acc",
      "machineName" : "5"
    }, {
      "name" : "chkout_2",
      "index" : 2,
      "statefulness" : "STATELESS",
      "serviceName" : "chkout",
      "machineName" : "2"
    }, {
      "name" : "office_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "office",
      "machineName" : "4"
    }, {
      "name" : "chkout_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "chkout",
      "machineName" : "3"
    } ],
    "machines" : [ {
      "name" : "1"
    }, {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    }, {
      "name" : "5"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 6,
      "sourceName" : "db_1",
      "targetName" : "acc_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "sourceName" : "db_1",
      "targetName" : "chkout_2"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "sourceName" : "db_1",
      "targetName" : "price_2"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 1,
      "sourceName" : "db_1",
      "targetName" : "price_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "sourceName" : "chkout_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 3,
      "sourceName" : "chkout_2",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 5,
      "sourceName" : "office_1",
      "targetName" : "db_1"
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "callId" : 2,
      "sourceName" : "db_1",
      "targetName" : "chkout_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 4,
      "sourceName" : "price_1",
      "targetName" : "chkout_1"
    }, {
      "optionality" : "COMPULSORY",
      "callId" : 4,
      "sourceName" : "price_2",
      "targetName" : "chkout_2"
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "chkout",
      "location" : {
        "x" : 326.4160129876096,
        "y" : 50.633123984079816
      }
    }, {
      "name" : "db",
      "location" : {
        "x" : 223.94750615343102,
        "y" : 134.2525978743147
      }
    }, {
      "name" : "acc",
      "location" : {
        "x" : 165.99935001458618,
        "y" : 293.955801251749
      }
    }, {
      "name" : "office",
      "location" : {
        "x" : 359.72697761064603,
        "y" : 297.56905945578853
      }
    }, {
      "name" : "price",
      "location" : {
        "x" : 136.08250285586496,
        "y" : 57.96221159862955
      }
    } ],
    "serviceMachines" : [ {
      "name" : "chkout_1",
      "location" : {
        "x" : 105.65735989266807,
        "y" : 77.64958095569267
      }
    }, {
      "name" : "price_2",
      "location" : {
        "x" : 469.7120117769304,
        "y" : 26.648514600704885
      }
    }, {
      "name" : "db_1",
      "location" : {
        "x" : 290.0212705228853,
        "y" : 170.99000039020433
      }
    }, {
      "name" : "price_1",
      "location" : {
        "x" : 109.37799935893935,
        "y" : 28.21863602809165
      }
    }, {
      "name" : "acc_1",
      "location" : {
        "x" : 147.25949753324667,
        "y" : 276.8705163656367
      }
    }, {
      "name" : "chkout_2",
      "location" : {
        "x" : 468.7059359470959,
        "y" : 80.67192419347819
      }
    }, {
      "name" : "office_1",
      "location" : {
        "x" : 466.0222064399358,
        "y" : 276.6737669923572
      }
    } ]
  }
}