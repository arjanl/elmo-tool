{
  "deployment" : {
    "architecture" : {
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "alpha",
        "sourceName" : "a",
        "targetName" : "d"
      }, {
        "id" : 2,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "alpha",
        "sourceName" : "b",
        "targetName" : "d"
      }, {
        "id" : 3,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "alpha",
        "sourceName" : "c",
        "targetName" : "d"
      }, {
        "id" : 4,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "alpha",
        "sourceName" : "d",
        "targetName" : "e"
      } ],
      "services" : [ {
        "name" : "a",
        "originalName" : "a",
        "description" : "a",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "alpha" ],
        "consumes" : [  ]
      }, {
        "name" : "b",
        "originalName" : "b",
        "description" : "b",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "alpha" ],
        "consumes" : [  ]
      }, {
        "name" : "c",
        "originalName" : "c",
        "description" : "c",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "alpha"],
        "consumes" : [  ]
      }, {
        "name" : "d",
        "originalName" : "d",
        "description" : "d",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [],
        "consumes" : ["alpha" ]
      }, {
        "name" : "e",
        "originalName" : "e",
        "description" : "e",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : ["alpha" ]
      } ]
    },
    "name" : "fig10.elm",
    "serviceMachines" : [ {
      "name" : "b_1",
      "index" : 1,
      "serviceName" : "b",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "a_1",
      "index" : 1,
      "serviceName" : "a",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "c_2",
      "index" : 2,
      "serviceName" : "c",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    }, {
      "name" : "e_2",
      "index" : 2,
      "serviceName" : "e",
      "machineName" : "2",
      "statefulness" : "STATELESS"
    }, {
      "name" : "d_2",
      "index" : 2,
      "serviceName" : "d",
      "machineName" : "3",
      "statefulness" : "STATELESS"
    } ],
    "machines" : [ {
      "name" : "2"
    }, {
      "name" : "3"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "d_2",
      "targetName" : "e_2",
      "callId" : 4
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "d_2",
      "callId" : 1
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "b_1",
      "targetName" : "d_2",
      "callId" : 2
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "c_2",
      "targetName" : "d_2",
      "callId" : 3
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "b",
      "location" : {
        "x" : 226.08227634429932,
        "y" : 80.69884538650513
      }
    }, {
      "name" : "c",
      "location" : {
        "x" : 342.0566203594208,
        "y" : 75.31420612335205
      }
    }, {
      "name" : "d",
      "location" : {
        "x" : 260.8204526901245,
        "y" : 215.5335568189621
      }
    }, {
      "name" : "e",
      "location" : {
        "x" : 100.63640878577843,
        "y" : 289.97837093935567
      }
    }, {
      "name" : "a",
      "location" : {
        "x" : 98.3130854293704,
        "y" : 74.69885331392288
      }
    } ],
    "serviceMachines" : [ {
      "name" : "a_1",
      "location" : {
        "x" : 101.69225120544434,
        "y" : 82.6922527551651
      }
    }, {
      "name" : "b_1",
      "location" : {
        "x" : 277.20501613616943,
        "y" : 80.15386027097702
      }
    }, {
      "name" : "c_2",
      "location" : {
        "x" : 421.1882866475392,
        "y" : 76.0268478925451
      }
    }, {
      "name" : "d_2",
      "location" : {
        "x" : 334.0796696374888,
        "y" : 182.45325132736878
      }
    }, {
      "name" : "e_2",
      "location" : {
        "x" : 98.54400470943983,
        "y" : 294.9030825237918
      }
    } ]
  }
}