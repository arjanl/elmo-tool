{
  "deployment" : {
    "architecture" : {
      "calls" : [ {
        "id" : 1,
        "type" : "PUSHES_TO",
        "routable" : true,
        "data" : "alpha",
        "sourceName" : "a",
        "targetName" : "b"
      }, {
        "id" : 2,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "gamma",
        "sourceName" : "a",
        "targetName" : "c"
      }, {
        "id" : 3,
        "type" : "PULLED_FROM",
        "routable" : true,
        "data" : "gamma",
        "sourceName" : "c",
        "targetName" : "b"
      } ],
      "services" : [ {
        "name" : "a",
        "originalName" : "a",
        "description" : "a",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ "alpha", "gamma" ],
        "consumes" : [ ]
      }, {
        "name" : "b",
        "originalName" : "b",
        "description" : "b",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ "alpha", "gamma" ]
      }, {
        "name" : "c",
        "originalName" : "c",
        "description" : "c",
        "statefulness" : "STATELESS",
        "partitioning" : null,
        "produces" : [ ],
        "consumes" : [ "gamma" ]
      } ]
    },
    "name" : "fig7.elm",
    "serviceMachines" : [ {
      "name" : "b_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "b",
      "machineName" : "3"
    }, {
      "name" : "a_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "a",
      "machineName" : "2"
    }, {
      "name" : "c_1",
      "index" : 1,
      "statefulness" : "STATELESS",
      "serviceName" : "c",
      "machineName" : "4"
    } ],
    "machines" : [ {
      "name" : "2"
    }, {
      "name" : "3"
    }, {
      "name" : "4"
    } ],
    "connections" : [ {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "c_1",
      "callId" : 2
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "a_1",
      "targetName" : "b_1",
      "callId" : 1
    }, {
      "optionality" : "DEPLOYMENT_OPTIONAL",
      "sourceName" : "c_1",
      "targetName" : "b_1",
      "callId" : 3
    } ]
  },
  "layout" : {
    "services" : [ {
      "name" : "a",
      "location" : {
        "x" : 87.3130854293704,
        "y" : 82.69885331392288
      }
    }, {
      "name" : "b",
      "location" : {
        "x" : 394.0822763442993,
        "y" : 86.69884538650513
      }
    }, {
      "name" : "c",
      "location" : {
        "x" : 243.05662035942078,
        "y" : 296.31420612335205
      }
    } ],
    "serviceMachines" : [ {
      "name" : "c_1",
      "location" : {
        "x" : 275.9999837875366,
        "y" : 278.10256469249725
      }
    }, {
      "name" : "b_1",
      "location" : {
        "x" : 398.20501613616943,
        "y" : 93.15386027097702
      }
    }, {
      "name" : "a_1",
      "location" : {
        "x" : 130.69225120544434,
        "y" : 101.6922527551651
      }
    } ]
  }
}