package nl.aardbeitje.elm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class SampleArchitectures {
    public static final Service M = new Service("M", "Master", Statefulness.STATEFUL, null, Collections.singleton(""), Collections.singleton("") );
    public static final Service A = new Service("A", "Service A", Statefulness.STATELESS, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service B = new Service("B", "Service B", Statefulness.STATELESS, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service C = new Service("C", "Service C", Statefulness.STATELESS, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service E = new Service("E", "External System", Statefulness.STATELESS, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service D = new Service("D", "Database", Statefulness.STATEFUL, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service F = new Service("F", "Service F", Statefulness.STATEFUL, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service H = new Service("H", "Service H", Statefulness.STATEFUL, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service R = new Service("R", "R&D Database", Statefulness.STATEFUL, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service G = new Service("G", "GUI", Statefulness.PARTITIONED, "user", Collections.singleton(""), Collections.singleton(""));
    public static final Service X = new Service("X", "Service X", Statefulness.STATELESS, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service Y = new Service("Y", "Service Y", Statefulness.STATELESS, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service QDB = new Service("Qdb", "Queue DB", Statefulness.STATEFUL, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service QR = new Service("Qr", "Queue Reader", Statefulness.STATELESS, null, Collections.singleton(""), Collections.singleton(""));
    public static final Service UD = new Service("UD", "User Database", Statefulness.PARTITIONED, "user", Collections.singleton(""), Collections.singleton(""));
    public static final Service US = new Service("US", "User Service", Statefulness.PARTITIONED, "user", Collections.singleton(""), Collections.singleton(""));
    
    private static int callId = 0;

    public static Architecture single() {
        Architecture architecture = new Architecture();
        architecture.addService(M);
        return architecture;
    }

    public static Architecture pushAToB() {
        Architecture architecture = new Architecture();
        architecture.addService(A);
        architecture.addService(B);
        architecture.addCall(++callId, A, CallType.PUSHES_TO, B, true, "");
        return architecture;
    }

    public static Architecture pushAToBToA() {
        Architecture architecture = new Architecture();
        architecture.addService(A);
        architecture.addService(B);
        architecture.addCall(++callId,A, CallType.PUSHES_TO, B, true, "");
        architecture.addCall(++callId,B, CallType.PUSHES_TO, A, true, "");
        return architecture;
    }

    public static Architecture pushAToBToCToA() {
        Architecture architecture = new Architecture();
        architecture.addService(A);
        architecture.addService(B);
        architecture.addService(C);
        architecture.addCall(++callId,A, CallType.PUSHES_TO, B, true, "");
        architecture.addCall(++callId,B, CallType.PUSHES_TO, C, true, "");
        architecture.addCall(++callId,C, CallType.PUSHES_TO, A, true, "");
        return architecture;
    }

    public static Architecture pushPullAToB() {
        Architecture architecture = new Architecture();
        architecture.addService(A);
        architecture.addService(B);
        architecture.addCall(++callId,A, CallType.PUSHES_TO, B, true, "");
        architecture.addCall(++callId,B, CallType.PULLED_FROM, A, true, "");
        return architecture;
    }

    public static Architecture complex() {
        Architecture architecture = new Architecture();

        architecture.addService(E);
        architecture.addService(A);
        architecture.addService(D);
        architecture.addService(B);
        architecture.addService(R);
        architecture.addService(G);
        architecture.addService(X);
        architecture.addService(Y);

        architecture.addCall(++callId,E, CallType.PUSHES_TO, A, true, "");
        architecture.addCall(++callId,A, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,D, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,D, CallType.PUSHES_TO, R, true, "");
        architecture.addCall(++callId,B, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,G, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,D, CallType.PULLED_FROM, G, true, "");
        architecture.addCall(++callId,R, CallType.PULLED_FROM, X, true, "");
        architecture.addCall(++callId,Y, CallType.PULLED_FROM, R, true, "");

        return architecture;
    }

    public static Architecture insensitiveStress() {
        Architecture architecture = new Architecture();

        architecture.addService(A);
        architecture.addService(B);
        architecture.addService(C);
        architecture.addService(D);
        architecture.addService(E);
        architecture.addService(F);
        architecture.addService(G);
        architecture.addService(H);

        architecture.addCall(++callId,D, CallType.PUSHES_TO, A, true, "");
        architecture.addCall(++callId,F, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,H, CallType.PUSHES_TO, E, true, "");

        architecture.addCall(++callId,C, CallType.PUSHES_TO, B, true, "");
        architecture.addCall(++callId,G, CallType.PUSHES_TO, C, true, "");

        return architecture;
    }

    public static Architecture externalFeedWithDbAndGui() {
        Architecture architecture = new Architecture();

        architecture.addService(E);
        architecture.addService(D);
        architecture.addService(G);

        architecture.addCall(++callId,E, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,G, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,D, CallType.PULLED_FROM, G, true, "");
        return architecture;
    }

    public static Architecture externalFeedWithQueueAndDbAndGui() {
        Architecture architecture = new Architecture();

        architecture.addService(E);
        architecture.addService(QDB);
        architecture.addService(QR);
        architecture.addService(D);
        architecture.addService(G);

        architecture.addCall(++callId,E, CallType.PUSHES_TO, QDB, true, "");
        architecture.addCall(++callId,QDB, CallType.PULLED_FROM, QR, true, "");
        architecture.addCall(++callId,QR, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,G, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,D, CallType.PULLED_FROM, G, true, "");
        return architecture;
    }

    public static Architecture simplePartitioning() {
        Architecture architecture = new Architecture();

        architecture.addService(G);
        architecture.addService(US);
        architecture.addService(UD);

        architecture.addCall(++callId,G, CallType.PUSHES_TO, US, true, "");
        architecture.addCall(++callId,US, CallType.PULLED_FROM, G, true, "");

        architecture.addCall(++callId,US, CallType.PUSHES_TO, UD, true, "");
        architecture.addCall(++callId,UD, CallType.PULLED_FROM, US, true, "");

        return architecture;
    }

    public static Architecture gbd() {
        Architecture architecture = new Architecture();
        architecture.addService(G);
        architecture.addService(B);
        architecture.addService(D);
        
        architecture.addCall(++callId,G, CallType.PUSHES_TO, B, true, "");
        architecture.addCall(++callId,B, CallType.PULLED_FROM, G, true, "");

        architecture.addCall(++callId,B, CallType.PUSHES_TO, D, true, "");
        architecture.addCall(++callId,D, CallType.PULLED_FROM, B, true, "");
        
        return architecture;
    }

    public static Deployment gbdCodeployed() {
        Architecture architecture = gbd();
        MachineConfig g1 = new MachineConfig(G, "1");
        MachineConfig b1 = new MachineConfig(B, "1");
        MachineConfig g2 = new MachineConfig(G, "2");
        MachineConfig b2 = new MachineConfig(B, "2");
        MachineConfig d1 = new MachineConfig(D, "3");
        MachineConfig d2 = new MachineConfig(D, "4");
        return Deployment.customMachinePerService("gbd-codeployed", architecture, g1, b1, g2, b2, d1, d2);
    }

    public static Deployment simplePartitioningDeployed(int nrOfMachines) {
        Architecture architecture = simplePartitioning();

        Collection<MachineConfig> machineConfigs = new ArrayList<>();
        for (int i = 0; i < nrOfMachines; i++) {
            MachineConfig mcUs = new MachineConfig(US.getName(), "" + i);
            MachineConfig mcUd = new MachineConfig(UD.getName(), "" + i);
            machineConfigs.add(mcUs);
            machineConfigs.add(mcUd);
        }
        return Deployment.customMachinePerService("test", architecture, machineConfigs);
    }

    public static Deployment insensitiveStressDeployment() {
        Architecture a = insensitiveStress();

        MachineConfig[] mc = new MachineConfig[8];

        mc[0] = new MachineConfig(A.getName(), "1");
        mc[1] = new MachineConfig(B.getName(), "1");
        mc[2] = new MachineConfig(E.getName(), "2");
        mc[3] = new MachineConfig(D.getName(), "2");
        mc[4] = new MachineConfig(C.getName(), "3");
        mc[5] = new MachineConfig(F.getName(), "4");
        mc[6] = new MachineConfig(G.getName(), "5");
        mc[7] = new MachineConfig(H.getName(), "6");

        return Deployment.customMachinePerService("test", a, mc);
    }

    public static Deployment pushPullAToBHorizontallyScaled() {
        Architecture a = pushPullAToB();
        MachineConfig[] mc = new MachineConfig[3];

        mc[0] = new MachineConfig(A.getName(), "1");
        mc[1] = new MachineConfig(A.getName(), "2");
        mc[2] = new MachineConfig(B.getName(), "3");
        return Deployment.customMachinePerService("test", a, mc);
    }

}
