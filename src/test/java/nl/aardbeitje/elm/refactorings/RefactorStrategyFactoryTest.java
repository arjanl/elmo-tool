package nl.aardbeitje.elm.refactorings;

import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import static org.junit.Assert.*;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.SampleArchitecturePaper;

public class RefactorStrategyFactoryTest {

    /**
     * Tests if all refactorings are correctly produced for the default example.
     */
    @Test
    public void testGeneration() {
        Deployment d = SampleArchitecturePaper.initialDeployment();
        Set<RefactorStrategy> strategies = RefactorStrategyFactory.generateStrategies(d);
        Set<String> names = strategies.stream().map(rs -> rs.toString()).collect(Collectors.toSet());
        names.forEach(System.out::println);
        
        assertEquals(50, strategies.size());
        assertTrue(names.contains("Flip call 'C->D' from push to pull"));
        assertTrue(names.contains("Flip call 'P>-C' from pull to push"));
        assertTrue(names.contains("Flip call 'D>-C' from pull to push"));
        assertTrue(names.contains("Flip call 'D>-A' from pull to push"));
        assertTrue(names.contains("Flip call 'O->D' from push to pull"));
        assertTrue(names.contains("Flip call 'D>-P' from pull to push"));
        assertTrue(names.contains("Add queue on call 'D>-C'"));
        assertTrue(names.contains("Add queue on call 'D>-P'"));
        assertTrue(names.contains("Add queue on call 'P>-C'"));
        assertTrue(names.contains("Add queue on call 'D>-A'"));
        assertTrue(names.contains("Add cache on call 'D>-A'"));
        assertTrue(names.contains("Add cache on call 'P>-C'"));
        assertTrue(names.contains("Add cache on call 'D>-P'"));
        assertTrue(names.contains("Add cache on call 'D>-C'"));
        assertTrue(names.contains("New instance of 'D' on 4"));
        assertTrue(names.contains("New instance of 'D' on 3"));
        assertTrue(names.contains("New instance of 'D' on 2"));
        assertTrue(names.contains("New instance of 'D' on 6"));
        assertTrue(names.contains("New instance of 'D' on 5"));
        assertTrue(names.contains("New instance of 'A' on 4"));
        assertTrue(names.contains("New instance of 'A' on 5"));
        assertTrue(names.contains("New instance of 'A' on 1"));
        assertTrue(names.contains("New instance of 'A' on 2"));
        assertTrue(names.contains("New instance of 'A' on 3"));
        assertTrue(names.contains("New instance of 'O' on 6"));
        assertTrue(names.contains("New instance of 'O' on 2"));
        assertTrue(names.contains("New instance of 'O' on 3"));
        assertTrue(names.contains("New instance of 'O' on 4"));
        assertTrue(names.contains("New instance of 'P' on 3"));
        assertTrue(names.contains("New instance of 'P' on 4"));
        assertTrue(names.contains("New instance of 'C' on 1"));
        assertTrue(names.contains("New instance of 'P' on 1"));
        assertTrue(names.contains("New instance of 'O' on 1"));
        assertTrue(names.contains("New instance of 'C' on 2"));
        assertTrue(names.contains("New instance of 'C' on 5"));
        assertTrue(names.contains("New instance of 'P' on 5"));
        assertTrue(names.contains("New instance of 'P' on 6"));
        assertTrue(names.contains("New instance of 'C' on 6"));
        assertTrue(names.contains("New instance of 'D' on new machine"));
        assertTrue(names.contains("New instance of 'O' on new machine"));
        assertTrue(names.contains("New instance of 'C' on new machine"));
        assertTrue(names.contains("New instance of 'P' on new machine"));
        assertTrue(names.contains("New instance of 'A' on new machine"));
        assertTrue(names.contains("Remove instance of 'C' on 3"));
        assertTrue(names.contains("Remove instance of 'C' on 4"));
        assertTrue(names.contains("Split service 'D' data into [order] and [prod]"));
		assertTrue(names.contains("Split service 'C' data into [prod, order] and [price]"));
		assertTrue(names.contains("Split service 'C' data into [price, order] and [prod]"));
		assertTrue(names.contains("Split service 'C' data into [order] and [prod, price]"));

    }

}
