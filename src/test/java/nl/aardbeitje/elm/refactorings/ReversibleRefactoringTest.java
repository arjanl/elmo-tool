package nl.aardbeitje.elm.refactorings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.collections15.CollectionUtils;
import org.junit.Test;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.SampleArchitecturePaper;

public class ReversibleRefactoringTest {

    @Test
    public void flipIsReversible() {
        testReversability((d) -> {
            Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(FlipStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.A)));
            assertEquals(1, strategies.size());
            return Collections.singletonList(strategies.iterator().next());
        });
    }

    @Test
    public void mergeIsReversible() {
        testReversability((d) -> {
            Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(MergeStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.P)));
            assertEquals(3, strategies.size());
            return Collections.singletonList(strategies.stream().filter(s -> s.toString().equals("Merge service 'P' with 'A'")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy")));
        });
    }

    @Test
    public void addCacheIsReversible() {
        testReversability((d) -> {
            Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(AddCacheStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.P)));
            assertEquals(1, strategies.size());
            return Collections.singletonList(strategies.stream().filter(s -> s.toString().equals("Add cache on call 'D>-P'")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy")));
        });
    }

    @Test
    public void addQueueIsReversible() {
        testReversability((d) -> {
            Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(AddQueueStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.P)));
            strategies.forEach(System.out::println);
            assertEquals(1, strategies.size());
            return Collections.singletonList(strategies.stream().filter(s -> s.toString().equals("Add queue on call 'P>-C'")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy")));
        });
    }

    @Test
    public void newInstanceNewMachineIsReversible() {
        testReversability((d) -> {
            Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.P)));
            strategies.forEach(System.out::println);
            assertEquals(6, strategies.size());
            return Collections.singletonList(strategies.stream().filter(s -> s.toString().equals("New instance of 'P' on new machine")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy")));
        });
    }

    @Test
    public void newInstanceExistingMachineIsReversible() {
        testReversability((d) -> {
            Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.P)));
            strategies.forEach(System.out::println);
            assertEquals(6, strategies.size());
            return Collections.singletonList(strategies.stream().filter(s -> s.toString().equals("New instance of 'P' on 1")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy")));
        });
    }

    @Test
    public void removeInstanceIsReversible() {
        testReversability((d) -> {
            Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(RemoveInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
            strategies.forEach(System.out::println);
            assertEquals(2, strategies.size());
            return Collections.singletonList(strategies.stream().filter(s -> s.toString().equals("Remove instance of 'C' on 3")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy")));
        });
    }

    @Test
    public void splitIsReversible() {
        testReversability((d) -> {
            Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(SplitStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.D)));
            strategies.forEach(System.out::println);
            assertEquals(1, strategies.size());
            return Collections.singletonList(strategies.stream().filter(s -> s.toString().equals("Split service 'D' data into [order] and [prod]")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy")));
        });
    }

    /**
     * Tests if combinations of new instance and remove instance in sequence can be rolled back.
     * Also tests if strategies are reproducable on another instance of the deployment.
     */
    @Test
    public void newInstanceAndThenRemove() {
        // [New instance of 'chkout' on new machine, Remove instance of 'chkout'
        // on 297]

        Deployment d = SampleArchitecturePaper.initialDeployment();
        
        Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy newInstance = strategies.stream().filter(s -> s.toString().equals("New instance of 'C' on 5")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        newInstance.apply(d);
        
        strategies = RefactorAlternatives.flatten(RemoveInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy removeInstance = strategies.stream().filter(s -> s.toString().equals("Remove instance of 'C' on 5")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        removeInstance.apply(d);

        strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy newInstance2 = strategies.stream().filter(s -> s.toString().equals("New instance of 'C' on 5")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        newInstance2.apply(d);
        
        strategies = RefactorAlternatives.flatten(RemoveInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy removeInstance2 = strategies.stream().filter(s -> s.toString().equals("Remove instance of 'C' on 5")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        removeInstance2.apply(d);

        strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy newInstance3 = strategies.stream().filter(s -> s.toString().equals("New instance of 'C' on 5")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        newInstance3.apply(d);

        List<RefactorStrategy> path = new ArrayList<>();
        path.add(newInstance);
        path.add(removeInstance);
        path.add(newInstance2);
        path.add(removeInstance2);
        path.add(newInstance3);
        testReversability(p -> path);
    }

     /**
     * Tests if combinations of new instance and remove instance in sequence can be rolled back.
     * Also tests if strategies are reproducable on another instance of the deployment.
     */
    @Test
    public void newMachineAndThenRemove() {
        Deployment d = SampleArchitecturePaper.initialDeployment();
        
        Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy newInstance = strategies.stream().filter(s -> s.toString().equals("New instance of 'C' on new machine")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        newInstance.apply(d);
        
        d.getServiceMachines().forEach(System.out::println);
        
        strategies = RefactorAlternatives.flatten(RemoveInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy removeInstance = strategies.stream().filter(s -> s.toString().equals("Remove instance of 'C' on 7")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        removeInstance.apply(d);

        d.getServiceMachines().forEach(System.out::println);

        strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy newInstance2 = strategies.stream().filter(s -> s.toString().equals("New instance of 'C' on new machine")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        newInstance2.apply(d);

        d.getServiceMachines().forEach(System.out::println);

        strategies = RefactorAlternatives.flatten(RemoveInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy removeInstance2 = strategies.stream().filter(s -> s.toString().equals("Remove instance of 'C' on 7")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        removeInstance2.apply(d);
        
        d.getServiceMachines().forEach(System.out::println);
        

        List<RefactorStrategy> path = new ArrayList<>();
        path.add(newInstance);
        path.add(removeInstance);
        path.add(newInstance2);
        path.add(removeInstance2);
        testReversability(p -> path);
    }
  

     /**
     * Tests if combinations of new instance and remove instance in sequence can be rolled back.
     * Also tests if strategies are reproducable on another instance of the deployment.
     */
  // Evaluating: [Remove instance of 'chkout' on 4, New instance of 'chkout' on 6]
    @Test
    public void removeInstanceAndNewInstance() {
        Deployment d = SampleArchitecturePaper.initialDeployment();
        
        Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(RemoveInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy newInstance = strategies.stream().filter(s -> s.toString().equals("Remove instance of 'C' on 4")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        newInstance.apply(d);
        
        d.getServiceMachines().forEach(System.out::println);
        
        strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        strategies.forEach(System.out::println);
        RefactorStrategy removeInstance = strategies.stream().filter(s -> s.toString().equals("New instance of 'C' on 6")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        removeInstance.apply(d);

        d.getServiceMachines().forEach(System.out::println);

        List<RefactorStrategy> path = new ArrayList<>();
        path.add(newInstance);
        path.add(removeInstance);
        testReversability(p -> path);
    }

 /*
 Evaluating: [Remove instance of 'chkout' on 4, New instance of 'price' on 3]
Evaluating: [Remove instance of 'chkout' on 4, New instance of 'chkout' on 6]
Vertices before   : [acc, price, office, chkout, db]
Vertices after   : [acc, price, office, chkout, db]
Edges before: [db>-price, db>-chkout, chkout->db, price>-chkout, office->db, db>-acc]
Edges after: [db>-price, db>-chkout, chkout->db, price>-chkout, office->db, db>-acc]
ServiceMachines before   : [[db_1:1], [price_1:2], [acc_1:6], [chkout_2:3], [office_1:5], [chkout_1:4]]
2019-03-17 16:51:58.886 java[77493:3544022] unrecognized type is 4294967295
2019-03-17 16:51:58.886 java[77493:3544022] *** Assertion failure in -[NSEvent _initWithCGEvent:eventRef:], /BuildRoot/Library/Caches/com.apple.xbs/Sources/AppKit/AppKit-1671.20.108/AppKit.subproj/NSEvent.m:1969
ServiceMachines after   : [[db_1:1], [price_1:2], [office_1:5], [acc_1:6], [chkout_1:4]]  
      
  */
    
        /**
     * Tests if combinations of new instance and remove instance in sequence can be rolled back.
     * Also tests if strategies are reproducable on another instance of the deployment.
     */
// Evaluating: [Remove instance of 'chkout' on 4, New instance of 'price' on 3]
// Evaluating: [Remove instance of 'chkout' on 4, New instance of 'chkout' on 6]
    @Test
    public void removeNewRemoveNew() {
        Deployment start = SampleArchitecturePaper.initialDeployment();
        
        Deployment d = SampleArchitecturePaper.initialDeployment();
        
        Set<RefactorStrategy> strategies = RefactorAlternatives.flatten(RemoveInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        RefactorStrategy removeInstance1 = strategies.stream().filter(s -> s.toString().equals("Remove instance of 'C' on 4")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        removeInstance1.apply(d);
        
        strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.P)));
        RefactorStrategy newInstance1 = strategies.stream().filter(s -> s.toString().equals("New instance of 'P' on 3")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        newInstance1.apply(d);
        
        newInstance1.reverse(d);
        removeInstance1.reverse(d);

        strategies = RefactorAlternatives.flatten(RemoveInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.C)));
        RefactorStrategy removeInstance2 = strategies.stream().filter(s -> s.toString().equals("Remove instance of 'C' on 4")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        removeInstance2.apply(d);
        
        strategies = RefactorAlternatives.flatten(NewInstanceStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.P)));
        RefactorStrategy newInstance2 = strategies.stream().filter(s -> s.toString().equals("New instance of 'P' on 3")).findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
        newInstance2.apply(d);
        
        newInstance2.reverse(d);
        removeInstance2.reverse(d);

        assertTrue(CollectionUtils.isEqualCollection(start.getArchitecture().getGraph().getVertices(), d.getArchitecture().getGraph().getVertices()));
        assertTrue(CollectionUtils.isEqualCollection(start.getArchitecture().getGraph().getEdges(), d.getArchitecture().getGraph().getEdges()));
        assertTrue(start.getArchitecture().isEqual(d.getArchitecture()));
        assertTrue(start.isEqual(d));
        
        List<RefactorStrategy> path1 = new ArrayList<>();
        path1.add(removeInstance1);
        path1.add(newInstance1);
        testReversability(p -> path1);

        List<RefactorStrategy> path2 = new ArrayList<>();
        path2.add(removeInstance2);
        path2.add(newInstance2);
        testReversability(p -> path2);

    }

    
    
    private void testReversability(Function<Deployment, List<RefactorStrategy>> generateStrategy) {
        Deployment start = SampleArchitecturePaper.initialDeployment();
        Deployment d = SampleArchitecturePaper.initialDeployment();

        List<RefactorStrategy> rss = generateStrategy.apply(d);

        System.out.println("Vertices before   : " + d.getArchitecture().getGraph().getVertices());
        System.out.println("Edges before   : " + d.getArchitecture().getGraph().getEdges());
        System.out.println("ServiceMachines before   : " + d.getServiceMachines());
        System.out.println("");
        for (RefactorStrategy rs : rss) {
            System.out.println("Applying " + rs);
            rs.apply(d);
            System.out.println("Vertices after   : " + d.getArchitecture().getGraph().getVertices());
            System.out.println("Edges after      : " + d.getArchitecture().getGraph().getEdges());
            System.out.println("ServiceMachines after   : " + d.getServiceMachines());
            System.out.println("");
        }

        Collections.reverse(rss);
        for (RefactorStrategy rs : rss) {
            System.out.println("Reversing " + rs);
            rs.reverse(d);
            System.out.println("Vertices reversed   : " + d.getArchitecture().getGraph().getVertices());
            System.out.println("Edges reversed: " + d.getArchitecture().getGraph().getEdges());
            System.out.println("ServiceMachines reversed   : " + d.getServiceMachines());
            System.out.println("");
        }

        System.out.println("expected:" + start.getArchitecture().getGraph().getEdges());
        System.out.println("was         :" + d.getArchitecture().getGraph().getEdges());
        assertTrue(CollectionUtils.isEqualCollection(start.getArchitecture().getGraph().getVertices(), d.getArchitecture().getGraph().getVertices()));
        assertTrue(CollectionUtils.isEqualCollection(start.getArchitecture().getGraph().getEdges(), d.getArchitecture().getGraph().getEdges()));
        assertTrue(start.getArchitecture().isEqual(d.getArchitecture()));
        assertTrue(start.isEqual(d));
    }

}
