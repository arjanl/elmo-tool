package nl.aardbeitje.elm.refactorings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.collections15.CollectionUtils;
import org.junit.Test;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.SampleArchitecturePaper;
import nl.aardbeitje.elm.io.clone.ClonerFactory;

public class ReproducibleRefactoringTest {

	/**
	 * Tests if the flip operation is reproducable. Given a start deployment, if we
	 * apply the same strategy sequence, we should end up in exactly the same
	 * deployment (including id's of edges and vertices).
	 */
	@Test
	public void flipIsReproducable() {
		Deployment start = SampleArchitecturePaper.initialDeployment();
		Deployment d1 = flipflip(ClonerFactory.getCloner().clone(start));
		Deployment d2 = flipflip(ClonerFactory.getCloner().clone(start));
		assertTrue(CollectionUtils.isEqualCollection(d1.getArchitecture().getGraph().getVertices(),
				d2.getArchitecture().getGraph().getVertices()));
		assertTrue(d1.getArchitecture().isEqual(d2.getArchitecture()));
		assertTrue(d1.isEqual(d2));
	}

	private Deployment flipflip(Deployment d) {

		// generate a single flip strategy for service 'A' (pulls from 'D')
		Set<RefactorStrategy> strategies = RefactorAlternatives
				.flatten(FlipStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.A)));
		assertEquals(1, strategies.size());

		// apply it
		RefactorStrategy rs = strategies.iterator().next();
		rs.apply(d);

		// generate a new strategy for service 'O'
		Set<RefactorStrategy> strategies2 = RefactorAlternatives
				.flatten(FlipStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.O)));
		assertEquals(1, strategies2.size());

		// apply it
		RefactorStrategy rs2 = strategies.iterator().next();
		rs2.apply(d);
		return d;
	}

	/**
	 * Tests if the flip operation is reproducable. Given a start deployment, if we
	 * apply the same strategy sequence, we should end up in exactly the same
	 * deployment (including id's of edges and vertices).
	 */
	@Test
	public void mergeIsReproducable() {
		Deployment start = SampleArchitecturePaper.initialDeployment();
		Deployment d1 = mergemerge(ClonerFactory.getCloner().clone(start));
		Deployment d2 = mergemerge(ClonerFactory.getCloner().clone(start));
		assertTrue(d1.isEqual(d2));
	}

	private Deployment mergemerge(Deployment d) {

		// generate merge strategies for service 'P', should be merge with A, O or D
		Set<RefactorStrategy> strategies = RefactorAlternatives
				.flatten(MergeStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.P)));
		assertEquals(3, strategies.size());

		// apply it
		RefactorStrategy rs = strategies.stream().filter(s -> s.toString().equals("Merge service 'P' with 'A'"))
				.findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
		rs.apply(d);

		// generate a new strategy for service 'O'
		Set<RefactorStrategy> strategies2 = RefactorAlternatives
				.flatten(MergeStrategy.generate(d, d.getArchitecture().getService(SampleArchitecturePaper.O)));
		strategies2.forEach(System.out::println);
		assertEquals(2, strategies2.size());

		// apply it
		RefactorStrategy rs2 = strategies2.stream().filter(s -> s.toString().equals("Merge service 'O' with 'P'"))
				.findFirst().orElseThrow(() -> new NoSuchElementException("cannot find proper strategy"));
		rs2.apply(d);
		return d;
	}

}
