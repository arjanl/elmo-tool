package nl.aardbeitje.elm.refactorings;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.activemq.artemis.api.core.ActiveMQException;
import org.apache.activemq.artemis.api.core.SimpleString;
import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.api.core.client.ActiveMQClient;
import org.apache.activemq.artemis.api.core.client.ClientConsumer;
import org.apache.activemq.artemis.api.core.client.ClientMessage;
import org.apache.activemq.artemis.api.core.client.ClientProducer;
import org.apache.activemq.artemis.api.core.client.ClientSession;
import org.apache.activemq.artemis.api.core.client.ClientSessionFactory;
import org.apache.activemq.artemis.api.core.client.ServerLocator;
import org.apache.activemq.artemis.core.remoting.impl.invm.InVMConnectorFactory;
import org.apache.activemq.artemis.core.server.embedded.EmbeddedActiveMQ;
import org.junit.Ignore;
import org.junit.Test;

import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.SampleArchitecturePaper;
import nl.aardbeitje.elm.io.clone.KryoCloner;

public class QueueTest {

    private static final SimpleString RS = new SimpleString("rs");
    private static final KryoCloner KRYO = new KryoCloner();

    @Test
    public void refactorPathSerializationTest() throws Exception {
        EmbeddedActiveMQ server = new EmbeddedActiveMQ();
        server.start();

        ServerLocator locator = ActiveMQClient.createServerLocatorWithoutHA(new TransportConfiguration(InVMConnectorFactory.class.getName()));

        ClientSessionFactory factory = locator.createSessionFactory();
        ClientSession session = factory.createSession();

        ClientProducer producer = session.createProducer("elmoTest");

        session.createTemporaryQueue("elmoTest", "elmoTest");
        ClientConsumer consumer = session.createConsumer("elmoTest");

        Deployment d = SampleArchitecturePaper.initialDeployment();
        Collection<RefactorStrategy> strategies = RefactorStrategyFactory.generateStrategies(d);

        System.out.println("Before queue:");
        strategies.forEach(System.out::println);

        strategies.forEach(rs -> {
            try {
                ClientMessage message = session.createMessage(false);
                List<RefactorStrategy> path = Collections.singletonList(rs);
                Output output = new Output(1024, -1);
                KRYO.writeClassAndObject(output, path);
                message.putBytesProperty(RS, output.toBytes());
                producer.send(message);
            } catch (ActiveMQException e) {
                throw new RuntimeException(e);
            }
        });

        System.out.println("Reading from queue:");

        session.start();

        ClientMessage msgReceived = consumer.receive(1000);
        while (msgReceived != null) {
            Input input = new Input((byte[]) msgReceived.getObjectProperty(RS));
            List<RefactorStrategy> path = (List<RefactorStrategy>) KRYO.readClassAndObject(input);
            System.out.println(path);
            msgReceived = consumer.receive(1000);
        }

        producer.close();
        consumer.close();
        session.deleteQueue("elmoTest");
        session.close();
    }

    @Test
    @Ignore // used for testing if paging works
    public void setupCoreQueueTest() throws Exception {
        EmbeddedActiveMQ server = new EmbeddedActiveMQ();
        server.start();

        ServerLocator locator = ActiveMQClient.createServerLocatorWithoutHA(new TransportConfiguration(InVMConnectorFactory.class.getName()));

        ClientSessionFactory factory = locator.createSessionFactory();
        ClientSession session = factory.createSession();

        ClientProducer producer = session.createProducer("elmoTest");

        session.createTemporaryQueue("elmoTest", "elmoTest");

        ClientConsumer consumer = session.createConsumer("elmoTest");

        for (int i = 0; i < 10000000; i++) {
            ClientMessage message = session.createMessage(false);
            message.getBodyBuffer().writeString("Hello");
            if (i % 1000 == 0)
                System.out.println("Sending message " + i);
            producer.send(message);
        }

        session.start();
        ClientMessage msgReceived = consumer.receive();

        System.out.println("message = " + msgReceived.getBodyBuffer().readString());

        session.deleteQueue("elmoTest");
        session.close();
    }
}
