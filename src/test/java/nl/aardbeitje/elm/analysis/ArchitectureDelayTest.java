package nl.aardbeitje.elm.analysis;

import static org.apache.commons.collections4.CollectionUtils.isEqualCollection;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.SampleArchitecturePaper;
import nl.aardbeitje.elm.Service;

public class ArchitectureDelayTest {
	

    @Test
    public void paperTrivialCasesC() {
        Architecture a = SampleArchitecturePaper.initialArchitecture();
		ArchitectureAnalysis aa = new ArchitectureAnalysis(a);
		
        List<Service> expectedPrice = Arrays.asList(a.getService(SampleArchitecturePaper.C), a.getService(SampleArchitecturePaper.P));
		Set<Service> actualPrice = aa.delay(a.getService(SampleArchitecturePaper.C), "price");
		
//		System.out.println("Price:");
//		System.out.println(expectedPrice);
//		System.out.println(actualPrice);
		assertTrue(isEqualCollection(expectedPrice, actualPrice));

		List<Service> expectedProd = Arrays.asList(a.getService(SampleArchitecturePaper.C), a.getService(SampleArchitecturePaper.O));
		Set<Service> actualProd = aa.delay(a.getService(SampleArchitecturePaper.C), "prod");
		
//		System.out.println("Prod:");
//		System.out.println(expectedProd);
//		System.out.println(actualProd);
		assertTrue(isEqualCollection(expectedProd, actualProd));

		List<Service> expectedOrder = Arrays.asList(a.getService(SampleArchitecturePaper.C));
		Set<Service> actualOrder = aa.delay(a.getService(SampleArchitecturePaper.C), "order");
		
//		System.out.println("Order:");
//		System.out.println(expectedOrder);
//		System.out.println(actualOrder);

		assertTrue(isEqualCollection(expectedOrder, actualOrder));
    }

    @Test
    public void paperEdgeCasesA() {
        Architecture a = SampleArchitecturePaper.initialArchitecture();
		ArchitectureAnalysis aa = new ArchitectureAnalysis(a);
		
		// db should not be in the list since it is passive
        List<Service> expectedOrder = Arrays.asList(a.getService(SampleArchitecturePaper.C), a.getService(SampleArchitecturePaper.A));
		Set<Service> actualOrder = aa.delay(a.getService(SampleArchitecturePaper.A), "order");
//		System.out.println("Order:");
//		System.out.println(expectedOrder);
//		System.out.println(actualOrder);
		assertTrue(isEqualCollection(expectedOrder, actualOrder));
		
        // should be null since A does not consume price
        assertNull(aa.delay(a.getService(SampleArchitecturePaper.A), "price"));
}

}
