package nl.aardbeitje.elm.analysis;
import static nl.aardbeitje.elm.SampleArchitectures.A;
import static nl.aardbeitje.elm.SampleArchitectures.B;
import static nl.aardbeitje.elm.SampleArchitectures.C;
import static nl.aardbeitje.elm.SampleArchitectures.D;
import static nl.aardbeitje.elm.SampleArchitectures.E;
import static nl.aardbeitje.elm.SampleArchitectures.G;
import static nl.aardbeitje.elm.SampleArchitectures.M;
import static nl.aardbeitje.elm.SampleArchitectures.R;
import static nl.aardbeitje.elm.SampleArchitectures.X;
import static nl.aardbeitje.elm.SampleArchitectures.Y;
import static org.apache.commons.collections4.CollectionUtils.isEqualCollection;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Test;

import nl.aardbeitje.elm.SampleArchitectures;
import nl.aardbeitje.elm.analysis.ArchitectureAnalysis;

public class ArchitectureConsistencyTest {
    
    @Test
    public void bla() {
        Set<String> original = new LinkedHashSet<>();
        original.add("x");
        original.add("y");
        original.add("z");
        
        Set<String> test = new LinkedHashSet<>();
        test.add("y");
        test.add("x");
        test.add("z");
        
        assertTrue(original.containsAll(test));
        
    }
    
    @Test
    public void single() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.single());
        assertTrue(isEqualCollection(Arrays.asList(M), a.consistency(M)));
    }

    @Test
    public void pushAToB() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToB());
        assertTrue(isEqualCollection(Arrays.asList(A), a.consistency(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.consistency(B)));
    }

    @Test
    public void pushAToBToA() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToBToA());
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.consistency(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.consistency(B)));
    }

    @Test
    public void pushAToBToCToA() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToBToCToA());
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.consistency(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.consistency(B)));
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.consistency(C)));
    }

    @Test
    public void pushPullAToB() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushPullAToB());
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.consistency(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.consistency(B)));
    }

    @Test
    public void complex() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.complex());
        assertTrue(isEqualCollection(Arrays.asList(E),a.consistency(E)));
        assertTrue(isEqualCollection(Arrays.asList(A,E), a.consistency(A)));
        assertTrue(isEqualCollection(Arrays.asList(E,A,B,G,D), a.consistency(D)));
        assertTrue(isEqualCollection(Arrays.asList(B), a.consistency(B)));
        assertTrue(isEqualCollection(Arrays.asList(G,E,A,B,D), a.consistency(G)));
        assertTrue(isEqualCollection(Arrays.asList(R,Y,E,A,B,G,D), a.consistency(R)));
        assertTrue(isEqualCollection(Arrays.asList(R,Y,E,A,B,G,D,X), a.consistency(X)));
        assertTrue(isEqualCollection(Arrays.asList(Y), a.consistency(Y)));
    }

    
}
