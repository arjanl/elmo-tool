package nl.aardbeitje.elm.analysis;

import static org.apache.commons.collections4.CollectionUtils.isEqualCollection;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.SampleArchitecturePaper;

public class ArchitectureStateTest {

	
	@Test
    public void paper() {
        Architecture a = SampleArchitecturePaper.initialArchitecture();
		ArchitectureAnalysis aa = new ArchitectureAnalysis(a);
        
        assertTrue(isEqualCollection( Collections.EMPTY_SET, aa.getStates(a.getService(SampleArchitecturePaper.A))));
        
        assertTrue(isEqualCollection( Collections.EMPTY_SET, aa.getStates(a.getService(SampleArchitecturePaper.O))));

        Set<String> statesD = new HashSet<>(Arrays.asList("prod", "order"));
        assertTrue(isEqualCollection( statesD, aa.getStates(a.getService(SampleArchitecturePaper.D))));

        assertTrue(isEqualCollection( Collections.EMPTY_SET, aa.getStates(a.getService(SampleArchitecturePaper.P))));

        assertTrue(isEqualCollection( Collections.EMPTY_SET, aa.getStates(a.getService(SampleArchitecturePaper.C))));
    }
}
