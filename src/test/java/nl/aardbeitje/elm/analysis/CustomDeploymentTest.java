package nl.aardbeitje.elm.analysis;
import static nl.aardbeitje.elm.SampleArchitectures.A;
import static nl.aardbeitje.elm.SampleArchitectures.B;
import static nl.aardbeitje.elm.SampleArchitectures.C;
import static nl.aardbeitje.elm.TestUtil.hasConnection;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.MachineConfig;
import nl.aardbeitje.elm.SampleArchitectures;

public class CustomDeploymentTest {
    
    @Test
    public void explicitConfiguration() {
        Architecture architecture = SampleArchitectures.pushAToBToCToA();
        MachineConfig a1 = new MachineConfig(A, "1");
        MachineConfig b2 = new MachineConfig(B, "2");
        MachineConfig c2 = new MachineConfig(C, "2");
        Deployment deployment = Deployment.customMachinePerService("test", architecture, a1,b2,c2);
        assertEquals(2, deployment.getMachines().size());
        assertEquals(3, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, A, "1", CallType.PUSHES_TO, B, "2"));
        assertTrue(hasConnection(deployment, B, "2", CallType.PUSHES_TO, C, "2"));
        assertTrue(hasConnection(deployment, C, "2", CallType.PUSHES_TO, A, "1"));
    }

    @Test
    public void explicitForOneMachineShouldFillOutOthers() {
        Architecture architecture = SampleArchitectures.pushAToBToCToA();
        MachineConfig a1 = new MachineConfig(A, "1");
        MachineConfig b1 = new MachineConfig(B, "1");
        Deployment deployment = Deployment.customMachinePerService("test", architecture, a1,b1);
        assertEquals(2, deployment.getMachines().size());
        assertEquals(3, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, B, "1", CallType.PUSHES_TO, C, "2"));
        assertTrue(hasConnection(deployment,C,  "2", CallType.PUSHES_TO, A, "1"));
        assertTrue(hasConnection(deployment, A, "1", CallType.PUSHES_TO, B, "1"));
    }

}
