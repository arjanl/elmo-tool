package nl.aardbeitje.elm.analysis;

import static nl.aardbeitje.elm.SampleArchitectures.A;
import static nl.aardbeitje.elm.SampleArchitectures.B;
import static nl.aardbeitje.elm.SampleArchitectures.C;
import static nl.aardbeitje.elm.SampleArchitectures.D;
import static nl.aardbeitje.elm.SampleArchitectures.E;
import static nl.aardbeitje.elm.SampleArchitectures.G;
import static nl.aardbeitje.elm.SampleArchitectures.M;
import static nl.aardbeitje.elm.SampleArchitectures.R;
import static nl.aardbeitje.elm.SampleArchitectures.X;
import static nl.aardbeitje.elm.SampleArchitectures.Y;
import static org.apache.commons.collections4.CollectionUtils.isEqualCollection;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.SampleArchitectures;

public class ArchitectureLatencyTest {
    
    @Test
    public void single() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.single());
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(M)));
    }

    @Test
    public void pushAToB() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToB());
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(A)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(B)));
    }

    @Test
    public void pushAToBToA() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToBToA());
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(A)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(B)));
    }

    @Test
    public void pushAToBToCToA() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToBToCToA());
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(A)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(B)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(C)));
    }

    @Test
    public void pushPullAToB() {
        Architecture architecture = SampleArchitectures.pushPullAToB();
        ArchitectureAnalysis a = new ArchitectureAnalysis(architecture);
        Call bPulledFromA = architecture.getCall(B, A, CallType.PULLED_FROM, true, "");
        assertTrue(isEqualCollection(Arrays.asList(bPulledFromA), a.latency(A)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(B)));
    }

    @Test
    public void complex() {
        Architecture architecture = SampleArchitectures.complex();
        ArchitectureAnalysis a = new ArchitectureAnalysis(architecture);
        Call dPulledFromG = architecture.getCall(D, G, CallType.PULLED_FROM, true, "");
        Call yPulledFromR = architecture.getCall(Y, R, CallType.PULLED_FROM, true, "");
        Call rPulledFromX = architecture.getCall(R,X, CallType.PULLED_FROM, true, "");
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(E)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(A)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(D)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(B)));
        assertTrue(isEqualCollection(Arrays.asList(dPulledFromG), a.latency(G)));
        assertTrue(isEqualCollection(Arrays.asList(yPulledFromR), a.latency(R)));
        assertTrue(isEqualCollection(Arrays.asList(yPulledFromR,rPulledFromX), a.latency(X)));
        assertTrue(isEqualCollection(Arrays.asList(), a.latency(Y)));
    }

    
}
