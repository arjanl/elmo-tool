package nl.aardbeitje.elm.analysis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.SampleArchitectures;

public class MultipleDeploymentTest {
    
    @Test
    public void single() {
        Architecture architecture = SampleArchitectures.single();
        Deployment deployment = Deployment.machinePerService("test",architecture);
        assertEquals(1, deployment.getMachines().size());
    }

    @Test
    public void pushAToB() {
        Architecture architecture = SampleArchitectures.pushAToB();
        Deployment deployment = Deployment.machinePerService("test",architecture);
        assertEquals(2, deployment.getMachines().size());
        assertEquals(1, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, "1", CallType.PUSHES_TO, "2"));
    }

    @Test
    public void pushAToBToA() {
        Architecture architecture = SampleArchitectures.pushAToBToA();
        Deployment deployment = Deployment.machinePerService("test",architecture);
        assertEquals(2, deployment.getMachines().size());
        assertEquals(2, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, "1", CallType.PUSHES_TO, "2"));
        assertTrue(hasConnection(deployment, "2", CallType.PUSHES_TO, "1"));
    }

    @Test
    public void pushAToBToCToA() {
        Architecture architecture = SampleArchitectures.pushAToBToCToA();
        Deployment deployment = Deployment.machinePerService("test",architecture);
        assertEquals(3, deployment.getMachines().size());
        assertEquals(3, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, "1", CallType.PUSHES_TO, "2"));
        assertTrue(hasConnection(deployment, "2", CallType.PUSHES_TO, "3"));
        assertTrue(hasConnection(deployment, "3", CallType.PUSHES_TO, "1"));
    }

    @Test
    public void pushPullAToB() {
        Architecture architecture = SampleArchitectures.pushPullAToB();
        Deployment deployment = Deployment.machinePerService("test",architecture);
        assertEquals(2, deployment.getMachines().size());
        assertEquals(2, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, "1", CallType.PUSHES_TO, "2"));
        assertTrue(hasConnection(deployment, "2", CallType.PULLED_FROM, "1"));
    }

    @Test
    public void complex() {
        Architecture architecture = SampleArchitectures.complex();
        Deployment deployment = Deployment.machinePerService("test",architecture);
        assertEquals(8, deployment.getMachines().size());
        assertEquals(9, deployment.getConnections().size());
    }
    
    private boolean hasConnection(Deployment deployment, String m1, CallType callType, String m2) {
        return deployment.getConnections(deployment.getMachineByName(m1), deployment.getMachineByName(m2)).stream().anyMatch( c-> c.getCallType()==callType);
    }

}
