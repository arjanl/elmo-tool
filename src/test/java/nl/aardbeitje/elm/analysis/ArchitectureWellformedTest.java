package nl.aardbeitje.elm.analysis;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import nl.aardbeitje.elm.SampleArchitecturePaper;
import nl.aardbeitje.elm.SampleArchitectures;
import nl.aardbeitje.elm.io.DeploymentReader;
import nl.aardbeitje.elm.io.ParsedDeployment;

public class ArchitectureWellformedTest {
	
    @Test
    public void single() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.single());
        assertTrue(a.isWellformed());
    }

    @Test
    public void paper() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitecturePaper.initialArchitecture());
        assertTrue(a.isWellformed());
    }

    @Test
    public void missingProduces() throws IOException {
    	DeploymentReader r = new DeploymentReader( "test",this.getClass().getResourceAsStream( "/unittests/missing produces.elm"));
    	ParsedDeployment pd = r.read();
        ArchitectureAnalysis a = new ArchitectureAnalysis(pd.getArchitecture());
        assertFalse(a.isWellformed());
    }

}
