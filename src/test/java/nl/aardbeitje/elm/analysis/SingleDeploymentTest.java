package nl.aardbeitje.elm.analysis;

import static nl.aardbeitje.elm.SampleArchitectures.A;
import static nl.aardbeitje.elm.SampleArchitectures.B;
import static nl.aardbeitje.elm.SampleArchitectures.C;
import static nl.aardbeitje.elm.TestUtil.hasConnection;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.SampleArchitectures;

public class SingleDeploymentTest {
    
    @Test
    public void single() {
        Architecture architecture = SampleArchitectures.single();
        Deployment deployment = Deployment.singleMachine("test",architecture);
        assertEquals(1, deployment.getMachines().size());
    }

    @Test
    public void pushAToB() {
        Architecture architecture = SampleArchitectures.pushAToB();
        Deployment deployment = Deployment.singleMachine("test",architecture);
        assertTrue(hasConnection(deployment, A, "1", CallType.PUSHES_TO, B, "1"));
    }

    @Test
    public void pushAToBToA() {
        Architecture architecture = SampleArchitectures.pushAToBToA();
        Deployment deployment = Deployment.singleMachine("test",architecture);
        assertEquals(1, deployment.getMachines().size());
        assertEquals(2, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, A, "1", CallType.PUSHES_TO, B, "1"));
        assertTrue(hasConnection(deployment, B, "1", CallType.PUSHES_TO, A, "1"));
    }

    @Test
    public void pushAToBToCToA() {
        Architecture architecture = SampleArchitectures.pushAToBToCToA();
        Deployment deployment = Deployment.singleMachine("test",architecture);
        assertEquals(1, deployment.getMachines().size());
        assertEquals(3, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, A, "1", CallType.PUSHES_TO, B, "1"));
        assertTrue(hasConnection(deployment, B, "1", CallType.PUSHES_TO, C, "1"));
        assertTrue(hasConnection(deployment, C, "1", CallType.PUSHES_TO, A, "1"));
    }

    @Test
    public void pushPullAToB() {
        Architecture architecture = SampleArchitectures.pushPullAToB();
        Deployment deployment = Deployment.singleMachine("test",architecture);
        assertEquals(1, deployment.getMachines().size());
        assertEquals(2, deployment.getConnections().size());
        assertTrue(hasConnection(deployment, A, "1", CallType.PUSHES_TO, B, "1"));
        assertTrue(hasConnection(deployment, B, "1", CallType.PULLED_FROM, A, "1"));
    }

    @Test
    public void complex() {
        Architecture architecture = SampleArchitectures.complex();
        Deployment deployment = Deployment.singleMachine("test",architecture);
        assertEquals(1, deployment.getMachines().size());
        assertEquals(9, deployment.getConnections().size());
    }
    

}
