package nl.aardbeitje.elm.analysis;

import static nl.aardbeitje.elm.SampleArchitectures.A;
import static nl.aardbeitje.elm.SampleArchitectures.B;
import static org.apache.commons.collections4.CollectionUtils.isEqualCollection;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.MachineConfig;
import nl.aardbeitje.elm.SampleArchitectures;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.Statefulness;

public class DeploymentResponsivenessTest {
    private int callId;

    @Test
    public void colocatedConsumersCanHaveDifferentResponsiveness() {
        final Service P = new Service("P", "Producer", Statefulness.STATELESS);
        final Service Y = new Service("Y", "Cached Consumer", Statefulness.STATELESS);
        final Service X = new Service("X", "Noncached Consumer", Statefulness.STATELESS);
        final Service CDB = new Service("Cdb", "Cache DB", Statefulness.STATEFUL);
        final Service CW = new Service("Cw", "Cache Writer", Statefulness.STATELESS);

        Architecture architecture = new Architecture();
        architecture.addService(P);
        architecture.addService(Y);
        architecture.addService(X);
        architecture.addService(CDB);
        architecture.addService(CW);

        architecture.addCall(++callId,CDB, CallType.PULLED_FROM, Y, false, "");
        architecture.addCall(++callId,CW, CallType.PUSHES_TO, CDB, false, "");
        architecture.addCall(++callId,P, CallType.PULLED_FROM, CW, false, "");
        architecture.addCall(++callId,P, CallType.PULLED_FROM, X, false, "");

        MachineConfig x1 = new MachineConfig(X, "1");
        MachineConfig y1 = new MachineConfig(Y, "1");
        MachineConfig cdb2 = new MachineConfig(CDB, "2");
        MachineConfig cw3 = new MachineConfig(CW, "3");
        MachineConfig p4 = new MachineConfig(P, "4");

        Deployment deployment = Deployment.customMachinePerService("cache vs noncache", architecture, x1, y1, cdb2, cw3, p4);

        Machine m1 = deployment.getMachineByName("1");
        Machine m2 = deployment.getMachineByName("2");
        Machine m3 = deployment.getMachineByName("3");
        Machine m4 = deployment.getMachineByName("4");

        DeploymentAnalysis da = new DeploymentAnalysis(deployment);
        // X's responsiveness should depend on P and not on the CDB
        assertTrue(da.delay(X, m1).contains(deployment.getServiceMachine(P, m4)));
        assertFalse(da.delay(X, m1).contains(deployment.getServiceMachine(CDB, m2)));

        // Y's responsiveness should depend on CDB and not on P
        assertFalse(da.delay(Y, m1).contains(deployment.getServiceMachine(P, m4)));
        assertTrue(da.delay(Y, m1).contains(deployment.getServiceMachine(CDB, m2)));
    }

    @Test
    public void pushPullAToBScaled() {
        Deployment d = SampleArchitectures.pushPullAToBHorizontallyScaled();

        Machine m1 = new Machine("1");
        Machine m2 = new Machine("2");
        Machine m3 = new Machine("3");

        DeploymentAnalysis da = new DeploymentAnalysis(d);

        ServiceMachine a1 = d.getServiceMachine(A, m1);
        ServiceMachine a2 = d.getServiceMachine(A, m2);
        ServiceMachine b3 = d.getServiceMachine(B, m3);
        assertTrue(isEqualCollection(Arrays.asList(a1, a2, b3), da.delay(A, m1)));
        assertTrue(isEqualCollection(Arrays.asList(a1, a2, b3), da.delay(A, m2)));
        assertTrue(isEqualCollection(Arrays.asList(a1, a2, b3), da.delay(B, m3)));
    }

}
