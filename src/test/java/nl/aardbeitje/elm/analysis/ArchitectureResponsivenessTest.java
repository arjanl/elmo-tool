package nl.aardbeitje.elm.analysis;

import static nl.aardbeitje.elm.SampleArchitectures.A;
import static nl.aardbeitje.elm.SampleArchitectures.B;
import static nl.aardbeitje.elm.SampleArchitectures.C;
import static nl.aardbeitje.elm.SampleArchitectures.D;
import static nl.aardbeitje.elm.SampleArchitectures.E;
import static nl.aardbeitje.elm.SampleArchitectures.G;
import static nl.aardbeitje.elm.SampleArchitectures.M;
import static nl.aardbeitje.elm.SampleArchitectures.R;
import static nl.aardbeitje.elm.SampleArchitectures.X;
import static nl.aardbeitje.elm.SampleArchitectures.Y;
import static org.apache.commons.collections4.CollectionUtils.isEqualCollection;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.SampleArchitectures;
import nl.aardbeitje.elm.analysis.ArchitectureAnalysis;

public class ArchitectureResponsivenessTest {
    private int callId = 0;
    
    @Test
    public void single() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.single());
        assertTrue(isEqualCollection(Collections.singletonList(M), a.responsiveness(M)));
    }

    @Test
    public void pushAToB() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToB());
        assertTrue(isEqualCollection(Arrays.asList(A), a.responsiveness(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.responsiveness(B)));
    }

    @Test
    public void pushAToBToA() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToBToA());
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.responsiveness(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.responsiveness(B)));
    }

    @Test
    public void pushAToBToCToA() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToBToCToA());
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.responsiveness(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.responsiveness(B)));
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.responsiveness(C)));
    }

    @Test
    public void pushPullAToB() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushPullAToB());
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.responsiveness(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.responsiveness(B)));
    }

    @Test
    public void complex() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.complex());
        assertTrue(isEqualCollection(Arrays.asList(E), a.responsiveness(E)));
        assertTrue(isEqualCollection(Arrays.asList(A,E), a.responsiveness(A)));
        assertTrue(isEqualCollection(Arrays.asList(E,A,B,G,D), a.responsiveness(D)));
        assertTrue(isEqualCollection(Arrays.asList(B), a.responsiveness(B)));
        assertTrue(isEqualCollection(Arrays.asList(G,E,A,B,D), a.responsiveness(G)));
        assertTrue(isEqualCollection(Arrays.asList(A,R,B,D,E,G,X,Y), a.responsiveness(R)));
        assertTrue(isEqualCollection(Arrays.asList(R,Y,E,A,B,G,D,X), a.responsiveness(X)));
        assertTrue(isEqualCollection(Arrays.asList(A,R,B,D,E,G,X,Y), a.responsiveness(Y)));
    }

    @Test
    public void notConAndNotStressImpliesNotResp() {
        Architecture architecture = new Architecture();
        architecture.addService(A);
        architecture.addService(B);
        architecture.addService(C);
        architecture.addService(D);
        architecture.addCall(++callId,B, CallType.PULLED_FROM, A, true, "");
        architecture.addCall(++callId,C, CallType.PUSHES_TO, B, true, "");
        architecture.addCall(++callId,C, CallType.PULLED_FROM, D, true, "");

        ArchitectureAnalysis a = new ArchitectureAnalysis(architecture);
        // proposition does not hold... as proved by Sung
        assertFalse(a.consistency(A).contains(D));
        assertFalse(a.stress(A).contains(D));
        assertTrue(a.responsiveness(A).contains(D));
        
    }
    
}
