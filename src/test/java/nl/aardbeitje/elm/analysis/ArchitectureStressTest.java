package nl.aardbeitje.elm.analysis;
import static nl.aardbeitje.elm.SampleArchitectures.A;
import static nl.aardbeitje.elm.SampleArchitectures.B;
import static nl.aardbeitje.elm.SampleArchitectures.C;
import static nl.aardbeitje.elm.SampleArchitectures.D;
import static nl.aardbeitje.elm.SampleArchitectures.E;
import static nl.aardbeitje.elm.SampleArchitectures.G;
import static nl.aardbeitje.elm.SampleArchitectures.M;
import static nl.aardbeitje.elm.SampleArchitectures.R;
import static nl.aardbeitje.elm.SampleArchitectures.X;
import static nl.aardbeitje.elm.SampleArchitectures.Y;
import static org.apache.commons.collections4.CollectionUtils.isEqualCollection;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import nl.aardbeitje.elm.SampleArchitectures;
import nl.aardbeitje.elm.analysis.ArchitectureAnalysis;

public class ArchitectureStressTest {
    
    @Test
    public void single() {
        ArchitectureAnalysis a = new ArchitectureAnalysis( SampleArchitectures.single());
        assertTrue(isEqualCollection(Collections.singletonList(M), a.stress(M)));
    }

    @Test
    public void pushAToB() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToB());
        assertTrue(isEqualCollection(Collections.singletonList(A), a.stress(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.stress(B)));
    }

    @Test
    public void pushAToBToA() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToBToA());
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.stress(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.stress(B)));
    }

    @Test
    public void pushAToBToCToA() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushAToBToCToA());
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.stress(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.stress(B)));
        assertTrue(isEqualCollection(Arrays.asList(A,B,C), a.stress(C)));
    }

    @Test
    public void pushPullAToB() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.pushPullAToB());
        assertTrue(isEqualCollection(Arrays.asList(A), a.stress(A)));
        assertTrue(isEqualCollection(Arrays.asList(A,B), a.stress(B)));
    }

    @Test
    public void complex() {
        ArchitectureAnalysis a = new ArchitectureAnalysis(SampleArchitectures.complex());
        assertTrue(isEqualCollection(Arrays.asList(E), a.stress(E)));
        assertTrue(isEqualCollection(Arrays.asList(A,E), a.stress(A)));
        assertTrue(isEqualCollection(Arrays.asList(E,A,B,G,D), a.stress(D)));
        assertTrue(isEqualCollection(Arrays.asList(B), a.stress(B)));
        assertTrue(isEqualCollection(Arrays.asList(G), a.stress(G)));
        assertTrue(isEqualCollection(Arrays.asList(R,X,E,A,B,G,D), a.stress(R)));
        assertTrue(isEqualCollection(Arrays.asList(X), a.stress(X)));
        assertTrue(isEqualCollection(Arrays.asList(Y,R,E,A,B,G,D,X), a.stress(Y)));
    }
    
}
