package nl.aardbeitje.elm.analysis;

import static nl.aardbeitje.elm.SampleArchitectures.A;
import static nl.aardbeitje.elm.SampleArchitectures.B;
import static nl.aardbeitje.elm.SampleArchitectures.C;
import static nl.aardbeitje.elm.SampleArchitectures.D;
import static nl.aardbeitje.elm.SampleArchitectures.E;
import static nl.aardbeitje.elm.SampleArchitectures.F;
import static nl.aardbeitje.elm.SampleArchitectures.G;
import static nl.aardbeitje.elm.SampleArchitectures.H;
import static org.apache.commons.collections4.CollectionUtils.isEqualCollection;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.SampleArchitectures;
import nl.aardbeitje.elm.ServiceMachine;

public class DeploymentStressTest {

    @Test
    public void gbdStress() {
        Deployment d = SampleArchitectures.gbdCodeployed();
        Machine m1 = d.getMachineByName("1");
        Machine m2 = d.getMachineByName("2");
        Machine m3 = d.getMachineByName("3");
        Machine m4 = d.getMachineByName("4");

        ServiceMachine g1 = d.getServiceMachine(G,m1);
        ServiceMachine g2 = d.getServiceMachine(G,m2);
        ServiceMachine b1 = d.getServiceMachine(B,m1);
        ServiceMachine b2 = d.getServiceMachine(B,m2);
        ServiceMachine d3 = d.getServiceMachine(D,m3);
        ServiceMachine d4 = d.getServiceMachine(D,m4);

        DeploymentAnalysis da = new DeploymentAnalysis(d);

        System.out.println("stress (g1): " + da.stress(g1));
        System.out.println("stress (g2): " + da.stress(g2));
        System.out.println("stress (b1): " + da.stress(b1));
        System.out.println("stress (b2): " + da.stress(b2));
        System.out.println("stress (d3): " + da.stress(d3));
        System.out.println("stress (d4): " + da.stress(d4));

        // note that these are not pruned so g1 also calls b2 etc
        assertTrue(isEqualCollection(da.stress(g1), Arrays.asList(g1, g2, b1)));
        assertTrue(isEqualCollection(da.stress(g2), Arrays.asList(g2, g1, b2)));
        assertTrue(isEqualCollection(da.stress(b1), Arrays.asList(g1, g2, b1)));
        assertTrue(isEqualCollection(da.stress(b2), Arrays.asList(g2, g1, b2)));
        assertTrue(isEqualCollection(da.stress(d3), Arrays.asList(g1, g2, b1, b2, d3)));
        assertTrue(isEqualCollection(da.stress(d4), Arrays.asList(g1, g2, b1, b2, d4)));
    }

    @Test
    public void insensitiveStress() {
        Deployment d = SampleArchitectures.insensitiveStressDeployment();
        Machine m1 = d.getMachineByName("1");
        Machine m2 = d.getMachineByName("2");
        Machine m3 = d.getMachineByName("3");
        Machine m4 = d.getMachineByName("4");
        Machine m5 = d.getMachineByName("5");
        Machine m6 = d.getMachineByName("6");

        ServiceMachine a1 = d.getServiceMachine(A,m1);
        ServiceMachine b1 = d.getServiceMachine(B,m1);
        ServiceMachine e2 = d.getServiceMachine(E,m2);
        ServiceMachine h6 = d.getServiceMachine(H,m6);
        ServiceMachine f4 = d.getServiceMachine(F,m4);
        ServiceMachine c3 = d.getServiceMachine(C,m3);
        ServiceMachine g5 = d.getServiceMachine(G,m5);

        DeploymentAnalysis da = new DeploymentAnalysis(d);

        // A-m1 should have same stress as B-m1 since they are on the same
        // machine
        assertTrue(isEqualCollection(da.stress(a1), da.stress(b1)));

        // A-m1 (and m1 in general) should not receive stress from E or H since
        // they don't push to m1 in any way although they share machines along
        // the path
        assertFalse(da.stress(a1).contains(e2));
        assertFalse(da.stress(a1).contains(h6));

        // A-m1 should receive stress from F-m4 since it pushes to A
        assertTrue(da.stress(a1).contains(f4));

        // A-m1 should receive stress from G-m5 since it pushes to B
        assertTrue(da.stress(a1).contains(c3));
        assertTrue(da.stress(a1).contains(g5));

    }

    @Test
    public void pushPullAToBScaled() {
        Deployment d = SampleArchitectures.pushPullAToBHorizontallyScaled();

        Machine m1 = new Machine("1");
        Machine m2 = new Machine("2");
        Machine m3 = new Machine("3");

        ServiceMachine a1 = d.getServiceMachine(A,m1);
        ServiceMachine a2 = d.getServiceMachine(A,m2);
        ServiceMachine b3 = d.getServiceMachine(B,m3);

        DeploymentAnalysis da = new DeploymentAnalysis(d);

        assertTrue(isEqualCollection(Arrays.asList(a1), da.stress(A, m1)));
        assertTrue(isEqualCollection(Arrays.asList(a2), da.stress(A, m2)));
        assertTrue(isEqualCollection(Arrays.asList(a1, a2, b3), da.stress(B, m3)));
    }

}
