package nl.aardbeitje.elm.io;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.SampleArchitectures;

@Ignore // rewrote file parsing
public class ReadWriteDeployments {
    
    @Test
    public void pushAToBToCToASingleMachine() throws IOException {
        Architecture architecture = SampleArchitectures.pushAToBToCToA();
        Deployment deploymentIn = Deployment.singleMachine("test",architecture);
        test(deploymentIn);
    }

    @Test
    public void pushAToBToCToAMachinePerService() throws IOException {
        Architecture architecture = SampleArchitectures.pushAToBToCToA();
        Deployment deploymentIn = Deployment.machinePerService("test",architecture);
        test(deploymentIn);
    }

    @Test
    public void complexSingleMachine() throws IOException {
        Architecture architecture = SampleArchitectures.complex();
        Deployment deploymentIn = Deployment.singleMachine("test",architecture);
        test(deploymentIn);
    }

    @Test
    public void complexMachinePerService() throws IOException {
        Architecture architecture = SampleArchitectures.complex();
        Deployment deploymentIn = Deployment.machinePerService("test",architecture);
        test(deploymentIn);
    }

    @Test
    public void simplePartitioningMachinePerService() throws IOException {
        Architecture architecture = SampleArchitectures.simplePartitioning();
        Deployment deploymentIn = Deployment.machinePerService("test",architecture);
        test(deploymentIn);
    }

    @Test
    public void simplePartitioningSingleMachine() throws IOException {
        Architecture architecture = SampleArchitectures.simplePartitioning();
        Deployment deploymentIn = Deployment.machinePerService("test",architecture);
        test(deploymentIn);
    }

    @Test
    public void simplePartitioningExplicitDeployed() throws IOException {
        Deployment deploymentIn = SampleArchitectures.simplePartitioningDeployed(5);
        test(deploymentIn);
    }

    private void test(Deployment deploymentIn) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DeploymentWriter w = new DeploymentWriter(bos);
        w.write(deploymentIn);
        bos.flush();
        
        byte[] result = bos.toByteArray();
        
        File tmpFile = new File("/tmp/test.elm");        
        FileOutputStream fos = new FileOutputStream(tmpFile);
        fos.write(result);
        fos.close();
        
        ByteArrayInputStream bin = new ByteArrayInputStream(result);
        DeploymentReader r = new DeploymentReader("test", bin);
        
        ParsedDeployment deploymentOut = r.read();
        
        assertEquals(deploymentIn, deploymentOut.getDeployment());
    }
}

