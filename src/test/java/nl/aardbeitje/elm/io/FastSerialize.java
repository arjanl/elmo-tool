package nl.aardbeitje.elm.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.io.DeploymentSerializer.SerializedDeployment;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)

public class FastSerialize {

    private Deployment deployment;

    public static void main(String[] args) throws Exception {
        Options opt = new OptionsBuilder().include(FastSerialize.class.getSimpleName()).forks(1).build();

        new Runner(opt).run();

        System.out.println("json: size of object: " + DeploymentSerializer.serialize(DeploymentSerializer.Mode.JSON, getExampleDeployment()).serialized.length);
        System.out.println("kryo: size of object: " + DeploymentSerializer.serialize(DeploymentSerializer.Mode.KRYO, getExampleDeployment()).serialized.length);

    }

    private static Deployment getExampleDeployment() throws IOException {
        DeploymentReader dr = new DeploymentReader("test", new FileInputStream(new File("/Users/arjanl/Documents/workspace/elmo-tool/src/test/resources/paper/psp-layout.elm")));
        ParsedDeployment pd = dr.read();
        return pd.getDeployment();

    }

    @Setup
    public void setUp() throws IOException {
        deployment = getExampleDeployment();
    }

//    @Benchmark
//    public SerializedDeployment testJson() throws IOException {
//        return DeploymentSerializer.serialize(DeploymentSerializer.Mode.JSON, deployment);
//    }

    @Benchmark
    public SerializedDeployment testKryo() throws IOException {
        return DeploymentSerializer.serialize(DeploymentSerializer.Mode.KRYO, deployment);
    }
}
