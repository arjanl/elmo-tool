package nl.aardbeitje.elm;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

import nl.aardbeitje.elm.io.DeploymentReader;
import nl.aardbeitje.elm.io.ParsedDeployment;
import nl.aardbeitje.elm.refactorings.RefactorGoal;
import nl.aardbeitje.elm.refactorings.RefactorWizard;
import nl.aardbeitje.elm.refactorings.RefactorWizardFactory;

public class TestUtil {
    public static boolean hasConnection(Deployment deployment, Service s1, String n1, CallType callType, Service s2, String n2) {
        Machine m1 = deployment.getMachineByName(n1);
        Machine m2= deployment.getMachineByName(n2);
        deployment.getConnections(m1, m2).stream().forEach(c->System.out.println(c));
        return deployment.getConnections(m1, m2).stream().anyMatch( 
                c-> c.getCallType()==callType && c.getCall().getSource().equals(s1) && c.getCall().getTarget().equals(s2)
            );
    }

    
    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException, ExecutionException {
        ParsedDeployment pd = new DeploymentReader("performance test", new FileInputStream("/Users/arjanl/Documents/workspace/elm/src/test/resources/edicircle/current-data.elm")).read();
        Deployment deployment = new Deployment("performance test", pd.getDeployment());
     
        RefactorGoal goal = new RefactorGoal(deployment.getArchitecture().getService("download"), deployment.getArchitecture().getService("bt"), true);
        RefactorWizard wizard = RefactorWizardFactory.createWizard(deployment, Collections.singletonList(goal));
        
        wizard.run();
        wizard.get();
    }
}
