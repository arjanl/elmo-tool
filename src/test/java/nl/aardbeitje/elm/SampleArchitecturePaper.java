package nl.aardbeitje.elm;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

public class SampleArchitecturePaper {
    public static final String D = "D"; 
    public static final String P = "P"; 
    public static final String C = "C"; 
    public static final String A = "A"; 
    public static final String O = "O"; 
    
    public static Service createD() {
    	return new Service(D, "Database", Statefulness.STATEFUL, null, Collections.emptySet(), Collections.emptySet());
    }
    public static Service createP() {
    	return new Service(P, "Price", Statefulness.STATELESS, null, Collections.singleton("price"), Collections.singleton("prod"));
    }
    public static Service createC() {
    	return new Service(C, "Checkout", Statefulness.PARTITIONED, "user", Collections.singleton("order"), new HashSet<>(Arrays.asList("price", "prod")));
    }
    public static Service createA() {
    	return new Service(A, "Accounting", Statefulness.STATEFUL, null, Collections.emptySet(), Collections.singleton("order"));
    }
    public static Service createO() {
    	return new Service(O, "Office", Statefulness.STATEFUL, null, Collections.singleton("prod"), Collections.emptySet());
    }
    
    public static Architecture initialArchitecture() {
        int callId = 0;
        Architecture architecture = new Architecture();

        Service d = createD();
        Service p = createP();
        Service c = createC();
        Service a = createA();
        Service o = createO();
		architecture.addService(d);
		architecture.addService(p);
		architecture.addService(c);
		architecture.addService(a);
		architecture.addService(o);

        architecture.addCall(++callId,d, CallType.PULLED_FROM, p, true, "prod");
        architecture.addCall(++callId,p, CallType.PULLED_FROM, c, true, "price");
        architecture.addCall(++callId,d, CallType.PULLED_FROM, c, true, "prod");
        architecture.addCall(++callId,c, CallType.PUSHES_TO, d, true, "order");
        architecture.addCall(++callId,d, CallType.PULLED_FROM, a, true, "order");
        architecture.addCall(++callId,o, CallType.PUSHES_TO, d, true, "prod");

        return architecture;
    }

    public static Deployment initialDeployment() {
        Architecture architecture = initialArchitecture();
        MachineConfig d1 = new MachineConfig(D, "1");
        MachineConfig p1 = new MachineConfig(P, "2");
        MachineConfig c1 = new MachineConfig(C, "3");
        MachineConfig c2 = new MachineConfig(C, "4");
        MachineConfig o1 = new MachineConfig(O, "5");
        MachineConfig a1 = new MachineConfig(A, "6");
        
        return Deployment.customMachinePerService("initial", architecture, d1,p1,c1,c2,o1,a1);
    }

}
