package nl.aardbeitje.elm.refactorings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.refactorings.queue.ArtemisQueue;
import nl.aardbeitje.elm.refactorings.queue.ElmoQueue;

/**
 * Generator for refactor paths.
 * 
 * Offer it an evaluated refactor path (which is not a solution yet) and it will
 * produce new refactor paths to try out.
 * 
 * The offered evaluated paths are placed on the evaluatedPaths queue which is
 * an embedded ActiveMQ Artemis queue that will use paging if memory becomes
 * low.
 * 
 * The generatedPaths is an memory based queue that contains the next paths to
 * try. These are generated in a batch from the top of the evaluatedPaths queue.
 */
class Generator implements Iterator<List<RefactorStrategy>> {

    private final Deployment deployment;
    private final Queue<List<RefactorStrategy>> generatedPaths = new ConcurrentLinkedQueue<>();
    private final int maxDepth;
    private final ElmoQueue evaluatedPathsQueue;

    public Generator(Deployment deployment, int maxDepth) {
        this.deployment = deployment;
        this.maxDepth = maxDepth;
        this.evaluatedPathsQueue = new ArtemisQueue();

        generate(Collections.emptyList(), deployment);
    }

    public void add(List<RefactorStrategy> evaluatedPath) {
        evaluatedPathsQueue.add(evaluatedPath);
    }

    @Override
    public boolean hasNext() {
        if (generatedPaths.isEmpty()) {
            generate();
        }
        return !generatedPaths.isEmpty();
    }

    @Override
    public List<RefactorStrategy> next() {
        if (generatedPaths.isEmpty()) {
            generate();
        }
        if (generatedPaths.isEmpty()) {
            throw new NoSuchElementException();
        }
        return generatedPaths.poll();
    }

    private void generate() {
        List<RefactorStrategy> evaluatedPath = evaluatedPathsQueue.poll();
        if (evaluatedPath == null) {
            return;
        }

        if (maxDepth!=RefactorWizardStreamingMemoryOptimizedBreadthFirst.INFINITE && evaluatedPath.size() >= maxDepth) {
            System.out.println("polled path of length " + evaluatedPath.size() + " but it is already at maxDepth: " + evaluatedPath);
            return;
        }

        RefactorWizardStreamingMemoryOptimizedBreadthFirst.apply(deployment, evaluatedPath);
        generate(evaluatedPath, deployment);
        RefactorWizardStreamingMemoryOptimizedBreadthFirst.reverse(deployment, evaluatedPath);
    }

    private void generate(List<RefactorStrategy> evaluatedPath, Deployment deployment) {
        Set<RefactorStrategy> strategies = RefactorStrategyFactory.generateStrategies(deployment);
        strategies.forEach(s -> {
            List<RefactorStrategy> newPath = new ArrayList<>(evaluatedPath);
            newPath.add(s);
            generatedPaths.offer(newPath);
        });
    }
}