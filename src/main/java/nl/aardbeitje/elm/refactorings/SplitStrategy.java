package nl.aardbeitje.elm.refactorings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.analysis.ArchitectureAnalysis;
import nl.aardbeitje.elm.refactorings.PartitionSet.Partition;

public class SplitStrategy extends RefactorStrategy {

    private final Service serviceRef;
    private final Partition<String> partition;

    // to be able to reverse
    private Service right;
    private List<ServiceMachine> addedServiceMachines = new ArrayList<>();
    private List<Connection> addedConnections = new ArrayList<>();
    private List<Connection> removedConnections = new ArrayList<>();
    private List<Call> addedCalls = new ArrayList<>();
    private List<Call> removedCalls = new ArrayList<>();

    public SplitStrategy(Service service, Partition<String> partition) {
        this.serviceRef = service;
        this.partition = partition;
    }

    @Override
    public void reverse(Deployment d) {
        if (addedServiceMachines.isEmpty() || addedConnections.isEmpty()) {
            throw new IllegalStateException("No reversable state found, is this strategy not yet applied or already reversed?");
        }
        
        Service service = serviceRef.recreate(d);
        service.getProduces().addAll(right.getProduces());
        service.getProduces().addAll(service.getProduces());
        service.getConsumes().addAll(right.getConsumes());
        service.getConsumes().addAll(service.getConsumes());

        d.getArchitecture().removeService(right);
        addedServiceMachines.forEach(d::remove);
        addedCalls.forEach(d.getArchitecture()::removeCall);
        removedCalls.forEach(d.getArchitecture()::addCall);
        addedConnections.forEach(d::removeConnection);
        removedConnections.forEach(d::addConnection);

        clearReverse();
    }

    private void clearReverse() {
        right = null;
        addedServiceMachines = new ArrayList<>();
        addedConnections = new ArrayList<>();
        removedConnections = new ArrayList<>();
        addedCalls = new ArrayList<>();
        removedCalls = new ArrayList<>();
    }

    @Override
    public void apply(Deployment d) {
        Service service = serviceRef.recreate(d);
        right = splitService(service);

        // rewire calls
        partition.getRight().forEach(p -> moveCalls(d, p, service, right));
        d.getArchitecture().addService(right);

        // rewire deployment
        cloneServiceMachines(d, service, right);
    }

    private void cloneServiceMachines(Deployment d, Service left, Service right) {
        Set<ServiceMachine> leftMachines = d.getServiceMachines(left);
        for (ServiceMachine leftSm : leftMachines) {
            ServiceMachine rightSm = new ServiceMachine(right, leftSm.getMachine(), d);
            d.add(rightSm);
            addedServiceMachines.add(rightSm);
            moveConnections(d, leftSm, rightSm);
        }
    }

    private void moveConnections(Deployment d, ServiceMachine leftSm, ServiceMachine rightSm) {
        Set<Call> sourceCalls = d.getArchitecture().getCalls().stream().filter(c -> c.getSource().equals(rightSm.getService())).collect(Collectors.toSet());

        Collection<Connection> originalConnections = new HashSet<>(d.getConnections());
        // move all connections that belong to calls to the cloned service
        originalConnections.stream().filter(c -> c.getSource().equals(leftSm)).filter(c -> sourceCalls.contains(c.getCall())).forEach(c -> {
            d.removeConnection(c);
            removedConnections.add(c);
            addedConnections.add(d.addConnection(rightSm, c.getCall(), c.getTarget(), c.getOptionality()));
        });

        Set<Call> targetCalls = d.getArchitecture().getCalls().stream().filter(c -> c.getTarget().equals(rightSm.getService())).collect(Collectors.toSet());

        originalConnections = new HashSet<>(d.getConnections());
        // move all connections that belong to calls to the cloned service
        originalConnections.stream().filter(c -> c.getTarget().equals(leftSm)).filter(c -> targetCalls.contains(c.getCall())).forEach(c -> {
            d.removeConnection(c);
            removedConnections.add(c);
            addedConnections.add(d.addConnection(c.getSource(), c.getCall(), rightSm, c.getOptionality()));
        });
    }

    private Service splitService(Service service) {
    	
        Service right = new Service(service.getName() + "'", service.getName(), service.getDescription(), service.getStatefulness(), service.getPartitioning());

        Set<String> originalProduces = new HashSet<>(service.getProduces());
        Set<String> originalConsumes = new HashSet<>(service.getConsumes());
        
        // add all originals and remove the moved state + produces
        service.getProduces().addAll(originalProduces);
        service.getProduces().removeAll(partition.getRight());
        right.getProduces().addAll(originalProduces);
        right.getProduces().removeAll(partition.getLeft());

        // add all originals and remove the moved state + consumes
        service.getConsumes().addAll(originalConsumes);
        service.getConsumes().removeAll(partition.getRight());
        right.getConsumes().addAll(originalConsumes);
        right.getConsumes().removeAll(partition.getLeft());

        return right;

    }

    private void moveCalls(Deployment d, String data, Service s, Service t) {
        Collection<Call> originalCalls = new HashSet<>(d.getArchitecture().getCalls());
        // move all calls that have data as source
        originalCalls.stream().filter(c -> c.getData().equals(data)).filter(c -> c.getSource().equals(s)).forEach(c -> {
            d.getArchitecture().removeCall(c);
            removedCalls.add(c);
            addedCalls.add(d.getArchitecture().addCall(c.getId(), t, c.getType(), c.getTarget(), c.isRoutable(), c.getData()));
        });

        // move all calls that have data as target
        originalCalls.stream().filter(c -> c.getData().equals(data)).filter(c -> c.getTarget().equals(s)).forEach(c -> {
            d.getArchitecture().removeCall(c);
            removedCalls.add(c);
            addedCalls.add(d.getArchitecture().addCall(c.getId(), c.getSource(), c.getType(), t, c.isRoutable(), c.getData()));
        });

    }

    @Override
    public String toString() {
        return "Split service '" + serviceRef.getName() + "' data into " + partition.getLeft() + " and " + partition.getRight();
    }

    public static Collection<RefactorAlternatives> generate(Deployment deployment, Service service) {
        Set<RefactorAlternatives> refactorings = new HashSet<>();

        ArchitectureAnalysis aa = new ArchitectureAnalysis(deployment.getArchitecture());

        // might be a bit much splitting here
        Set<String> states = new HashSet<>(aa.getStates(service));
        states.addAll(service.getProduces());
        states.addAll(service.getConsumes());
		if (states.size() > 1) {
            Set<Partition<String>> partitions = PartitionSet.allPartitions(states);
            for (Partition<String> p : partitions) {
                if (!p.getLeft().isEmpty() && !p.getRight().isEmpty()) {
                    RefactorAlternatives alternatives = new RefactorAlternatives();
                    alternatives.addStrategy(new SplitStrategy(service, p));
                    refactorings.add(alternatives);
                }
            }
        }

        return refactorings;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((partition == null) ? 0 : partition.hashCode());
        result = prime * result + ((serviceRef == null) ? 0 : serviceRef.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SplitStrategy other = (SplitStrategy) obj;
        if (partition == null) {
            if (other.partition != null)
                return false;
        } else if (!partition.equals(other.partition))
            return false;
        if (serviceRef == null) {
            if (other.serviceRef != null)
                return false;
        } else if (!serviceRef.equals(other.serviceRef))
            return false;
        return true;
    }

}
