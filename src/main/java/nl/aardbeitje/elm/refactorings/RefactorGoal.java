package nl.aardbeitje.elm.refactorings;

import nl.aardbeitje.elm.Service;

public class RefactorGoal {

    private final Service service;
    private final Service sensitivity;
    private final boolean makeInsensitive;

    public RefactorGoal(Service service, Service sensitivity, boolean makeInsensitive) {
        this.service = service;
        this.sensitivity = sensitivity;
        this.makeInsensitive = makeInsensitive;
    }

    public Service getService() {
        return service;
    }

    public Service getSensitivity() {
        return sensitivity;
    }

    public boolean isMakeInsensitive() {
        return makeInsensitive;
    }

    public String toString() {
        if (makeInsensitive) {
            return "Make '" + service + "' insensitive to '" + sensitivity + "'";
        } else {
            return "Make '" + service + "' voluntarily sensitive to '" + sensitivity + "'";
        }
    }

}
