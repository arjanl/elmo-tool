package nl.aardbeitje.elm.refactorings;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;

/**
 * Deploys a new instance of a service on a machine.
 */
public class NewInstanceStrategy extends RefactorStrategy {

    private final Machine machineRef;
    private final Service serviceRef;

    private ServiceMachine newServiceMachine;

    public NewInstanceStrategy(Service service, Machine machine) {
        this.serviceRef = service;
        this.machineRef = machine;
    }

    public NewInstanceStrategy(Service service) {
        this.serviceRef = service;
        this.machineRef = null;
    }

    @Override
    public void reverse(Deployment d) {
        if (newServiceMachine == null) {
            throw new IllegalStateException("No reversable state found, is this strategy not yet applied or already reversed?");
        }

        d.remove(newServiceMachine);
        clearReverse();
    }

    private void clearReverse() {
        newServiceMachine = null;
    }

    @Override
    public void apply(Deployment d) {
        Service service = serviceRef.recreate(d);
        if (service==null) {
            throw new IllegalArgumentException("cannot find service " + service + " to add instance for");
        }
        Machine machine = (machineRef == null ? null : machineRef.recreate(d));
        newServiceMachine = new ServiceMachine(service, (machine == null ? d.newMachine() : machine), d);
        d.addAndConnect(newServiceMachine);
    }

    @Override
    public String toString() {
        if (machineRef == null) {
            return "New instance of '" + serviceRef.getName() + "' on new machine";
        } else {
            return "New instance of '" + serviceRef.getName() + "' on " + machineRef.getName();
        }
    }

    public static Collection<RefactorAlternatives> generate(Deployment deployment, Service service) {
        RefactorAlternatives alternatives = new RefactorAlternatives();
        NewInstanceStrategy newMachine = new NewInstanceStrategy(service);

        alternatives.addStrategy(newMachine);

        Set<Machine> colocations = new HashSet<>(deployment.getMachines());
        colocations.removeAll(deployment.getMachines(service));

        colocations.forEach(machine -> {
            if (!deployment.getServices(machine).contains(service)) {
                NewInstanceStrategy nis = new NewInstanceStrategy(service, machine);
                alternatives.addStrategy(nis);
            }
        });

        return Collections.singleton(alternatives);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((machineRef == null) ? 0 : machineRef.hashCode());
        result = prime * result + ((serviceRef == null) ? 0 : serviceRef.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NewInstanceStrategy other = (NewInstanceStrategy) obj;
        if (machineRef == null) {
            if (other.machineRef != null)
                return false;
        } else if (!machineRef.equals(other.machineRef))
            return false;
        if (serviceRef == null) {
            if (other.serviceRef != null)
                return false;
        } else if (!serviceRef.equals(other.serviceRef))
            return false;
        return true;
    }

}
