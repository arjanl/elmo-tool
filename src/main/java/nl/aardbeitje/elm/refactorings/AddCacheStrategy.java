package nl.aardbeitje.elm.refactorings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Optionality;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.Statefulness;

/**
 * Refactoring that adds a cache on a pull instruction. The target of
 * information (the service pulling another one) now becomes insensitive to the
 * source of information. The cache consists of the cache database and a cache
 * writer (eager cache), both deployed on the target's machine.
 */
public class AddCacheStrategy extends RefactorStrategy {
    private final Call callRef;

    // store for reversal
    private Service cacheDb;
    private Service cacheWriter;
    private List<Connection> removedConnections = new ArrayList<>();

    public AddCacheStrategy(Call call) {
        this.callRef = call;
    }

    @Override
    public void reverse(Deployment d) {
        if (cacheDb == null || cacheWriter == null) {
            throw new IllegalStateException("No reversable state found, is this strategy not yet applied or already reversed?");
        }
        removeService(d, cacheDb);
        removeService(d, cacheWriter);

        d.getArchitecture().addCall(callRef);
        for (Connection c : removedConnections) {
            d.addConnection(c);
        }

        clearReverse();
    }

    private void clearReverse() {
        cacheDb = null;
        cacheWriter = null;
        removedConnections = new ArrayList<>();
    }

    @Override
    public void apply(Deployment d) {
        Call call = callRef.recreate(d);
        // create new services
        Service service = call.getTarget();
        cacheDb = new Service(service.getName() + "_" + call.getSourceName() + "_cd", service.getName(), service.getDescription(), Statefulness.STATEFUL, service.getPartitioning());
        cacheWriter = new Service(service.getName() + "_" + call.getSourceName() + "_cw", service.getName(), service.getDescription(), Statefulness.STATELESS, service.getPartitioning());

        // rewire services
        d.getArchitecture().removeCall(call);
        Call callService = new Call(call.getId(), cacheDb, service, CallType.PULLED_FROM, true, call.getData());
        d.getArchitecture().addCall(callService);
        Call callDb = new Call(d.getArchitecture().getNewCallId(), cacheWriter, cacheDb, CallType.PUSHES_TO, true, call.getData());
        d.getArchitecture().addCall(callDb);
        Call callWriter = new Call(d.getArchitecture().getNewCallId(), call.getSource(), cacheWriter, CallType.PULLED_FROM, true, call.getData());
        d.getArchitecture().addCall(callWriter);

        // deploy services
        d.getConnections(call).forEach(connection -> {
            Machine machine = connection.getTarget().getMachine();
            ServiceMachine smService = d.getServiceMachine(service, machine);
            ServiceMachine smCacheDb = new ServiceMachine(cacheDb, machine,d);
            ServiceMachine smCacheWriter = new ServiceMachine(cacheWriter, machine,d);

            removedConnections.add(connection);
            d.removeConnection(connection);
            d.addConnection(smCacheDb, callService, smService, Optionality.COMPULSORY);
            d.addConnection(smCacheWriter, callDb, smCacheDb, Optionality.COMPULSORY);
            d.addConnection(connection.getSource(), callWriter, smCacheWriter, connection.getOptionality());
        });
    }

    @Override
    public String toString() {
        return "Add cache on call '" + callRef.toString() + "'";
    }

    public static Collection<RefactorAlternatives> generate(Deployment deployment, Service service) {
        Collection<RefactorAlternatives> refactorings = new ArrayList<>();

        deployment.getArchitecture().getCalls().stream().filter(Call::isPulledFrom).filter(call -> call.getTarget().equals(service)).forEach(call -> {
            RefactorAlternatives alternatives = new RefactorAlternatives();
            alternatives.addStrategy(new AddCacheStrategy(call));
            refactorings.add(alternatives);
        });
        return refactorings;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((callRef == null) ? 0 : callRef.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AddCacheStrategy other = (AddCacheStrategy) obj;
        if (callRef == null) {
            if (other.callRef != null)
                return false;
        } else if (!callRef.equals(other.callRef))
            return false;
        return true;
    }

}
