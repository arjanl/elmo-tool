package nl.aardbeitje.elm.refactorings;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.analysis.DeploymentAnalysis;
import nl.aardbeitje.elm.io.DeploymentSerializer;
import nl.aardbeitje.elm.io.DeploymentSerializer.SerializedDeployment;

/**
 * A breadth-first search over all possible refactorings until the refactorGoal
 * is reached.
 */
public class RefactorWizardStreamingMemoryOptimizedBreadthFirst extends RefactorWizard {
    static final int INFINITE = Integer.MIN_VALUE;

    private static final boolean REMEMBER_EVALUATED = true;

    private final int maxEvaluationDepth;
    private final Deployment deployment;
    private final List<RefactorGoal> goals;
    private int evaluationCount;

    private Set<List<RefactorStrategy>> solutions = new LinkedHashSet<>();
    private Set<SerializedDeployment> evaluated = new LinkedHashSet<>();

    public RefactorWizardStreamingMemoryOptimizedBreadthFirst(Deployment deployment, List<RefactorGoal> goals) {
        this(deployment, goals, INFINITE);
    }

    public RefactorWizardStreamingMemoryOptimizedBreadthFirst(Deployment deployment, List<RefactorGoal> goals, int maxEvaluationDepth) {
        this.deployment = deployment;
        this.goals = goals;
        this.maxEvaluationDepth = maxEvaluationDepth;
        this.evaluationCount = 0;
    }

    @Override
    protected Void call() throws Exception {
        long start = System.currentTimeMillis();

        evaluate(maxEvaluationDepth);

        long end = System.currentTimeMillis();
        System.out.println("millis for refactor search: " + (end - start));
        System.out.println("# evaluations: " + evaluationCount);
        return null;
    }

    private void evaluate(int maxDepth) {
        Generator generator = new Generator(deployment, maxDepth);

        generator.forEachRemaining(p -> {
            if (isCancelled()) {
                return;
            }

            if (!blacklisted(solutions, p)) {
                apply(deployment, p);
                evaluationCount++;

                if (evaluationCount % 1000 == 0)
                    updateMessage("Current depth: " + p.size() + ". Evaluated " + evaluationCount + ". \n" + solutions.size() + " solutions found.");

                if (!alreadyEvaluated(deployment)) {
                    if (isAchieved(deployment)) {
                        solutions.add(new ArrayList<>(p));
                    } else {
                        if (maxDepth == INFINITE || (maxDepth != INFINITE && p.size() < maxDepth)) {
                            generator.add(p);
                        }
                    }
                }
                reverse(deployment, p);
            }
        });
        
        System.out.println("done");
    }

    private boolean alreadyEvaluated(Deployment d) {
        if (REMEMBER_EVALUATED) {
            try {

                SerializedDeployment result = DeploymentSerializer.serialize(d);
                if (evaluated.contains(result)) {
                    return true;
                } else {
                    evaluated.add(result);
                    return false;
                }
            } catch (Exception e) {
                System.err.println("Error converting deployment to string: " + e.getMessage());
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean isAchieved(Deployment d) {
        DeploymentAnalysis a = new DeploymentAnalysis(d);

        for (RefactorGoal g : goals) {
            if (!isAchieved(g, a, d)) {
                return false;
            }
        }

        return true;
    }

    private boolean isAchieved(RefactorGoal goal, DeploymentAnalysis a, Deployment d) {
        if (goal.isMakeInsensitive()) {
            return a.insensitive(goal.getService(), goal.getSensitivity());
        } else {
            return a.voluntarilySensitive(goal.getService(), goal.getSensitivity());
        }
    }

    /**
     * Evaluates a refactoring path by modifying the original deployment. Need
     * to call reverse() to undo the changes.
     * 
     * @param originalDeployment
     * @param refactorings
     * @return
     */
    static void apply(Deployment d, List<RefactorStrategy> refactorings) {
        for (RefactorStrategy rs : refactorings) {
            rs.apply(d);
        }
    }

    /**
     * Evaluates a refactoring path by modifying the original deployment. Need
     * to call reverse() to undo the changes.
     * 
     * @param originalDeployment
     * @param refactorings
     * @return
     */
    static void reverse(Deployment d, List<RefactorStrategy> refactorings) {
        for (int i = refactorings.size() - 1; i >= 0; i--) {
            refactorings.get(i).reverse(d);
        }
    }

    private boolean blacklisted(Set<List<RefactorStrategy>> blacklist, List<RefactorStrategy> newPath) {

        for (List<RefactorStrategy> blacklisted : blacklist) {
            // if the new path itself is already a solution, ignore it
            if (newPath.containsAll(blacklisted)) {
                return true;
            }
        }
        return false;
    }

    private StringBuilder toString(List<RefactorStrategy> path) {
        StringBuilder sb = new StringBuilder();
        sb.append("Proposed refactoring: \n");
        for (int i = 1; i <= path.size(); i++) {
            sb.append(" " + i + ") " + path.get(i - 1) + "\n");
        }

        return sb;
    }

    @Override
    public Set<List<RefactorStrategy>> getSolutions() {
        return solutions;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (List<RefactorStrategy> solution : solutions) {
            sb.append(toString(solution));
        }
        return sb.toString();
    }
}
