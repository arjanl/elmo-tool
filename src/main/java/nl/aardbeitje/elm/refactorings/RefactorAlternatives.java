package nl.aardbeitje.elm.refactorings;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Wrapper around a set of alternative/related refactorings.
 */
public class RefactorAlternatives {
    private final Set<RefactorStrategy> strategies = new HashSet<>();

    public void addStrategy(RefactorStrategy os) {
        strategies.add(os);
    }
    
    public Set<RefactorStrategy> getStrategies() {
        return strategies;
    }
    
    public static Set<RefactorStrategy> flatten( Collection<RefactorAlternatives> alternatives) {
        Set<RefactorStrategy> strategies = new HashSet<>();
        alternatives.forEach( c -> strategies.addAll(c.getStrategies()));
        return strategies;
    }
}
