package nl.aardbeitje.elm.refactorings.queue;

import java.util.List;

import nl.aardbeitje.elm.refactorings.RefactorStrategy;

public interface ElmoQueue {

    void add(List<RefactorStrategy> evaluatedPath);

    List<RefactorStrategy> poll();

}
