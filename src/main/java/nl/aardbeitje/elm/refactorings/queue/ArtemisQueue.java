package nl.aardbeitje.elm.refactorings.queue;

import java.util.List;

import org.apache.activemq.artemis.api.core.ActiveMQException;
import org.apache.activemq.artemis.api.core.SimpleString;
import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.api.core.client.ActiveMQClient;
import org.apache.activemq.artemis.api.core.client.ClientConsumer;
import org.apache.activemq.artemis.api.core.client.ClientMessage;
import org.apache.activemq.artemis.api.core.client.ClientProducer;
import org.apache.activemq.artemis.api.core.client.ClientSession;
import org.apache.activemq.artemis.api.core.client.ClientSessionFactory;
import org.apache.activemq.artemis.api.core.client.ServerLocator;
import org.apache.activemq.artemis.core.remoting.impl.invm.InVMConnectorFactory;
import org.apache.activemq.artemis.core.server.embedded.EmbeddedActiveMQ;

import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import nl.aardbeitje.elm.io.clone.KryoCloner;
import nl.aardbeitje.elm.refactorings.RefactorStrategy;

public class ArtemisQueue implements ElmoQueue {

    private static final SimpleString RS = new SimpleString("rs");
    private static final KryoCloner KRYO = new KryoCloner();
    private static final long MAX_WAIT = 1; // nr of seconds to wait for new solutions to pop up in the queue

    private final ClientProducer producer;
    private final ClientConsumer consumer;
    private final ClientSession session;

    static {
        try {
            EmbeddedActiveMQ server = new EmbeddedActiveMQ();
            server.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ArtemisQueue() {
        try {
            ServerLocator locator = ActiveMQClient.createServerLocatorWithoutHA(new TransportConfiguration(InVMConnectorFactory.class.getName()));
            ClientSessionFactory factory = locator.createSessionFactory();
            session = factory.createSession();

            session.createTemporaryQueue("evaluatedPaths", "evaluatedPaths");
            producer = session.createProducer("evaluatedPaths");
            consumer = session.createConsumer("evaluatedPaths");
            session.start();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public void add(List<RefactorStrategy> p) {
        try {
            ClientMessage message = session.createMessage(false);
            Output output = new Output(1024, -1);
            KRYO.writeClassAndObject(output, p);
            message.putBytesProperty(RS, output.toBytes());
            producer.send(message);
            // evaluatedPaths.offer(evaluatedPath);
        } catch (ActiveMQException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<RefactorStrategy> poll() {
        try {
            ClientMessage msgReceived = consumer.receive(MAX_WAIT);
            if (msgReceived == null) {
                System.out.println("No message received");
                return null;
            }
            Input input = new Input((byte[]) msgReceived.getObjectProperty(RS));
            List<RefactorStrategy> path = (List<RefactorStrategy>) KRYO.readClassAndObject(input);
            return path;
        } catch (ActiveMQException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

}
