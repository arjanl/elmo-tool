package nl.aardbeitje.elm.refactorings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.Statefulness;

/**
 * Merges two services together.
 */
public class MergeStrategy extends RefactorStrategy {

    private final Service serviceRef;
    private final Service mergeWithRef;

    // to be able to reverse
    private List<Call> removedCalls = new ArrayList<>();
    private List<Call> addedCalls = new ArrayList<>();
    private Statefulness statefulness;
    private Set<ServiceMachine> addedServiceMachines = new LinkedHashSet<>();
    private Set<ServiceMachine> removedServiceMachines = new LinkedHashSet<>();
	private Set<String> produces;
	private Set<String> consumes;

    public MergeStrategy(Service service, Service mergeWith) {
        this.serviceRef = service;
        this.mergeWithRef = mergeWith;
    }

    @Override
    public void reverse(Deployment d) {
        if (addedServiceMachines.isEmpty() || removedServiceMachines.isEmpty()) {
            throw new IllegalStateException("No reversable state found, is this strategy not yet applied or already reversed?");
        }

        Service service = serviceRef.recreate(d);
        service.setStatefulness(statefulness);
        service.getProduces().clear();
        service.getProduces().addAll(produces);
        service.getConsumes().clear();
        service.getConsumes().addAll(consumes);
        d.getArchitecture().addService(mergeWithRef);

        addedCalls.forEach(c -> d.getArchitecture().removeCall(c));
        addedServiceMachines.forEach(d::remove);
        removedCalls.forEach(c -> d.getArchitecture().addCall(c));
        removedServiceMachines.forEach(d::addAndConnect);

        clearReverse();
    }

    private void clearReverse() {
        removedCalls = new ArrayList<>();
        addedCalls = new ArrayList<>();
        statefulness = null;
        addedServiceMachines = new LinkedHashSet<>();
        removedServiceMachines = new LinkedHashSet<>();
    }

    @Override
    public void apply(Deployment d) {
        Service service = serviceRef.recreate(d);
        Service mergeWith = mergeWithRef.recreate(d);

        // move calls to this service and remove the merged service
        moveCalls(d, mergeWith, service);
        
        // stateless+stateful = stateful, if partitioned, the partitioning
        // should already be the same (or the refactoring would not have been
        // suggested)
        statefulness = service.getStatefulness();
        if (mergeWith.getStatefulness() == Statefulness.STATEFUL) {
            service.setStatefulness(Statefulness.STATEFUL);
        }
        produces = service.getProduces();
        consumes = service.getProduces();
        
        service.getProduces().addAll(mergeWith.getProduces());
        service.getConsumes().addAll(mergeWith.getConsumes());
        d.getArchitecture().removeService(mergeWith);
        

        Set<Machine> deployedOn = d.getMachines().stream().filter(m -> d.getServices(m).contains(service) || d.getServices(m).contains(mergeWith)).collect(Collectors.toSet());
        removedServiceMachines = d.getServiceMachines().stream().filter(m -> m.getService().equals(service) || m.getService().equals(mergeWith)).collect(Collectors.toSet());

        removedServiceMachines.forEach(d::remove);
        deployedOn.forEach(m -> {
            ServiceMachine sm = new ServiceMachine(service, m, d);
            addedServiceMachines.add(sm);
            d.addAndConnect(sm);
        });
    }

    private void moveCalls(Deployment d, Service s, Service t) {
        Collection<Call> originalCalls = new HashSet<>(d.getArchitecture().getCalls());
        // move all calls that have data as source
        originalCalls.stream().filter(c -> c.getSource().equals(s)).forEach(c -> {
            d.getArchitecture().removeCall(c);
            removedCalls.add(c);
            addedCalls.add(d.getArchitecture().addCall(c.getId(), t, c.getType(), c.getTarget(), c.isRoutable(), c.getData()));
        });

        // move all calls that have data as target
        // TODO not sure if this is complete: pushing the data to the original
        // service might still be required
        originalCalls.stream().filter(c -> c.getTarget().equals(s)).forEach(c -> {
            d.getArchitecture().removeCall(c);
            removedCalls.add(c);
            addedCalls.add(d.getArchitecture().addCall(c.getId(), c.getSource(), c.getType(), t, c.isRoutable(), c.getData()));
        });

    }

    @Override
    public String toString() {
        return "Merge service '" + serviceRef.getName() + "' with '" + mergeWithRef.getName() + "'";
    }

    public static Collection<RefactorAlternatives> generate(Deployment deployment, Service service) {
        Collection<RefactorAlternatives> refactorings = new ArrayList<>();
        RefactorAlternatives alternatives = new RefactorAlternatives();

        deployment.getArchitecture().getServices().stream()
        		.filter(s -> !s.equals(service))
                // do not allow to merge differently partitioned services
                .filter(s -> s.getStatefulness() != Statefulness.PARTITIONED || (service.getStatefulness() == Statefulness.PARTITIONED && service.getPartitioning().equals(s.getPartitioning())))
                .forEach(other -> alternatives.addStrategy(new MergeStrategy(service, other)));
        refactorings.add(alternatives);
        return refactorings;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mergeWithRef == null) ? 0 : mergeWithRef.hashCode());
        result = prime * result + ((serviceRef == null) ? 0 : serviceRef.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MergeStrategy other = (MergeStrategy) obj;
        if (mergeWithRef == null) {
            if (other.mergeWithRef != null)
                return false;
        } else if (!mergeWithRef.equals(other.mergeWithRef))
            return false;
        if (serviceRef == null) {
            if (other.serviceRef != null)
                return false;
        } else if (!serviceRef.equals(other.serviceRef))
            return false;
        return true;
    }

}
