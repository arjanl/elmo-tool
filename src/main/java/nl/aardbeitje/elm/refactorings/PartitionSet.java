package nl.aardbeitje.elm.refactorings;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Util class that derives all possible ways of dividing a set into two sets.
 *
 * @param <E> the type of the set
 */
public class PartitionSet<E> {

    public static class Partition<E> {
        private final Set<E> left;
        private final Set<E> right;
        public Partition(Set<E> left, Set<E> right) {
            this.left = left;
            this.right = right;
        }
        public Set<E> getLeft() {
            return left;
        }
        public Set<E> getRight() {
            return right;
        }
        
    }

    public static <E>  Set<Partition<E>> allPartitions(Set<E> original) {
        Set<Partition<E>> set = new HashSet<>();
        if (original.isEmpty() || original.size()==1) {
            set.add(new Partition<>(original, Collections.emptySet()));
            return set;
        }
        
        E head = original.iterator().next();
        
        Set<E> others = new HashSet<>( original);
        others.remove(head);
        
        Set<Partition<E>> divisions = allPartitions(others);
        for (Partition<E> d : divisions) {
            Set<E> left = new HashSet<>(d.left);
            left.add(head);
            Set<E> right = new HashSet<>(d.right);
            set.add( new Partition<>(left, right));
        
            left = new HashSet<>(d.left);
            right = new HashSet<>(d.right);
            right.add(head);
            set.add( new Partition<>(left, right));
        }
        
        return set;
    }
}
