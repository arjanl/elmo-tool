package nl.aardbeitje.elm.refactorings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Optionality;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.Statefulness;

/**
 * Adds a queue on a pull call. The source of information now becomes
 * insensitive to the target pulling it. Deploys a single queue on a new machine
 * by default.
 */
public class AddQueueStrategy extends RefactorStrategy {

    private final Call callRef;

    // store for reversal
    private Service queue;
    private List<Connection> removedConnections = new ArrayList<>();

    public AddQueueStrategy(Call callRef) {
        this.callRef = callRef;
    }

    @Override
    public void reverse(Deployment d) {
        if (queue == null) {
            throw new IllegalStateException("No reversable state found, is this strategy not yet applied or already reversed?");
        }

        removeService(d, queue);
        d.getArchitecture().addCall(callRef);

        for (Connection c : removedConnections) {
            d.addConnection(c);
        }

        clearReverse();
    }

    private void clearReverse() {
        queue = null;
        removedConnections = new ArrayList<>();
    }

    @Override
    public void apply(Deployment d) {
        Call call = callRef.recreate(d);
        Service producer = call.getSource();
        queue = new Service(producer.getName() + "_" + call.getTargetName() + "_q", producer.getName(), producer.getDescription(), Statefulness.STATEFUL, producer.getPartitioning());

        // rewire services
        d.getArchitecture().removeCall(call);
        Call callProducer = new Call(call.getId(), producer, queue, CallType.PUSHES_TO, true, call.getData());
        d.getArchitecture().addCall(callProducer);
        Call callQueue = new Call(d.getArchitecture().getNewCallId(), queue, call.getTarget(), CallType.PULLED_FROM, true, call.getData());
        d.getArchitecture().addCall(callQueue);

        ServiceMachine smQueue = new ServiceMachine(queue, d);
        // deploy services
        d.getConnections(call).forEach(connection -> {
            removedConnections.add(connection);
            d.removeConnection(connection);
            d.addConnection(connection.getSource(), callProducer, smQueue, Optionality.COMPULSORY);
            d.addConnection(smQueue, callQueue, connection.getTarget(), Optionality.COMPULSORY);
        });

    }

    @Override
    public String toString() {
        return "Add queue on call '" + callRef.toString() + "'";
    }

    public static Collection<RefactorAlternatives> generate(Deployment deployment, Service service) {
        Collection<RefactorAlternatives> refactorings = new ArrayList<>();

        deployment.getArchitecture().getCalls().stream().filter(Call::isPulledFrom).filter(call -> call.getSource().equals(service)).forEach(call -> {
            RefactorAlternatives alternatives = new RefactorAlternatives();
            alternatives.addStrategy(new AddQueueStrategy(call));
            refactorings.add(alternatives);
        });
        return refactorings;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((callRef == null) ? 0 : callRef.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AddQueueStrategy other = (AddQueueStrategy) obj;
        if (callRef == null) {
            if (other.callRef != null)
                return false;
        } else if (!callRef.equals(other.callRef))
            return false;
        return true;
    }

}
