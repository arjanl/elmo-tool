package nl.aardbeitje.elm.refactorings;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;

/**
 * Base class for refactoring strategies. Holds relation with related
 * refactorings.
 */
public abstract class RefactorStrategy {

    public abstract void apply(Deployment d);

    public abstract void reverse(Deployment d);

//    public abstract RefactorStrategy recreate(Deployment newDeployment);

    protected static void removeService(Deployment deployment, Service s) {
        for (ServiceMachine sm : deployment.getServiceMachines(s)) {
            deployment.remove(sm);
        }
        deployment.getArchitecture().removeService(s);
    }
}
