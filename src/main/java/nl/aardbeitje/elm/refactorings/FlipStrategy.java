package nl.aardbeitje.elm.refactorings;

import java.util.ArrayList;
import java.util.Collection;

import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;

/**
 * Simple refactoring that changes the direction of initiative of a call.
 */
public class FlipStrategy extends RefactorStrategy {

    private Call callRef;

    public FlipStrategy(Call callRef) {
        this.callRef = callRef;
    }
    
    @Override
    public void reverse(Deployment d) {
        apply(d);
    }

    @Override
    public void apply(Deployment d) {
        Call call = callRef.recreate(d);
        Call newCall = new Call(call.getId(), call.getSource(), call.getTarget(), call.getType()==CallType.PULLED_FROM?CallType.PUSHES_TO:CallType.PULLED_FROM, call.isRoutable(), call.getData());
        d.getArchitecture().removeCall(call);
        d.getArchitecture().addCall(newCall);
        
        d.getConnections(call).forEach(c->{
            d.removeConnection(c);
            d.addConnection(c.getSource(), newCall, c.getTarget(), c.getOptionality());
        });
    }

    @Override
    public String toString() {
        return "Flip call '" + callRef.toString() + "' from " + (callRef.isPulledFrom()?"pull":"push") + " to " + (callRef.isPushedTo()?"pull":"push");
    }

    public static Collection<RefactorAlternatives> generate(Deployment deployment, Service service) {
        Collection<RefactorAlternatives> refactorings = new ArrayList<>();
        deployment.getArchitecture().getCalls().stream()
                   .filter( c -> c.isUsed(service)).forEach( call -> {
                        RefactorAlternatives alternatives = new RefactorAlternatives();
                        alternatives.addStrategy(new FlipStrategy(call));
                        refactorings.add(alternatives);
                   });
        return refactorings;
    }

    public Call getCallRef() {
        return callRef;
    }

    public void setCallRef(Call callRef) {
        this.callRef = callRef;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((callRef == null) ? 0 : callRef.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FlipStrategy other = (FlipStrategy) obj;
        if (callRef == null) {
            if (other.callRef != null)
                return false;
        } else if (!callRef.equals(other.callRef))
            return false;
        return true;
    }
    
}
