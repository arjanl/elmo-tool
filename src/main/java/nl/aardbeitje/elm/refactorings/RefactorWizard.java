package nl.aardbeitje.elm.refactorings;

import java.util.List;
import java.util.Set;

import javafx.concurrent.Task;

// TODO remove dependency on Task here, so we can run it without JavaFX environment
public abstract class RefactorWizard extends Task<Void> {

    public abstract Set<List<RefactorStrategy>> getSolutions();

}
