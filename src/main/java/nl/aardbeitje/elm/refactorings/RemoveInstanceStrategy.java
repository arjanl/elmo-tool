package nl.aardbeitje.elm.refactorings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;

public class RemoveInstanceStrategy extends RefactorStrategy {

    private final Machine machineRef;
    private final Service serviceRef;
    private ServiceMachine removedServiceMachine;

    public RemoveInstanceStrategy(Service service, Machine machine) {
        this.serviceRef = service;
        this.machineRef = machine;
    }

    @Override
    public void reverse(Deployment d) {
        if (removedServiceMachine == null) {
            throw new IllegalStateException("No reversable state found, is this strategy not yet applied or already reversed?");
        }
        d.addAndConnect(removedServiceMachine);
        clearReverse();
    }

    private void clearReverse() {
        removedServiceMachine = null;
    }

    @Override
    public void apply(Deployment d) {
        Service service = serviceRef.recreate(d);
        Machine machine = machineRef.recreate(d);
        removedServiceMachine = d.getServiceMachine(service, machine);
        if (removedServiceMachine==null) {
            System.out.println("cannot find to be removed serviceMachine for service " + service + " and machine " + machine);
            throw new IllegalArgumentException("cannot find to be removed serviceMachine for service " + service + " and machine " + machine);
        }
        d.remove(removedServiceMachine);
    }

    @Override
    public String toString() {
        return "Remove instance of '" + serviceRef.getName() + "' on " + machineRef.getName();
    }

    public static Collection<RefactorAlternatives> generate(Deployment deployment, Service service) {
        Collection<RefactorAlternatives> refactorings = new ArrayList<>();

        RefactorAlternatives alternatives = new RefactorAlternatives();

        Set<Machine> machines = deployment.getMachines(service);

        if (machines.size() > 1) { // don't allow to remove the last instance
            machines.forEach(machine -> {
                RemoveInstanceStrategy nis = new RemoveInstanceStrategy(service, machine);
                alternatives.addStrategy(nis);
            });
        }

        refactorings.add(alternatives);
        return refactorings;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((machineRef == null) ? 0 : machineRef.hashCode());
        result = prime * result + ((serviceRef == null) ? 0 : serviceRef.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RemoveInstanceStrategy other = (RemoveInstanceStrategy) obj;
        if (machineRef == null) {
            if (other.machineRef != null)
                return false;
        } else if (!machineRef.equals(other.machineRef))
            return false;
        if (serviceRef == null) {
            if (other.serviceRef != null)
                return false;
        } else if (!serviceRef.equals(other.serviceRef))
            return false;
        return true;
    }

}
