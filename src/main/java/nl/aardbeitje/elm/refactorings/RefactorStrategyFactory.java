package nl.aardbeitje.elm.refactorings;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;

/**
 * This factory class produces all the available refactoring strategies.
 */
public class RefactorStrategyFactory {

    public static Set<RefactorAlternatives> getAllRefactoringStrategies(Deployment deployment, Service service) {

        Set<RefactorAlternatives> refactorings = new LinkedHashSet<>();

        refactorings.addAll(FlipStrategy.generate(deployment, service));
        refactorings.addAll(SplitStrategy.generate(deployment, service));
        refactorings.addAll(MergeStrategy.generate(deployment, service));

        refactorings.addAll(AddCacheStrategy.generate(deployment, service));
        refactorings.addAll(AddQueueStrategy.generate(deployment, service));

        refactorings.addAll(NewInstanceStrategy.generate(deployment, service));
        refactorings.addAll(RemoveInstanceStrategy.generate(deployment, service));
        return refactorings;
    }

    public static Set<RefactorAlternatives> getTrivialRefactoringStrategies(Deployment deployment, Service service) {

        Set<RefactorAlternatives> refactorings = new LinkedHashSet<>();

        refactorings.addAll(FlipStrategy.generate(deployment, service));
        refactorings.addAll(SplitStrategy.generate(deployment, service));
//        refactorings.addAll(MergeStrategy.generate(deployment, service));

        refactorings.addAll(AddCacheStrategy.generate(deployment, service));
        refactorings.addAll(AddQueueStrategy.generate(deployment, service));

        refactorings.addAll(NewInstanceStrategy.generate(deployment, service));
        refactorings.addAll(RemoveInstanceStrategy.generate(deployment, service));
        return refactorings;
    }

    public static Set<RefactorStrategy> generateStrategies(Deployment deployment) {
        Set<RefactorStrategy> strategies = new HashSet<>();
        for (Service service : deployment.getArchitecture().getServices()) {
            strategies.addAll(RefactorAlternatives.flatten(RefactorStrategyFactory.getTrivialRefactoringStrategies(deployment, service)));
        }
        return strategies;
    }
}
