package nl.aardbeitje.elm.refactorings;

import java.util.List;
import java.util.UUID;

import nl.aardbeitje.elm.Deployment;

public class RefactorWizardFactory {

    public static RefactorWizard createWizard(Deployment originalDeployment, List<RefactorGoal> goals) {
        Deployment deployment = new Deployment(UUID.randomUUID().toString(), originalDeployment);
        return new RefactorWizardStreamingMemoryOptimizedBreadthFirst(deployment, goals);
    }

}
