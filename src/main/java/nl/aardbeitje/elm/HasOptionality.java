package nl.aardbeitje.elm;

public interface HasOptionality {
    Optionality getOptionality();
}
