package nl.aardbeitje.elm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import nl.aardbeitje.elm.io.Constants;

/**
 * A call is a one-way transport of messages between two components as defined by the information graph.
 * The source is thus always the producer of information, the target is the consumer. Note that type indicates who takes initiative.
 * Should be referenced by id. Id generation should be deterministic. Equals is thus defined on id.
 * 
 * Should not reference an Architecture since that may leak memory. A Call instead provides a recreate() function that finds an equivalent instance in a target Architecture.
 */
public class Call implements Edge {
    private final int id;
    private final CallType type;
    private final boolean routable;
    private final Service source;
    private final Service target;
    private final String data;
    
    public Call(int id, Service source, Service target, CallType type, boolean routable, String data) {
        this.id = id;
        this.source = source;
        this.target = target;
        this.type = type;
        this.routable = routable;
        this.data = data;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Call other = (Call) obj;
        if (id != other.id)
            return false;
        return true;
    }
    
    public boolean isUsed(Service service) {
        return source.equals(service) || target.equals(service);
    }


    public String getSourceName() {
        return source.getName();
    }

    public String getTargetName() {
        return target.getName();
    }

    @JsonIgnore
    public Service getSource() {
        return source;
    }

    @JsonIgnore
    public Service getTarget() {
        return target;
    }

    public CallType getType() {
        return type;
    }

    @JsonIgnore
    public boolean isPushedTo() {
        return type == CallType.PUSHES_TO;
    }

    @JsonIgnore
    public boolean isPulledFrom() {
        return type == CallType.PULLED_FROM;
    }

    @Override
    public String toString() {
        return getSourceName() + asOperator() + getTargetName();
    }

    public boolean isRoutable() {
        return routable;
    }

    public String asOperator() {
        if (routable) {
            if (type == CallType.PULLED_FROM) {
                return Constants.ROUTABLE_PULL;
            } else {
                return Constants.ROUTABLE_PUSH;
            }
        } else {
            if (type == CallType.PULLED_FROM) {
                return Constants.NONROUTABLE_PULL;
            } else {
                return Constants.NONROUTABLE_PUSH;
            }
        }
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public Call recreate(Deployment newDeployment) {
        return newDeployment.getArchitecture().getCallById(getId());
    }

}
