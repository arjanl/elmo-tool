package nl.aardbeitje.elm.io;

import nl.aardbeitje.elm.Statefulness;

public class ServiceMachineReadModel {

    private String name;
    private Statefulness statefulness;
    private String serviceName;
    private String machineName;
    private int index;
    
    public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Statefulness getStatefulness() {
        return statefulness;
    }
    public void setStatefulness(Statefulness statefulness) {
        this.statefulness = statefulness;
    }
    public String getServiceName() {
        return serviceName;
    }
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    public String getMachineName() {
        return machineName;
    }
    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }


}
