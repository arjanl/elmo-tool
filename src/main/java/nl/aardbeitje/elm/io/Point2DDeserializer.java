package nl.aardbeitje.elm.io;

import java.awt.geom.Point2D;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.NumericNode;

class Point2DDeserializer extends JsonDeserializer<Point2D> {

    Point2DDeserializer() {
    }

    @Override
    public Point2D deserialize(JsonParser jp, DeserializationContext any) throws IOException {
        TreeNode node = jp.getCodec().readTree(jp);
        double x = ((NumericNode) node.get("x")).doubleValue();
        double y = ((NumericNode) node.get("y")).doubleValue();
        return new Point2D.Double(x, y);
    }
}