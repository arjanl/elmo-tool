package nl.aardbeitje.elm.io.clone;

public class ClonerFactory {
    private ClonerFactory() {
    }

    public static Cloner getCloner() {
        return new KryoCloner();
    }

}
