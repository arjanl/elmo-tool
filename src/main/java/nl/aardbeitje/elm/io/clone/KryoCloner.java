package nl.aardbeitje.elm.io.clone;

import org.objenesis.strategy.StdInstantiatorStrategy;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import edu.uci.ics.jung.graph.util.Pair;

public class KryoCloner extends Kryo implements Cloner {

     private static Serializer<Pair> PAIR_SERIALIZER = new Serializer<Pair>() {
            @Override
            public void write(Kryo kryo, Output output, Pair pair) {
                kryo.writeClassAndObject(output, pair.getFirst());
                kryo.writeClassAndObject(output, pair.getSecond());
            }

            @Override
            public Pair read(Kryo kryo, Input input, Class<Pair> type) {
                Object o1 = kryo.readClassAndObject(input);
                Object o2 = kryo.readClassAndObject(input);
                return new Pair(o1, o2);
            }

            @Override
            public Pair copy(Kryo kryo, Pair original) {
                Object o1 = kryo.copy(original.getFirst());
                Object o2 = kryo.copy(original.getSecond());
                return new Pair(o1, o2);
            }

        };
    public KryoCloner() {

//        register(java.util.HashSet.class, 0);
//        register(java.util.LinkedHashMap.class, 1);
//        register(java.util.HashMap.class, 2);
//        register(edu.uci.ics.jung.graph.DirectedSparseMultigraph.class, 3);
//        register(edu.uci.ics.jung.graph.util.EdgeType.class, 4);
//        register(edu.uci.ics.jung.graph.util.Pair.class, PAIR_SERIALIZER, 5);
//        register(nl.aardbeitje.elm.Deployment.class, 6);
//        register(nl.aardbeitje.elm.Architecture.class, 7);
//        register(nl.aardbeitje.elm.Call.class, 8);
//        register(nl.aardbeitje.elm.Service.class, 9);
//        register(nl.aardbeitje.elm.Statefulness.class, 10);
//        register(nl.aardbeitje.elm.CallType.class, 11);
//        register(nl.aardbeitje.elm.Connection.class, 12);
//        register(nl.aardbeitje.elm.Optionality.class, 13);
//        register(nl.aardbeitje.elm.ServiceMachine.class, 14);
//        register(nl.aardbeitje.elm.Machine.class, 15);

        setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));
        addDefaultSerializer(edu.uci.ics.jung.graph.util.Pair.class, PAIR_SERIALIZER);

    }
    
   

    public <T> T clone(T object) {
        return copy(object);
    }

//    public <T> byte[] toBytes(T object) {
//        ByteArrayOutputStream b = new ByteArrayOutputStream();
//        Output output = new Output(b);
//        writeObject(output, object);
//        return b.toByteArray();
//    }

    public <T> byte[] toBytes(T object) {
        Output output = new Output(15000, -1);
        writeObject(output, object);
        return output.toBytes();
    }

}
