package nl.aardbeitje.elm.io.clone;

public interface Cloner {

    <T> T clone(T object);
    <T> byte[] toBytes(T object);

}
