package nl.aardbeitje.elm.io;

public class ReadModel {

    private DeploymentReadModel deployment;
    private LayoutModel layout;
    
    public ReadModel() {
    }
    
    public ReadModel(DeploymentReadModel deployment, LayoutModel layout) {
        this.deployment = deployment;
        this.layout = layout;
    }
    
    public void setDeployment(DeploymentReadModel deployment) {
        this.deployment = deployment;
    }

    public void setLayout(LayoutModel layout) {
        this.layout = layout;
    }

    public DeploymentReadModel getDeployment() {
        return deployment;
    }

    public LayoutModel getLayout() {
        return layout;
    }
    
}
