package nl.aardbeitje.elm.io;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.Deployment;

public class ParsedDeployment {

    private final Architecture architecture;
    private final Deployment deployment;
    private final LayoutModel layout;

    public ParsedDeployment(Architecture architecture, Deployment deployment, LayoutModel layout) {
        this.architecture = architecture;
        this.deployment = deployment;
        this.layout = layout;
    }

    public Architecture getArchitecture() {
        return architecture;
    }

    public Deployment getDeployment() {
        return deployment;
    }

    public LayoutModel getLayout() {
        return layout;
    }

}
