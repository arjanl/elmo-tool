package nl.aardbeitje.elm.io;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.io.clone.Cloner;
import nl.aardbeitje.elm.io.clone.ClonerFactory;

/**
 * Serializes a deployment to a string for easy in-memory storage and
 * comparison. Should be optimized for speed and memory foot-print
 */
public class DeploymentSerializer {

    public enum Mode {
        KRYO, JSON
    }

    public static class SerializedDeployment {
        byte[] serialized;

        public SerializedDeployment(byte[] serialized) {
            this.serialized = serialized;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + Arrays.hashCode(serialized);
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            SerializedDeployment other = (SerializedDeployment) obj;
            if (!Arrays.equals(serialized, other.serialized))
                return false;
            return true;
        }
    }

    public static SerializedDeployment serialize(Deployment d) {
        return serialize(Mode.KRYO, d);
    }

    public static SerializedDeployment serialize(Mode mode, Deployment d) {
        switch (mode) {
        case JSON:
            return serializeJson(d);
        case KRYO:
            return serializeKryo(d);
        default:
            throw new RuntimeException("Unhandled mode: " + mode);
        }
    }

    static Cloner CLONER = ClonerFactory.getCloner();

    public static SerializedDeployment serializeKryo(Deployment d) {
        return new SerializedDeployment(CLONER.toBytes(d));
    }

    public static SerializedDeployment serializeJson(Deployment d) {
        ObjectMapper mapper = new ObjectMapper();
        StringWriter sw = new StringWriter();
        try {
            mapper.writeValue(sw, d);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return new SerializedDeployment(sw.toString().getBytes());

    }

}
