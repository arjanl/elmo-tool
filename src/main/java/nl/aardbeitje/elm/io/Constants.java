package nl.aardbeitje.elm.io;

public class Constants {
    public static final String REV_ROUTABLE_PUSH = "->";
    public static final String REV_ROUTABLE_PULL = "-<";
    public static final String ROUTABLE_PUSH = "->";
    public static final String ROUTABLE_PULL = ">-";
    public static final String NONROUTABLE_PUSH = "=>";
    public static final String NONROUTABLE_PULL = ">=";
    public static final String REV_NONROUTABLE_PUSH = "<=";
    public static final String REV_NONROUTABLE_PULL = "=<";

}
