package nl.aardbeitje.elm.io;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import nl.aardbeitje.elm.Optionality;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectionReadModel {
    
    private Optionality optionality;
    private int callId;
    private String sourceName;
    private String targetName;

    public Optionality getOptionality() {
        return optionality;
    }
    public void setOptionality(Optionality optionality) {
        this.optionality = optionality;
    }
    public int getCallId() {
        return callId;
    }
    public void setCallId(int callId) {
        this.callId = callId;
    }
    public String getSourceName() {
        return sourceName;
    }
    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
    public String getTargetName() {
        return targetName;
    }
    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    
}
