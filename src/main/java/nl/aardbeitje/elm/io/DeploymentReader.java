package nl.aardbeitje.elm.io;

import static nl.aardbeitje.elm.io.Constants.NONROUTABLE_PULL;
import static nl.aardbeitje.elm.io.Constants.NONROUTABLE_PUSH;
import static nl.aardbeitje.elm.io.Constants.REV_NONROUTABLE_PULL;
import static nl.aardbeitje.elm.io.Constants.REV_NONROUTABLE_PUSH;
import static nl.aardbeitje.elm.io.Constants.REV_ROUTABLE_PULL;
import static nl.aardbeitje.elm.io.Constants.REV_ROUTABLE_PUSH;
import static nl.aardbeitje.elm.io.Constants.ROUTABLE_PULL;
import static nl.aardbeitje.elm.io.Constants.ROUTABLE_PUSH;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.MachineConfig;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.Statefulness;

public class DeploymentReader {
	private static final Logger LOGGER = LoggerFactory.getLogger(DeploymentReader.class);

	private int callId;
	private final Architecture architecture;
	private final Set<MachineConfig> machineConfigs = new HashSet<>();
	private final InputStream in;
	private final String name;

	public DeploymentReader(String name, InputStream in) {
		this.name = name;
		this.architecture = new Architecture();
		this.in = in;
	}

	public ParsedDeployment read() throws IOException {
		try {
			return readJson();
		} catch (Exception e) {
			LOGGER.warn("Unable to read the file as json, trying legacy format: " + e.getMessage(), e);
			in.reset();
			return oldRead();
		}
	}

	private ParsedDeployment readJson() throws IOException {
		ReadModel readModel = readModel();
		readArchitecture(readModel);
		Deployment deployment = readDeployment(readModel);
		return new ParsedDeployment(architecture, deployment, readModel.getLayout());
	}

	private Deployment readDeployment(ReadModel readModel) throws IOException {
		Deployment d = new Deployment(readModel.getDeployment().getName(), architecture);
		for (ServiceMachineReadModel smrm : readModel.getDeployment().getServiceMachines()) {
			Service s = architecture.getService(smrm.getServiceName());
			Machine m = new Machine(smrm.getMachineName());
			ServiceMachine sm = new ServiceMachine(s, m, smrm.getName());
			d.add(sm);
		}

		for (ConnectionReadModel crm : readModel.getDeployment().getConnections()) {
			ServiceMachine source = d.getServiceMachineByName(crm.getSourceName());
			ServiceMachine target = d.getServiceMachineByName(crm.getTargetName());
			Call call = architecture.getCallById(crm.getCallId());
			if (!source.getService().equals(call.getSource())) {
				throw new IOException("Service of source of connection for service machine " + source.getName()
						+ " does not match the call (" + call.getId() + ") source service " + call.getSource());
			}
			if (!target.getService().equals(call.getTarget())) {
				throw new IOException("Service of target of connection for service machine " + source.getName()
						+ " does not match the call(" + call.getId() + ") target service" + call.getTarget());
			}
			d.addConnection(source, call, target, crm.getOptionality());
		}
		return d;
	}

	private void readArchitecture(ReadModel readModel) {
		for (ServiceReadModel srm : readModel.getDeployment().getArchitecture().getServices()) {
			Service s = new Service(srm.getName(), srm.getOriginalName(), srm.getDescription(), srm.getStatefulness(),
					srm.getPartitioning(), srm.getProduces(), srm.getConsumes());
			architecture.addService(s);
		}

		for (CallReadModel crm : readModel.getDeployment().getArchitecture().getCalls()) {
			Service source = architecture.getService(crm.getSourceName());
			Service target = architecture.getService(crm.getTargetName());
			architecture.addCall(crm.getId(), source, crm.getType(), target, crm.isRoutable(), crm.getData());
		}
	}

	private ReadModel readModel() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.AUTO_CLOSE_SOURCE, false);
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Point2D.class, new Point2DDeserializer());
		mapper.registerModule(module);
		return mapper.readValue(in, ReadModel.class);
	}

	@Deprecated
	private ParsedDeployment oldRead() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));

		int i = 1;
		String l = br.readLine();
		try {

			while (l != null) {
				parse(i, l);
				l = br.readLine();
				i++;
			}
		} catch (Exception e) {
			LOGGER.error("Unable to parse file: " + e.getMessage() + " while parsing line: \n" + l, e);
			throw new RuntimeException(e);
		}

		Deployment deployment = Deployment.customMachinePerService(name, architecture, machineConfigs);
		return new ParsedDeployment(architecture, deployment, null);
	}

	@Deprecated
	void parse(int i, String line) {
		// check if it contains a '>', only possible for call's
		if (line.startsWith("#")) {
		} else if (line.contains(">")) {
			parseArchitecture(line);
		} else if (line.contains("<")) {
			parseArchitecture(line);

			// if it contains '=' but not a '>', it is a deployment line
		} else if (line.contains("=")) {
			parseDeployment(line);

			// service definitions are with a ':'
		} else if (line.contains(":")) {
			parseService(line);

		} else if (line.trim().isEmpty()) {
			// skip
		} else {
			throw new IllegalArgumentException("parse error at line " + i);
		}
	}

	@Deprecated
	private void parseService(String line) {
		String[] parts = line.split("[\\{\\,\\}:]");
		String serviceName = parts[0].trim();

		Statefulness statefulness = null;
		String partitioning = null;

		for (int i = 1; i < parts.length; i++) {
			String s = parts[i];
			if (!s.trim().isEmpty()) {
				if (statefulness == null) {
					statefulness = Statefulness.valueOf(s);
				} else {
					if (statefulness == Statefulness.PARTITIONED) {
						partitioning = parts[i];
					}
				}
			}
		}

		Service service = (statefulness == Statefulness.PARTITIONED
				? createService(serviceName, statefulness, partitioning)
				: createService(serviceName, statefulness));
		architecture.addService(service);
	}

	@Deprecated
	private void parseDeployment(String line) {
		String[] parts = line.split("=");
		String machineName = parts[0].trim();
		String servicesDeployed = parts[1];

		String[] serviceNames = servicesDeployed.split("[\\{\\,\\}]");
		for (String s : serviceNames) {
			String name = s.trim();
			if (name.isEmpty()) {
				continue;
			}

			if (name.contains(":")) {
				// has an explicit name so keep that
				String serviceName = name.split(":")[0];
				String serviceMachineName = name.split(":")[1];
				MachineConfig mc = new MachineConfig(serviceName, machineName, serviceMachineName);
				machineConfigs.add(mc);
			} else {
				MachineConfig mc = new MachineConfig(name, machineName);
				machineConfigs.add(mc);
			}
		}

	}

	@Deprecated
	private void parseArchitecture(String line) {
		boolean push = line.contains(ROUTABLE_PUSH) || line.contains(NONROUTABLE_PUSH)
				|| line.contains(REV_ROUTABLE_PUSH) || line.contains(REV_NONROUTABLE_PUSH);
		boolean routable = line.contains(ROUTABLE_PUSH) || line.contains(ROUTABLE_PULL)
				|| line.contains(REV_ROUTABLE_PUSH) || line.contains(REV_ROUTABLE_PULL);
		String[] parts = line.split(ROUTABLE_PUSH + "|" + NONROUTABLE_PUSH + "|" + ROUTABLE_PULL + "|"
				+ NONROUTABLE_PULL + "|" + REV_ROUTABLE_PUSH + "|" + REV_NONROUTABLE_PUSH + "|" + REV_ROUTABLE_PULL
				+ "|" + REV_NONROUTABLE_PULL);

		String serviceName1 = parts[0].trim();
		String serviceName2 = parts[1].trim();
		if (line.contains("<")) { // reversed notation
			String tmp = serviceName1;
			serviceName1 = serviceName2;
			serviceName2 = tmp;
		}

		Service s1 = architecture.getService(serviceName1);
		Service s2 = architecture.getService(serviceName2);
		architecture.addCall(++callId, s1, push ? CallType.PUSHES_TO : CallType.PULLED_FROM, s2, routable, "");
	}

	@Deprecated
	private Service createService(String serviceName, Statefulness statefulness) {
		if (statefulness == Statefulness.PARTITIONED) {
			throw new IllegalArgumentException("Partitioned statefulness requires a partitioning schema");
		}
		return new Service(serviceName, serviceName, statefulness);
	}

	@Deprecated
	private Service createService(String serviceName, Statefulness statefulness, String partitioning) {
		return new Service(serviceName, serviceName, statefulness, partitioning);
	}
}
