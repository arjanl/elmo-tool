package nl.aardbeitje.elm.io;

import java.util.Set;

public class DeploymentReadModel {

    private ArchitectureReadModel architecture;
    private String name;
    private Set<MachineReadModel> machines;
    private Set<ServiceMachineReadModel> serviceMachines;
    private Set<ConnectionReadModel> connections;

    public ArchitectureReadModel getArchitecture() {
        return architecture;
    }

    public void setArchitecture(ArchitectureReadModel architecture) {
        this.architecture = architecture;
    }

    public Set<MachineReadModel> getMachines() {
        return machines;
    }

    public void setMachines(Set<MachineReadModel> machines) {
        this.machines = machines;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ServiceMachineReadModel> getServiceMachines() {
        return serviceMachines;
    }

    public void setServiceMachines(Set<ServiceMachineReadModel> serviceMachines) {
        this.serviceMachines = serviceMachines;
    }

    public Set<ConnectionReadModel> getConnections() {
        return connections;
    }

    public void setConnections(Set<ConnectionReadModel> connections) {
        this.connections = connections;
    }
    
}
