package nl.aardbeitje.elm.io;

import nl.aardbeitje.elm.Deployment;

public class WriteModel {
    private final Deployment deployment;
    private final LayoutModel layout;
    
    public WriteModel(Deployment deployment, LayoutModel layout) {
        this.deployment = deployment;
        this.layout = layout;
    }

    public Deployment getDeployment() {
        return deployment;
    }

    public LayoutModel getLayout() {
        return layout;
    }
    
}
