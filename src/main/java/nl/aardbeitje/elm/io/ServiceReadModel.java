package nl.aardbeitje.elm.io;

import java.util.Set;

import nl.aardbeitje.elm.Statefulness;

public class ServiceReadModel {
    private String name;
    private String originalName;
	private String description;
    private Statefulness statefulness;
    private String partitioning;
    private Set<String> produces;
    private Set<String> consumes;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Statefulness getStatefulness() {
        return statefulness;
    }
    public void setStatefulness(Statefulness statefulness) {
        this.statefulness = statefulness;
    }
    public String getPartitioning() {
        return partitioning;
    }
    public void setPartitioning(String partitioning) {
        this.partitioning = partitioning;
    }
    public Set<String> getProduces() {
        return produces;
    }
    public void setProduces(Set<String> produces) {
        this.produces = produces;
    }
    public Set<String> getConsumes() {
        return consumes;
    }
    public void setConsumes(Set<String> consumes) {
        this.consumes = consumes;
    }
    public String getOriginalName() {
		return originalName;
	}
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
}
