package nl.aardbeitje.elm.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.Statefulness;

public class DeploymentWriter {
    private final OutputStream out;
    
    public DeploymentWriter(OutputStream out) {
        this.out = out;
    }
    
    public void write(Deployment deployment) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
        
        writeServices(bw,deployment.getArchitecture());
        bw.newLine();
        writeArchitecture(bw,deployment.getArchitecture());
        bw.newLine();
        writeDeployment(bw,deployment);
        bw.flush();
    }
    
    private void writeDeployment(BufferedWriter bw, Deployment deployment) throws IOException {
        for (Machine m : deployment.getMachines()) {
            bw.write(m.getName() + " = {" );
            for (ServiceMachine sm : deployment.getServiceMachines(m)) {
                bw.write( sm.getService().getName() + ":" + sm.getName());
                bw.write(", ");
            }
            bw.write("}");
            bw.newLine();
        }
        
    }

    private void writeServices(BufferedWriter bw, Architecture architecture) throws IOException {
        for (Service s : architecture.getServices()) {
            bw.write(s.getName() + " : " + "{" +s.getStatefulness().toString() );
            if (s.getStatefulness()==Statefulness.PARTITIONED) {
                bw.write(":" + s.getPartitioning());
            }
            bw.write( "}");
            bw.newLine();
        }
        
    }

    private void writeArchitecture(BufferedWriter bw, Architecture architecture) throws IOException {
        for (Call c : architecture.getCalls()) {
            Service s1 = architecture.getCallSource(c);
            Service s2 = architecture.getCallTarget(c);
            bw.write(s1.getName() + " " + (c.asOperator()) + " " + s2.getName());
            bw.newLine();
        }
        
    }

}
