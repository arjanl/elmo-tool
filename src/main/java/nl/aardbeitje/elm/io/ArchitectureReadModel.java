package nl.aardbeitje.elm.io;

import java.util.Set;

public class ArchitectureReadModel {

    private Set<ServiceReadModel> services;
    private Set<CallReadModel> calls;
    

    public Set<ServiceReadModel> getServices() {
        return services;
    }

    public void setServices(Set<ServiceReadModel> services) {
        this.services = services;
    }

    public Set<CallReadModel> getCalls() {
        return calls;
    }

    public void setCalls(Set<CallReadModel> calls) {
        this.calls = calls;
    }
    
}
