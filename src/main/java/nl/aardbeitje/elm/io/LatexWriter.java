package nl.aardbeitje.elm.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.LinkedHashMap;
import java.util.Map;

import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Optionality;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.Statefulness;

public class LatexWriter implements AutoCloseable {

    private static final String N = "\n";
    private static final String T = "\t";
    private static final String TT = "\t\t";
    private static final String TTT = "\t\t\t";
    private static final String TTTT = "\t\t\t\t";
    private static final float RESIZE = 1.0f;
    private static final boolean INITIATIVE_GRAPH = true; // set to true for
                                                         // outputting the base
                                                         // graph as initiative
                                                         // graph rather than
                                                         // information graph

    private static final String[] partitionColors = { "teal", "olive", "tealblue", "tealgreen", "oldlavender", "oldmauve", "oldrose", "palebrown" };
    private BufferedWriter writer;
    private Map<String, String> partitionColorAssignments = new LinkedHashMap<>();

    public LatexWriter(OutputStream outputStream) {
        this.writer = new BufferedWriter(new OutputStreamWriter(outputStream));
    }

    public void write(Deployment deployment) throws IOException {
        writer.write("\\centering" + N);
        writer.write("\\begin{mygraph}[" + RESIZE + "]" + N);
        writer.write(T + "\\begin{dot2tex}[dot, tikz, options=-t math]" + N);
        writer.write(TT + "digraph exported {" + N);
        writer.write(TTT + "rankdir=LR;" + N);

        for (Machine m : deployment.getMachines()) {
            writeMachine(deployment, m);
        }

        for (Connection c : deployment.getConnections()) {
            writeConnection(c);
        }

        writer.write(TT + "}" + N);
        writer.write(T + "\\end{dot2tex}" + N);
        writer.write("\\end{mygraph}" + N);
        writer.write("\\caption{generated caption}" + N);
        writer.write("\\label{fig:generated:label}" + N);
    }

    private void writeConnection(Connection c) throws IOException {
        ServiceMachine source = c.getSource();
        ServiceMachine target = c.getTarget();
        String bend = "bend left=10";

        if (INITIATIVE_GRAPH && c.isPulledFrom()) {
            source = c.getTarget();
            target = c.getSource();
            bend = "bend right=10";
        }
        String optionality = optionalityToStyle(c.getOptionality());
        String callType = callTypeToStyle(c.getCallType());

        writer.write(TTT + source.getName() + "->" + target.getName());
        writer.write(" [style=\"");
        writer.write(optionality + ",");
        writer.write(callType + "\",");
        writer.write(" topath=\"" + bend + "\"");
        writer.write("];" + N);
    }

    private String callTypeToStyle(CallType callType) {
        switch (callType) {
        case PULLED_FROM:
            if (INITIATIVE_GRAPH) {
                return "pullcall reversed";
            } else {
                return "pullcall";
            }
        case PUSHES_TO:
            return "pushcall";
        default:
            throw new IllegalArgumentException("CallType " + callType + " unknown");
        }
    }

    private String optionalityToStyle(Optionality optionality) {
        switch (optionality) {
        case COMPULSORY:
            return "comp";
        case DEPLOYMENT_OPTIONAL:
            return "depopt";
        case RUNTIME_OPTIONAL:
            return "runopt";
        default:
            throw new IllegalArgumentException("Optionality " + optionality + " unknown");
        }
    }

    private void writeMachine(Deployment d, Machine m) throws IOException {
        writer.write(TTT + "subgraph cluster_" + m.getName() + " {" + N);
        for (ServiceMachine sm : d.getServiceMachines(m)) {
            writeServiceMachine(sm);
        }
        writer.write(TTTT + "label=\"" + m.getName() + "\"" + N);
        writer.write(TTT + "}" + N);
    }

    private void writeServiceMachine(ServiceMachine sm) throws IOException {
        Service s = sm.getService();
        String style = statefulnessToStyle(s.getStatefulness());
        String color = null;
        if (s.getStatefulness() == Statefulness.PARTITIONED) {
            color = partitionColor(s.getPartitioning());
        } else {
            color = "black";
        }
        writer.write(TTTT + "node [style=\"" + style + ", " + color + "\"]; " + sm.getName() + ";" + N);
    }

    private String partitionColor(String partitioning) {
        return partitionColorAssignments.computeIfAbsent(partitioning, p -> partitionColors[partitionColorAssignments.size() % partitionColors.length]);
    }

    private String statefulnessToStyle(Statefulness statefulness) {
        switch (statefulness) {
        case PARTITIONED:
            return "partitionedservice";
        case STATEFUL:
            return "statefulservice";
        case STATELESS:
            return "statelessservice";
        default:
            throw new IllegalArgumentException("Unknown statefulness: " + statefulness);
        }
    }

    public void close() throws IOException {
        writer.close();
    }

}
