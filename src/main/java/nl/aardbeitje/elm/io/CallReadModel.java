package nl.aardbeitje.elm.io;

import nl.aardbeitje.elm.CallType;

public class CallReadModel {

    private int id;
    private CallType type;
    private boolean routable;
    private String sourceName;
    private String targetName;
    private String data;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public CallType getType() {
        return type;
    }
    public void setCallType(CallType type) {
        this.type = type;
    }
    public boolean isRoutable() {
        return routable;
    }
    public void setRoutable(boolean routable) {
        this.routable = routable;
    }
    public String getSourceName() {
        return sourceName;
    }
    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
    public String getTargetName() {
        return targetName;
    }
    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }

}
