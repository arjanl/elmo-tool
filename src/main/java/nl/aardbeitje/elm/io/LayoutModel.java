package nl.aardbeitje.elm.io;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashSet;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;

public class LayoutModel {

    private Collection<VertexPoint2D> services = new HashSet<>();
    private Collection<VertexPoint2D> serviceMachines = new HashSet<>();
    
    public static class VertexPoint2D {
        private String name;
        private Point2D location;
        
        public VertexPoint2D() {
        }
        
        public VertexPoint2D(String name, Point2D location) {
            this.name = name;
            this.location = location;
        }
        
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public Point2D getLocation() {
            return location;
        }
        public void setLocation(Point2D location) {
            this.location = location;
        }
        
    }
    
    public static LayoutModel createLayoutModel(VisualizationViewer<Service, Call> serviceViewer, VisualizationViewer<ServiceMachine, Connection> serviceMachineViewer) {
        LayoutModel layoutModel = new LayoutModel();
        
        Layout<Service, Call> servicesLayout = serviceViewer.getModel().getGraphLayout();
        for (Service s : servicesLayout.getGraph().getVertices()) {
            layoutModel.getServices().add(new VertexPoint2D(s.getName(), servicesLayout.transform(s)));
        }

        Layout<ServiceMachine, Connection> serviceMachinesLayout = serviceMachineViewer.getModel().getGraphLayout();
        for (ServiceMachine sm : serviceMachinesLayout.getGraph().getVertices()) {
            layoutModel.getServiceMachines().add(new VertexPoint2D(sm.getName(), serviceMachinesLayout.transform(sm)));
        }

        return layoutModel;
    }
    
    public static void applyServicesLayoutModel( LayoutModel layout, VisualizationViewer<Service, Call> serviceViewer, Deployment deployment) {
        Layout<Service, Call> servicesLayout = serviceViewer.getModel().getGraphLayout();
        for (Service s : servicesLayout.getGraph().getVertices()) {
            Point2D location = locate(layout.getServices(), s, deployment);
            if (location!=null) {              
                servicesLayout.setLocation(s, location);
            }
        }
    }
    
    
    public static void applyServiceMachinesLayoutModel(LayoutModel layout, VisualizationViewer<ServiceMachine, Connection> serviceMachineVisualizationViewer) {
        Layout<ServiceMachine, Connection> serviceMachinesLayout = serviceMachineVisualizationViewer.getModel().getGraphLayout();
        for (ServiceMachine sm : serviceMachinesLayout.getGraph().getVertices()) {
            Point2D location = find(layout.getServiceMachines(), sm);
            if (location!=null) {
                serviceMachinesLayout.setLocation(sm, location);
            }
        }
    }

    private static Point2D locate(Collection<VertexPoint2D> services, Service service, Deployment deployment) {
        Point2D original = find(services,service);

        // new service without a location yet, so find a related service to locate it next to. 
        if (original==null) {
            for( Call c: deployment.getArchitecture().getCalls(service)) {
                original = find( services, c.getSource().equals(service)?c.getTarget():c.getSource());
                if (original!=null) {
                    original.setLocation(original.getX() + 200, original.getY());
                    break;
                } 
            }
        }
        
        return original;
    }
    
    private static Point2D find(Collection<VertexPoint2D> services, Service service) {
        return services.stream().filter(s -> s.getName().equals(service.getName())).map( VertexPoint2D::getLocation).findFirst().orElse(null);
    }
    private static Point2D find(Collection<VertexPoint2D> serviceMachines, ServiceMachine serviceMachine) {
        return serviceMachines.stream().filter(s -> s.getName().equals(serviceMachine.getName())).map( VertexPoint2D::getLocation).findFirst().orElse(null);
    }


    public Collection<VertexPoint2D> getServices() {
        return services;
    }

    public void setServices(Collection<VertexPoint2D> services) {
        this.services = services;
    }

    public Collection<VertexPoint2D> getServiceMachines() {
        return serviceMachines;
    }

    public void setServiceMachines(Collection<VertexPoint2D> serviceMachines) {
        this.serviceMachines = serviceMachines;
    }

    

}
