package nl.aardbeitje.elm;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Connection implements Edge, HasOptionality {
    private Optionality optionality;
    private final Call call;
    private ServiceMachine source;
    private ServiceMachine target;

    public Connection(ServiceMachine source, ServiceMachine target, Call call) {
        this.source = source;
        this.target = target;
        this.call = call;
        this.optionality = deriveOptionality(call);
    }

    public Connection(ServiceMachine source, ServiceMachine target, Call call, Optionality optionality) {
        this.source = source;
        this.target = target;
        this.call = call;
        this.optionality = optionality;
    }

    private Optionality deriveOptionality(Call c) {
        if (c.isRoutable()) {
            Service t = c.isPulledFrom() ? c.getSource() : c.getTarget();
            switch (t.getStatefulness()) {
            case STATEFUL:
                return Optionality.DEPLOYMENT_OPTIONAL;
            case PARTITIONED:
                return Optionality.RUNTIME_OPTIONAL;
            case STATELESS:
                return Optionality.DEPLOYMENT_OPTIONAL;
            default:
                throw new IllegalArgumentException("Unknown statefulness: " + t.getStatefulness());
            }
        } else {
            return Optionality.COMPULSORY;
        }
    }

    public void updateOptionality(Optionality optionality) {
        this.optionality = optionality;
    }

    @JsonIgnore
    public CallType getCallType() {
        return call.getType();
    }

    @JsonIgnore
    public boolean isPushedTo() {
        return call.getType() == CallType.PUSHES_TO;
    }

    @JsonIgnore
    public boolean isPulledFrom() {
        return call.getType() == CallType.PULLED_FROM;
    }

    @JsonIgnore
    public boolean isLocal() {
        return getSource().getMachine().equals(getTarget().getMachine());
    }

    public String getSourceName() {
        return source.getName();
    }

    @JsonIgnore
    public ServiceMachine getSource() {
        return source;
    }

    public String getTargetName() {
        return target.getName();
    }

    @JsonIgnore
    public ServiceMachine getTarget() {
        return target;
    }

    public String toString() {
        return getSource().getName() + getCall().asOperator() + getTarget().getName();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getSource() == null) ? 0 : getSource().hashCode());
        result = prime * result + ((getTarget() == null) ? 0 : getTarget().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Connection other = (Connection) obj;
        if (getSource() == null) {
            if (other.getSource() != null)
                return false;
        } else if (!getSource().equals(other.getSource()))
            return false;
        if (getTarget() == null) {
            if (other.getTarget() != null)
                return false;
        } else if (!getTarget().equals(other.getTarget()))
            return false;
        return true;
    }

    public Optionality getOptionality() {
        return optionality;
    }

    @JsonIgnore
    public Call getCall() {
        return call;
    }

    public int getCallId() {
        return call.getId();
    }

    @JsonIgnore
    public Connection recreate(Deployment newDeployment) {
        return newDeployment.getConnection(call.recreate(newDeployment), source.getMachine().recreate(newDeployment), target.getMachine().recreate(newDeployment));
    }

}
