package nl.aardbeitje.elm.gui;

import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.analysis.Delta;
import nl.aardbeitje.elm.analysis.DeploymentAnalysis;
import nl.aardbeitje.elm.refactorings.RefactorAlternatives;
import nl.aardbeitje.elm.refactorings.RefactorStrategy;
import nl.aardbeitje.elm.refactorings.RefactorStrategyFactory;

public class ManualRefactoringsController {

    private Supplier<VBox> getBox;
    private Supplier<Deployment> getDeployment;
    private Supplier<DeploymentAnalysis> getDeploymentAnalysis;
    private Supplier<Set<String>> getSelectedServices;
    private Runnable resetLayout;
    private Consumer<Set<String>> selectServices;

    public ManualRefactoringsController(Supplier<VBox> getBox, Supplier<Deployment> getDeployment, Supplier<DeploymentAnalysis> getDeploymentAnalysis, Supplier<Set<String>> getSelectedServices, Consumer<Set<String>> selectServices, Runnable resetLayout) {
        this.getBox = getBox;
        this.getDeployment = getDeployment;
        this.getDeploymentAnalysis = getDeploymentAnalysis;
        this.getSelectedServices = getSelectedServices;
        this.selectServices = selectServices;
        this.resetLayout = resetLayout;
    }

    public void createManualRefactoringButtons(Service service) {
        VBox box = getBox.get();
        box.getChildren().clear();
        Deployment deployment = getDeployment.get();
        for (RefactorAlternatives oa : RefactorStrategyFactory.getAllRefactoringStrategies(deployment, service)) {
            createRefactorBar(deployment, oa);
        }
    }

    public void createManualRefactoringButtons(Call call) {
        VBox box = getBox.get();
        box.getChildren().clear();
    }

    private void createRefactorBar(Deployment deployment, RefactorAlternatives oa) {
        VBox box = getBox.get();
        ToolBar bar = new ToolBar();
        bar.setOrientation(Orientation.HORIZONTAL);
        for (RefactorStrategy os : oa.getStrategies()) {
            bar.getItems().add(createRefactorButton(deployment, os));
        }
        box.getChildren().add(bar);
    }

    private Button createRefactorButton(Deployment deployment, RefactorStrategy rs) {
        Button button = new Button();
        button.setText(rs.toString());
        button.setTooltip(new Tooltip(getEffect(getDeploymentAnalysis.get(), rs)));
        button.setOnAction(e -> apply(deployment, rs));
        return button;
    }

    private String getEffect(DeploymentAnalysis deploymentAnalyse, RefactorStrategy refactorStrategy) {
        Deployment deployment = getDeployment.get();
        Deployment deploymentB = new Deployment(UUID.randomUUID().toString(), deployment);
        refactorStrategy.apply(deploymentB);
        Delta delta = new Delta(deploymentAnalyse, new DeploymentAnalysis(deploymentB));
        return delta.toString();
    }

    private void apply(Deployment deployment, RefactorStrategy strategy) {
        Set<String> selectedServices = getSelectedServices.get();

        // preserve layout
        strategy.apply(deployment);
        resetLayout.run();

        selectServices.accept(selectedServices);
    }
}
