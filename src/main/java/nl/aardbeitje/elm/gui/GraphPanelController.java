package nl.aardbeitje.elm.gui;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.SwingUtilities;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode;
import edu.uci.ics.jung.visualization.picking.PickedState;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;
import javafx.application.Platform;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.analysis.DeploymentAnalysis;
import nl.aardbeitje.elm.analysis.PrintAnalysis;
import nl.aardbeitje.elm.gui.graphlayout.ArrowShapeTransformer;
import nl.aardbeitje.elm.gui.graphlayout.CallLabelTransformer;
import nl.aardbeitje.elm.gui.graphlayout.ConnectionLabelTransformer;
import nl.aardbeitje.elm.gui.graphlayout.MachineGroupPainter;
import nl.aardbeitje.elm.gui.graphlayout.OptionalityStrokeTransformer;
import nl.aardbeitje.elm.gui.graphlayout.ReversibleArrowEdgeRenderer;
import nl.aardbeitje.elm.gui.graphlayout.ReversibleQuadCurve;
import nl.aardbeitje.elm.gui.graphlayout.ServiceColorTransformer;
import nl.aardbeitje.elm.gui.graphlayout.ServiceLabelTransformer;
import nl.aardbeitje.elm.gui.graphlayout.ServiceMachineColorTransformer;
import nl.aardbeitje.elm.gui.graphlayout.ServiceMachineLabelTransformer;
import nl.aardbeitje.elm.gui.graphlayout.ServiceMachineTransformer;
import nl.aardbeitje.elm.gui.graphlayout.ServiceTransformer;
import nl.aardbeitje.elm.gui.graphlayout.StatefulnessStrokeTransformer;
import nl.aardbeitje.elm.io.DeploymentReader;
import nl.aardbeitje.elm.io.LayoutModel;
import nl.aardbeitje.elm.io.ParsedDeployment;

/**
 * Quick hack to show deployment and architecture graphs.
 */
public class GraphPanelController {
    private Deployment deployment;

    private DeploymentAnalysis deploymentAnalysis;
    private PrintAnalysis printAnalysis;

    @FXML
    private AnchorPane servicesPane;
    @FXML
    private SwingNode servicesNode;

    @FXML
    private AnchorPane machinesPane;
    @FXML
    private SwingNode machinesNode;

    @FXML
    private Label architectureAnalysisLabel;

    @FXML
    private VBox manualRefactoringsBox;

    @FXML
    private VBox editArchitectureBox;

    @FXML
    private VBox proposeRefactoringsBox;

    @FXML
    private Label deploymentAnalysisLabel;

    @FXML
    private RadioButton impactRadio;
    @FXML
    private RadioButton sensitivitiesRadio;

    private volatile Boolean isClickInProgress = false;

    // we need these as a field since the event handlers below need to keep
    // their reference up to date (or we'd have to reinstall them each time)
    private VisualizationViewer<ServiceMachine, Connection> serviceMachinesViewer;
    private VisualizationViewer<Service, Call> servicesViewer;
    private MainController mainController;
    private EditArchitectureController editArchitectureController;
    private ProposeRefactoringsController proposeRefactoringsController;
    private ManualRefactoringsController manualRefactoringsController;

    public GraphPanelController() {
        editArchitectureController = new EditArchitectureController(() -> editArchitectureBox, this::getDeployment, this::resetLayout);
        proposeRefactoringsController = new ProposeRefactoringsController(() -> proposeRefactoringsBox, this::getDeployment, () -> deploymentAnalysis, () -> {
            Node currentFile = mainController.getCurrentFile();
            return mainController.fileClone(currentFile);
        });
        manualRefactoringsController = new ManualRefactoringsController(() -> manualRefactoringsBox, this::getDeployment, () -> deploymentAnalysis, this::getSelectedServices, this::selectServices, this::resetLayout);

    }

    public void readFile(String name, InputStream in) throws IOException {
        LayoutModel layout = loadGraph(name, in);
        createGraphs(deployment, layout);

        servicesPane.widthProperty().addListener((w, o, n) -> servicesNode.resize(n.intValue(), servicesPane.getHeight()));
        servicesPane.heightProperty().addListener((w, o, n) -> servicesNode.resize(servicesPane.getWidth(), n.intValue()));
        machinesPane.widthProperty().addListener((w, o, n) -> machinesNode.resize(n.intValue(), machinesPane.getHeight()));
        machinesPane.heightProperty().addListener((w, o, n) -> machinesNode.resize(machinesPane.getWidth(), n.intValue()));
    }

    private void createGraphs(Deployment deployment, LayoutModel layout) {
        createServicesGraph(deployment, layout);
        createServiceMachinesGraph(deployment, layout);
    }

    public void clone(String name, GraphPanelController other) {
        Deployment newDeployment = new Deployment(name, other.getDeployment());
        initDeployment(newDeployment);
        createGraphs(newDeployment, null);
        applyLayout(other.servicesViewer, other.serviceMachinesViewer);
    }

    private void initDeployment(Deployment newDeployment) {
        deployment = newDeployment;
        deploymentAnalysis = new DeploymentAnalysis(deployment);
        printAnalysis = new PrintAnalysis(deploymentAnalysis);
    }

    private void applyLayout(VisualizationViewer<Service, Call> otherServicesViewer, VisualizationViewer<ServiceMachine, Connection> otherServiceMachinesViewer) {

        Layout<Service, Call> servicesLayout = servicesViewer.getModel().getGraphLayout();
        Layout<Service, Call> otherServicesLayout = otherServicesViewer.getModel().getGraphLayout();
        for (Service s : servicesLayout.getGraph().getVertices()) {
            servicesLayout.setLocation(s, otherServicesLayout.transform(s));
        }

        Layout<ServiceMachine, Connection> serviceMachinesLayout = serviceMachinesViewer.getModel().getGraphLayout();
        Layout<ServiceMachine, Connection> otherServiceMachinesLayout = otherServiceMachinesViewer.getModel().getGraphLayout();
        for (ServiceMachine sm : serviceMachinesLayout.getGraph().getVertices()) {
            serviceMachinesLayout.setLocation(sm, otherServiceMachinesLayout.transform(sm));
        }

    }

    public VisualizationViewer<Service, Call> getServiceVisualizationViewer() {
        return servicesViewer;
    }

    public VisualizationViewer<ServiceMachine, Connection> getServiceMachineVisualizationViewer() {
        return serviceMachinesViewer;
    }

    private void createServicesGraph(Deployment deployment, LayoutModel layout) {
        servicesViewer = createServicesGraph(deployment);
        createServicesPickedState();
        createCallsPickedState();

        if (layout != null) {
            LayoutModel.applyServicesLayoutModel(layout, servicesViewer, deployment);
        }

        GraphZoomScrollPane scrollPane = new GraphZoomScrollPane(servicesViewer);
        servicesNode.setContent(scrollPane);
    }

    private void createServiceMachinesGraph(Deployment deployment, LayoutModel layout) {
        serviceMachinesViewer = createServiceMachinesGraph(deployment);
        createMachinesPickedState();
        createConnectionsPickedState();

        if (layout != null) {
            LayoutModel.applyServiceMachinesLayoutModel(layout, serviceMachinesViewer);
        }

        GraphZoomScrollPane scrollPane = new GraphZoomScrollPane(serviceMachinesViewer);
        machinesNode.setContent(scrollPane);
    }

    private VisualizationViewer<Service, Call> createServicesGraph(Deployment deployment) {
        Layout<Service, Call> l = new FRLayout<>(deployment.getArchitecture().getGraph());
        l.setSize(new Dimension((int) servicesNode.getBoundsInParent().getWidth(), (int) servicesNode.getBoundsInParent().getHeight()));
        VisualizationViewer<Service, Call> viewer = new VisualizationViewer<>(l);
        viewer.getRenderer().setEdgeRenderer(new ReversibleArrowEdgeRenderer<Service, Call>());
        viewer.getRenderContext().setEdgeArrowTransformer(new ArrowShapeTransformer<>());
        viewer.getRenderContext().setEdgeShapeTransformer(new ReversibleQuadCurve<>());
        viewer.getRenderContext().setVertexShapeTransformer(new ServiceTransformer());
        viewer.getRenderContext().setVertexLabelTransformer(new ServiceLabelTransformer());
        viewer.getRenderContext().setVertexStrokeTransformer(new StatefulnessStrokeTransformer<Service>());
        viewer.getRenderContext().setEdgeLabelTransformer(new CallLabelTransformer());
        viewer.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);
        viewer.getRenderContext().setVertexFillPaintTransformer(new ServiceColorTransformer(deployment.getArchitecture(), viewer.getPickedVertexState(), sensitivitiesRadio.selectedProperty()));
        DefaultModalGraphMouse<Service, Call> graphMouse = new DefaultModalGraphMouse<>();
        graphMouse.setMode(Mode.PICKING);
        viewer.setGraphMouse(graphMouse);
        return viewer;
    }

    private PickedState<Call> createCallsPickedState() {
        PickedState<Call> callsPickedState = servicesViewer.getPickedEdgeState();
        callsPickedState.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED && !isClickInProgress) {
                runLater(() -> {
                    isClickInProgress = true;
                    click((Call) e.getItem());
                    isClickInProgress = false;
                });
            }

            
        });
        return callsPickedState;
    }

    private PickedState<Service> createServicesPickedState() {
        // Attach the listener that will print when the vertices selection
        // changes.
        PickedState<Service> servicesPickedState = servicesViewer.getPickedVertexState();
        servicesPickedState.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED && !isClickInProgress) {
                runLater(() -> {
                    isClickInProgress = true;
                    click((Service) e.getItem());
                    isClickInProgress = false;
                });
            }
        });
        return servicesPickedState;
    }

    private void click(Service service) {
        for (ServiceMachine sm : deployment.getServiceMachines()) {
            serviceMachinesViewer.getPickedVertexState().pick(sm, sm.getService().equals(service));
        }
        analyse(service);
    }
    
    private void click(Call call) {
        analyse(call);
    }

    private void click(Connection connection) {
        analyse(connection);
    }

    private VisualizationViewer<ServiceMachine, Connection> createServiceMachinesGraph(Deployment deployment) {

        Layout<ServiceMachine, Connection> l = new FRLayout<>(deployment.getGraph());
        l.setSize(new Dimension((int) machinesNode.getBoundsInParent().getWidth(), (int) machinesNode.getBoundsInParent().getHeight()));
        serviceMachinesViewer = new VisualizationViewer<>(l);

        serviceMachinesViewer.getRenderer().setEdgeRenderer(new ReversibleArrowEdgeRenderer<ServiceMachine, Connection>());
        serviceMachinesViewer.getRenderContext().setEdgeArrowTransformer(new ArrowShapeTransformer<>());
        serviceMachinesViewer.getRenderContext().setEdgeShapeTransformer(new ReversibleQuadCurve<>());
        serviceMachinesViewer.getRenderContext().setEdgeStrokeTransformer(new OptionalityStrokeTransformer<Connection>());
        serviceMachinesViewer.getRenderContext().setVertexShapeTransformer(new ServiceMachineTransformer());
        serviceMachinesViewer.getRenderContext().setVertexLabelTransformer(new ServiceMachineLabelTransformer());
        serviceMachinesViewer.getRenderContext().setVertexStrokeTransformer(new StatefulnessStrokeTransformer<ServiceMachine>());
        serviceMachinesViewer.getRenderContext().setVertexFillPaintTransformer(new ServiceMachineColorTransformer(deployment, serviceMachinesViewer.getPickedVertexState(), sensitivitiesRadio.selectedProperty()));
        serviceMachinesViewer.getRenderContext().setEdgeLabelTransformer(new ConnectionLabelTransformer());
        serviceMachinesViewer.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);
        serviceMachinesViewer.addPreRenderPaintable(new MachineGroupPainter(serviceMachinesViewer, deployment));
        DefaultModalGraphMouse<ServiceMachine, Connection> graphMouse = new DefaultModalGraphMouse<>();
        graphMouse.setMode(Mode.PICKING);
        serviceMachinesViewer.setGraphMouse(graphMouse);

        return serviceMachinesViewer;
    }

    private PickedState<Connection> createConnectionsPickedState() {
        PickedState<Connection> connectionsPickedState = serviceMachinesViewer.getPickedEdgeState();
        connectionsPickedState.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED && !isClickInProgress) {
                runLater(() -> {
                    isClickInProgress = true;
                    click((Connection) e.getItem());
                    isClickInProgress = false;
                });
            }
        });
        return connectionsPickedState;
    }

    private PickedState<ServiceMachine> createMachinesPickedState() {
        // Attach the listener that will print when the vertices selection
        // changes.
        PickedState<ServiceMachine> machinesPickedState = serviceMachinesViewer.getPickedVertexState();
        machinesPickedState.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED && !isClickInProgress) {
                runLater(() -> {
                    isClickInProgress = true;
                    click((ServiceMachine) e.getItem());
                    isClickInProgress = false;
                });
            }
        });

        return machinesPickedState;
    }

    private void click(ServiceMachine sm) {
        for (Service service : deployment.getArchitecture().getServices()) {
            servicesViewer.getPickedVertexState().pick(service, sm.getService().equals(service));
        }

        analyse(sm);
    }

    private void createRefactorButtons(Service service) {
        editArchitectureController.createButtons(service);
        manualRefactoringsController.createManualRefactoringButtons(service);
        proposeRefactoringsController.createButtons(service);

    }

    private void createRefactorButtons(Call call) {
        editArchitectureController.createButtons(call);
        manualRefactoringsController.createManualRefactoringButtons(call);
        proposeRefactoringsController.createButtons(call);
    }

    public Set<String> getSelectedServices() {
        return servicesViewer.getPickedVertexState().getPicked().stream().map(Service::getName).collect(Collectors.toSet());
    }

    public void selectServices(Set<String> selectedServices) {
        // deselect all
        for (Service s : deployment.getArchitecture().getServices()) {
            servicesViewer.getPickedVertexState().pick(s, false);
        }
        isClickInProgress = true;
        // select previously selected one
        for (String selectedService : selectedServices) {
            for (Service s : deployment.getArchitecture().getServices()) {
                if (s.getName().equals(selectedService)) {
                    servicesViewer.getPickedVertexState().pick(s, true);
                }
            }
        }
        isClickInProgress = false;

    }

    public void resetLayout() {
        LayoutModel layout = LayoutModel.createLayoutModel(servicesViewer, serviceMachinesViewer);
        initDeployment(deployment);
        createGraphs(deployment, layout);
    }

    private static void runLater(Runnable runnable) {
        SwingUtilities.invokeLater(() -> Platform.runLater(runnable));
    }

    private LayoutModel loadGraph(String name, InputStream in) throws IOException {
        ParsedDeployment pd = new DeploymentReader(name, in).read();
        DeploymentAnalysis da = new DeploymentAnalysis(pd.getDeployment());
        if (!da.isWellformed()) {
        	Alert alert = new Alert(AlertType.WARNING);
        	alert.setTitle("Not wellformed");
        	alert.setContentText("The read file contains a deployment or architecture that is not well-formed. Check log file for hints.");
        	alert.showAndWait();
        }
        initDeployment(new Deployment(name, pd.getDeployment()));
        return pd.getLayout();
    }

    private void analyse(ServiceMachine sm) {
        createRefactorButtons(sm.getService());
        architectureAnalysisLabel.setText(printAnalysis.analyse(sm));
    }

    private void analyse(Connection c) {
        createRefactorButtons(c.getCall());
        architectureAnalysisLabel.setText(printAnalysis.analyse(c.getCall()));
    }

    private void analyse(Service s) {
        long start = System.currentTimeMillis();

        createRefactorButtons(s);
        architectureAnalysisLabel.setText(printAnalysis.analyse(s));
        long end = System.currentTimeMillis();
        System.out.println("millis for analysis: " + (end-start));
    }

    private void analyse(Call c) {
        createRefactorButtons(c);
        architectureAnalysisLabel.setText(printAnalysis.analyse(c));
    }

    public Deployment getDeployment() {
        return deployment;
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

}
