package nl.aardbeitje.elm.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Orientation;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.VBox;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.analysis.DeploymentAnalysis;
import nl.aardbeitje.elm.refactorings.RefactorGoal;
import nl.aardbeitje.elm.refactorings.RefactorStrategy;
import nl.aardbeitje.elm.refactorings.RefactorWizard;
import nl.aardbeitje.elm.refactorings.RefactorWizardFactory;

public class ProposeRefactoringsController {

    private Supplier<VBox> getBox;
    private Supplier<Deployment> getDeployment;
    private Supplier<DeploymentAnalysis> getDeploymentAnalysis;
    private Supplier<GraphPanelController> cloner;

    private final List<RefactorGoal> goals = new ArrayList<>();
    private Label targets;

    public ProposeRefactoringsController(Supplier<VBox> getBox, Supplier<Deployment> getDeployment, Supplier<DeploymentAnalysis> getDeploymentAnalysis, Supplier<GraphPanelController> cloner) {
        this.getBox = getBox;
        this.getDeployment = getDeployment;
        this.getDeploymentAnalysis = getDeploymentAnalysis;
        this.cloner = cloner;
        this.targets = new Label();
    }

    public void createButtons(Call call) {
        VBox box = getBox.get();
        box.getChildren().clear();
    }

    public void createButtons(Service service) {
        VBox box = getBox.get();
        DeploymentAnalysis deploymentAnalysis = getDeploymentAnalysis.get();
        box.getChildren().clear();

        ToolBar bar = new ToolBar();
        Button goButton = new Button("go!");
        goButton.setOnAction(e -> apply());
        Button clearButton = new Button("clear");
        clearButton.setOnAction(e -> {
            goals.clear();
            renderTargets();
        });

        renderTargets();

        bar.getItems().add(goButton);
        bar.getItems().add(clearButton);
        bar.getItems().add(targets);

        box.getChildren().add(bar);

        for (Service sensitivity : deploymentAnalysis.forciblySensitiveServices(service)) {
            if (!sensitivity.equals(service)) {
                createSensitivityBar(service, sensitivity);
            }
        }
        for (Service sensitivity : deploymentAnalysis.voluntarilySensitiveServices(service)) {
            if (!sensitivity.equals(service)) {
                createSensitivityBar(service, sensitivity);
            }
        }
    }

    private void renderTargets() {
        targets.setText(String.join(", ", goals.stream().map(g -> g.toString()).collect(Collectors.toList())));
    }

    private void createSensitivityBar(Service service, Service sensitivity) {
        VBox box = getBox.get();
        DeploymentAnalysis deploymentAnalysis = getDeploymentAnalysis.get();
        ToolBar bar = new ToolBar();

        RefactorGoal refactoringGoal = new RefactorGoal(service, sensitivity, deploymentAnalysis.voluntarilySensitive(service, sensitivity));
        Button button = new Button();
        button.setText(refactoringGoal.toString());
        button.setOnAction(e -> add(button, refactoringGoal));
        bar.getItems().add(button);

        bar.setOrientation(Orientation.HORIZONTAL);
        box.getChildren().add(bar);
    }

    private void add(Button button, RefactorGoal goal) {
        button.setDisable(true);
        goals.add(goal);
        renderTargets();
    }

    private void apply() {
        
        if (goals.isEmpty()) {
            Alert alert = new Alert(AlertType.INFORMATION, "No goals selected yet, choose one or more goals first", ButtonType.OK);
            alert.showAndWait();
        } else {
            Deployment deployment = getDeployment.get();
            ProgressForm progressForm = new ProgressForm();
    
            RefactorWizard task = RefactorWizardFactory.createWizard(deployment, goals);
            progressForm.activateProgressBar(task);
            task.setOnSucceeded(event -> showWizardResult(progressForm, task));
            task.setOnCancelled(event -> showWizardResult(progressForm, task));
    
            progressForm.getDialogStage().show();
    
            Thread thread = new Thread(task);
            thread.start();
        }
    }

    private void showWizardResult(ProgressForm progressForm, RefactorWizard task) {
        progressForm.getDialogStage().close();

        Set<List<RefactorStrategy>> selected = createSelectRefactoringDialog(task);

        for (List<RefactorStrategy> solution : selected) {
            GraphPanelController clone = cloner.get();
            Deployment deployment = clone.getDeployment();
            for (RefactorStrategy s : solution) {
                s.apply(deployment);
            }
        }

    }

    private Set<List<RefactorStrategy>> createSelectRefactoringDialog(RefactorWizard task) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Suggested refactorings:");
        alert.setTitle("Suggested refactorings");
        alert.setResizable(true);

        ListView<Item> listView = new ListView<>();
        for (List<RefactorStrategy> solution : task.getSolutions()) {
            Item item = new Item(solution.toString(), true, solution);
            listView.getItems().add(item);
        }

        listView.setCellFactory(CheckBoxListCell.forListView(Item::onProperty));

        ButtonBar buttonBar = new ButtonBar();
        Button selectAll = new Button("select all");
        Button selectNone = new Button("select none");
        selectAll.setOnAction(e -> listView.getItems().forEach(i -> i.setOn(true)));
        selectNone.setOnAction(e -> listView.getItems().forEach(i -> i.setOn(false)));
        buttonBar.getButtons().add(selectAll);
        buttonBar.getButtons().add(selectNone);

        listView.setMaxHeight(300);
        listView.setPrefWidth(800);

        alert.getDialogPane().setHeader(buttonBar);
        alert.getDialogPane().setContent(listView);

        alert.showAndWait();

        return listView.getItems().stream().filter(Item::isOn).map(Item::getSolution).collect(Collectors.toSet());
    }

    public static class Item {
        private final StringProperty name = new SimpleStringProperty();
        private final BooleanProperty on = new SimpleBooleanProperty();
        private final List<RefactorStrategy> solution;

        public Item(String name, boolean on, List<RefactorStrategy> solution) {
            this.solution = solution;
            setName(name);
            setOn(on);
        }

        public final StringProperty nameProperty() {
            return this.name;
        }

        public final String getName() {
            return this.nameProperty().get();
        }

        public final void setName(final String name) {
            this.nameProperty().set(name);
        }

        public final BooleanProperty onProperty() {
            return this.on;
        }

        public final boolean isOn() {
            return this.onProperty().get();
        }

        public final void setOn(final boolean on) {
            this.onProperty().set(on);
        }

        public List<RefactorStrategy> getSolution() {
            return solution;
        }

        @Override
        public String toString() {
            return getName();
        }

    }

}
