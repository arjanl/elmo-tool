package nl.aardbeitje.elm.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.io.LatexWriter;
import nl.aardbeitje.elm.io.LayoutModel;
import nl.aardbeitje.elm.io.MarkableFileInputStream;
import nl.aardbeitje.elm.io.WriteModel;

public class MainController implements DeploymentsView {
    private static final ExtensionFilter EXTENSION_FILTER_LATEX_FILES = new ExtensionFilter("LaTex files", "*.tex");

    private static final ExtensionFilter EXTENSION_FILTER_ELM_FILES = new ExtensionFilter("ELM files", "*.elm");

    private static final ExtensionFilter EXTENSION_FILTER_ALL_FILES = new ExtensionFilter("All Files", "*.*");

    @FXML
    private MenuItem menuOpen;

    @FXML
    private MenuItem menuSaveAs;

    @FXML
    private MenuItem menuClose;

    @FXML
    private MenuItem menuExport;

    @FXML
    private MenuItem menuClone;
    
    @FXML
    private TabPane mainTabPane;
    
    

    @FXML
    private Tab comparisonTab;

    private Stage stage;

    private Map<Node, GraphPanelController> deployments = new LinkedHashMap<>();

    public void initialize() {
        menuOpen.setOnAction(e -> fileOpen());
        menuSaveAs.setOnAction(e -> fileSaveAs());
        menuClose.setOnAction(e -> fileClose());
        menuExport.setOnAction(e -> fileExport());
        menuClone.setOnAction(e -> fileClone());

        createComparisonTab();

    }

    private void createComparisonTab() {
        try {
            FXMLLoader fxmlLoader = ResourceLoader.getFXMLLoader("comparisonpanel.fxml");
            Parent root = fxmlLoader.load();
            comparisonTab.setContent(root);
            ComparisonController controller = fxmlLoader.getController();
            controller.setDeploymentsView(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fileSaveAs() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save as ELM (Json) files");
            fileChooser.getExtensionFilters().addAll(EXTENSION_FILTER_ELM_FILES, EXTENSION_FILTER_ALL_FILES);
            File selectedFile = fileChooser.showSaveDialog(stage);
            saveAsFile(selectedFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fileExport() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Export as dot2tex");
            fileChooser.getExtensionFilters().addAll(EXTENSION_FILTER_LATEX_FILES, EXTENSION_FILTER_ALL_FILES);
            File selectedFile = fileChooser.showSaveDialog(stage);
            exportFile(selectedFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fileOpen() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open deployment or architecture");
            fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Enterprise Latency Modelling Files", "*.elm"), EXTENSION_FILTER_ALL_FILES);
            File selectedFile = fileChooser.showOpenDialog(stage);
            openFile(selectedFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fileClose() {
        Tab tab = mainTabPane.getSelectionModel().getSelectedItem();
        deployments.remove(tab.getContent());
        mainTabPane.getTabs().remove(tab);
    }

    private GraphPanelController fileClone() {
     return fileClone(getCurrentFile());
    }
    
    Node getCurrentFile() {
        return mainTabPane.getSelectionModel().getSelectedItem().getContent();
    }
    
    GraphPanelController fileClone(Node current) {
        try {
            String name = deployments.get(current).getDeployment().getName();
            int i = 1;
            while (!isUniqueName(name + "_" + i)) {
                i++;
            }
            Parent graphPanel = createGraphPanel(name + "_" + i, deployments.get(current));
            Tab tab = new Tab(deployments.get(graphPanel).getDeployment().getName());
            tab.setContent(graphPanel);
            mainTabPane.getTabs().add(tab);
            mainTabPane.getSelectionModel().select(tab);
            return deployments.get(graphPanel);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean isUniqueName(String name) {
        return deployments.values().stream().noneMatch( d -> d.getDeployment().getName().equals(name));
    }

    public void openFile(File selectedFile) throws IOException {
        Parent graphPanel = createGraphPanel(selectedFile.getName(), new MarkableFileInputStream(new FileInputStream(selectedFile)));
        Tab tab = new Tab(deployments.get(graphPanel).getDeployment().getName());
        tab.setContent(graphPanel);
        mainTabPane.getTabs().add(tab);
        mainTabPane.getSelectionModel().select(tab);
    }

    private void exportFile(File selectedFile) {
        Node node = mainTabPane.getSelectionModel().getSelectedItem().getContent();

        try (LatexWriter writer = new LatexWriter(new FileOutputStream(selectedFile))) {
            writer.write(deployments.get(node).getDeployment());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveAsFile(File selectedFile) {
        try {
            Node node = mainTabPane.getSelectionModel().getSelectedItem().getContent();
            GraphPanelController graphPanelController = deployments.get(node);
            Deployment deployment = graphPanelController.getDeployment();
            LayoutModel layout = LayoutModel.createLayoutModel(graphPanelController.getServiceVisualizationViewer(), graphPanelController.getServiceMachineVisualizationViewer());
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.writeValue(selectedFile, new WriteModel(deployment, layout));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Parent createGraphPanel(String name, InputStream in) throws IOException {
        FXMLLoader fxmlLoader = ResourceLoader.getFXMLLoader("graphpanel.fxml");
        Parent root = fxmlLoader.load();

        GraphPanelController controller = fxmlLoader.getController();
        controller.setMainController(this);
        controller.readFile(name, in);
        deployments.put(root, controller);

        return root;
    }

    private Parent createGraphPanel(String name, GraphPanelController cloneFrom) throws IOException {
        FXMLLoader fxmlLoader = ResourceLoader.getFXMLLoader("graphpanel.fxml");
        Parent root = fxmlLoader.load();

        GraphPanelController controller = fxmlLoader.getController();
        controller.setMainController(this);
        controller.clone(name, cloneFrom);
        deployments.put(root, controller);
        
        return root;
    }
    
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public Map<String, Deployment> getDeployments() {
        return deployments.values().stream().map(GraphPanelController::getDeployment).collect(Collectors.toMap(Deployment::getName, d -> d));
    }

}
