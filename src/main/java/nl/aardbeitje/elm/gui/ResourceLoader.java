package nl.aardbeitje.elm.gui;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;

/**
 * Simple helper class to solve eclipse/maven/fatjar resource loading issues.
 * 
 * @author arjanl
 *
 */
public class ResourceLoader {

    public static FXMLLoader getFXMLLoader(String name) {
        URL location = ResourceLoader.class.getResource("/" + name);
        FXMLLoader fxmlLoader = new FXMLLoader(location);

        try {
            fxmlLoader.load();
            // if successful, reset and load again
            location = ResourceLoader.class.getResource("/" + name);
            fxmlLoader = new FXMLLoader(location);

        } catch (IOException e) {
            location = ResourceLoader.class.getResource("/resources/" + name);
            fxmlLoader = new FXMLLoader(location);
        }
        return fxmlLoader;
    }

}
