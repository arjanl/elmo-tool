package nl.aardbeitje.elm.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.util.Callback;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.analysis.Comparison;
import nl.aardbeitje.elm.analysis.LoopException;

public class ComparisonController {

	@FXML
	private TableView<ServiceComparison> comparisonTable;

	@FXML
	private Button refreshButton;

	private DeploymentsView deploymentsView;

	// represents the row value
	private static class ServiceComparison {
		private final String name;
		private final Map<String, DeployedService> deployedServices;

		public ServiceComparison(String name, Map<String, DeployedService> deployedServices) {
			super();
			this.name = name;
			this.deployedServices = deployedServices;
		}
	}

	// represents the cell value
	private static class DeployedService {
		private final Deployment deployment;
		private final Set<Service> services;
		private final Comparison comparison;
		private final String originalServiceName;

		public DeployedService(Comparison comparison, Deployment deployment, String originalServiceName, Set<Service> services) {
			this.comparison = comparison;
			this.deployment = deployment;
			this.originalServiceName = originalServiceName;
			this.services = services;
		}
	}

	private static class ComparisonCell extends TableCell<ServiceComparison, DeployedService> {
		@Override
		protected void updateItem(DeployedService item, boolean empty) {
			if (!empty && item != null) {
				TextFlow text = generateCell(item.comparison, item.deployment, item.originalServiceName, item.services);
				setGraphic(text);
				this.setPrefHeight(100.0d);
			}
		}

		private TextFlow generateCell(Comparison comparison, Deployment d, String originalServiceName, Set<Service> services) {
			TextFlow content = new TextFlow();

			if (d.getArchitecture().getServiceByOriginalName(originalServiceName).isEmpty()) {
				content.getChildren().add(new Text("---"));
			} else {
				
				if (services.size()==1) {
					content.getChildren().add(new Text("Service " + originalServiceName + ": \n"));
				} else {
					StringBuilder sb = new StringBuilder("Service " + originalServiceName);
					
					for( Service s : services ) {
						sb.append( " / " + s.getName() );
					}
					sb.append(": \n");
					content.getChildren().add(new Text(sb.toString()));
					
				}
				compareSensitivity(comparison, d, originalServiceName, services, content);
				compareLatency(comparison, d, originalServiceName, services, content);
			}

			return content;
		}

		private static void compareLatency(Comparison comparison, Deployment d, String originalServiceName, Set<Service> services, TextFlow content) {
			long minimalDepth;
			try {
				minimalDepth = comparison.minimalNetworkDepth(originalServiceName);
			} catch (LoopException e) {
				content.getChildren().add(new Text("\n"));
				content.getChildren().add(new Text("network depth: infinity"));
				content.getChildren().add(new Text("\n"));
				return;
			}
			long depth = comparison.analyse(d).maximumNetworkDepth(originalServiceName);

			content.getChildren().add(new Text("\n"));
			if (depth > minimalDepth) {
				if (depth == Long.MAX_VALUE) {
					content.getChildren().add(new Text("network depth: "));
					Text t = new Text("(loop)");
					t.setFill(Color.RED);
					content.getChildren().add(t);
				} else {
					content.getChildren().add(new Text("network depth: " + depth));
					Text t = new Text("(+" + (depth - minimalDepth) + ")");
					t.setFill(Color.RED);
					content.getChildren().add(t);
				}
			} else {
				content.getChildren().add(new Text("network depth: " + depth));
			}
			content.getChildren().add(new Text("\n"));
		}

		private static void compareSensitivity(Comparison comparison, Deployment d, String originalServiceName, Set<Service> services, TextFlow content) {
			Set<Service> minimalForced = comparison.minimalForciblySensitiveServices(originalServiceName);
			Set<Service> minimalVoluntarily = comparison.minimalVoluntarilySensitiveServices(originalServiceName);

			minimalVoluntarily.removeAll(minimalForced);

			Set<Service> currentForced = comparison.analyse(d).allForciblySensitivities(originalServiceName);
			Set<Service> currentVoluntarily = comparison.analyse(d).allVoluntarilySensitivities(originalServiceName);

			currentVoluntarily.removeAll(currentForced);

			content.getChildren().add(new Text("Forcibly sensitive to: "));

			currentForced.forEach(s -> {
				Text t = new Text(s.getName() + ", ");
				if (minimalForced.contains(s)) {
					t.setFill(Color.BLACK);
				} else {
					t.setFill(Color.RED);
				}
				content.getChildren().add(t);
			});
			content.getChildren().add(new Text("\n"));
			content.getChildren().add(new Text("In addition, voluntarily sensitive to: "));

			currentVoluntarily.forEach(s -> {
				Text t = new Text(s.getName() + ", ");
				if (minimalVoluntarily.contains(s)) {
					t.setFill(Color.BLACK);
				} else {
					t.setFill(Color.RED);
				}
				content.getChildren().add(t);
			});
			content.getChildren().add(new Text("\n"));
		}

	}

	public void initialize() {
		refreshButton.setOnAction(e -> refresh());
	}

	public void refresh() {
		Comparison comparison = new Comparison(deploymentsView.getDeployments().values());
		Collection<Deployment> deployments = deploymentsView.getDeployments().values();
		
		equivalenceCheck(deployments);

		comparisonTable.getColumns().setAll(generateTableViewColumns(deployments));
		comparisonTable.getItems().clear();

		Set<String> allServiceNames = new HashSet<>();
		deploymentsView.getDeployments().values()
				.forEach(d -> d.getArchitecture().getServices().forEach(s -> allServiceNames.add(s.getOriginalName())));

		allServiceNames.forEach(s -> {
			Map<String, DeployedService> deployedServices = new LinkedHashMap<>();
			deployments.forEach(d -> {
				deployedServices.put(d.getName(), new DeployedService(comparison, d, s, d.getArchitecture().getServiceByOriginalName(s)));
			});
			ServiceComparison sc = new ServiceComparison(s, deployedServices);
			comparisonTable.getItems().add(sc);
		});

	}

	private void equivalenceCheck(Collection<Deployment> deployments) {
		Deployment d = deployments.iterator().next();
		for( Deployment d2 : deployments) {
			if (!d.isEquivalent(d2)) {
				Alert a = new Alert(AlertType.WARNING);
				a.setTitle("Comparing non-equivalent deployments");
				a.setContentText("Deployment " + d.getName() + " and " + d2.getName() + " are not equivalent. Comparison may not make sense.");
				a.showAndWait();
			}
		}
		
	}

	private Collection<TableColumn<ServiceComparison, ?>> generateTableViewColumns(Collection<Deployment> deployments) {
		Collection<TableColumn<ServiceComparison, ?>> tableViewColumns = new ArrayList<>();
		tableViewColumns.add(getServiceNameColumn());
		deployments.forEach(d -> tableViewColumns.add(getServiceComparisonColumn(d)));
		return tableViewColumns;
	}

	private TableColumn<ServiceComparison, String> getServiceNameColumn() {
		TableColumn<ServiceComparison, String> c = new TableColumn<>("Service");
		c.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<ServiceComparison, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(CellDataFeatures<ServiceComparison, String> value) {
						return new ReadOnlyObjectWrapper<>(value.getValue().name);
					}
				});
		return c;
	}

	private TableColumn<ServiceComparison, DeployedService> getServiceComparisonColumn(Deployment d) {
		TableColumn<ServiceComparison, DeployedService> c = new TableColumn<>(d.getName());
		c.setCellFactory(
				new Callback<TableColumn<ServiceComparison, DeployedService>, TableCell<ServiceComparison, DeployedService>>() {
					@Override
					public TableCell<ServiceComparison, DeployedService> call(
							TableColumn<ServiceComparison, DeployedService> value) {
						return new ComparisonCell();
					}
				});
		c.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ServiceComparison,DeployedService>, ObservableValue<DeployedService>>() {
			@Override
			public ObservableValue<DeployedService> call(CellDataFeatures<ServiceComparison, DeployedService> value) {
				return new ReadOnlyObjectWrapper<>(value.getValue().deployedServices.get(d.getName()));
			}
			
		});
		return c;
	}

	public void setDeploymentsView(DeploymentsView deploymentsView) {
		this.deploymentsView = deploymentsView;
	}
}
