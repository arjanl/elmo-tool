package nl.aardbeitje.elm.gui;

import nl.aardbeitje.elm.Deployment;

public interface DeploymentView {
    Deployment getDeployment();
}
