package nl.aardbeitje.elm.gui;

import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.VBox;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.Statefulness;

public class EditArchitectureController {

    private Supplier<VBox> getBox;
    private Runnable resetLayout;
    private Supplier<Deployment> getDeployment;

    public EditArchitectureController(Supplier<VBox> getBox, Supplier<Deployment> getDeployment, Runnable resetLayout) {
        this.getBox = getBox;
        this.getDeployment = getDeployment;
        this.resetLayout = resetLayout;
    }

    public void createButtons(Call call) {
        VBox box = getBox.get();
        box.getChildren().clear();
        ToolBar bar = new ToolBar();
        bar.setOrientation(Orientation.HORIZONTAL);
        bar.getItems().add(new Label("Change call"));
        bar.getItems().add(createChangeDataButton(call));
        bar.getItems().add(createDeleteCallButton(call));
        box.getChildren().add(bar);
    }

    public void createButtons(Service source) {
        Deployment deployment = getDeployment.get();
        VBox box = getBox.get();
        
        box.getChildren().clear();

        ToolBar bar = new ToolBar();
        bar.setOrientation(Orientation.HORIZONTAL);
        bar.getItems().add(new Label("Add push-to"));
        
        for (Service target : deployment.getArchitecture().getServices()) {
            bar.getItems().add(createAddCallButton(source, CallType.PUSHES_TO, target));
        }
        box.getChildren().add(bar);

        ToolBar bar3 = new ToolBar();
        bar3.setOrientation(Orientation.HORIZONTAL);
        bar3.getItems().add(new Label("Add pulled-from"));
        for (Service target : deployment.getArchitecture().getServices()) {
            bar3.getItems().add(createAddCallButton(source, CallType.PULLED_FROM, target));
        }
        box.getChildren().add(bar3);

        addGenericEditArchitectureButtons();
    }

    private void addGenericEditArchitectureButtons() {
        VBox box = getBox.get();
        ToolBar bar = new ToolBar();
        bar.setOrientation(Orientation.HORIZONTAL);
        bar.getItems().add(new Label("Add service"));
        bar.getItems().add(createAddServiceButton(Statefulness.STATELESS));
        bar.getItems().add(createAddServiceButton(Statefulness.STATEFUL));
        bar.getItems().add(createAddServiceButton(Statefulness.PARTITIONED));
        box.getChildren().add(bar);
    }

    private Button createAddCallButton(Service source, CallType pushesTo, Service target) {
        Button button = new Button();
        button.setText(target.getName());
        button.setOnAction(e -> createNewCall(source, pushesTo, target));
        return button;
    }

    private Button createChangeDataButton(Call call) {
        Button button = new Button();
        button.setText("Change data");
        button.setOnAction(e -> changeData(call));
        return button;
    }

    private Button createDeleteCallButton(Call call) {
        Button button = new Button();
        button.setText("Delete call");
        button.setOnAction(e -> deleteCall(call));
        return button;
    }

    private Button createAddServiceButton(Statefulness statefulness) {
        Button button = new Button();
        button.setText(statefulness.toString().toLowerCase());
        button.setOnAction(e -> createNewService(statefulness));
        return button;
    }

    private void deleteCall(Call call) {
        Deployment deployment = getDeployment.get();

        deployment.getArchitecture().removeCall(call);
        Set<Connection> connections = deployment.getConnections(call);
        connections.forEach(deployment::removeConnection);
        resetLayout.run();
    }

    private void changeData(Call call) {
        Deployment deployment = getDeployment.get();

        TextInputDialog dialog = new TextInputDialog(call.getData());
        dialog.setTitle("New call");
        dialog.setHeaderText("What data is transfered over this call?");
        dialog.setContentText("Data:");

        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()) {
            String data = result.get();
            Call newCall = new Call(call.getId(), call.getSource(), call.getTarget(), call.getType(), call.isRoutable(), data);
            deployment.getArchitecture().removeCall(call);
            deployment.getArchitecture().addCall(newCall);
            resetLayout.run();
        }
    }

    private void createNewCall(Service source, CallType callType, Service target) {
        Deployment deployment = getDeployment.get();

        TextInputDialog dialog = new TextInputDialog("Call data");
        dialog.setTitle("New call");
        dialog.setHeaderText("What data is transfered over this call?");
        dialog.setContentText("Data:");

        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()) {
            String data = result.get();
            Call call = deployment.getArchitecture().addCall(deployment.getArchitecture().getNewCallId(), source, callType, target, true, data);
            for (ServiceMachine sm : deployment.getServiceMachines(source)) {
                for (ServiceMachine sn : deployment.getServiceMachines(target)) {
                    deployment.addConnection(sm, call, sn);
                }
            }
            resetLayout.run();
        }
    }

    private void createNewService(Statefulness statefulness) {
        Deployment deployment = getDeployment.get();

        TextInputDialog dialog = new TextInputDialog("service name");
        dialog.setTitle("New " + statefulness + " service");
        dialog.setHeaderText("Please give this service a unique name.");
        dialog.setContentText("Service name:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Service service;

            if (statefulness == Statefulness.PARTITIONED) {
                TextInputDialog partitioningDialog = new TextInputDialog("service name");
                partitioningDialog.setTitle("Partitioning key for " + statefulness + " service");
                partitioningDialog.setHeaderText("Indicate the partitioning key for this service");
                partitioningDialog.setContentText("Partitioning key:");
                Optional<String> presult = partitioningDialog.showAndWait();
                if (presult.isPresent()) {
                    service = new Service(result.get(), result.get(), statefulness, presult.get());
                } else {
                    return;
                }
            } else {
                service = new Service(result.get(), result.get(), statefulness);
            }
            Machine machine = deployment.newMachine();
            ServiceMachine sm = new ServiceMachine(service, machine, deployment);
            deployment.getArchitecture().addService(service);
            deployment.addAndConnect(sm);
            resetLayout.run();
        }

    }

}
