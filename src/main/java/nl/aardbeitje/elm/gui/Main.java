package nl.aardbeitje.elm.gui;

/**
 * Fix for LauncherHelp as per
 * https://stackoverflow.com/questions/52653836/maven-shade-javafx-runtime-components-are-missing
 */
public class Main {

    public static void main(String[] args) {
        Gui.main(args);
    }

}
