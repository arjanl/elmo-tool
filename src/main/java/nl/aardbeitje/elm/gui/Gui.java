package nl.aardbeitje.elm.gui;

import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Gui extends Application {
    private static String[] args;

    public static void main(String[] args) {
        Gui.args = args;
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        try {
            stage.setTitle("Enterprise latency modelling");

            Parent main = createMainPanel(stage);

            // Create the scene and setup the stage
            Scene scene = new Scene(main, 1280, 1024);
            stage.setScene(scene);
            stage.show();

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Parent createMainPanel(Stage stage) throws IOException {
        
        FXMLLoader fxmlLoader = ResourceLoader.getFXMLLoader("mainpanel.fxml");
        Parent root = fxmlLoader.load();
        MainController controller = fxmlLoader.getController();
        controller.setStage(stage);

        for (String arg : args) {
            File f = new File(arg);
            controller.openFile(f);
        }

        return root;
    }

}
