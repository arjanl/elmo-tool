package nl.aardbeitje.elm.gui.graphlayout;

import java.awt.Shape;
import java.awt.geom.GeneralPath;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.visualization.util.ArrowFactory;
import nl.aardbeitje.elm.Edge;

public class ArrowShapeTransformer<V,E extends Edge> implements Transformer<Context<Graph<V,E>, E>, Shape> {
    public Shape transform(Context<Graph<V, E>, E> c) {

        if (c.element.isPulledFrom()) {
            GeneralPath gp = getReversedNotchedArrow(10f,10f,0f);
            return gp.createTransformedShape(null);
        } else {
            GeneralPath gp = ArrowFactory.getNotchedArrow(10f,10f,10f);
            return gp.createTransformedShape(null);
        }
    }
    
    
    /**
     * Returns an arrowhead in the shape of an isosceles triangle
     * with an isoceles-triangle notch taken out of the base,
     * with the specified base and height measurements.  It is placed
     * with the vertical axis along the negative x-axis, with its base
     * centered on (0,0).
     */
    public static GeneralPath getReversedNotchedArrow(float base, float height, float notchHeight)
    {
        GeneralPath arrow = new GeneralPath();

        arrow.moveTo(0,base/2.0f);
        arrow.lineTo(-height, 0);
        arrow.lineTo(0, -base/2.0f);
        arrow.lineTo(-(height - notchHeight), 0);
        arrow.lineTo(0,base/2.0f);
        return arrow;
    }

}