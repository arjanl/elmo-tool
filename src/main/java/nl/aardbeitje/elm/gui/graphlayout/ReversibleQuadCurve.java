package nl.aardbeitje.elm.gui.graphlayout;

import java.awt.Shape;
import java.awt.geom.QuadCurve2D;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import nl.aardbeitje.elm.Edge;

public class ReversibleQuadCurve<V, E extends Edge> extends EdgeShape.QuadCurve<V, E> {

    @Override
    public Shape transform(Context<Graph<V, E>, E> context) {
        Shape shape = super.transform(context);
        if (shape instanceof QuadCurve2D.Float) {
            QuadCurve2D.Float s = (QuadCurve2D.Float) shape;
            if (context.element.isPulledFrom()) {
                s.setCurve(s.getX1(), s.getY1(), s.getCtrlX(), -s.getCtrlY(), s.getX2(), s.getY2());
            }
            return s;
        } else {
            return shape;
        }
    }
}
