package nl.aardbeitje.elm.gui.graphlayout;

import java.awt.BasicStroke;
import java.awt.Stroke;

import org.apache.commons.collections15.Transformer;

import nl.aardbeitje.elm.HasStatefulness;

public class StatefulnessStrokeTransformer<T extends HasStatefulness> implements Transformer<T, Stroke> {

    @Override
    public Stroke transform(T s) {
        switch (s.getStatefulness()) {
        case STATEFUL:
            return new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 10.0f, new float[] { 1.0f }, 0.0f);
        case PARTITIONED:
            return new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[] { 10.0f }, 0.0f);
        case STATELESS:
            return new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[] { 1.0f, 1.0f }, 0.0f);
        default:
            throw new IllegalArgumentException("Unknown statefulness: " + s.getStatefulness());
        }
    }

}
