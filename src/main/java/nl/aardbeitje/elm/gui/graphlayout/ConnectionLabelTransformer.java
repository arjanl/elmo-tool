package nl.aardbeitje.elm.gui.graphlayout;

import org.apache.commons.collections15.Transformer;

import nl.aardbeitje.elm.Connection;

public class ConnectionLabelTransformer implements Transformer<Connection, String> {
    @Override
    public String transform(Connection c) {
        return c.getCall().getData();
    }

}
