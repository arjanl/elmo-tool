package nl.aardbeitje.elm.gui.graphlayout;

import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.graph.util.EdgeIndexFunction;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.RenderContext;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.EdgeShape.IndexedRendering;
import edu.uci.ics.jung.visualization.renderers.BasicEdgeRenderer;
import edu.uci.ics.jung.visualization.transform.LensTransformer;
import edu.uci.ics.jung.visualization.transform.MutableTransformer;
import edu.uci.ics.jung.visualization.transform.shape.GraphicsDecorator;
import nl.aardbeitje.elm.Edge;

public class ReversibleArrowEdgeRenderer<V, E extends Edge> extends BasicEdgeRenderer<V, E> {

    /**
     * Draws the edge <code>e</code>, whose endpoints are at
     * <code>(x1,y1)</code> and <code>(x2,y2)</code>, on the graphics context
     * <code>g</code>. The <code>Shape</code> provided by the
     * <code>EdgeShapeFunction</code> instance is scaled in the x-direction so
     * that its width is equal to the distance between <code>(x1,y1)</code> and
     * <code>(x2,y2)</code>.
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void drawSimpleEdge(RenderContext<V, E> rc, Layout<V, E> layout, E e) {

        GraphicsDecorator g = rc.getGraphicsContext();
        Graph<V, E> graph = layout.getGraph();
        Pair<V> endpoints = graph.getEndpoints(e);

        V v1;
        V v2;
        if (e.isPulledFrom()) {
            v2 = endpoints.getFirst();
            v1 = endpoints.getSecond();
        } else {
            v1 = endpoints.getFirst();
            v2 = endpoints.getSecond();
        }

        Point2D p1 = layout.transform(v1);
        Point2D p2 = layout.transform(v2);
        p1 = rc.getMultiLayerTransformer().transform(Layer.LAYOUT, p1);
        p2 = rc.getMultiLayerTransformer().transform(Layer.LAYOUT, p2);
        float x1 = (float) p1.getX();
        float y1 = (float) p1.getY();
        float x2 = (float) p2.getX();
        float y2 = (float) p2.getY();

        boolean isLoop = v1.equals(v2);
        Shape s2 = rc.getVertexShapeTransformer().transform(v2);
        Shape edgeShape = rc.getEdgeShapeTransformer().transform(Context.<Graph<V, E>, E> getInstance(graph, e));

        boolean edgeHit = true;
        boolean arrowHit = true;
        Rectangle deviceRectangle = null;
        JComponent vv = rc.getScreenDevice();
        if (vv != null) {
            Dimension d = vv.getSize();
            deviceRectangle = new Rectangle(0, 0, d.width, d.height);
        }

        AffineTransform xform = AffineTransform.getTranslateInstance(x1, y1);

        if (isLoop) {
            loop(s2, edgeShape, xform);
        } else if (rc.getEdgeShapeTransformer() instanceof EdgeShape.Orthogonal) {
            edgeShape = orthogonalEdge(rc, e, graph, x1, y1, x2, y2);
        } else {
            normalEdge(x1, y1, x2, y2, xform);
        }

        edgeShape = xform.createTransformedShape(edgeShape);

        MutableTransformer vt = rc.getMultiLayerTransformer().getTransformer(Layer.VIEW);
        if (vt instanceof LensTransformer) {
            vt = ((LensTransformer) vt).getDelegate();
        }
        edgeHit = vt.transform(edgeShape).intersects(deviceRectangle);

        if (edgeHit) {

            Paint oldPaint = g.getPaint();

            // get Paints for filling and drawing
            // (filling is done first so that drawing and label use same Paint)
            Paint fillPaint = rc.getEdgeFillPaintTransformer().transform(e);
            if (fillPaint != null) {
                g.setPaint(fillPaint);
                g.fill(edgeShape);
            }
            Paint drawPaint = rc.getEdgeDrawPaintTransformer().transform(e);
            if (drawPaint != null) {
                g.setPaint(drawPaint);
                g.draw(edgeShape);
            }

            float scalex = (float) g.getTransform().getScaleX();
            float scaley = (float) g.getTransform().getScaleY();
            // see if arrows are too small to bother drawing
            if (scalex < .3 || scaley < .3)
                return;

            if (rc.getEdgeArrowPredicate().evaluate(Context.<Graph<V, E>, E> getInstance(graph, e))) {

                Stroke newStroke = rc.getEdgeArrowStrokeTransformer().transform(e);
                Stroke oldStroke = g.getStroke();
                if (newStroke != null)
                    g.setStroke(newStroke);

                Shape destVertexShape = rc.getVertexShapeTransformer().transform(graph.getEndpoints(e).getSecond());

                AffineTransform xf = AffineTransform.getTranslateInstance(x2, y2);
                destVertexShape = xf.createTransformedShape(destVertexShape);

                arrowHit = rc.getMultiLayerTransformer().getTransformer(Layer.VIEW).transform(destVertexShape).intersects(deviceRectangle);
                if (arrowHit) {

                    AffineTransform at = edgeArrowRenderingSupport.getArrowTransform(rc, edgeShape, destVertexShape);
                    if (at == null)
                        return;
                    Shape arrow = rc.getEdgeArrowTransformer().transform(Context.<Graph<V, E>, E> getInstance(graph, e));
                    arrow = at.createTransformedShape(arrow);
                    g.setPaint(rc.getArrowFillPaintTransformer().transform(e));
                    g.fill(arrow);
                    g.setPaint(rc.getArrowDrawPaintTransformer().transform(e));
                    g.draw(arrow);
                }
                if (graph.getEdgeType(e) == EdgeType.UNDIRECTED) {
                    Shape vertexShape = rc.getVertexShapeTransformer().transform(graph.getEndpoints(e).getFirst());
                    xf = AffineTransform.getTranslateInstance(x1, y1);
                    vertexShape = xf.createTransformedShape(vertexShape);

                    arrowHit = rc.getMultiLayerTransformer().getTransformer(Layer.VIEW).transform(vertexShape).intersects(deviceRectangle);

                    if (arrowHit) {
                        AffineTransform at = edgeArrowRenderingSupport.getReverseArrowTransform(rc, edgeShape, vertexShape, !isLoop);
                        if (at == null)
                            return;
                        Shape arrow = rc.getEdgeArrowTransformer().transform(Context.<Graph<V, E>, E> getInstance(graph, e));
                        arrow = at.createTransformedShape(arrow);
                        g.setPaint(rc.getArrowFillPaintTransformer().transform(e));
                        g.fill(arrow);
                        g.setPaint(rc.getArrowDrawPaintTransformer().transform(e));
                        g.draw(arrow);
                    }
                }
                // restore paint and stroke
                if (newStroke != null)
                    g.setStroke(oldStroke);

            }

            // restore old paint
            g.setPaint(oldPaint);
        }
    }

    private void normalEdge(float x1, float y1, float x2, float y2, AffineTransform xform) {
        // this is a normal edge. Rotate it to the angle between
        // vertex endpoints, then scale it to the distance between
        // the vertices
        float dx = x2 - x1;
        float dy = y2 - y1;
        float thetaRadians = (float) Math.atan2(dy, dx);
        xform.rotate(thetaRadians);
        float dist = (float) Math.sqrt(dx * dx + dy * dy);
        xform.scale(dist, 1.0);
    }

    private Shape orthogonalEdge(RenderContext<V, E> rc, E e, Graph<V, E> graph, float x1, float y1, float x2, float y2) {
        Shape edgeShape;
        float dx = x2 - x1;
        float dy = y2 - y1;

        int index = 0;
        if (rc.getEdgeShapeTransformer() instanceof IndexedRendering) {
            EdgeIndexFunction<V, E> peif = ((IndexedRendering<V, E>) rc.getEdgeShapeTransformer()).getEdgeIndexFunction();
            index = peif.getIndex(graph, e);
            index *= 20;
        }
        GeneralPath gp = new GeneralPath();
        gp.moveTo(0, 0);// the xform will do the translation to x1,y1
        if (x1 > x2) {
            if (y1 > y2) {
                gp.lineTo(0, index);
                gp.lineTo(dx - index, index);
                gp.lineTo(dx - index, dy);
                gp.lineTo(dx, dy);
            } else {
                gp.lineTo(0, -index);
                gp.lineTo(dx - index, -index);
                gp.lineTo(dx - index, dy);
                gp.lineTo(dx, dy);
            }

        } else {
            if (y1 > y2) {
                gp.lineTo(0, index);
                gp.lineTo(dx + index, index);
                gp.lineTo(dx + index, dy);
                gp.lineTo(dx, dy);

            } else {
                gp.lineTo(0, -index);
                gp.lineTo(dx + index, -index);
                gp.lineTo(dx + index, dy);
                gp.lineTo(dx, dy);

            }

        }

        edgeShape = gp;
        return edgeShape;
    }

    private void loop(Shape s2, Shape edgeShape, AffineTransform xform) {
        // this is a self-loop. scale it is larger than the vertex
        // it decorates and translate it so that its nadir is
        // at the center of the vertex.
        Rectangle2D s2Bounds = s2.getBounds2D();
        xform.scale(s2Bounds.getWidth(), s2Bounds.getHeight());
        xform.translate(0, -edgeShape.getBounds2D().getWidth() / 2);
    }

}
