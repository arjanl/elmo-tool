package nl.aardbeitje.elm.gui.graphlayout;

import java.awt.BasicStroke;
import java.awt.Stroke;

import org.apache.commons.collections15.Transformer;

import nl.aardbeitje.elm.HasOptionality;

public class OptionalityStrokeTransformer<T extends HasOptionality> implements Transformer<T, Stroke> {

    @Override
    public Stroke transform(T o) {
        switch (o.getOptionality()) {
        case COMPULSORY:
            return new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 10.0f, new float[] { 1.0f }, 0.0f);
        case RUNTIME_OPTIONAL:
            return new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[] { 10.0f }, 0.0f);
        case DEPLOYMENT_OPTIONAL:
            return new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[] { 1.0f, 1.0f }, 0.0f);
        default:
            throw new IllegalArgumentException("Unknown optionality: " + o.getOptionality());
        }
    }

}
