package nl.aardbeitje.elm.gui.graphlayout;

import org.apache.commons.collections15.Transformer;

import nl.aardbeitje.elm.Service;

public class ServiceLabelTransformer implements Transformer<Service, String> {

    @Override
    public String transform(Service s) {
        return s.getName();
    }

}
