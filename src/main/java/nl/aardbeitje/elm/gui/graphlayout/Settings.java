package nl.aardbeitje.elm.gui.graphlayout;

public class Settings {
    
    public static final float VERTEX_WIDTH = 100;
    public static final float VERTEX_HEIGHT = 30;
    public static final float VERTEX_ARC = 5;

    public static final float MACHINE_MARGIN = 10;

}
