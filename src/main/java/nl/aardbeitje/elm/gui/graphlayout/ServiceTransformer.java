package nl.aardbeitje.elm.gui.graphlayout;

import static nl.aardbeitje.elm.gui.graphlayout.Settings.VERTEX_ARC;
import static nl.aardbeitje.elm.gui.graphlayout.Settings.VERTEX_HEIGHT;
import static nl.aardbeitje.elm.gui.graphlayout.Settings.VERTEX_WIDTH;

import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import org.apache.commons.collections15.Transformer;

import nl.aardbeitje.elm.Service;

public class ServiceTransformer implements Transformer<Service, Shape> {

    @Override
    public Shape transform(Service s) {
        return new RoundRectangle2D.Float(-VERTEX_WIDTH / 2, -VERTEX_HEIGHT / 2, VERTEX_WIDTH, VERTEX_HEIGHT, VERTEX_ARC, VERTEX_ARC);
    }
}