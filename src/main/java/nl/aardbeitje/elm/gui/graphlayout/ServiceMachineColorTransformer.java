package nl.aardbeitje.elm.gui.graphlayout;

import java.awt.Color;
import java.awt.Paint;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.visualization.picking.PickedState;
import javafx.beans.property.BooleanProperty;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.ServiceMachine;
import nl.aardbeitje.elm.analysis.DeploymentAnalysis;

public class ServiceMachineColorTransformer implements Transformer<ServiceMachine, Paint> {

    private final PickedState<ServiceMachine> pickedState;
    private final Deployment deployment;
    private final BooleanProperty sensitivitiesPerspective;

    public ServiceMachineColorTransformer(Deployment deployment, PickedState<ServiceMachine> pickedState, BooleanProperty sensitivitiesPerspective) {
        this.deployment = deployment;
        this.pickedState = pickedState;
        this.sensitivitiesPerspective = sensitivitiesPerspective;
    }

    public Paint transform(ServiceMachine sm) {
        if (sensitivitiesPerspective.get()) {
            return transformSensitivities(sm);
        } else {
            return transformImpact(sm);
        }
    }

    public Paint transformSensitivities(ServiceMachine sm) {
        boolean isInsensitive = true;
        boolean isVoluntarilySensitive = true;

        DeploymentAnalysis da = new DeploymentAnalysis(deployment);

        for (ServiceMachine pickedService : pickedState.getPicked()) {
            if (da.forciblySensitive(pickedService, sm)) {
                isInsensitive = false;
                isVoluntarilySensitive = false;
            }
            if (!da.insensitive(pickedService, sm)) {
                isInsensitive = false;
            }
        }

        if (pickedState.isPicked(sm)) {
            return Color.MAGENTA;
        } else {
            if (isInsensitive) {
                return Color.WHITE;
            } else if (isVoluntarilySensitive) {
                return Color.ORANGE;
            } else {
                return Color.RED;
            }
        }
    }

    public Paint transformImpact(ServiceMachine sm) {
        boolean isStressedBy = false;
        boolean isSlowedBy = false;

        DeploymentAnalysis da = new DeploymentAnalysis(deployment);

        for (ServiceMachine pickedServiceMachine : pickedState.getPicked()) {
            isStressedBy = isStressedBy || da.stress(sm).contains(pickedServiceMachine);
            isSlowedBy = isSlowedBy || da.responsiveness(sm).contains(pickedServiceMachine);
        }

        if (pickedState.isPicked(sm)) {
            return Color.MAGENTA;
        } else {
            if (isStressedBy) {
                return Color.RED;
            } else if (isSlowedBy) {
                return Color.ORANGE;
            } else {
                return Color.WHITE;
            }
        }
    }
}