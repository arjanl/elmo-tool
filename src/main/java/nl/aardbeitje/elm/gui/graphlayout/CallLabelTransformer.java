package nl.aardbeitje.elm.gui.graphlayout;

import org.apache.commons.collections15.Transformer;

import nl.aardbeitje.elm.Call;

public class CallLabelTransformer implements Transformer<Call, String> {
    @Override
    public String transform(Call c) {
        return c.getData();
    }

}
