package nl.aardbeitje.elm.gui.graphlayout;

import static nl.aardbeitje.elm.gui.graphlayout.Settings.MACHINE_MARGIN;
import static nl.aardbeitje.elm.gui.graphlayout.Settings.VERTEX_HEIGHT;
import static nl.aardbeitje.elm.gui.graphlayout.Settings.VERTEX_WIDTH;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.ServiceMachine;

public class MachineGroupPainter implements VisualizationViewer.Paintable {

    private VisualizationViewer<ServiceMachine, Connection> vv;
    private Deployment deployment;

    public MachineGroupPainter(VisualizationViewer<ServiceMachine, Connection> vv, Deployment deployment) {
        this.vv = vv;
        this.deployment = deployment;
    }

    @Override
    public void paint(Graphics gr) {
        Graphics2D g = (Graphics2D) gr;

        Layout<ServiceMachine, Connection> layout = vv.getGraphLayout();
        AffineTransform transform = vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT).getTransform();

        for (Machine m : deployment.getMachines()) {
            Rectangle2D boundingBox = computeBoundingBox(deployment.getServiceMachines(m), layout, transform);

            double d = 20;
            Shape rect = new RoundRectangle2D.Double(boundingBox.getMinX() - d, boundingBox.getMinY() - d, boundingBox.getWidth() + d + d, boundingBox.getHeight() + d + d, d, d);
            g.setColor(new Color(0xE6, 0xDE, 0xDC));
            g.fill(rect);
            g.setColor(Color.BLACK);
            g.draw(rect);

            double x = rect.getBounds2D().getX() + rect.getBounds2D().getWidth() - g.getFontMetrics().stringWidth(m.getName()) - 5;
            double y = rect.getBounds2D().getY() + rect.getBounds2D().getHeight() - g.getFontMetrics().getAscent() + 5;
            g.drawString(m.getName(), (float) x, (float) y);
        }
    }

    private static <V> Rectangle2D computeBoundingBox(Iterable<V> vertices, Layout<V, ?> layout, AffineTransform at) {
        double minX = Double.MAX_VALUE;
        double minY = Double.MAX_VALUE;
        double maxX = -Double.MAX_VALUE;
        double maxY = -Double.MAX_VALUE;
        for (V vertex : vertices) {
            Point2D location = layout.transform(vertex);
            at.transform(location, location);
            minX = Math.min(minX, location.getX() - VERTEX_WIDTH / 2 - MACHINE_MARGIN);
            minY = Math.min(minY, location.getY() - VERTEX_HEIGHT / 2 - MACHINE_MARGIN);
            maxX = Math.max(maxX, location.getX() + VERTEX_WIDTH / 2 + MACHINE_MARGIN);
            maxY = Math.max(maxY, location.getY() + VERTEX_HEIGHT / 2 + MACHINE_MARGIN);
        }
        return new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
    }

    @Override
    public boolean useTransform() {
        return true;
    }

}
