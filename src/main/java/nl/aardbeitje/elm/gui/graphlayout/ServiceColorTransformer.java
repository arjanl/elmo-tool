package nl.aardbeitje.elm.gui.graphlayout;

import java.awt.Color;
import java.awt.Paint;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.visualization.picking.PickedState;
import javafx.beans.property.BooleanProperty;
import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.analysis.ArchitectureAnalysis;

public class ServiceColorTransformer implements Transformer<Service, Paint> {

    private final PickedState<Service> pickedState;
    private final Architecture architecture;
    private final BooleanProperty sensitivitiesPerspective;

    public ServiceColorTransformer(Architecture architecture, PickedState<Service> pickedState, BooleanProperty sensitivitiesPerspective) {
        this.architecture = architecture;
        this.pickedState = pickedState;
        this.sensitivitiesPerspective = sensitivitiesPerspective;
    }

    public Paint transform(Service s) {
        if (sensitivitiesPerspective.get()) {
            return transformSensitivities(s);
        } else {
            return transformImpact(s);
        }
    }

    private Paint transformSensitivities(Service s) {
        boolean isInsensitive = true;
        boolean isVoluntarilySensitive = true;

        ArchitectureAnalysis a = new ArchitectureAnalysis(architecture);

        for (Service pickedService : pickedState.getPicked()) {
            if (a.forciblySensitive(pickedService, s)) {
                isInsensitive = false;
                isVoluntarilySensitive = false;
            }
            if (!a.insensitive(pickedService, s)) {
                isInsensitive = false;
            }
        }

        if (pickedState.isPicked(s)) {
            return Color.MAGENTA;
        } else {
            if (isInsensitive) {
                return Color.WHITE;
            } else if (isVoluntarilySensitive) {
                return Color.ORANGE;
            } else {
                return Color.RED;
            }
        }
    }

    private Paint transformImpact(Service s) {
        boolean isStressedBy = false;
        boolean isSlowedBy = false;

        ArchitectureAnalysis a = new ArchitectureAnalysis(architecture);

        for (Service pickedService : pickedState.getPicked()) {
            isStressedBy = isStressedBy || a.stress(s).contains(pickedService);
            isSlowedBy = isSlowedBy || a.responsiveness(s).contains(pickedService);
        }

        if (pickedState.isPicked(s)) {
            return Color.MAGENTA;
        } else {
            if (isStressedBy) {
                return Color.RED;
            } else if (isSlowedBy) {
                return Color.ORANGE;
            } else {
                return Color.WHITE;
            }
        }
    }
}