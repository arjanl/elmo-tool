package nl.aardbeitje.elm.gui.graphlayout;

import org.apache.commons.collections15.Transformer;

import nl.aardbeitje.elm.ServiceMachine;

public class ServiceMachineLabelTransformer implements Transformer<ServiceMachine, String> {

    @Override
    public String transform(ServiceMachine sm) {
        return sm.getName();
    }

}
