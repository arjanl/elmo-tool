package nl.aardbeitje.elm.gui.graphlayout;
import static nl.aardbeitje.elm.gui.graphlayout.Settings.*;

import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import org.apache.commons.collections15.Transformer;

import nl.aardbeitje.elm.ServiceMachine;

public class ServiceMachineTransformer implements Transformer<ServiceMachine, Shape> {
     
    @Override
    public Shape transform(ServiceMachine sm) {
        return new RoundRectangle2D.Float(-VERTEX_WIDTH/2, -VERTEX_HEIGHT/2, VERTEX_WIDTH, VERTEX_HEIGHT, VERTEX_ARC, VERTEX_ARC);
    }
}