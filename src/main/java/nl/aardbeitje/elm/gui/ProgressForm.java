package nl.aardbeitje.elm.gui;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import nl.aardbeitje.elm.refactorings.RefactorWizard;

public class ProgressForm {
    private final Stage dialogStage;
    private final ProgressBar progressBar = new ProgressBar();
    private final Button cancelButton;
    private final Label label = new Label();

    public ProgressForm() {
        dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.setResizable(false);
        dialogStage.setWidth(600);
        dialogStage.setHeight(200);
        dialogStage.initModality(Modality.APPLICATION_MODAL);

        cancelButton = new Button();
        cancelButton.setText("stop");
        label.setText("Calculating...");

        progressBar.setProgress(-1F);

        final HBox hb = new HBox();
        hb.setSpacing(5);
        hb.setAlignment(Pos.CENTER);
        hb.getChildren().addAll(label, progressBar, cancelButton);

        Scene scene = new Scene(hb);
        dialogStage.setScene(scene);
    }

    public void activateProgressBar(final RefactorWizard task) {
        cancelButton.setOnAction(e -> {
            task.cancel();
        });
        progressBar.progressProperty().bind(task.progressProperty());
        label.textProperty().bind(task.messageProperty());
        dialogStage.show();
    }

    public Stage getDialogStage() {
        return dialogStage;
    }
}