package nl.aardbeitje.elm.gui;

import java.util.Map;

import nl.aardbeitje.elm.Deployment;

public interface DeploymentsView {
    Map<String, Deployment> getDeployments();
}
