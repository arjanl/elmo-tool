package nl.aardbeitje.elm;

public class MachineConfig {
    
    private final String serviceName;
    private final String machineName;
    private final String serviceMachineName;

    public MachineConfig(String serviceName, String machineName) {
        this.serviceName = serviceName;
        this.machineName = machineName;
        this.serviceMachineName = null;
    }
    public MachineConfig(Service service, String machineName) {
        this.serviceName = service.getName();
        this.machineName = machineName;
        this.serviceMachineName = null;
    }

    public MachineConfig(String serviceName, String machineName, String serviceMachineName) {
        this.serviceName = serviceName;
        this.machineName = machineName;
        this.serviceMachineName = serviceMachineName;
    }
    public String getServiceName() {
        return serviceName;
    }

    public String getMachineName() {
        return machineName;
    }
    public String getServiceMachineName() {
        return serviceMachineName;
    }
    
    
}
