package nl.aardbeitje.elm;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a service.
 * Should be references by name; name should be unique. Equals is thus defined on name.
 * 
 *  Should not reference an Architecture since that may leak memory. A Service instead provides a recreate() function that finds an equivalent instance in a target Architecture.
 */
public class Service implements Vertex, HasStatefulness {
    private final String name;
    private final String originalName; 
    private final String description;
    
    @Deprecated
    private Statefulness statefulness;

    private String partitioning;
    private final Set<String> produces = new HashSet<>(); 
    private final Set<String> consumes = new HashSet<>();

	public Service(String name, String description, Statefulness statefulness) {
        if (statefulness==Statefulness.PARTITIONED) {
            throw new IllegalArgumentException("Partitioning requires a partitioning scheme");
        }
        this.description = description;
        this.name = name;
        this.originalName = name;
        this.statefulness = statefulness;
        this.partitioning = null;
    }

    public Service(String name, String description, Statefulness statefulness, String partitioning) {
        this.description = description;
        this.name = name;
        this.originalName = name;
        this.statefulness = statefulness;
        this.partitioning = partitioning;
    }

    public Service(String name, String description, Statefulness statefulness, String partitioning, Set<String> produces, Set<String> consumes) {
        this.description = description;
        this.name = name;
        this.originalName = name;
        this.statefulness = statefulness;
        this.partitioning = partitioning;
        this.produces.addAll(produces);
        this.consumes.addAll(consumes);
    }

    public Service(String name, String originalName, String description, Statefulness statefulness, String partitioning) {
        this.description = description;
        this.name = name;
        if (originalName==null || originalName.isEmpty()) {
        	this.originalName = name;
        } else {
        	this.originalName = originalName;
        }
        this.statefulness = statefulness;
        this.partitioning = partitioning;
    }

    public Service(String name, String originalName, String description, Statefulness statefulness, String partitioning, Set<String> produces, Set<String> consumes) {
        this.description = description;
        this.name = name;
        if (originalName==null || originalName.isEmpty()) {
        	this.originalName = name;
        } else {
        	this.originalName = originalName;
        }
        this.statefulness = statefulness;
        this.partitioning = partitioning;
        this.produces.addAll(produces);
        this.consumes.addAll(consumes);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Service other = (Service) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public String getDescription() {
        return description;
    }

    @Deprecated
    public Statefulness getStatefulness() {
        return statefulness;
    }
    
    @Deprecated
    public void setStatefulness(Statefulness statefulness) {
        this.statefulness = statefulness;
    }

    public String getPartitioning() {
        return partitioning;
    }

    public void setPartitioning(String partitioning) {
        this.partitioning = partitioning;
    }
    
    public Service recreate(Deployment newDeployment) {
        return newDeployment.getArchitecture().getService(getName());
    }

	public Set<String> getProduces() {
		return produces;
	}

	public Set<String> getConsumes() {
		return consumes;
	}

	/**
	 * When refactoring, multiple services may be synthesized. To compare them, we group them under their originating name.
	 * @return the name of the original service this service is derived from
	 */
	public String getOriginalName() {
		return originalName;
	}

}
