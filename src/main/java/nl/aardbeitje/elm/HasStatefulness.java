package nl.aardbeitje.elm;

public interface HasStatefulness {
    Statefulness getStatefulness();
}
