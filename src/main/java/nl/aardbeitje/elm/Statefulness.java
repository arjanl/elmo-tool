package nl.aardbeitje.elm;

public enum Statefulness {
    STATELESS, PARTITIONED, STATEFUL
}
