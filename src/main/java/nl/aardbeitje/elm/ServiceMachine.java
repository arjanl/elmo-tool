package nl.aardbeitje.elm;

import com.fasterxml.jackson.annotation.JsonIgnore;

// TODO add unique identifier to services or service machines so we can deploy more than one instance of same service on machine
public class ServiceMachine implements Vertex, HasStatefulness {
    private final String name;
    private final Service service;
    private final Machine machine;
    private final int index;

    public ServiceMachine(Service s, Deployment d) {
        this.service = s;
        this.machine = d.newMachine();
        this.name = d.generateServiceMachineName(s);
        this.index=findIndex(name);
    }

    public ServiceMachine(Service s, Machine m, Deployment d) {
        this.service = s;
        this.machine = m;
        this.name = d.generateServiceMachineName(s);
        this.index=findIndex(name);
    }

    public ServiceMachine(Service s, Machine m, String serviceMachineName) {
        this.service = s;
        this.machine = m;
        // check if the name is properly formatted
        this.index=findIndex(serviceMachineName);
        this.name = serviceMachineName;
    }
    
    private int findIndex(String name) {
        String[] split = name.split("_");
        return Integer.valueOf(split[split.length-1]);
    }

    public String getName() {
        return name;
    }

    public String getServiceName() {
        return service.getName();
    }

    @JsonIgnore
    public Service getService() {
        return service;
    }

    public String getMachineName() {
        return machine.getName();
    }

    @JsonIgnore
    public Machine getMachine() {
        return machine;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ServiceMachine other = (ServiceMachine) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "[" + getName() + ":"+getMachine()+"]";
    }

    @Override
    public Statefulness getStatefulness() {
        return service.getStatefulness();
    }

    public int getIndex() {
        return index;
    }


}
