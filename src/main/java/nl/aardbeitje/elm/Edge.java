package nl.aardbeitje.elm;

public interface Edge {
    boolean isPushedTo();
    boolean isPulledFrom();
}
