package nl.aardbeitje.elm.analysis;

import java.util.List;

import nl.aardbeitje.elm.Connection;

public class LoopException extends Exception {
    private static final long serialVersionUID = 1L;
    private final transient List<Connection> path;

    LoopException(List<Connection> path) {
        this.path = path;
    }

    public List<Connection> getPath() {
        return path;
    }
}
