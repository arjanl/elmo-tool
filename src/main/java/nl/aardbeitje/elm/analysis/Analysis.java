package nl.aardbeitje.elm.analysis;

import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.Graph;
import nl.aardbeitje.elm.Edge;
import nl.aardbeitje.elm.Vertex;

public class Analysis<V extends Vertex, E extends Edge> {

    private DirectedGraph<V, E> graph;

    private final Map<V, Set<V>> directStressCache = new LinkedHashMap<>();
    private Map<V, List<V>> stronglyConnectedComponents = null;

    public Analysis(DirectedGraph<V, E> graph) {
        this.graph = graph;
    }

    protected Graph<V, E> getGraph() {
        return graph;
    }

    public boolean forciblySensitive(V a, V b) {
        // responsiveness check is redundant but here for symmetry reasons
        return stress(a).contains(b); 
    }

    public boolean voluntarilySensitive(V a, V b) {
        return !stress(a).contains(b) && responsiveness(a).contains(b);
    }

    public boolean sensitive(V a, V b) {
        // responsiveness check is redundant but here for symmetry reasons
        return responsiveness(a).contains(b);
    }

    public boolean insensitive(V a, V b) {
        return !responsiveness(a).contains(b);
    }

    public Set<V> stress(V v) {
        return stress(new HashSet<>(), v);
    }

    public Set<V> consistency(V v) {
        return consistency(new HashSet<>(), v);
    }

    public Set<V> responsiveness(V v) {
        return responsiveness(new HashSet<>(), v);
    }

    public Set<V> directStress(V v) {
        return directStressCache.computeIfAbsent(v, vv -> {
            Set<V> result = directPushFrom(v);
            result.addAll(directPullFrom(v));
            return result;
        });
    }

    private Set<V> directConsistency(V v) {
        Set<V> result = directPushFrom(v);
        result.addAll(directPullTo(v));
        return result;
    }

    // m -push-> ?
    protected Set<V> directPushTo(V v) {
        return directPushToEdges(v).stream().map(e -> graph.getDest(e)).collect(Collectors.toSet());
    }

    protected Set<E> directPushToEdges(V v) {
        return graph.getOutEdges(v).stream().filter(E::isPushedTo).collect(Collectors.toSet());
    }

    // ? -push-> m
    protected Set<V> directPushFrom(V v) {
        return directPushFromEdges(v).stream().map(e -> graph.getSource(e)).collect(Collectors.toSet());
    }

    protected Set<E> directPushFromEdges(V v) {
        return graph.getInEdges(v).stream().filter(E::isPushedTo).collect(Collectors.toSet());
    }

    // v -< ???
    protected Set<V> directPullTo(V v) {
        return directPullToEdges(v).stream().map(e -> graph.getSource(e)).collect(Collectors.toSet());
    }

    // all edges that are incoming to v (thus provide v with information) and
    // are pull edges
    protected Set<E> directPullToEdges(V v) {
        return graph.getInEdges(v).stream().filter(E::isPulledFrom).collect(Collectors.toSet());
    }

    // m -pull-{ ?
    protected Set<V> directPullFrom(V v) {
        return directPullFromEdges(v).stream().map(e -> graph.getDest(e)).collect(Collectors.toSet());
    }

    protected Set<E> directPullFromEdges(V v) {
        return graph.getOutEdges(v).stream().filter(E::isPulledFrom).collect(Collectors.toSet());
    }

    Set<V> stress(Set<V> path, V v) {
        if (path.contains(v)) {
            return path;
        } else {
            path.add(v);
            Set<V> stressSources = directStress(v);
            for (V w : stressSources) {
                path.addAll(stress(path, w));
            }
            return path;
        }
    }

    Set<V> consistency(Set<V> path, V v) {
        if (path.contains(v)) {
            return path;
        } else {
            path.add(v);
            Set<V> consistencySources = directConsistency(v);
            for (V w : consistencySources) {
                path.addAll(consistency(path, w));
            }
            return path;
        }
    }

    Set<V> responsiveness(Set<V> path, V v) {
        if (path.contains(v)) {
            return path;
        } else {
            path.addAll(stress(v));
            Set<V> responsivenessSources = directPullTo(v);
            for (V w : responsivenessSources) {
                path.addAll(responsiveness(path, w));
            }
            return path;
        }
    }

    public Map<V, List<V>> getStronglyConnectedComponents() {
        if (stronglyConnectedComponents==null) {
                stronglyConnectedComponents = new TarjanStronglyConnectedDetector<V,E>(graph).getResult();
        }
        return stronglyConnectedComponents;
    }
}
