package nl.aardbeitje.elm.analysis;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;

public class Comparison {

    private final Collection<Deployment> deployments;
    private final Map<String, DeploymentAnalysis> deploymentAnalyses = new LinkedHashMap<>();
    
    public Comparison(Collection<Deployment> deployments) {
        this.deployments = deployments;
    }

    private Set<Service> minimal(Service s, BiFunction<Deployment, Machine, Set<Machine>> f) {
        Set<Service> minimalSet = null;
        for (Deployment d : deployments) {
            for (Machine m : d.getMachines(s)) {
                Set<Service> services = d.asServices(f.apply(d, m));
                if (minimalSet==null) {
                    minimalSet = services;
                } else {
                    minimalSet.retainAll(services);
                }
            }
        }
        return minimalSet;
    }

    public DeploymentAnalysis analyse(Deployment d) {
        return deploymentAnalyses.computeIfAbsent(d.getName(), dd-> new DeploymentAnalysis(d));
    }
    
    public long minimalNetworkDepth(Service s) throws LoopException {
        long minimal = Integer.MAX_VALUE;
        boolean hasValue = false;
        for (Deployment d : deployments) {
            for (Machine m : d.getMachines(s)) {
                try {
                    long depth = analyse(d).networkDepth(s, m);
                    hasValue = true;
                    if (depth<minimal) {
                        minimal = depth;
                    }
                } catch(LoopException e) {
                    // ignore this one
                }
            }
        }
        if (hasValue) {
            return minimal;
        } else {
            throw new LoopException(null);
        }
    }

    /**
     * calculate the minimal network depth for a set of related services. E.g. when splitting services, the worst case should still be considered for comparison. 
     * @param originalName the originalName for the services to consider
     * @return
     * @throws LoopException
     */
    public long minimalNetworkDepth(String originalName) throws LoopException {
    	long minimal = Integer.MAX_VALUE;
        boolean hasValue = false;
        for (Deployment d : deployments) {
            try {
	        	long depth = maximalNetworkDepth(d, originalName);
	            hasValue = true;
	            if (depth<minimal) {
	                minimal = depth;
	            }
            } catch( LoopException e) {
            	// ignore this one
            }
        }
        if (hasValue) {
            return minimal;
        } else {
            throw new LoopException(null);
        }
    }
    
    private long maximalNetworkDepth(Deployment d, String originalName) throws LoopException {
    	long max = Integer.MIN_VALUE;
        boolean hasLoop = false;
        
        for (Service s : d.getArchitecture().getServiceByOriginalName(originalName)) {
        	for (Machine m : d.getMachines(s)) {
                try {
                    long depth = analyse(d).networkDepth(s, m);
                    if (depth>max) {
                    	max = depth;
                    }
                } catch(LoopException e) {
                    hasLoop = true;
                }
            }
        }
        
        if (hasLoop) {
        	throw new LoopException(null);
        } else {
            return max;
        }
	}

	/**
     * Returns the set of services that are part of the stress in all of the deployments for the given service.
     * @param s the service to give the minimal stress for
     * @return the minimal stress
     */
    public Set<Service> minimalStress(Service s) {
        return minimal(s, (d,m) -> analyse(d).stress(s,m).stream().map(ServiceMachine::getMachine).collect(Collectors.toSet()) );
    }

    /**
     * Returns the set of services that the given service can be forcibly sensitive to in all of the deployments for the given service.
     * @param s the service to give the minimal service set for
     * @return the minimal service set
     */
    public Set<Service> minimalForciblySensitiveServices(Service s) {
        Set<Service> minimalSet = null;
        for (Deployment d : deployments) {
            Set<Service> all = analyse(d).allForciblySensitivities(s);
            if (minimalSet==null) {
                minimalSet = all;
            } else {
                minimalSet.retainAll(all);
            }
        }
        return minimalSet;
    }

    /**
     * Returns the set of services that the given service can be forcibly sensitive to in all of the deployments for the given service.
     * @param s the original service name group to give the minimal service set for
     * @return the minimal service set
     */
    public Set<Service> minimalForciblySensitiveServices(String originalServiceName) {
        Set<Service> minimalSet = null;
        for (Deployment d : deployments) {
        	for (Service s : d.getArchitecture().getServiceByOriginalName(originalServiceName)) {
	            Set<Service> all = analyse(d).allForciblySensitivities(s);
	            if (minimalSet==null) {
	                minimalSet = all;
	            } else {
	                minimalSet.retainAll(all);
	            }
        	}
        }
        return minimalSet;
    }

    /**
     * Returns the set of services that the given service can be voluntarily sensitive to in all of the deployments for the given service.
     * @param s the service to give the minimal service set for
     * @return the minimal service set
     */
    public Set<Service> minimalVoluntarilySensitiveServices(Service s) {
        Set<Service> minimalSet = null;
        for (Deployment d : deployments) {
            Set<Service> all = analyse(d).allVoluntarilySensitivities(s);
            if (minimalSet==null) {
                minimalSet = all;
            } else {
                minimalSet.retainAll(all);
            }
        }
        return minimalSet;
    }

    /**
     * Returns the set of services that the given service can be voluntarily sensitive to in all of the deployments for the given service.
     * @param s the service to give the minimal service set for
     * @param s the original service name group to give the minimal service set for
     * @return the minimal service set
     */
    public Set<Service> minimalVoluntarilySensitiveServices(String originalServiceName) {
        Set<Service> minimalSet = null;
        for (Deployment d : deployments) {
        	for (Service s : d.getArchitecture().getServiceByOriginalName(originalServiceName)) {
	            Set<Service> all = analyse(d).allVoluntarilySensitivities(s);
	            if (minimalSet==null) {
	                minimalSet = all;
	            } else {
	                minimalSet.retainAll(all);
	            }
        	}
        }
        return minimalSet;
    }

}
