package nl.aardbeitje.elm.analysis;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;

public class DeploymentAnalysis {

	private final Analysis<ServiceMachine, Connection> analysis;
	private final ArchitectureAnalysis architectureAnalysis;
	private final Deployment deployment;

	private Map<Machine, Set<ServiceMachine>> cachedStress = new LinkedHashMap<>();
	private Map<ServiceMachine, Set<ServiceMachine>> cachedResponsiveness = new LinkedHashMap<>();

	private Map<Service, Set<Service>> allForciblySensitivitiesCache = new LinkedHashMap<>();
	private Map<Service, Set<Service>> allVoluntarilySensitivitiesCache = new LinkedHashMap<>();
	private Map<String, Set<Service>> allForciblySensitivitiesOriginalServiceNameCache = new LinkedHashMap<>();
	private Map<String, Set<Service>> allVoluntarilySensitivitiesOriginalServiceNameCache = new LinkedHashMap<>();

	public DeploymentAnalysis(Deployment deployment) {
		this.deployment = deployment;
		this.analysis = new Analysis<>(deployment.getGraph());
		this.architectureAnalysis = new ArchitectureAnalysis(deployment.getArchitecture());
	}
	
	
	public boolean isWellformed() {
		return architectureAnalysis.isWellformed();
	}


	public long networkDepth(Service s, Machine m) throws LoopException {
		return networkPaths(s, m).stream().mapToLong(p -> p.stream().filter(c -> !c.isLocal()).count()).max().orElse(0);
	}

	public Set<List<Connection>> networkPaths(Service s, Machine m) throws LoopException {
		return networkPaths(new ArrayList<>(), s, m);
	}

	public boolean forciblySensitive(ServiceMachine a, ServiceMachine b) {
		return stress(a).contains(b) && responsiveness(a).contains(b);
	}

	public boolean voluntarilySensitive(ServiceMachine a, ServiceMachine b) {
		return !stress(a).contains(b) && responsiveness(a).contains(b);
	}

	public boolean sensitive(ServiceMachine a, ServiceMachine b) {
		return responsiveness(a).contains(b);
	}

	public boolean insensitive(ServiceMachine a, ServiceMachine b) {
		return !responsiveness(a).contains(b);
	}

	public boolean forciblySensitive(Service a, Service b) {
		for (Machine m : getDeployment().getMachines(a)) {
			if (stress(a, m).stream().map(ServiceMachine::getService).anyMatch(s -> s == b)) {
				return true;
			}
		}
		return false;
	}

	public boolean voluntarilySensitive(Service a, Service b) {

		for (Machine m : getDeployment().getMachines(a)) {
			if (stress(a, m).stream().map(ServiceMachine::getService).anyMatch(s -> s.equals(b))) {
				return false;
			}
		}

		for (Machine m : getDeployment().getMachines(a)) {
			if (responsiveness(a, m).stream().map(ServiceMachine::getService).anyMatch(s -> s.equals(b))) {
				return true;
			}
		}

		return true;
	}

	public boolean insensitive(Service a, Service b) {
		Set<ServiceMachine> bs = getDeployment().getServiceMachines(b);
		return getDeployment().getServiceMachines(a).stream()
				.noneMatch(am -> bs.stream().anyMatch(bm -> !insensitive(am, bm)));
	}

	public Set<Service> forciblySensitiveServices(Service s) {
		return getDeployment().getServiceMachines(s).stream().flatMap(sm -> forciblySensitiveServices(sm).stream())
				.map(ServiceMachine::getService).collect(Collectors.toSet());
	}

	public Set<ServiceMachine> forciblySensitiveServices(ServiceMachine s) {
		return getDeployment().getServiceMachines().stream().filter(t -> forciblySensitive(s, t))
				.collect(Collectors.toSet());
	}

	public Set<Service> voluntarilySensitiveServices(Service s) {
		return getDeployment().getServiceMachines(s).stream().flatMap(sm -> voluntarilySensitiveServices(sm).stream())
				.map(ServiceMachine::getService).collect(Collectors.toSet());
	}

	public Set<ServiceMachine> voluntarilySensitiveServices(ServiceMachine s) {
		return getDeployment().getServiceMachines().stream().filter(t -> voluntarilySensitive(s, t))
				.collect(Collectors.toSet());
	}

	public Set<Service> insensitiveServices(Service s) {
		return getDeployment().getServiceMachines(s).stream().flatMap(sm -> insensitiveServices(sm).stream())
				.map(ServiceMachine::getService).collect(Collectors.toSet());
	}

	public Set<ServiceMachine> insensitiveServices(ServiceMachine s) {
		return getDeployment().getServiceMachines().stream().filter(t -> insensitive(s, t)).collect(Collectors.toSet());
	}

	public Set<Service> sensitiveServices(Service s) {
		return getDeployment().getServiceMachines(s).stream().flatMap(sm -> sensitiveServices(sm).stream())
				.map(ServiceMachine::getService).collect(Collectors.toSet());
	}

	public Set<ServiceMachine> sensitiveServices(ServiceMachine s) {
		return getDeployment().getServiceMachines().stream().filter(t -> insensitive(s, t)).collect(Collectors.toSet());
	}

	Set<List<Connection>> networkPaths(List<Connection> currentPath, Service service, Machine machine)
			throws LoopException {

		Set<List<Connection>> pathsFromHere = new HashSet<>();
		Set<Service> services = getArchitectureAnalysis().directPullTo(service);

		if (services.isEmpty()) {
			pathsFromHere.add(currentPath);
			return pathsFromHere;
		}

		for (Service s : services) {
			// add all machines where pull source is located
			for (Machine m : getDeployment().getMachines(s)) {
				List<Connection> path = new ArrayList<>(currentPath);
				Connection connection = getDeployment().getConnection(m, machine, CallType.PULLED_FROM);
				if (connection != null) {
					if (currentPath.contains(connection)) {
						throw new LoopException(currentPath);
					}
					path.add(connection);
					pathsFromHere.addAll(networkPaths(path, s, m));
				}
			}
		}
		return pathsFromHere;
	}

	public Set<ServiceMachine> stress(Service s, Machine m) {
		return stress(deployment.getServiceMachine(s, m));
	}

	public Set<ServiceMachine> responsiveness(Service s, Machine m) {
		return responsiveness(deployment.getServiceMachine(s, m));
	}

	public Set<ServiceMachine> responsiveness(ServiceMachine sm) {
		return cachedResponsiveness.computeIfAbsent(sm,
				serviceMachine -> responsiveness(new HashSet<ServiceMachine>(), sm));
	}

	private Set<ServiceMachine> responsiveness(Set<ServiceMachine> path, ServiceMachine sm) {
		path.add(sm);

		Set<ServiceMachine> resp = new HashSet<>();
		resp.addAll(stress(sm.getMachine()));

		deployment.getConnections().stream()
				.filter(c -> c.getCallType() == CallType.PULLED_FROM && c.getTarget().equals(sm)).forEach(c -> {
					if (!path.contains(c.getSource())) {
						resp.addAll(stress(c.getSource()));
						resp.addAll(responsiveness(path, c.getSource()));
					}

				});

		return resp;
	}

	public Set<ServiceMachine> stress(Machine m) {
		return cachedStress.computeIfAbsent(m, machine -> {
			Set<ServiceMachine> all = new HashSet<>();

			// add up stress for all service on this machine
			deployment.getServiceMachines(m).stream().forEach(sm2 -> all.addAll(stress(all, sm2)));

			return all;
		});
	}

	public Set<ServiceMachine> stress(ServiceMachine sm) {
		return stress(sm.getMachine());
	}

	private Set<ServiceMachine> stress(Set<ServiceMachine> all, ServiceMachine sm) {
		if (all.contains(sm)) {
			return all;
		}

		all.add(sm);
		Set<ServiceMachine> stressSources = analysis.directStress(sm);
		for (ServiceMachine source : stressSources) {
			all.addAll(stress(all, source));
		}

		return all;
	}

	/**
	 * @deprecated picks the first service machine on a service, rather than an
	 *             explicit one. For if there are multiple instance of the same
	 *             service deployed on the same machine.
	 * @param s the service to pick
	 * @param m the machine to pick it on
	 * @return the responsiveness
	 */
	@Deprecated
	public Set<ServiceMachine> delay(Service s, Machine m) {
		return responsiveness(deployment.getServiceMachine(s, m));
	}

	public long maximumNetworkDepth(String originalServiceName) {
		return deployment.getArchitecture().getServiceByOriginalName(originalServiceName).stream().mapToLong(s -> maximumNetworkDepth(s)).max().getAsLong();
	}

	public long maximumNetworkDepth(Service service) {
		return getDeployment().getMachines(service).stream().mapToLong(m -> {
			try {
				return networkDepth(service, m);
			} catch (LoopException e) {
				return Long.MAX_VALUE;
			}
		}).max().getAsLong();
	}

	public Set<Service> allForciblySensitivities(Service s) {
		return allForciblySensitivitiesCache.computeIfAbsent(s, ss -> {
			Set<Service> all = new HashSet<>();
			for (ServiceMachine sm : getDeployment().getServiceMachines(s)) {
				Set<Service> services = forciblySensitiveServices(sm).stream().map(ServiceMachine::getService)
						.collect(Collectors.toSet());
				all.addAll(services);
			}
			return all;
		});
	}

	public Set<Service> allForciblySensitivities(String originalServiceName) {
		return allForciblySensitivitiesOriginalServiceNameCache.computeIfAbsent(originalServiceName, ss -> {
			Set<Service> all = new HashSet<>();
			for (Service s: getDeployment().getArchitecture().getServiceByOriginalName(originalServiceName)) {
				for (ServiceMachine sm : getDeployment().getServiceMachines(s)) {
					Set<Service> services = forciblySensitiveServices(sm).stream().map(ServiceMachine::getService)
							.collect(Collectors.toSet());
					all.addAll(services);
				}
			}
			return all;
		});
	}
	
	public Set<Service> allVoluntarilySensitivities(Service s) {
		return allVoluntarilySensitivitiesCache.computeIfAbsent(s, ss -> {
			Set<Service> all = new HashSet<>();
			for (ServiceMachine sm : getDeployment().getServiceMachines(s)) {
				Set<Service> services = voluntarilySensitiveServices(sm).stream().map(ServiceMachine::getService)
						.collect(Collectors.toSet());
				all.addAll(services);
			}
			return all;
		});
	}

	public Set<Service> allVoluntarilySensitivities(String originalServiceName) {
		return allVoluntarilySensitivitiesOriginalServiceNameCache.computeIfAbsent(originalServiceName, ss -> {
			Set<Service> all = new HashSet<>();
			for (Service s: getDeployment().getArchitecture().getServiceByOriginalName(originalServiceName)) {
				for (ServiceMachine sm : getDeployment().getServiceMachines(s)) {
					Set<Service> services = voluntarilySensitiveServices(sm).stream().map(ServiceMachine::getService)
							.collect(Collectors.toSet());
					all.addAll(services);
				}
			}
			return all;
		});
	}

	public Deployment getDeployment() {
		return deployment;
	}

	public ArchitectureAnalysis getArchitectureAnalysis() {
		return architectureAnalysis;
	}

}
