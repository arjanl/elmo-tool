package nl.aardbeitje.elm.analysis;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.CallType;
import nl.aardbeitje.elm.Configuration;
import nl.aardbeitje.elm.Connection;
import nl.aardbeitje.elm.Deployment;
import nl.aardbeitje.elm.Machine;
import nl.aardbeitje.elm.Service;
import nl.aardbeitje.elm.ServiceMachine;

/**
 * Prints an analysis for the given component.
 */
public class PrintAnalysis {
    private Deployment deployment;
    private DeploymentAnalysis deploymentAnalysis;
    private ArchitectureAnalysis architectureAnalysis;

    public PrintAnalysis(DeploymentAnalysis deploymentAnalysis) {
        this.deploymentAnalysis = deploymentAnalysis;
        this.architectureAnalysis = deploymentAnalysis.getArchitectureAnalysis();
        this.deployment = deploymentAnalysis.getDeployment();
    }

    public String analyse(ServiceMachine sm) {
        StringBuilder sb = new StringBuilder();
        sb.append("Analysis of service instance " + sm.getName() +"\n");
        sb.append("=====================\n");

        Machine m = sm.getMachine();
        Service s = sm.getService();
        sb.append("Machine: " + m.getName() + "\n");
        sb.append("Service: " + s.getName() + "\n");
        sb.append("\n");
        sb.append(analyseLocalServiceMachine(sm));
        sb.append("\n");
        sb.append(analyse(sm.getService()));
        return sb.toString();
    }

    public String analyse(Machine m) {
        StringBuilder sb = new StringBuilder();
        sb.append("Machine: " + m.getName() + "\n");
        return sb.toString();
    }

    public String analyse(Service s) {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Analysis for service " + s.getName()+ "\n");
        sb.append("===========================\n");

        sb.append(analyseService(s));
        sb.append("\n");
        sb.append(analyseDeployedService(s));

        return sb.toString();
    }
    
    private String analyseService(Service s) {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Service " + s.getName() + "(" + s.getDescription() + ") is " + s.getStatefulness() + "\n");

        if (!architectureAnalysis.getStronglyConnectedComponents().isEmpty()) {
            sb.append("(!) Architecture is not Pareto Efficient (PE_f)");
            if (architectureAnalysis.getStronglyConnectedComponents().containsKey(s)) {
                List<Service> scc = architectureAnalysis.getStronglyConnectedComponents().get(s);
                sb.append("\nService is part of a loop: ");
                for (Service ss : scc ) {
                    sb.append( ss.getName() + ",");
                }
                sb.append("\n ");
            } else {
                sb.append(" but not due to this service.\n ");
            }
        }
        
        sb.append("* Forcibly sensitive to " + deploymentAnalysis.forciblySensitiveServices(s) + "\n");
        sb.append("* Voluntarily sensitive to " + architectureAnalysis.voluntarilySensitiveServices(s) + "\n");
        sb.append("* Insensitive to " + architectureAnalysis.insensitiveServices(s) + "\n");
        sb.append("* Stress(" + s.getName() + ") = " + architectureAnalysis.stress(s) + "\n");
        if (Configuration.SHOW_EXPERIMENTAL_FEATURES) {
            sb.append("* Consistency(" + s.getName() + ") = " + architectureAnalysis.consistency(s) + "\n");
        }
        sb.append("* Responsiveness(" + s.getName() + ") = " + architectureAnalysis.responsiveness(s) + "\n");
        sb.append("* Latency(" + s.getName() + ") = " + asCalls(architectureAnalysis.latency(s)) + "\n");
        sb.append("\n");
        if (!s.getProduces().isEmpty()) {
            sb.append("Produces:\n");
            for (String type : s.getProduces()) {
                sb.append("* " + type + "\n");
            }
        }
        if (!s.getConsumes().isEmpty()) {
            sb.append("Consumes:\n");
            for (String data : s.getConsumes()) {
                sb.append("* Delay("+s.getName() +"," + data + ") = "+architectureAnalysis.delay(s,data)+"\n");
            }
        }
        sb.append("Running on: \n");
        for (Machine m : deployment.getMachines(s)) {
            sb.append("* " + m.getName() + "\n");
        }

        return sb.toString();
    }

    private String analyseDeployedService(Service s) {
        StringBuilder sb = new StringBuilder();

        sb.append("\nSummary for deployed instances for service " + s.getName() + ": \n");
        sb.append("--------------------------------------------------\n");
        sb.append("* Forcibly sensitive to " + deploymentAnalysis.forciblySensitiveServices(s) + "\n");
        sb.append("* Voluntarily sensitive to " + deploymentAnalysis.voluntarilySensitiveServices(s) + "\n");
        sb.append("* Insensitive to " + deploymentAnalysis.insensitiveServices(s) + "\n");
        
        return sb.toString();
    }

    private String analyseLocalServiceMachine(ServiceMachine sm) {
        StringBuilder sb = new StringBuilder();

        Service s = sm.getService();
        Machine m = sm.getMachine();

        sb.append("\nLocal analysis for service instance "+sm.getName()+": \n");
        sb.append("-------------------------------\n");
        sb.append("* Forcibly sensitive to " + deploymentAnalysis.forciblySensitiveServices(sm) + "\n");
        sb.append("* Voluntarily sensitive to " + deploymentAnalysis.voluntarilySensitiveServices(sm) + "\n");
        sb.append("* Insensitive to " + deploymentAnalysis.insensitiveServices(sm) + "\n");
        sb.append("* Stress(" + s.getName() + "," + m.getName() + ") = " + deploymentAnalysis.stress(sm) + "\n");
        sb.append("* Responsiveness(" + s.getName() + "," + m.getName() + ") = " + deploymentAnalysis.responsiveness(sm) + "\n");
        try {
            sb.append("* Network depth(" + s.getName() + "," + m.getName() + ") = " + deploymentAnalysis.networkDepth(s, m) + "\n");
            sb.append("* Network traces(" + s.getName() + "," + m.getName() + ") = {");
            Set<List<Connection>> networkPaths = deploymentAnalysis.networkPaths(s, m);
            for (List<Connection> path : networkPaths) {
                sb.append(asConnections(path) + ", ");
            }
            sb.append("}\n");
        } catch (LoopException e) {
            sb.append("* Network depth: infinite (loop detected)\n");
        }

        if (deployment.getServices(m).size() > 1) {
            sb.append("Codeployed with: \n");
            for (Service s2 : deployment.getServices(m)) {
                if (!s2.equals(s)) {
                    sb.append("* " + s2.getDescription() + "\n");
                }
            }
        }

        return sb.toString();
    }

    public String analyse(Connection c) {
        StringBuilder sb = new StringBuilder();

        sb.append("Connection " + c.getSource().getName() + " to " + c.getTarget().getName() + "\n");
        sb.append("* call type: " + c.getCallType() + "\n");
        sb.append("* optionality: " + c.getOptionality() + "\n");

        sb.append("* For call " + c.getCall().getSource().getName() + " to " + c.getCall().getTarget().getName() + "\n");
        sb.append("  * type: " + c.getCall().getType() + "\n");
        sb.append("  * routable: " + c.getCall().isRoutable());

        return sb.toString();
    }

    public String analyse(Call c) {
        StringBuilder sb = new StringBuilder();

        sb.append("Call " + c.getSource().getName() + " to " + c.getTarget().getName() + "\n");
        sb.append("* type: " + c.getType() + "\n");
        sb.append("* routable: " + c.isRoutable());

        return sb.toString();
    }

    private String asCalls(Set<Call> calls) {
        StringBuilder sb = new StringBuilder("[");

        for (Call c : calls) {
            sb.append(c.getTarget());
            sb.append(c.getType() == CallType.PULLED_FROM ? "-<" : "<-");
            sb.append(c.getSource());
            sb.append(",");
        }
        sb.append("]");
        return sb.toString();
    }

    private String asConnections(Collection<Connection> connections) {
        StringBuilder sb = new StringBuilder("[");

        for (Connection c : connections) {
            sb.append(c.getSource());
            sb.append(c.getCallType() == CallType.PULLED_FROM ? "-<" : "<-");
            sb.append(c.getTarget());
            sb.append(",");
        }
        sb.append("]");
        return sb.toString();
    }

}
