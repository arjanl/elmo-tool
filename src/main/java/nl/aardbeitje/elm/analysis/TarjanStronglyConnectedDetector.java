package nl.aardbeitje.elm.analysis;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import edu.uci.ics.jung.graph.DirectedGraph;
import nl.aardbeitje.elm.Edge;
import nl.aardbeitje.elm.Vertex;

/**
 * Based on
 * https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
 */
public class TarjanStronglyConnectedDetector<V extends Vertex, E extends Edge> {

    private int i;
    private final Map<V, Integer> index;
    private final Map<V, Integer> lowLink;
    private final Deque<V> visited;
    private final DirectedGraph<V, E> graph;

    private final Map<V, List<V>> result = new LinkedHashMap<>();

    public TarjanStronglyConnectedDetector(DirectedGraph<V, E> graph) {
        this.graph = graph;
        i = 0;
        index = new LinkedHashMap<>(graph.getVertexCount());
        lowLink = new LinkedHashMap<>(graph.getVertexCount());
        visited = new ArrayDeque<>(graph.getVertexCount());
        detect();
    }

    Map<V, List<V>> getResult() {
        return result;
    }

    private void detect() {
        for (V v : graph.getVertices()) {
            if (!index.containsKey(v)) {
                strongConnect(v);
            }
        }
    }

    private void strongConnect(V v) {
        index.put(v, i);
        lowLink.put(v, i);
        i++;
        visited.push(v);

        
        List<V> stressed = graph.getOutEdges(v).stream().filter( E::isPushedTo).map(graph::getDest).collect(Collectors.toList());
        stressed.addAll( graph.getInEdges(v).stream().filter( E::isPulledFrom).map(graph::getSource).collect(Collectors.toList()));
        for (V w : stressed) {
            if (!index.containsKey(w)) {
                strongConnect(w);
                lowLink.put(v, Math.min(lowLink.get(v), lowLink.get(w)));
            } else {
                // Successor w is in stack S and hence in the current SCC
                // If w is not on stack, then (v, w) is a cross-edge in the DFS
                // tree and must be ignored
                // Note: The next line may look odd - but is correct.
                // It says w.index not w.lowlink; that is deliberate and from
                // the original paper
                lowLink.put(v, Math.min(lowLink.get(v), index.get(w)));
            }
        }

        if (lowLink.get(v).equals(index.get(v))) {
            List<V> scc = new ArrayList<>();
            while (true) {
                V w = visited.pop();
                scc.add(w);
                if (w.equals(v)) {
                    break;
                }
            }
            // don't return trivial sccs in the form of single nodes.
            if (scc.size() > 1) {
                
                for( V x : scc) {
                    result.put( x, scc);
                }
            }
        }
    }
}
