package nl.aardbeitje.elm.analysis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import nl.aardbeitje.elm.Service;

public class Delta {

    // TODO: add latency
    // TODO: add machines
    
    public class DeltaService {
        private final Service service;
        private Collection<Service> plusForcibleSensitiveServices;
        private Collection<Service> minusForcibleSensitiveServices;
        private Collection<Service> plusVoluntarilySensitiveServices;
        private Collection<Service> minusVoluntarilySensitiveServices;
        
        public DeltaService(Service service) {
            this.service = service;
        }
        
        public boolean isChanged() {
            return !(plusForcibleSensitiveServices.isEmpty() &&minusForcibleSensitiveServices.isEmpty() && plusVoluntarilySensitiveServices.isEmpty() && minusVoluntarilySensitiveServices.isEmpty());
        }
        
        public String toString() {
            StringBuilder sb = new StringBuilder( service.getName()+"[" );
            
            sb.append(append( "++", plusForcibleSensitiveServices));
            sb.append(append( "--", minusForcibleSensitiveServices));

            sb.append(append( "+", plusVoluntarilySensitiveServices));
            sb.append(append( "-", minusVoluntarilySensitiveServices));
            sb.append("]");
            
            return sb.toString();

        }
    }
    
    private final DeploymentAnalysis analyseA;
    private final DeploymentAnalysis analyseB;
    private final Collection<DeltaService> deltas = new ArrayList<>();
    
    
    public Delta( DeploymentAnalysis aa, DeploymentAnalysis ab) {
        this.analyseA = aa;
        this.analyseB = ab;
        analyseA.getDeployment().getArchitecture().getServices().forEach( s -> deltas.add(getDelta(s)));
    }
    
    public String append(String prefix, Collection<Service> deltas) {
        if (deltas.isEmpty()) { return "";}
        
        StringBuilder sb = new StringBuilder();
        deltas.forEach( d -> sb.append( prefix + d.getName() + " "));
        
        return sb.toString().trim();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        addDeltas(sb);
        addMachineCount(sb);
        return sb.toString();
    }

    private void addMachineCount(StringBuilder sb) {
        int diff = analyseB.getDeployment().getMachines().size() - analyseA.getDeployment().getMachines().size();
        if (diff!=0) {
            sb.append("Machines: " + diff + "\n");
        }
        
    }

    private void addDeltas(StringBuilder sb) {
        deltas.stream().filter(DeltaService::isChanged ) .forEach( d -> sb.append(d.toString() + "\n"));
    }
    
    private DeltaService getDelta(Service s) {
        DeltaService ds = new DeltaService(s);
        
        Set<Service> fsa = analyseA.allForciblySensitivities(s);
        Set<Service> fsb = analyseB.allForciblySensitivities(s);
        ds.plusForcibleSensitiveServices = remove(fsa, fsb); 
        ds.minusForcibleSensitiveServices = remove(fsb, fsa); 

        Set<Service> vsa = analyseA.allVoluntarilySensitivities(s);
        Set<Service> vsb = analyseB.allVoluntarilySensitivities(s);
        ds.plusVoluntarilySensitiveServices = remove(vsa, vsb); 
        ds.minusVoluntarilySensitiveServices = remove(vsb, vsa); 

        return ds;
    }
    
    
    private static Collection<Service>  remove(Collection<Service> a, Collection<Service> b) {
        Set<Service> c = new HashSet<>(a);
        c.removeAll(b);
        return c;
    }
    
}
