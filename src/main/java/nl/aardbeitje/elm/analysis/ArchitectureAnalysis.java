package nl.aardbeitje.elm.analysis;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import nl.aardbeitje.elm.Architecture;
import nl.aardbeitje.elm.Call;
import nl.aardbeitje.elm.Service;

/**
 * 
 * Performs analysis on an architecture. Contains (intermediate cached) state so this class is only valid as long as the architecture does not change.
 */
public class ArchitectureAnalysis extends Analysis<Service, Call> {

	private Architecture a;

	private Map<String, Set<Service>> consumable = new HashMap<>();
	private Map<String, Map<Service,Set<Service>>> delaySetCache = new HashMap<>();

	public ArchitectureAnalysis(Architecture a) {
		super(a.getGraph());
		this.a = a;
	}

	public boolean isWellformed() {
		
		// check if all producers and consumers are connected
		Set<String> datas = new HashSet<>();
		// check if all consumers have a path to a data source
		for (Service s : a.getServices()) {
			for (String data : s.getConsumes()) {
				datas.add(data);
				if (!isConsumableBy(s, data)) {
					return false;
				}
			}
		}
		
		// check if all edges carry only consumed data
		System.out.println("consumed data: " + datas);
		System.out.println("transported data: " + a.getCalls().stream().map( c -> c.getData()).collect(Collectors.toSet()));
		return a.getCalls().stream().allMatch( c -> datas.contains(c.getData()));
	}

	public Set<Service> delay(Service s, String data) {
		
		return calculateDelays(data).get(s);
		//return delaySetCache.computeIfAbsent(data, d -> calculateDelays(d)).get(s);
	}

	
	private Map<Service,Set<Service>> calculateDelays(String d ) {
		System.out.println("adding delays for data " + d);
		Map<Service,Set<Service>> delays = new HashMap<>();
		a.getServices().stream().filter(s -> s.getProduces().contains(d)).forEach(s -> addDelay(s, d, delays, new HashSet<>(Arrays.asList(s)), new HashSet<>()));
		System.out.println(delays);
		return delays;
		
	}

	/**
	 * @param s the service to add to the list
	 * @param d the data type we are exploring
	 * @param delays current discovered delay sets
	 * @param delay the current delay path
	 * @param path 
	 */
	private void addDelay(Service s, String d, Map<Service,Set<Service>> delays, Set<Service> path, Set<Service> done) {
		System.out.println("  adding delays for service " + s + " with path " + path);

		// add this service to done list
		done.add(s);
		
		// register the current service's path
		Set<Service> ds = delays.get(s);
		if (ds==null) {
			ds = new HashSet<>();
			delays.put(s, ds);
		}
		ds.addAll(path);

		// propagate data
		
		a.getOutboundCalls(s, d).forEach( c -> {
			if (!done.contains(c.getTarget())) {
				Set<Service> newPath = new HashSet<>(path);
				if (c.isPushedTo()) {
					newPath.add(c.getSource());
				} else {
					newPath.add(c.getTarget());
				}
				addDelay( c.getTarget(), d, delays, newPath, done);
			}
		});
	}

	/**
	 * Checks if a given data type is consumable by service s. Recursively checks if
	 * there are calls than can deliver data from a source.
	 * 
	 * @param s    the service to check
	 * @param data the data to check for
	 * @return true if s can consume data, false otherwise.
	 */
	public boolean isConsumableBy(Service s, String data) {
		Set<Service> services = consumable.get(data);
		if (services == null) {
			services = new HashSet<>();
		}
		
		// if this is the source of the data, then it is consumable
		if (s.getProduces().contains(data)) {
			System.out.println( s.getName() + " produces " + data + " so it can consume it");
			return true;
		} else if (services.contains(s)) {
			System.out.println( s.getName() + " is already calculated: can consume " + data );
			return true;
		} else {

			// check all inbound calls for the given data type
			Collection<Call> inboundCalls = a.getInboundCalls(s, data);
			if (inboundCalls.isEmpty()) {
				System.out.println("No inbound calls for " + data + " for " + s + ", so cannot consume");
				// no inbound calls, so s cannot consume data
				return false;
			}
			// check if each call's source can consume data
			for (Call c : inboundCalls) {
				System.out.println("Checking inbound call from " + c.getSource());
				if (!isConsumableBy(c.getSource(), data)) {
					return false;
				}
			}

			services.add(s);
			consumable.put(data, services);
			return true;
		}
	}

	public Set<Call> latency(Service s) {
		return latency(new HashSet<>(), s);
	}

	Set<Call> latency(Set<Call> path, Service s) {
		for (Call c : getGraph().getInEdges(s)) {
			if (!path.contains(c) && c.isPulledFrom()) {
				path.add(c);
				path.addAll(latency(path, getGraph().getSource(c)));
			}
		}
		return path;
	}

	@Override
	public boolean forciblySensitive(Service a, Service b) {
		return stress(a).contains(b) && responsiveness(a).contains(b);
	}

	@Override
	public boolean voluntarilySensitive(Service a, Service b) {
		return !stress(a).contains(b) && responsiveness(a).contains(b);
	}

	@Override
	public boolean sensitive(Service a, Service b) {
		return responsiveness(a).contains(b);
	}

	@Override
	public boolean insensitive(Service a, Service b) {
		return !responsiveness(a).contains(b);
	}

	public Set<Service> forciblySensitiveServices(Service s) {
		return getGraph().getVertices().stream().filter(t -> forciblySensitive(s, t)).collect(Collectors.toSet());
	}

	public Set<Service> voluntarilySensitiveServices(Service s) {
		return getGraph().getVertices().stream().filter(t -> voluntarilySensitive(s, t)).collect(Collectors.toSet());
	}

	public Set<Service> insensitiveServices(Service s) {
		return getGraph().getVertices().stream().filter(t -> insensitive(s, t)).collect(Collectors.toSet());
	}

	public Set<Service> sensitiveServices(Service s) {
		return getGraph().getVertices().stream().filter(t -> sensitive(s, t)).collect(Collectors.toSet());
	}

	/**
	 * Gives the minimal amount of state in a service s.
	 * This is data in an interaction gap: that is being pulled from s which cannot be pulled itself if required.
	 * @param s the set of data that s must have in its state
	 * @return
	 */
	public Set<String> getStates(Service s) {

		// all data that s needs to be able to produce
		Set<String> pulledData = directPullFromEdges(s).stream().map( c -> c.getData()).collect(Collectors.toSet());
		
		// filter out data that can be consumed on demand (thus pulled by s)
		directPullToEdges(s).stream().forEach(c -> pulledData.remove(c.getData()));
		// filter out data that can be produced on demand (thus produced by s)
		pulledData.removeAll(s.getProduces());
		
		System.out.println("Service "+s.getName()+" has state " + pulledData);
		return pulledData;
		
	
	}


}
