package nl.aardbeitje.elm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections15.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import nl.aardbeitje.elm.analysis.ArchitectureAnalysis;
import nl.aardbeitje.elm.io.clone.Cloner;
import nl.aardbeitje.elm.io.clone.ClonerFactory;

/**
 * An architecture is a graph of services which push or pull each other.
 * 
 * Note that equals() is not defined (so it does an instance compare). To see if
 * two architectures are the same, either compare their names (if you want to
 * know they originate from the same file), or use isEqual to see if they
 * describe the same solution (all edges and vertices are equal). Use isEquivalent to see if they represent the
 * same functionality (e.g. they mimick each others information flows).
 * 
 * @author arjanl
 */
public class Architecture {

	private final DirectedGraph<Service, Call> graph;
	private static final Cloner cloner = ClonerFactory.getCloner();

	public Architecture() {
		graph = new DirectedSparseMultigraph<>();
	}

	public Architecture(Architecture other) {
		graph = cloner.clone(other.getGraph());
	}

	public void addService(Service s) {
		graph.addVertex(s);
	}

	public void removeService(Service s) {
		graph.removeVertex(s);
	}

	public Collection<Service> getServices() {
		return graph.getVertices();
	}

	public Call addCall(int id, Service source, CallType type, Service target, boolean routable, String data) {
		Call call = new Call(id, source, target, type, routable, data);
		graph.addEdge(call, source, target);
		return call;
	}

	public void addCall(Call call) {
		graph.addEdge(call, call.getSource(), call.getTarget());
	}

	public void removeCall(Call call) {
		graph.removeEdge(call);
	}

	/**
	 * Returns all calls that deliver data from s to another service (s producing
	 * data).
	 * 
	 * @param s the service to consider the producer
	 * @return all calls that are outbound wrt data
	 */
	public Collection<Call> getOutboundCalls(Service s) {
		return graph.getOutEdges(s);
	}

	/**
	 * Returns all calls that deliver data from s to another service (s producing
	 * data).
	 * 
	 * @param s    the service to consider the producer
	 * @param data the data that is consumed
	 * @return all calls that are outbound wrt data
	 */
	public Collection<Call> getOutboundCalls(Service s, String data) {
		return graph.getOutEdges(s).stream().filter(c -> c.getData().equals(data)).collect(Collectors.toList());
	}

	/**
	 * Returns all calls that deliver data to s from another service (s consuming
	 * data).
	 * 
	 * @param s the service to consider the consumer
	 * @return all calls that are inbound wrt data
	 */
	public Collection<Call> getInboundCalls(Service s) {
		return graph.getInEdges(s);
	}

	/**
	 * Returns all calls that deliver data to s from another service (s consuming
	 * data).
	 * 
	 * @param s    the service to consider the consumer
	 * @param data the data that is consumed
	 * @return all calls that are inbound wrt data
	 */
	public Collection<Call> getInboundCalls(Service s, String data) {
		return graph.getInEdges(s).stream().filter(c -> c.getData().equals(data)).collect(Collectors.toList());
	}

	/**
	 * Get the target (consumer) of a call, regardless of initiative.
	 * 
	 * @param c the call to consider
	 * @return which service is on the consuming end of the data transported via the
	 *         call
	 */
	public Service getCallTarget(Call c) {
		return graph.getDest(c);
	}

	/**
	 * Get the source (producer) of a call, regardless of initiative.
	 * 
	 * @param c the call to consider
	 * @return which service is on the producing end of the data transported via the
	 *         call
	 */
	public Service getCallSource(Call c) {
		return graph.getSource(c);
	}

	public Set<Call> getCalls(final Service source, final Service target) {
		return getCalls().stream().filter(c -> graph.getSource(c).getName().equals(source.getName())
				&& graph.getDest(c).getName().equals(target.getName())).collect(Collectors.toSet());
	}

	public Call getCall(Service source, Service target, CallType callType, boolean routable, String data) {
		return getCalls(source, target).stream().filter(c -> c.getType() == callType)
				.filter(c -> (c.getData() == null && data == null) || c.getData().equals(data))
				.filter(c -> c.isRoutable() == routable).findFirst().orElse(null);
	}

	@JsonIgnore
	public DirectedGraph<Service, Call> getGraph() {
		return graph;
	}

	@JsonIgnore
	public Service getService(String serviceName) {
		return getServices().stream().filter(s -> s.getName().equals(serviceName)).findFirst().orElse(null);
	}

	@JsonIgnore
	public Set<Service> getServiceByOriginalName(String serviceName) {
		return getServices().stream().filter(s -> s.getOriginalName().equals(serviceName)).collect(Collectors.toSet());
	}

	public Call getCallById(int callId) {
		return getCalls().stream().filter(c -> c.getId() == callId).findFirst().orElse(null);
	}

	public Collection<Call> getCalls(Service s) {
		Collection<Call> calls = new ArrayList<>(getOutboundCalls(s));
		calls.addAll(getInboundCalls(s));
		return calls;
	}

	public Collection<Call> getCalls() {
		return graph.getEdges();
	}

	public boolean isEquivalent(Architecture a) {
		System.out.println("Checking equivalence");
		if (this == a)
			return true;
		if (a == null)
			return false;

		// check if all producers are the same
		// check if all consumers are the same

		// check if all producers and consumers of this architecture exist in a
		for (Service s : getServices()) {
			System.out.println("Checking equivalence for " + s.getName());
			if (!s.getProduces().isEmpty()) {
				// there must be a service with the same origin that also produces
				if (!existEquivalentProducer(a, s)) {
					System.out.println("  No equivalent for " + s.getName());
					return false;
				}
				if (!existEquivalentConsumer(a, s)) {
					System.out.println("  No equivalent for " + s.getName());
					return false;
				}
			}
		}
		// and the reverse, check if all producers and consumers of a exist here
		for (Service s : a.getServices()) {
			System.out.println("Checking equivalence for " + s.getName());
			if (!s.getProduces().isEmpty()) {
				// there must be a service with the same origin that also produces
				if (!existEquivalentProducer(this, s)) {
					System.out.println("  No equivalent for " + s.getName());
					return false;
				}
				if (!existEquivalentConsumer(this, s)) {
					System.out.println("  No equivalent for " + s.getName());
					return false;
				}
			}
		}
		System.out.println("Checking wellformedness");

		// check if both are well-formed (e.g. producers deliver to consumers)
		ArchitectureAnalysis aa1 = new ArchitectureAnalysis(this);
		ArchitectureAnalysis aa2 = new ArchitectureAnalysis(a);
		System.out.println("wellformed aa1 = " + aa1.isWellformed());
		System.out.println("wellformed aa2 = " + aa2.isWellformed());
		return aa1.isWellformed() && aa2.isWellformed();
	}

	private boolean existEquivalentProducer(Architecture a, Service s) {
		System.out.println("  Checking producing equivalence for " + s.getName());
		// for all data s produces, find a service with the same original name that also produces s
		return s.getProduces().stream().allMatch( d -> 
			a.getServiceByOriginalName(s.getOriginalName()).stream().anyMatch( ss -> ss.getProduces().contains(d))
		);
	}

	private boolean existEquivalentConsumer(Architecture a, Service s) {
		System.out.println("  Checking consuming equivalence for " + s.getName());
		// for all data s produces, find a service with the same original name that also produces s
		return s.getConsumes().stream().allMatch( d -> 
			a.getServiceByOriginalName(s.getOriginalName()).stream().anyMatch( ss -> ss.getConsumes().contains(d))
		);
	}

	public boolean isEqual(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Architecture other = (Architecture) obj;
		if (graph == null) {
			if (other.graph != null)
				return false;
		} else if (!CollectionUtils.isEqualCollection(graph.getVertices(), other.graph.getVertices())
				&& !CollectionUtils.isEqualCollection(graph.getEdges(), other.graph.getEdges()))
			return false;
		return true;
	}

	@JsonIgnore
	public int getNewCallId() {
		return getCalls().stream().mapToInt(Call::getId).max().orElse(0) + 1;
	}

}
