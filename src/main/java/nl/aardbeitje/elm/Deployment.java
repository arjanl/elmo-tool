package nl.aardbeitje.elm;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections15.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import nl.aardbeitje.elm.io.clone.Cloner;
import nl.aardbeitje.elm.io.clone.ClonerFactory;

/**
 * A deployment of an architecture. A deployment consists of services assigned
 * to on machines.
 * 
 * Note that equals() is not defined (so it does an instance compare). To see if
 * two architectures are the same, either compare their names (if you want to
 * know they originate from the same file), or use isEqual to see if they describe the same solution. Use isEquivalent to see if they represent the same functionality (e.g. they mimick each others information flows).
 * 
 * @author arjanl
 */
public class Deployment {
	protected final Architecture architecture;
	protected final DirectedGraph<ServiceMachine, Connection> graph;
	private final String name;
	private static final Cloner cloner = ClonerFactory.getCloner();

	private static class Cache {

		Set<Machine> getMachines;
		Map<Service, Set<ServiceMachine>> getServiceMachinesForService = new LinkedHashMap<>();
		Map<Machine, Set<ServiceMachine>> getServiceMachinesForMachine = new LinkedHashMap<>();
		Map<Service, Set<Machine>> getMachinesForService = new LinkedHashMap<>();

		public void invalidate() {
			getMachines = null;
			getServiceMachinesForService.clear();
			getServiceMachinesForMachine.clear();
			getMachinesForService.clear();
		}

	}

	private final Cache cache = new Cache();

	public Deployment(String name, Deployment other) {
		this.name = name;
		this.architecture = new Architecture(other.architecture);
		graph = cloner.clone(other.getGraph());
	}

	public Deployment(String name, Architecture architecture) {
		this.name = name;
		this.graph = new DirectedSparseMultigraph<>();
		this.architecture = architecture;
	}

	/**
	 * Creates a deployment for the given architecture using a single machine. Thus,
	 * all services run on the same machine.
	 * 
	 * @param architecture the architecture to deploy
	 * @return the deployment consisting of a single machine
	 */
	public static Deployment singleMachine(String name, Architecture architecture) {
		Deployment d = new Deployment(name, architecture);
		Machine m = new Machine("1");
		for (Service s : architecture.getServices()) {
			d.add(new ServiceMachine(s, m, d));
		}

		d.deriveEdges();
		return d;
	}

	public String generateServiceMachineName(Service service) {
		int maxInstance = getServiceMachines(service).stream().mapToInt(sm -> sm.getIndex()).max().orElse(0);
		return service.getName() + "_" + (maxInstance + 1);
	}

	/**
	 * Creates a deployment for the given architecture using a unique machine per
	 * service. Thus, all services have their own machine.
	 * 
	 * @param architecture the architecture to deploy
	 * @return the deployment consisting of as many machines as services
	 */
	public static Deployment machinePerService(String name, Architecture architecture) {
		Deployment d = new Deployment(name, architecture);
		int i = 1;
		for (Service s : architecture.getServices()) {
			Machine m = new Machine("" + i);
			d.add(new ServiceMachine(s, m, d));
			i++;
		}
		d.deriveEdges();
		return d;
	}

	public void remove(ServiceMachine sm) {
		cache.invalidate();
		graph.removeVertex(sm);
	}

	/**
	 * Creates a deployment for the given architecture using predeployed machines. A
	 * set of machines with deployed services is given. All other services are
	 * automatically deployed on their own machines.
	 * 
	 * @param architecture the architecture to deploy
	 * @return the deployment with explicit machines as given
	 */
	public static Deployment customMachinePerService(String name, Architecture architecture,
			MachineConfig... machineConfigs) {
		return customMachinePerService(name, architecture, Arrays.asList(machineConfigs));
	}

	/**
	 * Creates a deployment for the given architecture using predeployed machines. A
	 * set of machines with deployed services is given. All other services are
	 * automatically deployed on their own machines.
	 * 
	 * @param architecture the architecture to deploy
	 * @return the deployment with explicit machines as given
	 */
	public static Deployment customMachinePerService(String name, Architecture architecture,
			Collection<MachineConfig> machineConfigs) {
		Deployment d = new Deployment(name, architecture);

		deployMachineConfigs(architecture, machineConfigs, d);

		for (Service s : architecture.getServices()) {
			if (d.getMachines(s).isEmpty()) {
				Machine m = d.newMachine();
				d.add(new ServiceMachine(s, m, d));
			}
		}
		d.deriveEdges();
		return d;

	}

	private static void deployMachineConfigs(Architecture architecture, Collection<MachineConfig> machineConfigs,
			Deployment d) {
		for (MachineConfig mc : machineConfigs) {
			Service s = architecture.getService(mc.getServiceName());
			Machine m = d.getMachineByName(mc.getMachineName());
			if (m == null) {
				m = new Machine(mc.getMachineName());
			}
			ServiceMachine sm = new ServiceMachine(s, m, d);
			d.add(sm);
		}
	}

	public void add(ServiceMachine sm) {
		cache.invalidate();
		graph.addVertex(sm);
	}

	public void addAndConnect(ServiceMachine sm) {
		cache.invalidate();
		if (!graph.addVertex(sm)) {
			System.out.println("could not add already existing service machine: " + sm);
			throw new IllegalArgumentException("could not add already existing service machine: " + sm);
		}
		getArchitecture().getInboundCalls(sm.getService()).forEach(c -> getMachines(c.getSource()).stream()
				.forEach(m -> addConnection(getServiceMachine(c.getSource(), m), c, sm)));
		getArchitecture().getOutboundCalls(sm.getService()).forEach(c -> getMachines(c.getTarget()).stream()
				.forEach(m -> addConnection(sm, c, getServiceMachine(c.getTarget(), m))));
	}

	public Machine getMachineByName(String name) {
		return getMachines().stream().filter(m -> m.getName().equals(name)).findFirst().orElse(null);
	}

	private void deriveEdges() {
		getMachines().forEach(this::deriveEdges);
	}

	private void deriveEdges(final Machine m) {
		getServiceMachines(m).forEach(this::deriveEdges);
	}

	private void deriveEdges(ServiceMachine sm) {
		architecture.getOutboundCalls(sm.getService()).forEach(c -> deriveEdges(sm, c));
	}

	private void deriveEdges(ServiceMachine sms, Call c) {
		Service t = architecture.getCallTarget(c);
		getServiceMachines().stream().filter(smt -> smt.getService().equals(t))
				.forEach(smt -> addConnection(sms, c, smt));
	}

	public void removeConnection(Connection c) {
		cache.invalidate();
		graph.removeEdge(c);
	}

	public Connection addConnection(ServiceMachine sm, Call c, ServiceMachine sn) {
		cache.invalidate();
		Connection connection = new Connection(sm, sn, c);
		graph.addEdge(connection, sm, sn);
		return connection;
	}

	public Connection addConnection(Connection connection) {
		cache.invalidate();
		graph.addEdge(connection, connection.getSource(), connection.getTarget());
		return connection;
	}

	public Connection addConnection(ServiceMachine sm, Call c, ServiceMachine sn, Optionality optionality) {
		cache.invalidate();
		Connection connection = new Connection(sm, sn, c, optionality);
		graph.addEdge(connection, sm, sn);
		return connection;
	}

	public Set<Connection> getConnections(final Machine source, final Machine target) {
		return getConnections().stream()
				.filter(c -> graph.getSource(c).getMachine().getName().equals(source.getName())
						&& graph.getDest(c).getMachine().getName().equals(target.getName()))
				.collect(Collectors.toSet());
	}

	public Connection getConnection(Machine source, Machine target, CallType callType) {
		return getConnections(source, target).stream().filter(c -> c.getCallType() == callType).findFirst()
				.orElse(null);
	}

	public Connection getConnection(Call call, Machine source, Machine target) {
		return getConnections(source, target).stream().filter(c -> c.getCall().equals(call)).findFirst().orElse(null);
	}

	public Set<Connection> getConnections(Call call) {
		return getConnections().stream().filter(c -> c.getCall().equals(call)).collect(Collectors.toSet());
	}

	public Set<Connection> getInitiativeConnections(Machine m, Service s) {
		Set<Connection> pushes = getConnections().stream().filter(c -> c.getSource().getMachine().equals(m))
				.filter(c -> c.getCall().getSource().equals(s)).filter(c -> c.getCallType() == CallType.PUSHES_TO)
				.collect(Collectors.toSet());
		Set<Connection> pulls = getConnections().stream().filter(c -> c.getTarget().getMachine().equals(m))
				.filter(c -> c.getCall().getTarget().equals(s)).filter(c -> c.getCallType() == CallType.PULLED_FROM)
				.collect(Collectors.toSet());

		pulls.addAll(pushes);

		return pulls;
	}

	public Collection<ServiceMachine> getServiceMachines() {
		return graph.getVertices();
	}

	public Set<Machine> getMachines() {

//        return graph.getVertices().stream().map(ServiceMachine::getMachine).collect(Collectors.toSet());
		if (cache.getMachines == null) {
			cache.getMachines = graph.getVertices().stream().map(ServiceMachine::getMachine)
					.collect(Collectors.toSet());
		}
		return cache.getMachines;
	}

	public Set<ServiceMachine> getServiceMachines(final Service s) {
//        return getServiceMachines().stream().filter(sm -> sm.getService().equals(s)).collect(Collectors.toSet());
		return cache.getServiceMachinesForService.computeIfAbsent(s,
				v -> getServiceMachines().stream().filter(sm -> sm.getService().equals(s)).collect(Collectors.toSet()));
	}

	public Set<ServiceMachine> getServiceMachines(final Machine m) {
//        return getServiceMachines().stream().filter(sm -> sm.getMachine().equals(m)).collect(Collectors.toSet());
		return cache.getServiceMachinesForMachine.computeIfAbsent(m,
				v -> getServiceMachines().stream().filter(sm -> sm.getMachine().equals(m)).collect(Collectors.toSet()));
	}

	@JsonIgnore
	public ServiceMachine getServiceMachine(final Service service, final Machine machine) {
		return getServiceMachines().stream().filter(sm -> sm.getService().equals(service))
				.filter(sm -> sm.getMachine().equals(machine)).findFirst().orElse(null);
	}

	@JsonIgnore
	public Set<Machine> getMachines(final Service s) {
//        return getServiceMachines().stream().filter(sm -> sm.getService().equals(s)).map(ServiceMachine::getMachine).collect(Collectors.toSet());

		return cache.getMachinesForService.computeIfAbsent(s, v -> getServiceMachines().stream()
				.filter(sm -> sm.getService().equals(s)).map(ServiceMachine::getMachine).collect(Collectors.toSet()));
	}

	public boolean contains(Machine m, Service s) {
		return getServices(m).contains(s);
	}

	public void addService(Service s, Machine m) {
		add(new ServiceMachine(s, m, this));
	}

	public Set<Service> getServices(Machine m) {
		return getServiceMachines().stream().filter(sm -> sm.getMachine().equals(m)).map(ServiceMachine::getService)
				.collect(Collectors.toSet());
	}

	public ServiceMachine getServiceMachineByName(String smName) {
		return getServiceMachines().stream().filter(sm -> sm.getName().equals(smName)).findFirst().orElse(null);
	}

	@JsonIgnore
	public DirectedGraph<ServiceMachine, Connection> getGraph() {
		return graph;
	}

	public Collection<Connection> getConnections() {
		return graph.getEdges();
	}

	public Machine newMachine() {
		cache.invalidate();
		return new Machine("" + (currentMax() + 1));
	}

	private int currentMax() {
		int max = 1;
		for (Machine m : getMachines()) {
			try {
				int i = Integer.parseInt(m.getName());
				if (i > max) {
					max = i;
				}
			} catch (NumberFormatException e) {
				// don't care
			}
		}
		return max;
	}

	public Set<Service> asServices(Set<Machine> machines) {
		Set<Service> services = new HashSet<>();
		for (Machine m : machines) {
			for (Service s : getServices(m)) {
				services.add(s);
			}
		}
		return services;
	}

	public ServiceMachine getConnectionTarget(Connection c) {
		return graph.getDest(c);
	}

	public ServiceMachine getConnectionSource(Connection c) {
		return graph.getSource(c);
	}

	public Architecture getArchitecture() {
		return architecture;
	}

	public int count(Service service) {
		return (int) getServiceMachines().stream().filter(sm -> sm.getService().equals(service)).count();
	}


	public boolean isEquivalent(Deployment d) {
		return getArchitecture().isEquivalent(d.getArchitecture());
	}

	public boolean isEqual(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deployment other = (Deployment) obj;
		if (architecture == null) {
			if (other.architecture != null)
				return false;
		} else if (!architecture.isEqual(other.architecture))
			return false;
		if (graph == null) {
			if (other.graph != null)
				return false;
		} else if (!CollectionUtils.isEqualCollection(graph.getVertices(), other.graph.getVertices())
				&& !CollectionUtils.isEqualCollection(graph.getEdges(), other.graph.getEdges()))
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

}
