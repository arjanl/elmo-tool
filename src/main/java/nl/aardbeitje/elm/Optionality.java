package nl.aardbeitje.elm;

public enum Optionality {
    DEPLOYMENT_OPTIONAL, RUNTIME_OPTIONAL, COMPULSORY
}
